
singleton Material(Marble1Mat)
{
   mapTo = "marble1.skin";
   diffuseMap[0] = "art/shapes/marbles/marble1.skin.png";
   materialTag0 = "Miscellaneous";
   diffuseColor[0] = "1 1 1 1";
   pixelSpecular[0] = "0";
   //cubemap = "DesertSkyCubemap";
   cubemap = "BeginnerSkyCubemap";
   subSurface[0] = "1";
};


singleton Material(Marble32Mat)
{
   mapTo = "marble32.skin";
   diffuseMap[0] = "art/shapes/marbles/marble32.skin.png";
   materialTag0 = "Miscellaneous";
   diffuseColor[0] = "1 1 1 1";
   pixelSpecular[0] = "0";
   //cubemap = "DesertSkyCubemap";
   cubemap = "BeginnerSkyCubemap";
   subSurface[0] = "1";
};

singleton Material(DefaultMaterial2)
{
   mapTo = "marble37.skin";
   diffuseMap[0] = "art/shapes/marbles/marble37.skin";
   alphaRef = "20";
   materialTag0 = "Miscellaneous";
};
