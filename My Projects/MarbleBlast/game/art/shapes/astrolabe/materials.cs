singleton Material(Material_astrolabe)
{
	mapTo = "astrolabe_glow";
	
	diffuseMap[0] = "art/shapes/astrolabe/astrolabe_glow";
	
	// ENABLE TO LAUGH BUTTOX OFF
    //glow[0] = true;
	
	emissive[0] = true;
	renderBin = "SkyShape";
	specular[0] = "1 1 1 1";
	specularPower[0] = 8.0;
	translucent = true;
	
	texCompression[0] = DXT5;
};

singleton Material(Material_astrolabe_solid)
{
	mapTo = "astrolabe_solid";
	
	diffuseMap[0] = "art/shapes/astrolabe/astrolabe_solid_glow";
	
	emissive[0] = true;
	renderBin = "SkyShape";
	specular[0] = "1 1 1 1";
	specularPower[0] = 8.0;
	translucent = true;
	
	texCompression[0] = DXT5;
};
