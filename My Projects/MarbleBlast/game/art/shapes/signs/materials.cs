
singleton Material(DefaultMaterial4)
{
   mapTo = "finishsign_01";
   diffuseMap[0] = "art/shapes/signs/finishsign_01";
   translucentZWrite = "1";
   alphaRef = "20";
   materialTag0 = "Miscellaneous";
   glow[0] = "0";
};

singleton Material(DefaultMaterial9)
{
   mapTo = "finishback_01";
   diffuseMap[0] = "art/shapes/signs/finishback_01";
   translucentZWrite = "1";
   alphaRef = "20";
   materialTag0 = "Miscellaneous";
   glow[0] = "0";
};

singleton Material(DefaultMaterial5)
{
   mapTo = "finishsign_02";
   diffuseMap[0] = "art/shapes/signs/finishsign_02";
   translucentZWrite = "1";
   alphaRef = "20";
   materialTag0 = "Miscellaneous";
   diffuseColor[1] = "1 1 1 1";
   glow[0] = "1";
};

singleton Material(DefaultMaterial6)
{
   mapTo = "finishsign_03";
   diffuseMap[0] = "art/shapes/signs/finishsign_03";
   translucentZWrite = "1";
   alphaRef = "20";
   materialTag0 = "Miscellaneous";
   glow[0] = "1";
};

singleton Material(DefaultMaterial7)
{
   mapTo = "finishsign_05";
   diffuseMap[0] = "art/shapes/signs/finishsign_05";
   specular[3] = "White";
   translucentZWrite = "1";
   alphaRef = "20";
   materialTag0 = "Miscellaneous";
   glow[0] = "1";
};

singleton Material(DefaultMaterial8)
{
   mapTo = "finishsign_04";
   diffuseMap[0] = "art/shapes/signs/finishsign_04";
   translucentZWrite = "1";
   alphaRef = "20";
   materialTag0 = "Miscellaneous";
   glow[0] = "1";
};

singleton Material(DefaultMaterial10)
{
   mapTo = "finishback_02";
   diffuseMap[0] = "art/shapes/signs/finishback_02";
   translucentZWrite = "1";
   alphaRef = "20";
   materialTag0 = "Miscellaneous";
   glow[0] = "1";
};

singleton Material(DefaultMaterial11)
{
   mapTo = "finishback_03";
   diffuseMap[0] = "art/shapes/signs/finishback_03";
   translucentZWrite = "1";
   alphaRef = "20";
   materialTag0 = "Miscellaneous";
   glow[0] = "1";
};

singleton Material(DefaultMaterial12)
{
   mapTo = "finishback_05";
   diffuseColor[0] = "White";
   diffuseMap[0] = "art/shapes/signs/finishback_05";
   translucentZWrite = "1";
   alphaRef = "20";
   materialTag0 = "Miscellaneous";
   glow[0] = "1";
};

singleton Material(DefaultMaterial13)
{
   mapTo = "finishback_04";
   diffuseMap[0] = "art/shapes/signs/finishback_04";
   translucentZWrite = "1";
   alphaRef = "20";
   materialTag0 = "Miscellaneous";
   glow[0] = "1";
};

singleton Material(DefaultMaterial3)
{
   mapTo = "finishsign_metal";
   diffuseMap[0] = "art/shapes/signs/finishsign_metal.png";
   glow[0] = "0";
   translucentZWrite = "1";
   alphaRef = "20";
   materialTag0 = "Miscellaneous";
};
