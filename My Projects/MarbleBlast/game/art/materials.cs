singleton CubemapData( sky_environment )
{
   cubeFace[0] = "art/skies/env_SO";
   cubeFace[1] = "art/skies/env_NO";
   cubeFace[2] = "art/skies/env_EA";
   cubeFace[3] = "art/skies/env_WE";
   cubeFace[4] = "art/skies/env_UP";
   cubeFace[5] = "art/skies/env_DN";
};

singleton CubemapData( marbleCubemap3 )
{
   cubeFace[0] = "art/skies/marbleCubemap3_SO";
   cubeFace[1] = "art/skies/marbleCubemap3_NO";
   cubeFace[2] = "art/skies/marbleCubemap3_EA";
   cubeFace[3] = "art/skies/marbleCubemap3_WE";
   cubeFace[4] = "art/skies/marbleCubemap3_UP";
   cubeFace[5] = "art/skies/marbleCubemap3_DN";
};

singleton CubemapData( gemCubemap )
{
   cubeFace[0] = "art/skies/gemCubemapUp";
   cubeFace[1] = "art/skies/gemCubemapUp";
   cubeFace[2] = "art/skies/gemCubemapUp";
   cubeFace[3] = "art/skies/gemCubemapUp";
   cubeFace[4] = "art/skies/gemCubemapUp";
   cubeFace[5] = "art/skies/gemCubemapUp";
};

singleton CubemapData( gemCubemap2 )
{
   cubeFace[0] = "art/skies/gemCubemapUp2";
   cubeFace[1] = "art/skies/gemCubemapUp2";
   cubeFace[2] = "art/skies/gemCubemapUp2";
   cubeFace[3] = "art/skies/gemCubemapUp2";
   cubeFace[4] = "art/skies/gemCubemapUp2";
   cubeFace[5] = "art/skies/gemCubemapUp2";
};

singleton CubemapData( gemCubemap3 )
{
   cubeFace[0] = "art/skies/gemCubemapUp3";
   cubeFace[1] = "art/skies/gemCubemapUp3";
   cubeFace[2] = "art/skies/gemCubemapUp3";
   cubeFace[3] = "art/skies/gemCubemapUp3";
   cubeFace[4] = "art/skies/gemCubemapUp3";
   cubeFace[5] = "art/skies/gemCubemapUp3";
};


//-----------------------------------------------------------------------------
// ShaderData
//-----------------------------------------------------------------------------
//singleton ShaderData( RefractPix )
//{
   //DXVertexShaderFile 	= "shaders/refractV.hlsl";
   //DXPixelShaderFile 	= "shaders/refractP.hlsl";
   //pixVersion = 2.0;
//};
//
//singleton ShaderData( StdTex )
//{
   //DXVertexShaderFile 	= "shaders/standardTexV.hlsl";
   //DXPixelShaderFile 	= "shaders/standardTexP.hlsl";
   //pixVersion = 2.0;
//};

singleton Material(Material_Marble_BB)
{
   mapTo = "marble.BB.skin";

   diffuseMap[0] = "art/shapes/balls/marble.BB.bump";
   normalMap[0] = "$backbuff";
   texture[2] = "art/shapes/balls/marble.BB.skin";

   specular[0] = "1 1 1 1.0";
   specularPower[0] = 12.0;

   version = 2.0;
   refract = true;
   shader = RefractPix;

   pass[0] = Mat_Glass_NoRefract;
   
};


%mat = singleton Material(Material_cap)
{
   diffuseMap[0] = "art/shapes/balls/cap";
   normalMap[0] = "art/shapes/balls/cap_normal";
   cubemap = Lobby;
   pixelSpecular[0] = true;
   specular[0] = "0.8 0.8 0.8 1.0";
   specularPower[0] = 12.0;
};

//pball_round.dts
%mat = singleton Material(Material_Bumper)
{
   mapTo = bumper;
   
   friction = 0.5;
   restitution = 0;
   force = 15;

   pixelSpecular[0] = true;
   specular[0] = "0.8 0.8 0.8 1.0";
   specularPower[0] = 12.0;
   
   diffuseMap[0] = "art/shapes/bumpers/bumper";
};

//ductfan.dts
%mat = singleton Material(Material_HazardFan)
{
   mapTo = fan;
   
   diffuseMap[0] = "art/shapes/hazards/fan";
   //normalMap[0] = "art/shapes/signs/arrowsign_post_bump";

   pixelSpecular[0] = true;
   specular[0] = "0.8 0.8 0.6 1.0";
   specularPower[0] = 12.0;
};

//trapdoor.dts
%mat = singleton Material(Material_Trapdoor)
{
   mapTo = "trapdoor";

   diffuseMap[0] = "art/shapes/hazards/trapdoor";
};



//copter

%mat = singleton Material(Material_Helicopter)
{
   mapTo = copter_skin;

   // stage 0
   diffuseMap[0] = "art/shapes/images/copter_skin";
   //normalMap[0] = "art/shapes/images/copter_bump";
   pixelSpecular[0] = true;
   specular[0] = "1.0 1.0 1.0 1.0";
   specularPower[0] = 32.0;
};



//blast.dts

%mat = singleton Material(Material_blastOrbit)
{
   mapTo = blast_orbit_skin;
   
   diffuseMap[0] = "art/shapes/images/blast_orbit_skin";
   normalMap[0] = "art/shapes/images/blast_orbit_bump";

   pixelSpecular[0] = true;
   specular[0] = "0.8 0.8 0.6 1.0";
   specularPower[0] = 32.0;
};


%mat = singleton Material(Material_item_glow)
{
   mapTo = item_glow;

   // stage 0
   diffuseMap[0] = "art/shapes/items/item_glow";
   
   glow[0] = true;
   emissive[0] = true;
};


%mat = singleton Material(Material_blast_glow)
{
   mapTo = blast_glow;

   // stage 0
   diffuseMap[0] = "art/shapes/images/blast_glow";
   

   pixelSpecular[0] = true;
   specular[0] = "0.8 0.8 0.6 1.0";
   specularPower[0] = 32.0;
   glow[0]="1";
   materialTag0 = "Miscellaneous";
   specular[3] = "1 1 1 1";
   texture2 = "art/textures/noise";
   force = "0";
   friction = "1";
   restitution = "1";
   version = "2";
   shader = "NoiseTile";
   
};


//grow.dts
%mat = singleton Material(Material_grow)
{
   mapTo = grow;
   
   diffuseMap[0] = "art/shapes/images/grow";
   normalMap[0] = "art/shapes/images/grow_bump";

   pixelSpecular[0] = true;
   specular[0] = "0.8 0.8 0.6 1.0";
   specularPower[0] = 32.0;
};

%mat = singleton Material(Material_Grow_Glow)
{
   mapTo = grow_glow;

   // stage 0
   diffuseMap[0] = "art/shapes/images/grow_glow";
   emissive[0] = true;
   glow[0]=true;
   
   pixelSpecular[0] = true;
   specular[0] = "1.0 1.0 1.0 1.0";
   specularPower[0] = 32.0;
};


//antiGravity.dts

%mat = singleton Material(Material_AntiGravSkin)
{
   mapTo = antigrav_skin;
   
   diffuseMap[0] = "art/shapes/items/antigrav_skin";
   normalMap[0] = "art/shapes/items/antigrav_bump";

   pixelSpecular[0] = true;
   specular[0] = "0.8 0.8 0.6 1.0";
   specularPower[0] = 32.0;
};

%mat = singleton Material(Material_AntiGravGlow)
{
   mapTo = antigrav_glow;

   // stage 0
   diffuseMap[0] = "art/shapes/items/antigrav_glow";
   glow[0]=true;
   emissive[0] = true;
};


//egg.dts
%mat = singleton Material(Material_EasterEgg)
{
   mapTo = "egg_skin";

   diffuseMap[0] = "art/shapes/items/egg_skin";
   pixelSpecular[0] = true;
   specular[0] = "1.0 1.0 1.0 1.0";
   specularPower[0] = 32.0;

   cubemap = gemCubemap;
};

//gem.dts
%mat = singleton Material(Material_BaseGem)
{
   mapTo = "base.gem";
   diffuseMap[0] = "art/shapes/items/red.gem";
   cubemap = gemCubemap;
};

%mat = singleton Material(Material_RedGem)
{
   diffuseMap[0] = "art/shapes/items/red.gem";
   cubemap = gemCubemap;
};

%mat = singleton Material(Material_BlueGem)
{
   diffuseMap[0] = "art/shapes/items/blue.gem";
   cubemap = gemCubemap3;
};

%mat = singleton Material(Material_YellowGem)
{
   diffuseMap[0] = "art/shapes/items/yellow.gem";
   cubemap = gemCubemap2;
};


%mat = singleton Material(Material_GemShine)
{
   diffuseMap[0] = "art/shapes/items/gemshine";
   translucentBlendOp = add;
   translucent = true;
};

//superJump.dts

%mat = singleton Material(Material_SuperJump)
{
   mapTo = superJump_skin;

   // stage 0
   diffuseMap[0] = "art/shapes/items/superJump_skin";
   normalMap[0] = "art/shapes/items/superJump_bump";
   
   pixelSpecular[0] = true;
   specular[0] = "1.0 1.0 1.0 1.0";
   specularPower[0] = 32.0;
};


//itemArrow used in several powerups
%mat = singleton Material(Material_ItemArrow)
{
   diffuseMap[0] = "art/shapes/items/itemArrow";
};

//superSpeed.dts

%mat = singleton Material(Material_SuperSpeed)
{
   mapTo = superSpeed_skin;

   // stage 0
   diffuseMap[0] = "art/shapes/items/superSpeed_skin";
   //normalMap[0] = "art/shapes/items/superSpeed_bump";
   pixelSpecular[0] = true;
   specular[0] = "1.0 1.0 1.0 1.0";
   specularPower[0] = 32.0;
};


%mat = singleton Material(Material_SuperSpeedStar)
{
   mapTo = superSpeed_star;

   diffuseMap[0] = "art/shapes/items/superSpeed_star";
   emissive[0] = true;
   glow[0] = true;
   

};


//timetravel.dts
%mat = singleton Material(Material_TimeTravelSkin)
{
   mapto = timeTravel_skin;	
   
   diffuseMap[0] = "art/shapes/items/timeTravel_skin";
   pixelSpecular[0] = true;
   specular[0] = "1.0 1.0 1.0 1.0";
   specularPower[0] = 32.0;
};


//endarea.dts

%mat = singleton Material(Material_endpad_glow)
{
   mapTo = endpad_glow;
   
   diffuseMap[0] = "art/shapes/pads/endpad_glow";
   glow[0] = "1";
   emissive[0] = true;
   translucent[0] = true;
   texture2 = "art/textures/noise";
   force = "0";
   friction = "1";
   restitution = "1";
   version = "2";
   shader = "NoiseTile";
   materialTag0 = "Miscellaneous";
   //cubemap = sky_environment;
};

%mat = singleton Material(Material_checkpad)
{
   mapTo = checkpad;
   diffuseMap[0] = "art/shapes/pads/checkpad";
   pixelSpecular[0] = true;
   specular[0] = "1.0 1.0 1.0 1.0";
   specularPower[0] = 32.0;
};

//startarea.dts

%mat = singleton Material(Material_ringtex)
{
   mapTo = "ringtex";

   diffuseMap[0] = "art/shapes/pads/ringtex";
   pixelSpecular[0] = true;
   specular[0] = "0.3 0.3 0.3 0.7";
   specularPower[0] = "14";
   emissive[0] = "0";
   translucent[0] = "0";
   glow[0] = "0";
   materialTag0 = "Miscellaneous";
   specular[2] = "1 1 1 1";
   diffuseColor[0] = "1 1 1 1";
   translucentBlendOp = "LerpAlpha";
   alphaRef = "1";
};


%mat = singleton Material(Material_ringtex)
{
   mapTo = ringtex;

   //normalMap[0] = "art/shapes/pads/pad_base2.normal";
   diffuseMap[0] = "art/shapes/pads/ringtex";
   pixelSpecular[0] = true;
   specular[0] = "0.3 0.3 0.3 0.7";
   specularPower[0] = 14.0;
   
 };
 
//Unused atm - Tim
// %mat = singleton Material(Material_center)
//{
//   mapTo = center;
 //normalMap[0] = "art/shapes/pads/pad_base2.normal";
//   diffuseMap[0] = "art/shapes/pads/center";
//   pixelSpecular[0] = true;
//   specular[0] = "0.3 0.3 0.3 1.0";
//   specularPower[0] = 12.0;
//   emissive[0] = true;
// };

%mat = singleton Material(Material_abyss)
{
   mapTo = abyss;

   //normalMap[0] = "art/shapes/pads/pad_base2.normal";
   diffuseMap[0] = "art/shapes/pads/abyss";
   emissive[0] = true;
   //glow[0] = true;
   animFlags[0] = $rotate;
   rotPivotOffset[0] = "-0.5 -0.5";
   rotSpeed[0] = 1.0;
   

 };
 
 %mat = singleton Material(Material_abyss2)
{
   mapTo = abyss2;

   diffuseMap[0] = "art/shapes/pads/abyss2";
  // emissive[0] = true;
   glow[0] = true;
   animFlags[0] = $rotate;
   rotPivotOffset[0] = "-0.5 -0.5";
   rotSpeed[0] = 1.0;
   
 };
 
 

%mat = singleton Material(Material_misty)
{
   mapTo = misty;

   //normalMap[0] = "art/shapes/pads/pad_base2.normal";
   diffuseMap[0] = "art/shapes/pads/misty";

   translucent[0] = true;
   translucentBlendOp = LerpAlpha;
   
   animFlags[0] = $scroll;
   scrollDir[0] = "0.0 1.0";
   scrollSpeed[0] = 0.5;
   emissive[0] = true;
   glow[0] = true;


 };
 
 %mat = singleton Material(Material_corona)
{
   mapTo = "corona";

   diffuseMap[0] = "art/shapes/images/corona";
   glow[0] = "1";
   emissive[0] = true;
   translucent[0] = true;
    animFlags[0] = "0x00000002";
    rotSpeed[0] = "3";
    rotPivotOffset[0] = "-0.5 -0.5";
    translucentBlendOp = "Add";
    texture2 = "art/textures/noise";
    force = "0";
    friction = "1";
    restitution = "1";
    version = "2";
    shader = "NoiseTile";
    materialTag0 = "Miscellaneous";
   //cubemap = sky_environment;
};

%mat = singleton Material(Material_corona)
{
   mapTo = corona;

   //normalMap[0] = "art/shapes/pads/pad_base2.normal";
   diffuseMap[0] = "art/shapes/images/corona";
   glow[0] = true;
   emissive[0] = true;
   translucent[0] = true;
   translucentBlendOp = Add;

   animFlags[0] = $rotate;
   rotPivotOffset[0] = "-0.5 -0.5";
   rotSpeed[0] = 3.0;
 };

//cautionsign.dts
%mat = singleton Material(Material_BaseCautionSign)
{
   diffuseMap[0] = "art/shapes/signs/base.cautionsign";
};

%mat = singleton Material(Material_CautionCautionSign)
{
   diffuseMap[0] = "art/shapes/signs/caution.cautionsign";
};

%mat = singleton Material(Material_DangerCautionSign)
{
   diffuseMap[0] = "art/shapes/signs/danger.cautionsign";
};

%mat = singleton Material(Material_CautionSignWood)
{
   diffuseMap[0] = "art/shapes/signs/cautionsignwood";
};

%mat = singleton Material(Material_CautionSignPole)
{
   diffuseMap[0] = "art/shapes/signs/cautionsign_pole";
};

//plainsign.dts
%mat = singleton Material(Material_PlainSignWood)
{
   diffuseMap[0] = "art/shapes/signs/plainsignwood";
};

%mat = singleton Material(Material_BasePlainSign)
{
   diffuseMap[0] = "art/shapes/signs/base.plainSign";
};

%mat = singleton Material(Material_DownPlainSign)
{
   diffuseMap[0] = "art/shapes/signs/down.plainSign";
};

%mat = singleton Material(Material_LeftPlainSign)
{
   diffuseMap[0] = "art/shapes/signs/left.plainSign";
};

%mat = singleton Material(Material_RightPlainSign)
{
   diffuseMap[0] = "art/shapes/signs/right.plainSign";
};

%mat = singleton Material(Material_UpPlainSign)
{
   diffuseMap[0] = "art/shapes/signs/up.plainSign";
};

%mat = singleton Material(Material_PlainSignWood2)
{
   diffuseMap[0] = "art/shapes/signs/signwood2";
};

%mat = singleton Material(Material_SignWood)
{
   diffuseMap[0] = "art/shapes/signs/signwood";
};

//%mat = singleton Material(Material_astrolabe)
//{
   //mapTo = "astrolabe_glow";
//
   //diffuseMap[0] = "art/shapes/astrolabe/astrolabe_glow";
//
   //translucent[0] = true;
//
   //emissive[0] = true;
   //translucentZWrite = "1";
   //shader = "RefractPix";
   //materialTag0 = "Miscellaneous";
   //texture2 = "art/shapes/structures/glass";
   //refract = "1";
   //version = "2";
   //pass0 = "Mat_Glass_NoRefract";
   //blendOp = "LerpAlpha";
//};
//
//%mat = singleton Material(Material_astrolabe_solid)
//{
   //mapTo = "astrolabe_solid";
//
   //diffuseMap[0] = "art/shapes/astrolabe/astrolabe_solid_glow";
//
   //translucent[0] = true;
//
   //emissive[0] = true;
   //translucentZWrite = "1";
   //shader = "RefractPix";
   //materialTag0 = "Miscellaneous";
   //texture2 = "art/shapes/structures/glass";
   //refract = "1";
   //version = "2";
   //pass0 = "Mat_Glass_NoRefract";
   //blendOp = "LerpAlpha";
//};

%mat = singleton Material(Material_clouds_beginner)
{
   mapTo = "clouds_beginner";

   diffuseMap[0] = "art/shapes/astrolabe/clouds_beginner";

   translucent[0] = true;

   emissive[0] = true;
};

%mat = singleton Material(Material_clouds_intermediate)
{
   mapTo = "clouds_intermediate";

   diffuseMap[0] = "art/shapes/astrolabe/clouds_intermediate";

   translucent[0] = true;

   emissive[0] = true;
};

%mat = singleton Material(Material_clouds_advanced)
{
   mapTo = "clouds_advanced";

   diffuseMap[0] = "art/shapes/astrolabe/clouds_advanced";

   translucent[0] = true;

   emissive[0] = true;
   translucentZWrite = "1";
   shader = "RefractPix";
   materialTag0 = "Miscellaneous";
   texture2 = "art/shapes/structures/glass";
   refract = "1";
   version = "2";
   pass0 = "Mat_Glass_NoRefract";
   blendOp = "LerpAlpha";
};

singleton Material(Mat_Glass_NoRefract)
{
   
   diffuseMap[0] = "art/shapes/structures/glass2";
   
   friction = 1;
   restitution = 1;
   force = 0;

   version = 2.0;
   translucent = true;
   translucentZWrite = false;
   blendOp = LerpAlpha;
   shader = StdTex;
};

singleton Material(Material_glass)
{
   mapTo = "glass";

   diffuseMap[0] = "art/shapes/structures/glass2.png";
   normalMap[0] = "art/shapes/structures/glass.normal.png";
   texture[2] = "art/shapes/structures/glass";
   
   friction = 1;
   restitution = 1;
   force = 0;

   specular[0] = "1.0 1.0 1.0 1.0";
   specularPower[0] = 12.0;

   version = 2;
   refract = 1;
   shader = RefractPix;

   pass[0] = Mat_Glass_NoRefract;
   translucent = "1";
   effectColor[1] = "InvisibleBlack";
   materialTag0 = "Miscellaneous";
   texture2 = "art/shapes/structures/glass";
   pass0 = "Mat_Glass_NoRefract";
   blendOp = "LerpAlpha";
   pixelSpecular[0] = "1";
   subSurface[0] = "1";
};



//%mat = singleton Material(Material_glass)
//{
//eventually we want to use refraction here
//   mapTo = "glass.png";
   
//   diffuseMap[0] = "art/shapes/structures/glass";
//   translucent[0] = true;
   //translucentZwrite = true;
   //normalMap[0] = "art/shapes/structures/glass.normal";

//   pixelSpecular[0] = true;
//   specular[0] = "1 1 1 1.0";
//   specularPower[0] = 10.0;
//};

%mat = singleton Material(Material_GemBeam)
{
   diffuseMap[0] = "art/shapes/items/gembeam";
   translucent[0] = true;
   //translucentZwrite = true;
   emissive[0] = true;

   pixelSpecular[0] = true;
   specular[0] = "0.5 0.6 0.5 0.6";
   specularPower[0] = 12.0;
};

%mat = singleton Material(Material_ArrowSignArrow)
{
   mapTo = arrowsign_arrow;
	
   diffuseMap[0] = "art/shapes/signs/arrowsign_arrow";
   normalMap[0] = "art/shapes/items/arrow_bump";
   //emissive[0] = true;
   //glow[0]=true;

   pixelSpecular[0] = true;
   specular[0] = "1 1 1 1";
   specularPower[0] = 32.0;
};

%mat = singleton Material(Material_ArrowSignArrowGlow)
{
   mapTo = arrowsign_arrow_glow;
	
   diffuseMap[0] = "art/shapes/signs/arrowsign_arrow";

      pixelSpecular[0] = true;
   specular[0] = ".3 .3 .3 .3";
   specularPower[0] = 32.0;

   glow[0]=true;

};

%mat = singleton Material(Material_ArrowSignPost)
{
   mapTo = arrowpostUVW;
   
   diffuseMap[0] = "art/shapes/signs/arrowpostUVW";
   //normalMap[0] = "art/shapes/signs/arrowsign_post_bump";

   pixelSpecular[0] = true;
   specular[0] = "0.8 0.8 0.6 1.0";
   specularPower[0] = 32.0;
};

%mat = singleton Material(Material_ArrowSignChain)
{
   mapTo = arrowsign_chain;
   
   diffuseMap[0] = "art/shapes/signs/arrowsign_chain";
   
   emissive[0] = true;
   glow[0] = true;
   translucent[0] = true;
   //translucentblendop[0] = add;
   
};

%mat = singleton Material(Material_ArrowSignPost)
{
   mapTo = arrowsign_post;
   
   diffuseMap[0] = "art/shapes/signs/arrowsign_post";
   normalMap[0] = "art/shapes/signs/arrowsign_post_bump";

   pixelSpecular[0] = true;
   specular[0] = "0.8 0.8 0.6 1.0";
   specularPower[0] = 12.0;
};

singleton Material(Material_TimeTravelGlass)
{
   mapto = timeTravel_glass;

   diffuseMap[0] = "art/shapes/structures/time.normal";
   //normalMap[0] = "$backbuff";
   texture[2] = "art/shapes/structures/glass";

   friction = 1;
   restitution = 1;
   force = 0;
   
   translucent = true;

   specular[0] = "1 1 1 1.0";
   specularPower[0] = 10.0;

   version = 2;
   refract = 1;
   shader = RefractPix;
   diffuseColor[0] = "0.996078 0.992157 0.992157 1";
   translucentBlendOp = "AddAlpha";
   alphaRef = "0";
   materialTag0 = "Miscellaneous";
   texture2 = "art/shapes/structures/glass";
};

singleton Material(Material_distort_d)
{
   mapto = distort_d;

   diffuseMap[0] = "art/shapes/Particles/distort_n";
   normalMap[0] = "$backbuff";
   texture[2] = "art/shapes/Particles/distort_d";
   //specular[0] = "1 1 1 1.0";
   //specularPower[0] = 10.0;

   version = 2.0;
   refract = true;
   shader = RefractPix;
};



singleton Material(Material_cube_glass)
{
   mapTo = "cube_glass";

   diffuseMap[0] = "art/shapes/structures/cube_glass.normal";
   normalMap[0] = "$backbuff";
   texture[2] = "art/shapes/structures/cube_glass";

   friction = 0.8;
   restitution = 0.1;
   force = 0;

   specular[0] = "1 1 1 1.0";
   specularPower[0] = 10.0;

   version = 2.0;
   refract = true;
   shader = RefractPix;

   pass[0] = Mat_Glass_NoRefract;
};

singleton Material(Material_blastwave)
{
   mapto = "blastwave";

   diffuseMap[0] = "art/shapes/images/blastwave";
   texture[2] = "art/shapes/pads/refract";

   friction = 1;
   restitution = 1;
   force = 0;

   specular[0] = "1 1 1 1.0";
   specularPower[0] = "8";

   version = 2;
   refract = true;
   shader = NoiseTile;
   specular[3] = "1 1 1 1";
   emissive[0] = "1";
   translucent = "1";
   translucentBlendOp = "Add";
   materialTag0 = "Miscellaneous";
   glow[0] = "1";
   texture2 = "art/textures/noise";
};

%mat = singleton Material(Material_blastwave)
{
   mapTo = blastwave;
   //normalMap[0] = "art/shapes/pads/pad_base2.normal";
   diffuseMap[0] = "art/shapes/images/blastwave";
   glow[0] = true;
   emissive[0] = true;
   translucent[0] = true;
   translucentBlendOp = Add;

 };
 
 %mat = singleton Material(Material_sigil)
{
   mapTo = sigil;

   // stage 0
   diffuseMap[0] = "art/shapes/pads/sigil";
   //emissive[0] = true;
   glow[0]=true;
   emissive[0] = true;
   translucent[0] = true;
   translucentBlendOp = Add;
    alphaTest = "1";
    materialTag0 = "Miscellaneous";
};

 %mat = singleton Material(Material_sigil_glow)
{
   mapTo = sigil_glow;

   // stage 0
   diffuseMap[0] = "art/shapes/pads/sigil_glow";
   //emissive[0] = true;
   glow[0]=true;
   emissive[0] = true;
   translucent[0] = true;
   translucentBlendOp = Add;
   animFlags[0] = $scroll;
   scrollDir[0] = "1.0 0.0";
   scrollSpeed[0] = 0.3;
};

 %mat = singleton Material(Material_sigiloff)
{
   mapTo = sigiloff;

   // stage 0
   diffuseMap[0] = "art/shapes/pads/sigiloff";
   //emissive[0] = true;
//   glow[0]=true;
//   emissive[0] = true;
   translucent[0] = true;
//   translucentBlendOp = Add;
};

 %mat = singleton Material(lightning1frame1)
{
   mapTo = lightning1frame1;
   diffuseMap[0] = "art/shapes/bumpers/lightning1frame1";

	emmisive[0] = true;
	glow[0] = true;
	animFlags[0] = $sequence;
	sequenceFramePerSec[0] = 4.0;
	sequenceSegmentSize[0] = 0.25;
    translucent[0] = true;
};

new CubemapData(testReflect)
{
   cubeFace[0] = "art/skies/skybox_1.jpg";
   cubeFace[1] = "art/skies/skybox_1.jpg";
   cubeFace[2] = "art/skies/skybox_1.jpg";
   cubeFace[3] = "art/skies/skybox_1.jpg";
   cubeFace[4] = "art/skies/skybox_1.jpg";
   cubeFace[5] = "art/skies/skybox_1.jpg";
};

singleton Material(Material_ringglass)
{
   mapTo = "ringglass";
   diffuseColor[0] = "0.996078 0.992157 0.992157 1";
   diffuseMap[0] = "art/shapes/pads/ringglass";
   normalMap[0] = "art/shapes/pads/ringnormal";
   specular[0] = "0.8 0.8 0.8 1";
   specularPower[0] = "12";
   pixelSpecular[0] = "1";
   emissive[0] = "1";
   alphaRef = "0";
   materialTag0 = "Miscellaneous";
   translucent = "1";
   cubemap = "BeginnerSkyCubemap";
};

singleton Material(tile_beginner_jpg_mat)
{
   mapTo = "tile_beginner.jpg";
   diffuseMap[0] = "art/textures/tile_beginner.png";
   materialTag0 = "Miscellaneous";
   normalMap[0] = "art/textures/tile_intermediate.normal.png";
   specularPower[0] = "100";
   pixelSpecular[0] = "1";
};

singleton Material(tile_beginner_jpg_mat)
{
   mapTo = "tile_beginner.jpg";
   diffuseMap[0] = "art/textures/tile_beginner.png";
   materialTag0 = "Miscellaneous";
   normalMap[0] = "art/textures/tile_intermediate.normal.png";
   specularPower[0] = "45";
   pixelSpecular[0] = "1";
};

singleton Material(edge_white_jpg_mat)
{
   mapTo = "edge_white.jpg";
   diffuseMap[0] = "art/textures/edge_white.png";
   materialTag0 = "Miscellaneous";
   normalMap[0] = "art/textures/edge.normal.png";
};

singleton Material(tile_advanced_jpg_mat)
{
   mapTo = "tile_advanced.jpg";
   diffuseMap[0] = "art/textures/tile_advanced.png";
   materialTag0 = "Miscellaneous";
   normalMap[0] = "art/textures/tile_intermediate.normal.png";
   diffuseColor[3] = "White";
   specularPower[0] = "100";
   pixelSpecular[0] = "1";
};

singleton Material(tile_intermediate_jpg_mat)
{
   mapTo = "tile_intermediate.jpg";
   diffuseMap[0] = "art/textures/tile_intermediate.png";
   normalMap[0] = "art/textures/tile_intermediate.normal.png";
   materialTag0 = "Miscellaneous";
   specularPower[0] = "100";
   pixelSpecular[0] = "1";
};

singleton Material(tile_intermediate_jpg_mat)
{
   mapTo = "tile_intermediate.jpg";
   diffuseMap[0] = "art/textures/tile_intermediate.png";
   normalMap[0] = "art/textures/tile_intermediate.normal.png";
   materialTag0 = "Miscellaneous";
};

singleton Material(beam_side_jpg_mat)
{
   mapTo = "beam_side.jpg";
   diffuseMap[0] = "art/textures/beam_side.png";
   normalMap[0] = "art/textures/beam_side.normal.png";
   materialTag0 = "Miscellaneous";
};

singleton Material(tile_underside_jpg_mat)
{
   mapTo = "tile_underside.jpg";
   diffuseMap[0] = "art/textures/tile_underside.png";
   normalMap[0] = "art/textures/tile_intermediate.normal.png";
   specularPower[0] = "100";
   pixelSpecular[0] = "1";
   materialTag0 = "Miscellaneous";
};

singleton Material(plate_1_jpg_mat)
{
   mapTo = "plate_1.jpg";
   diffuseMap[0] = "art/textures/plate.randomize.png";
   normalMap[0] = "art/textures/plate.normal.png";
   specularPower[0] = "100";
   pixelSpecular[0] = "1";
   materialTag0 = "Miscellaneous";
};

singleton Material(edge_white2_mat)
{
   mapTo = "edge_white2";
   diffuseMap[0] = "art/textures/mbg/edge_white2.jpg";
   pixelSpecular[0] = "1";
   subSurface[0] = "1";
   materialTag0 = "Miscellaneous";
   shader = "NoiseTile";
   texture2 = "art/textures/noise";
   version = "2";
   normalMap[0] = "art/textures/mbg/edge_white2.normal.jpg";
   specularPower[0] = "50";
};

singleton Material(stripe_cool_mat)
{
   mapTo = "stripe_cool";
   diffuseMap[0] = "art/textures/mbg/stripe_cool.jpg";
   materialTag0 = "Miscellaneous";
   shader = "NoiseTile";
   texture2 = "art/textures/noise";
   version = "2";
   pixelSpecular[0] = "1";
   subSurface[0] = "1";
   specularPower[0] = "40";
};

singleton Material(stripe_cool_mat)
{
   mapTo = "stripe_cool";
   diffuseMap[0] = "art/textures/mbg/stripe_cool.jpg";
   pixelSpecular[0] = "1";
   subSurface[0] = "1";
   materialTag0 = "Miscellaneous";
   shader = "NoiseTile";
   texture2 = "art/textures/noise";
   version = "2";
};

singleton Material(wall_cool2_mat)
{
   mapTo = "wall_cool2";
   diffuseMap[0] = "art/textures/mbg/wall_cool2.jpg";
   normalMap[0] = "art/textures/mbg/wall.bump.png";
   specularPower[0] = "40";
   pixelSpecular[0] = "1";
   subSurface[0] = "1";
};

singleton Material(finishsign_env2_mat)
{
   mapTo = "finishsign_env2";
   diffuseMap[0] = "finishsign_env2";
   materialTag0 = "Miscellaneous";
};
