singleton CubemapData( BeginnerSkyCubemap )
{
   cubeFace[0] = "art/skies/beginner/beg_SO";
   cubeFace[1] = "art/skies/beginner/beg_NO";
   cubeFace[2] = "art/skies/beginner/beg_EA";
   cubeFace[3] = "art/skies/beginner/beg_WE";
   cubeFace[4] = "art/skies/beginner/beg_UP";
   cubeFace[5] = "art/skies/beginner/beg_DN";
};

singleton Material( BeginnerSkyMat )
{
   cubemap = BeginnerSkyCubemap;
   materialTag0 = "Skies";
};
