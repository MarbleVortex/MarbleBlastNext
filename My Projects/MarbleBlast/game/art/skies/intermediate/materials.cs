singleton CubemapData( IntermediateSkyCubemap )
{
   cubeFace[0] = "art/skies/intermediate/adv_SO";
   cubeFace[1] = "art/skies/intermediate/adv_NO";
   cubeFace[2] = "art/skies/intermediate/adv_EA";
   cubeFace[3] = "art/skies/intermediate/adv_WE";
   cubeFace[4] = "art/skies/intermediate/adv_UP";
   cubeFace[5] = "art/skies/intermediate/adv_DN";
};

singleton Material( IntermediateSkyMat )
{
   cubemap = IntermediateSkyCubemap;
   materialTag0 = "Skies";
};
