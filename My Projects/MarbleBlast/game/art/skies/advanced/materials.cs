singleton CubemapData( AdvancedSkyCubemap )
{
   cubeFace[0] = "art/skies/advanced/exp_SO";
   cubeFace[1] = "art/skies/advanced/exp_NO";
   cubeFace[2] = "art/skies/advanced/exp_EA";
   cubeFace[3] = "art/skies/advanced/exp_WE";
   cubeFace[4] = "art/skies/advanced/exp_UP";
   cubeFace[5] = "art/skies/advanced/exp_DN";
};

singleton Material( AdvancedSkyMat )
{
   cubemap = AdvancedSkyCubemap;
   materialTag0 = "Skies";
};
