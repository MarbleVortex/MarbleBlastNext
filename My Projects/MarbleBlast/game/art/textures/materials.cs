new CubemapData( iceCubemap )
{
   cubeFace[0] = "art/textures/acubexpos2";
   cubeFace[1] = "art/textures/acubexneg2";
   cubeFace[2] = "art/textures/acubezneg2";
   cubeFace[3] = "art/textures/acubezpos2";
   cubeFace[4] = "art/textures/acubeypos2";
   cubeFace[5] = "art/textures/acubeyneg2";
};

//-----------------------------------------------------------------------------
// Set Dressing Textures
//-----------------------------------------------------------------------------


// Metal Plate random tile texture

%mat = singleton Material( Material_Plate )
{
   mapTo = plate_1;
   diffuseMap[0] = "art/textures/plate.randomize";
   normalMap[0] = "art/textures/plate.normal";

   friction = 1;
   restitution = 1;
   force = 0;   

   specular[0] = "1.0 1.0 0.8 1.0";
   specularPower[0] = 8.0;
   pixelSpecular[0] = "1";
   subSurface[0] = "1";
};

%mat = singleton Material( Material_Tile_Beginner )
{
   mapTo = tile_beginner;
   diffuseMap[0] = "art/textures/tile_beginner";
   normalMap[0] = "art/textures/tile_intermediate.normal";
   noiseMap[0] = "art/textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = "40";

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2;
   pixelSpecular[0] = "1";
   materialTag0 = "Miscellaneous";
   detailScale[0] = "1 1";
   subSurface[0] = "1";
};

// Matt: Alternate beginner tile using MBU NoiseTile shader
// %mat = new CustomMaterial( Material_Tile_Beginner )
// {
   // mapTo = tile_beginner;
   // sampler["diffuseMap"] = "art/textures/tile_beginner";
   // sampler["bumpMap"] = "art/textures/tile_intermediate.normal";
   // sampler["noiseMap"] = "art/textures/noise";
   
   // specular[0] = "1 1 1 1";
   // specularPower[0] = 40.0;

   // friction = 1;
   // restitution = 1;
   // force = 0;

   // shader = NoiseTile;
   // version = 2.0;
// };

%mat = singleton Material( Material_Tile_Beginner_Shadow )
{
   mapTo = tile_beginner_shadow;
   diffuseMap[0] = "art/textures/tile_beginner";
   normalMap[0] = "art/textures/tile_intermediate.normal";
   texture[2] = "art/textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = "40";

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2;
   pixelSpecular[0] = "1";
   texture2 = "art/textures/noise";
   materialTag0 = "Miscellaneous";
   detailScale[0] = "1 1";
   subSurface[0] = "1";
};

%mat = singleton Material( Material_Tile_Beginner_Red )
{
   mapTo = tile_beginner_red;
   diffuseMap[0] = "art/textures/tile_beginner_red";
   normalMap[0] = "art/textures/tile_intermediate.normal";
   texture[2] = "art/textures/noise_red";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = "40";

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2;
   //diffuseColor[0] = "0.905882 1 0 1";
   effectColor[1] = "0 0 0 0";
   materialTag0 = "Miscellaneous";
   texture2 = "art/textures/noise_red";
   pixelSpecular[0] = "1";
   detailScale[0] = "1 1";
   subSurface[0] = "1";
};

%mat = singleton Material( Material_Tile_Beginner_Red_Shadow )
{
   mapTo = tile_beginner_red_shadow;
   diffuseMap[0] = "art/textures/tile_beginner_red";
   normalMap[0] = "art/textures/tile_intermediate.normal";
   texture[2] = "art/textures/noise_red";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = "40";

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2;
   //diffuseColor[0] = "0.905882 1 0 1";
   effectColor[1] = "0 0 0 0";
   materialTag0 = "Miscellaneous";
   texture2 = "art/textures/noise_red";
   pixelSpecular[0] = "1";
   detailScale[0] = "1 1";
   subSurface[0] = "1";
};

%mat = singleton Material( Material_Tile_Beginner_Blue )
{
   mapTo = tile_beginner_blue;
   diffuseMap[0] = "art/textures/tile_beginner_blue";
   normalMap[0] = "art/textures/tile_intermediate.normal";
   texture[2] = "art/textures/noise_blue";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = "40";

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2;
   //diffuseColor[0] = "0.517647 0.788235 0.52549 1";
   effectColor[1] = "0 0 0 0";
   materialTag0 = "Miscellaneous";
   texture2 = "art/textures/noise_blue";
   pixelSpecular[0] = "1";
   detailScale[0] = "1 1";
   subSurface[0] = "1";
};

%mat = singleton Material( Material_Tile_Beginner_Blue_Shadow )
{
   mapTo = tile_beginner_blue_shadow;
   diffuseMap[0] = "art/textures/tile_beginner_blue";
   normalMap[0] = "art/textures/tile_intermediate.normal";
   texture[2] = "art/textures/noise_blue";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = "40";

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2;
   //diffuseColor[0] = "0.517647 0.788235 0.52549 1";
   effectColor[1] = "0 0 0 0";
   materialTag0 = "Miscellaneous";
   texture2 = "art/textures/noise_blue";
   pixelSpecular[0] = "1";
   detailScale[0] = "1 1";
   subSurface[0] = "1";
};

%mat = singleton Material( Material_Tile_Intermediate )
{
   mapTo = tile_intermediate;
   diffuseMap[0] = "art/textures/tile_intermediate";
   normalMap[0] = "art/textures/tile_intermediate.normal";
   texture[2] = "art/textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
      force = 0;

   shader = NoiseTile;
   version = 2.0;
   subSurface[0] = "1";
   pixelSpecular[0] = "1";
};

%mat = singleton Material( Material_Tile_Intermediate_Shadow )
{
   mapTo = tile_intermediate_shadow;
   diffuseMap[0] = "art/textures/tile_intermediate";
   normalMap[0] = "art/textures/tile_intermediate.normal";
   texture[2] = "art/textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
      force = 0;

   shader = NoiseTile;
   version = 2.0;
   subSurface[0] = "1";
   pixelSpecular[0] = "1";
};

%mat = singleton Material( Material_Tile_Intermediate_Green )
{
   mapTo = tile_intermediate_green;
   diffuseMap[0] = "art/textures/tile_intermediate_green";
   normalMap[0] = "art/textures/tile_intermediate.normal";
   texture[2] = "art/textures/noise_green";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2;
   subSurface[0] = "1";
   pixelSpecular[0] = "1";
   //diffuseColor[0] = "0.12549 0.898039 0.752941 1";
   materialTag0 = "Miscellaneous";
   texture2 = "art/textures/noise_green";
};

%mat = singleton Material( Material_Tile_Intermediate_Green_Shadow )
{
   mapTo = tile_intermediate_green_shadow;
   diffuseMap[0] = "art/textures/tile_intermediate_green";
   normalMap[0] = "art/textures/tile_intermediate.normal";
   texture[2] = "art/textures/noise_green";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2;
   subSurface[0] = "1";
   pixelSpecular[0] = "1";
   //diffuseColor[0] = "0.12549 0.898039 0.752941 1";
   materialTag0 = "Miscellaneous";
   texture2 = "art/textures/noise_green";
};

%mat = singleton Material( Material_Tile_Intermediate_Red )
{
   mapTo = tile_intermediate_red;
   diffuseMap[0] = "art/textures/tile_intermediate_red";
   normalMap[0] = "art/textures/tile_intermediate.normal";
   texture[2] = "art/textures/noise_red";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2;
   subSurface[0] = "1";
   pixelSpecular[0] = "1";
   //diffuseColor[0] = "0.345098 0.615686 0.996078 1";
   materialTag0 = "Miscellaneous";
   texture2 = "art/textures/noise_red";
};

%mat = singleton Material( Material_Tile_Intermediate_Red_Shadow )
{
   mapTo = tile_intermediate_red_shadow;
   diffuseMap[0] = "art/textures/tile_intermediate_red";
   normalMap[0] = "art/textures/tile_intermediate.normal";
   texture[2] = "art/textures/noise_red";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2;
   subSurface[0] = "1";
   pixelSpecular[0] = "1";
   //diffuseColor[0] = "0.345098 0.615686 0.996078 1";
   materialTag0 = "Miscellaneous";
   texture2 = "art/textures/noise_red";
};

%mat = singleton Material( Material_Tile_Advanced )
{
   mapTo = "tile_advanced";
   diffuseMap[0] = "art/textures/tile_advanced";
   normalMap[0] = "art/textures/tile_intermediate.normal";
   texture[2] = "art/textures/noise";
   
   specular[0] = "White";
   specularPower[0] = "40";

   friction = 1;
   restitution = 1;
      force = 0;

   shader = NoiseTile;
   version = 2;
   emissive[0] = "0";
   texture2 = "art/textures/noise";
   materialTag0 = "Miscellaneous";
   translucent = "0";
   pixelSpecular[0] = "1";
   diffuseColor[0] = "1 1 1 1";
   detailScale[0] = "1 1";
   subSurface[0] = "1";
};

%mat = singleton Material( Material_Tile_Advanced_Shadow )
{
   mapTo = "tile_advanced_shadow";
   diffuseMap[0] = "art/textures/tile_advanced";
   normalMap[0] = "art/textures/tile_intermediate.normal";
   texture[2] = "art/textures/noise";
   
   specular[0] = "White";
   specularPower[0] = "40";

   friction = 1;
   restitution = 1;
      force = 0;

   shader = NoiseTile;
   version = 2;
   emissive[0] = "0";
   texture2 = "art/textures/noise";
   materialTag0 = "Miscellaneous";
   translucent = "0";
   pixelSpecular[0] = "1";
   diffuseColor[0] = "1 1 1 1";
   detailScale[0] = "1 1";
   subSurface[0] = "1";
};

%mat = singleton Material( Material_Tile_Advanced_Blue )
{
   mapTo = "tile_advanced_blue";
   diffuseMap[0] = "art/textures/tile_advanced_blue";
   normalMap[0] = "art/textures/tile_intermediate.normal";
   texture[2] = "art/textures/noise_green";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = "40";

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2;
   //diffuseColor[0] = "0.807843 0.54902 0.156863 1";
   effectColor[1] = "0 0 0 0";
   materialTag0 = "Miscellaneous";
   texture2 = "art/textures/noise_blue";
   pixelSpecular[0] = "1";
   detailScale[0] = "1 1";
   subSurface[0] = "1";
};

%mat = singleton Material( Material_Tile_Advanced_Blue_Shadow )
{
   mapTo = "tile_advanced_blue_shadow";
   diffuseMap[0] = "art/textures/tile_advanced_blue";
   normalMap[0] = "art/textures/tile_intermediate.normal";
   texture[2] = "art/textures/noise_green";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = "40";

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2;
   //diffuseColor[0] = "0.807843 0.54902 0.156863 1";
   effectColor[1] = "0 0 0 0";
   materialTag0 = "Miscellaneous";
   texture2 = "art/textures/noise_blue";
   pixelSpecular[0] = "1";
   detailScale[0] = "1 1";
   subSurface[0] = "1";
};

%mat = singleton Material( Material_Tile_Advanced_Green )
{
   mapTo = "tile_advanced_green";
   diffuseMap[0] = "art/textures/tile_advanced_green";
   normalMap[0] = "art/textures/tile_intermediate.normal";
   texture[2] = "art/textures/noise_red";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = "40";

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2;
   //diffuseColor[0] = "0.996078 0.984314 0.572549 1";
   effectColor[1] = "0 0 0 0";
   materialTag0 = "Miscellaneous";
   texture2 = "art/textures/noise_green";
   pixelSpecular[0] = "1";
   detailScale[0] = "1 1";
   subSurface[0] = "1";
};

%mat = singleton Material( Material_Tile_Advanced_Green_Shadow )
{
   mapTo = "tile_advanced_green_shadow";
   diffuseMap[0] = "art/textures/tile_advanced_green";
   normalMap[0] = "art/textures/tile_intermediate.normal";
   texture[2] = "art/textures/noise_red";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = "40";

   friction = 1;
   restitution = 1;
   force = 0;

   shader = NoiseTile;
   version = 2;
   //diffuseColor[0] = "0.996078 0.984314 0.572549 1";
   effectColor[1] = "0 0 0 0";
   materialTag0 = "Miscellaneous";
   texture2 = "art/textures/noise_green";
   pixelSpecular[0] = "1";
   detailScale[0] = "1 1";
   subSurface[0] = "1";
};

// %mat = singleton Material( Material_Tile_Advanced_Green  )
// {
   // mapTo = "tile_advanced_green";
   // diffuseMap[0] = "art/textures/tile_advanced";
   // normalMap[0] = "art/textures/tile_intermediate.normal";
   // texture[2] = "art/textures/noise";
   
   // specular[0] = "1 1 1 1";
   // specularPower[0] = "40";

   // friction = 1;
   // restitution = 1;
   // force = 0;

   // shader = NoiseTile;
   // version = 2;
   // diffuseColor[0] = "0.996078 0.984314 0.572549 1";
   // pixelSpecular[0] = "1";
   // texture2 = "art/textures/noise_green";
   // materialTag0 = "Miscellaneous";
   // detailScale[0] = "1 1";
   // subSurface[0] = "1";
// };

%mat = singleton Material( Material_Tile_Underside )
{
   mapTo = tile_underside;
   diffuseMap[0] = "art/textures/tile_underside";
   normalMap[0] = "art/textures/tile_intermediate.normal";
   texture[2] = "art/textures/noise";
   
   specular[0] = "1 1 1 1";
   specularPower[0] = 40.0;

   shader = NoiseTile;
   version = 2.0;
   subSurface[0] = "1";
   pixelSpecular[0] = "1";
};

%mat = singleton Material(Material_Wall_Beginner ) {
   mapTo="wall_beginner";

   diffuseMap[0] = "art/textures/wall_beginner";
   //normalMap[0] = "art/textures/plate.normal";
};

%mat = singleton Material(Material_Edge_White )
{
   mapTo = "edge_white";
   diffuseMap[0] = "art/textures/edge_white";
   normalMap[0] = "art/textures/edge.normal";

   specular[0] = "0.8 0.8 0.8 1.0";
   specularPower[0] = "50.0";
   diffuseColor[3] = "White";
   pixelSpecular[0] = "1";
   texture2 = "art/textures/noise";
   version = "2";
   materialTag0 = "Miscellaneous";
   shader = "NoiseTile";
   subSurface[0] = "1";
};

%mat = singleton Material(Material_Edge_White_Shadow )
{
   mapTo = "edge_white_shadow";
   diffuseMap[0] = "art/textures/edge_white";
   normalMap[0] = "art/textures/edge.normal";

   specular[0] = "0.8 0.8 0.8 1.0";
   specularPower[0] = "50.0";
   diffuseColor[3] = "White";
   pixelSpecular[0] = "1";
   texture2 = "art/textures/noise";
   version = "2";
   materialTag0 = "Miscellaneous";
   shader = "NoiseTile";
   subSurface[0] = "1";
};

%mat = singleton Material(Material_beam ) {
   diffuseMap[0] = "art/textures/beam";
   normalMap[0] = "art/textures/beam.normal";
   mapTo = "beam";
   pixelSpecular[0] = "0";
   subSurface[0] = "1";
   materialTag0 = "Miscellaneous";
};

%mat = singleton Material(Material_BeamSide ) {
   diffuseMap[0] = "art/textures/beam_side";
   normalMap[0] = "art/textures/beam_side.normal";
   mapTo = "beam_side";
   pixelSpecular[0] = "0";
   subSurface[0] = "1";
   materialTag0 = "Miscellaneous";
};

%mat = singleton Material(Tube_beginner )
{
   mapto = "tube_neutral";
   diffuseMap[0] = "art/textures/tube_beginner";
};

%mat = singleton Material(Tube_intermediate )
{
   mapto="tube_cool";
   diffuseMap[0] = "art/textures/tube_intermediate";
};

%mat = singleton Material(Tube_Advanced )
{
   mapto="tube_warm";
   diffuseMap[0] = "art/textures/tube_advanced";
};

//%mat = singleton Material(cement : CementMaterial)
//{
   //mapto = "cement";
   //diffuseMap[0] = "art/textures/cement";
//};

// ---------------------------------------------------------------------------
// Bounce Materials
// ---------------------------------------------------------------------------

%mat = singleton Material( Material_LowFriction )
{
   mapTo = "friction_low";
   diffuseMap[0] = "art/textures/friction_low";
   normalMap[0] = "art/textures/friction_low.normal";
   texture[2] = "art/textures/noise";
   
   specular[0] = "1 1 1 0.8";
   specularPower[0] = "128";
   pixelSpecular[0] = "1";

   shader = NoiseTile;
   version = 2;
   
   friction = "0.2";
   restitution = "1";
   force = 0;
   subSurface[0] = "1";
   cubemap = "iceCubemap";
   texture2 = "art/textures/noise";
   materialTag0 = "Miscellaneous";
};

%mat = singleton Material( Material_LowFriction_Shadow )
{
   mapTo = "friction_low_shadow";
   diffuseMap[0] = "art/textures/friction_low";
   normalMap[0] = "art/textures/friction_low.normal";
   texture[2] = "art/textures/noise";
   
   specular[0] = "1 1 1 0.8";
   specularPower[0] = "128";
   pixelSpecular[0] = "1";

   shader = NoiseTile;
   version = 2;
   
   friction = "0.2";
   restitution = "1";
   force = 0;
   subSurface[0] = "1";
   cubemap = "iceCubemap";
   texture2 = "art/textures/noise";
   materialTag0 = "Miscellaneous";
};

// ---------------------------------------------------------------------------
// Friction Materials
// ---------------------------------------------------------------------------

%mat = singleton Material(Material_HighFriction) {
   diffuseMap[0] = "art/textures/friction_high";
   normalMap[0] = "art/textures/friction_high.normal";
   friction = "4.5";
   restitution = "0.5";
   force = 0;
   
   mapTo = "friction_high";
   
   
   //FUCK ICE
   //diffuseMap[1] = "art/textures/friction_low";
   //normalMap[1] = "art/textures/friction_low.normal";
   //translucent[1] = true;
   pixelSpecular[0] = true;
   specular[0] = "0.3 0.3 0.35 1";
   specularPower[0] = "10";
   subSurface[0] = "1";
   shader = "NoiseTile";
   version = "2";
   texture2 = "art/textures/noise";
   materialTag0 = "Miscellaneous";
};

%mat = singleton Material(Material_HighFriction_Shadow) {
   diffuseMap[0] = "art/textures/friction_high";
   normalMap[0] = "art/textures/friction_high.normal";
   friction = "4.5";
   restitution = "0.5";
   force = 0;
   
   mapTo = "friction_high_shadow";
   
   
   //FUCK ICE
   //diffuseMap[1] = "art/textures/friction_low";
   //normalMap[1] = "art/textures/friction_low.normal";
   //translucent[1] = true;
   pixelSpecular[0] = true;
   specular[0] = "0.3 0.3 0.35 1";
   specularPower[0] = "10";
   subSurface[0] = "1";
   shader = "NoiseTile";
   version = "2";
   texture2 = "art/textures/noise";
   materialTag0 = "Miscellaneous";
};

//updated
// %mat = singleton Material(Material_HighFriction ) {
   // friction = 4.5;
   // restitution = 0.5;
   // force = 0;
  
   // specular[0] = "0.3 0.3 0.35 1.0";
   // specularPower[0] = 10.0;
   
   // MAPTO = "friction_high";
   // diffuseMap[0] = "art/textures/friction_high";
   // normalMap[0] = "art/textures/friction_high.normal";
   // pixelSpecular[0] = "1";
   // texture2 = "art/textures/noise";
   // version = "2";
   // materialTag0 = "Miscellaneous";
   // shader = "NoiseTile";
// };

%mat = singleton Material(Material_VeryHighFriction ) {
   friction = 2;
   restitution = 1;
   force = 0;
   
   diffuseMap[0] = "art/textures/mbg/friction_ramp_yellow";
   normalMap[0] = "art/textures/mbg/friction_ramp_yellow.bump";
};


//singleton Material(RubberFloorMaterial) {
//   friction = 1;
//   restitution = 1;
//   force = 0;
//};
//
//singleton Material(IceMaterial) {
//   friction = 0.05;
//   restitution = 0.5;
//   force = 0;
//};
//
//singleton Material(BumperMaterial) {
//   friction = 0.5;
//   restitution = 0;
//   force = 15;
//};
//
//singleton Material(ButtonMaterial) {
//   friction = 1;
//   restitution = 1;
//   force = 0;
//};

// ---------------------------------------------------------------------------
// Grid materials
// ---------------------------------------------------------------------------

%mat = singleton Material(Material_GridWarm1 )
{
   diffuseMap[0] = "art/textures/mbg/grid_warm1";
   normalMap[0] = "art/textures/mbg/grid_4square.bump";
};

%mat = singleton Material(Material_GridWarm2 )
{
   diffuseMap[0] = "art/textures/mbg/grid_warm2";
   normalMap[0] = "art/textures/mbg/grid_square.bump";
};

%mat = singleton Material(Material_GridWarm3 )
{
   diffuseMap[0] = "art/textures/mbg/grid_warm3";
   normalMap[0] = "art/textures/mbg/grid_square.bump";
};

%mat = singleton Material(Material_grid_neutral3 )
{
   diffuseMap[0] = "art/textures/mbg/grid_neutral3";
   normalMap[0] = "art/textures/mbg/grid_square.bump";
};

// ---------------------------------------------------------------------------
// Edge, Wall, Stripe materials
// ---------------------------------------------------------------------------

%mat = singleton Material(Material_wall_white )
{
   diffuseMap[0] = "art/textures/mbg/wall_white";
   mapTo = "wall_white";
   normalMap[0] = "art/textures/mbg/wall.bump.png";
   pixelSpecular[0] = "1";
   subSurface[0] = "1";
   materialTag0 = "Miscellaneous";
   specularPower[0] = "40";
};

%mat = singleton Material(Material_wall_warm2 )
{
   diffuseMap[0] = "art/textures/mbg/wall_warm2";
   normalMap[0] = "art/textures/mbg/wall.bump";
};

%mat = singleton Material(Material_wall_warm3 )
{
   diffuseMap[0] = "art/textures/mbg/wall_warm3";
   normalMap[0] = "art/textures/mbg/wall.bump";
   mapTo = "wall_warm3";
   pixelSpecular[0] = "1";
   subSurface[0] = "1";
   materialTag0 = "Miscellaneous";
   specularPower[0] = "40";
};

%mat = singleton Material(Material_stripe_caution )
{
   diffuseMap[0] = "art/textures/mbg/stripe_caution";
   mapTo = "stripe_caution";
   pixelSpecular[0] = "1";
   subSurface[0] = "1";
   materialTag0 = "Miscellaneous";
   shader = "NoiseTile";
   texture2 = "art/textures/noise";
   version = "2";
   specularPower[0] = "40";
};

%mat = singleton Material(Material_stripe_warm2 )
{
   diffuseMap[0] = "art/textures/mbg/stripe_warm2";
};


// ---------------------------------------------------------------------------
// Pattern materials
// ---------------------------------------------------------------------------
%mat = singleton Material(Material_pattern_cool2 )
{
   diffuseMap[0] = "art/textures/mbg/pattern_cool2";
   normalMap[0] = "art/textures/mbg/pattern_cool2.bump";
   mapTo = "pattern_cool2";
   specularPower[0] = "40";
   pixelSpecular[0] = "1";
   subSurface[0] = "1";
};

%mat = singleton Material(Material_pattern_warm3 )
{
   diffuseMap[0] = "art/textures/mbg/pattern_warm3";
};

%mat = singleton Material(Material_pattern_warm4 )
{
   diffuseMap[0] = "art/textures/mbg/pattern_warm4";
};

%mat = singleton Material(Material_chevron_warm )
{
   diffuseMap[0] = "art/textures/mbg/chevron_warm";
   normalMap[0] = "art/textures/mbg/chevron_warm.bump";
};

%mat = singleton Material(Material_trim_warm2 )
{
   diffuseMap[0] = "art/textures/mbg/trim_warm2";
   normalMap[0] = "art/textures/mbg/trim_warm2.bump";
   mapTo = "trim_warm2";
   pixelSpecular[0] = "1";
   subSurface[0] = "1";
   materialTag0 = "Miscellaneous";
   shader = "NoiseTile";
   texture2 = "art/textures/noise";
   version = "2";
   specularPower[0] = "40";
};

%mat = singleton Material(Material_wall_neutral2 )
{
   diffuseMap[0] = "art/textures/mbg/wall_neutral2";
   normalMap[0] = "art/textures/mbg/wall_neutral2.bump";
};

%mat = singleton Material(grid_neutral )
{
   diffuseMap[0] = "art/textures/mbg/grid_neutral";
   normalMap[0] = "art/textures/mbg/grid_square.bump.png";
   mapTo = "grid_neutral";
   diffuseColor[3] = "1 1 1 1";
   pixelSpecular[0] = "1";
   minnaertConstant[0] = "-1";
   subSurface[0] = "1";
   materialTag0 = "Miscellaneous";
   specularPower[0] = "40";
};

//singleton ShaderData( ReflectBump )
//{
//   DXVertexShaderFile 	= "shaders/common/planarReflectBumpV.hlsl";
//   DXPixelShaderFile 	= "shaders/common/planarReflectBumpP.hlsl";
//   pixVersion = 2.0;
//};
//
//%mat = singleton Material(Material_Grey)
//{
//   friction = 1;
//   restitution = 1;
//   force = 0;
//
//   normalMap[0] = "$backbuff";
//   diffuseMap[0] = "marble/data/interiors/grey";
//   texture[2] = "marble/data/interiors/noise.bump";
//   shader = ReflectBump;
//   version = 2.0;
//   planarReflection = true;
//};
//

%mat = singleton Material(Material_wall_neutral3 )
{
   diffuseMap[0] = "art/textures/mbg/wall_neutral3";
   normalMap[0] = "art/textures/mbg/wall.bump";
   mapTo = "wall_neutral3";
   pixelSpecular[0] = "1";
   minnaertConstant[0] = "-1";
   subSurface[0] = "1";
   materialTag0 = "Miscellaneous";
   specularPower[0] = "40";
};

%mat = singleton Material(Material_grid_neutral1 )
{
   diffuseMap[0] = "art/textures/mbg/grid_neutral1";
   normalMap[0] = "art/textures/mbg/grid_square.bump";
};

%mat = singleton Material(Pattern_Cool1 )
{
   diffuseMap[0] = "art/textures/mbg/pattern_cool1";
};

%mat = singleton Material(Pattern_Warm1 )
{
   diffuseMap[0] = "art/textures/mbg/pattern_warm1";
};

%mat = singleton Material(Grid_Cool )
{
   diffuseMap[0] = "art/textures/mbg/grid_cool";
   mapTo = "grid_cool";
   diffuseColor[2] = "1 1 1 1";
   normalMap[0] = "art/textures/mbg/grid_circle.bump.png";
   pixelSpecular[0] = "1";
   minnaertConstant[0] = "-1";
   subSurface[0] = "1";
   materialTag0 = "Miscellaneous";
   specularPower[0] = "40";
};

%mat = singleton Material(Grid_Cool2 )
{
   diffuseMap[0] = "art/textures/mbg/grid_cool2";
};

%mat = singleton Material(Trim_Cool1 )
{
   diffuseMap[0] = "art/textures/mbg/trim_cool1";
};

%mat = singleton Material(Solid_Neutral2 )
{
   diffuseMap[0] = "art/textures/mbg/solid_neutral2";
};

%mat = singleton Material(Wall_Cool1 )
{
   diffuseMap[0] = "art/textures/mbg/Wall_Cool1";
};

%mat = singleton Material(Solid_Warm2 )
{
   diffuseMap[0] = "art/textures/mbg/solid_warm2";
};

%mat = singleton Material(Grid_Warm )
{
   diffuseMap[0] = "art/textures/mbg/grid_warm";
   mapTo = "grid_warm";
   normalMap[0] = "art/textures/mbg/grid_circle.bump.png";
   specularPower[0] = "40";
   pixelSpecular[0] = "1";
   subSurface[0] = "1";
};

%mat = singleton Material(Grid_Neutral4 )
{
   diffuseMap[0] = "art/textures/mbg/grid_neutral4";
};

%mat = singleton Material(Trim_Cool3 )
{
   diffuseMap[0] = "art/textures/mbg/trim_cool3";
};
