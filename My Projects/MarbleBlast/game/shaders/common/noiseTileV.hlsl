#define IN_HLSL
#include "shdrConsts.h"

//*****************************************************************************
// TSE -- HLSL procedural shader                                               
//*****************************************************************************
//-----------------------------------------------------------------------------
// Structures                                                                  
//-----------------------------------------------------------------------------
struct VertData
{
   float2 texCoord        : TEXCOORD0;
   float2 lmCoord         : TEXCOORD1;
//   float3 T               : TEXCOORD2;
//   float3 B               : TEXCOORD3;
//   float3 N               : TEXCOORD4;
   float3 normal          : NORMAL;
   float4 position        : POSITION;
};


struct ConnectData
{
   float4 hpos            : POSITION;
   float4 shading         : COLOR;
   float2 outTexCoord     : TEXCOORD0;
   float3 pos             : TEXCOORD1;
   float3 outEyePos       : TEXCOORD2;
   float4 outLightVec     : TEXCOORD3;
};


//-----------------------------------------------------------------------------
// Main                                                                        
//-----------------------------------------------------------------------------
ConnectData main( VertData IN,
                  uniform float4x4 modelview       : register(VC_WORLD_PROJ),
                  uniform float3   inLightVec      : register(VC_LIGHT_DIR1),
                  uniform float4   inLightColor[4]    : register(VC_LIGHT_DIFFUSE1),
                  uniform float3   eyePos          : register(VC_EYE_POS),
                  uniform float3   fogData         : register(VC_FOGDATA),
                  uniform float4x4 objTrans        : register(VC_OBJ_TRANS)
)
{
    // Default shading is transparent black
    float4 defColor;
    defColor[0] = 0.0;
    defColor[1] = 0.0;
    defColor[2] = 0.0;
    defColor[3] = 0.0;

   ConnectData OUT;

   OUT.hpos = mul(modelview, IN.position);
   OUT.outTexCoord = IN.texCoord;
   
   // T3D uses multiple lights
   OUT.shading = defColor;
   int i;
   for ( i = 0; i < 4; i++ )
       OUT.shading += inLightColor[i];
   
   //float3x3 objToTangentSpace;
   //objToTangentSpace[0] = IN.T;
   //objToTangentSpace[1] = IN.B;
   //objToTangentSpace[2] = IN.normal;

   // T3D is broken with IN.T, IN.B, IN.N/IN.normal, so do this instead.
   float3 binormal = float3( 1, 0, 0 );
   float3 tangent = float3( 0, 1, 0 );
   float3 normal;
      
   binormal = binormal;
   tangent = tangent;
   normal = cross( binormal, tangent );
   
   float3x3 objToTangentSpace;
   objToTangentSpace[0] = binormal;
   objToTangentSpace[1] = tangent;
   objToTangentSpace[2] = normal;

   OUT.outLightVec.xyz = -inLightVec;
   OUT.outLightVec.xyz = mul(objToTangentSpace, OUT.outLightVec);
   OUT.pos = mul(objToTangentSpace, IN.position.xyz / 100.0);;
   OUT.outEyePos.xyz = mul(objToTangentSpace, eyePos.xyz / 100.0);;
   OUT.outLightVec.w = step( 0.0, dot( -inLightVec, IN.normal ) );

   return OUT;
}
