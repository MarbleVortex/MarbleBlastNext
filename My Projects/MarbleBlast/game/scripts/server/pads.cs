//-----------------------------------------------------------------------------
// Torque Game Engine
// 
// Copyright (c) 2001 GarageGames.Com
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------

datablock StaticShapeData(StartPad)
{
   className = "Pad";
   category = "Pads";
   shapeFile = "art/shapes/pads/startArea.dts";
   scopeAlways = true;
   emap = false;
   cubeMapReflected = true;
};

function StartPad::onAdd(%this, %obj)
{
   $Game::StartPad = %obj;
   %obj.setName("StartPoint");
   
   //error("Client Count: " @ ClientGroup.getCount());
   //for (%i = 0; %i < ClientGroup.getCount(); %i++)
   //{
      //%client = ClientGroup.getObject(%i);
      //%ghostID = %client.getGhostID($Game::StartPad);
      //error("calling InitStartPad -> ID = " @ %ghostID);
      //commandToClient(%client, 'InitStartPad', %ghostID);
   //} 
}

function StartPad::onGhostAdded(%this, %obj, %client, %ghostID)
{
   //error("onGhostAdded(" @ %this.getName() @ ", " @ %obj.getId() @ ", " @ %client.getId() @ ", " @ %ghostID @ ");");
   commandToClient(%client, 'InitStartPad', %ghostID);
}

//-----------------------------------------------------------------------------

datablock StaticShapeData(EndPad)
{
   className = "Pad";
   category = "Pads";
   shapeFile = "art/shapes/pads/endArea.dts";
   scopeAlways = true;
   emap = false;
   cubeMapReflected = true;
   addToHUDRadar = true;
   referenceColor = "0.8 0.8 0.8";
};

function EndPad::onAdd(%this, %obj)
{
   $Game::EndPad = %obj;
   %obj.setName("EndPoint");
   %obj.playThread(0,"ambient");
   %obj.setScopeAlways();
}
