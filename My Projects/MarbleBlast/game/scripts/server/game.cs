//-----------------------------------------------------------------------------
// Copyright (c) 2012 GarageGames, LLC
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

// Game duration in secs, no limit if the duration is set to 0
//$Game::Duration = 20 * 60;

// When a client score reaches this value, the game is ended.
//$Game::EndGameScore = 30;

// Pause while looking over the end game screen (in secs)
//$Game::EndGamePause = 10;

//-----------------------------------------------------------------------------
// Penalty and bonus times.
$Game::TimeTravelBonus = 5000;
$Game::CurrentMode = "Scrum";
// Item respawn values, only powerups currently respawn
$Item::PopTime = 10 * 1000;
$Item::RespawnTime = 7 * 1000;

// Game duration in secs, no limit if the duration is set to 0
$Game::Duration = 0;

// Pause while looking over the end game screen (in secs)
$Game::EndGamePause = 5;

// Simulated net parameters
$Game::packetLoss = 0;
$Game::packetLag = 0;

//-----------------------------------------------------------------------------
// Variables extracted from the mission
$Game::GemCount = 0;
$Game::StartPad = 0;
$Game::FakeEndPad = 0;
$Game::RealEndPad = 0;
$Game::EndPad = 0;

// Variables for tracking end game condition
$Game::GemsFound = 0;
$Game::ClientsFinished = 0;

// Variables for Controls
$Game::NoJump = 0;
$Game::OutOfBounds = 0;

//-----------------------------------------------------------------------------

function onServerCreated()
{
   // Server::GameType is sent to the master server.
   // This variable should uniquely identify your game and/or mod.
   $Server::GameType = $appName;

   // Server::MissionType sent to the master server.  Clients can
   // filter servers based on mission type.
   $Server::MissionType = "Deathmatch";

   // GameStartTime is the sim time the game started. Used to calculated
   // game elapsed time.
   $Game::StartTime = 0;

   // Create the server physics world.
   physicsInitWorld( "server" );

   // Load up any objects or datablocks saved to the editor managed scripts
   %datablockFiles = new ArrayObject();
   %datablockFiles.add( "art/ribbons/ribbonExec.cs" );   
   %datablockFiles.add( "art/particles/managedParticleData.cs" );
   %datablockFiles.add( "art/particles/managedParticleEmitterData.cs" );
   %datablockFiles.add( "art/decals/managedDecalData.cs" );
   %datablockFiles.add( "art/datablocks/managedDatablocks.cs" );
   %datablockFiles.add( "art/forest/managedItemData.cs" );
   %datablockFiles.add( "art/datablocks/datablockExec.cs" );   
   loadDatablockFiles( %datablockFiles, true );

   // Run the other gameplay scripts in this folder
   exec("./scriptExec.cs");

   // Keep track of when the game started
   $Game::StartTime = $Sim::Time;
}

function onServerDestroyed()
{
   // This function is called as part of a server shutdown.

   physicsDestroyWorld( "server" );

   // Clean up the GameCore package here as it persists over the
   // life of the server.
   if (isPackage(GameCore))
   {
      deactivatePackage(GameCore);
   }
}

//-----------------------------------------------------------------------------

$preferredTimescale = 1.0;
$pauseTimescale = 0.00001;
function pauseGame()
{
   if ($Server::ServerType $= "SinglePlayer" && $Game::Running)
   {
      $timeScale = $pauseTimescale;
      $gamePaused = true;
   }
}

function resumeGame()
{
   $timeScale = $preferredTimescale;
   $gamePaused = false;
}

//-----------------------------------------------------------------------------

function faceGems( %player )
{
   // In multi-player, set the position, and face gems
   if( (MissionInfo.gameMode $= "Hunt" || MissionInfo.gameMode $= "Scrum") &&
       isObject( $LastFilledGemSpawnGroup ) &&
       $LastFilledGemSpawnGroup.getCount() )
   {
      %avgGemPos = 0;
      // Ok, for right now, just use the first gem in the group. In final implementation
      // do an average of all the positions of the gems, and look at that.
      for( %i = 0; %i < $LastFilledGemSpawnGroup.getCount(); %i++ )
      {
         %gem = $LastFilledGemSpawnGroup.getObject( %i );
         
         %avgGemPos = VectorAdd( %avgGemPos, %gem.getPosition() );
      }
      %avgGemPos = VectorScale( %avgGemPos, 1 / $LastFilledGemSpawnGroup.getCount() );
      
      %player.cameraLookAtPt( %avgGemPos );
   }
}

function onGameDurationEnd()
{
   if (isEventPending($Game::CycleSchedule))
      cancel($Game::CycleSchedule);
   if (isEventPending($Game::Schedule))   
      cancel($Game::Schedule);
   // This "redirect" is here so that we can abort the game cycle if
   // the $Game::Duration variable has been cleared, without having
   // to have a function to cancel the schedule.

   if ($Game::Duration && !(EditorIsActive() && GuiEditorIsActive()))
   {
      for (%i = 0; %i < ClientGroup.getCount(); %i++)
      {
         %cl = ClientGroup.getObject(%i);
         commandToClient(%cl, 'setTimer', "set", $Game::Duration);
         if ($pref::Marble::finishShake)
            %cl.player.shakeCamera(2.0, 10.0, 10.0, 1.0);
         %cl.player.setMode(Victory);
         messageClient(%cl, 'MsgRaceOver', $Text::Tagged::Congratulations );
      }
      serverplay2d(WonRaceSfx);
      setGameState("end");
      //Game.onGameDurationEnd();
   }
}

function onMissionReset()
{
   //serverEndFireWorks();
   if (isObject(ServerGroup))
      ServerGroup.onMissionReset();

   // Reset globals for next round
   $tied = false;
   $Game::GemsFound = 0;
   $Game::ClientsJoined = 0;
   $Game::ClientsFinished = 0;
   $timeKeeper = 0;

   // Reset the players
   for( %clientIndex = 0; %clientIndex < ClientGroup.getCount(); %clientIndex++ ) {
      %cl = ClientGroup.getObject( %clientIndex );
      commandToClient(%cl, 'GameStart');
      %cl.resetStats();
   }

   if (MissionInfo.gameMode $= "Scrum")
   {
      SetupGems(MissionInfo.numGems);
   }

   $Game::Running = true;
}

function SimGroup::onMissionReset(%this, %checkpointConfirmNum )
{
   for(%i = 0; %i < %this.getCount(); %i++)
      %this.getObject(%i).onMissionReset( %checkpointConfirmNum );
}

function SimObject::onMissionReset(%this, %checkpointConfirmNum)
{
}

function GameBase::onMissionReset(%this, %checkpointConfirmNum)
{
   %this.getDataBlock().onMissionReset(%this, %checkpointConfirmNum);
}

//-----------------------------------------------------------------------------

function cycleGame()
{
   // This is setup as a schedule so that this function can be called
   // directly from object callbacks.  Object callbacks have to be
   // carefull about invoking server functions that could cause
   // their object to be deleted.

   if (!$Game::Cycling)
   {
      $Game::Cycling = true;
      $Game::Schedule = schedule(0, 0, "onCycleExec");
   }
}

function onCycleExec()
{
   // End the current game and start another one, we'll pause for a little
   // so the end game victory screen can be examined by the clients.

   //endGame();
   endMission();
   $Game::Schedule = schedule($Game::EndGamePause * 1000, 0, "onCyclePauseEnd");
}

function onCyclePauseEnd()
{
   $Game::Cycling = false;

   // Just cycle through the missions for now.

   %search = $Server::MissionFileSpec;
   %oldMissionFile = makeRelativePath( $Server::MissionFile );
      
   for( %file = findFirstFile( %search ); %file !$= ""; %file = findNextFile( %search ) )
   {
      if( %file $= %oldMissionFile )
      {
         // Get the next one, back to the first if there is no next.
         %file = findNextFile( %search );
         if( %file $= "" )
            %file = findFirstFile(%search);
         break;
      }
   }

   if( %file $= "" )
      %file = findFirstFile( %search );

   loadMission(%file);
}

//-----------------------------------------------------------------------------
// GameConnection Methods
// These methods are extensions to the GameConnection class. Extending
// GameConnection makes it easier to deal with some of this functionality,
// but these could also be implemented as stand-alone functions.
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------

function GameConnection::onLeaveMissionArea(%this)
{
   // The control objects invoke this method when they
   // move out of the mission area.

   messageClient(%this, 'MsgClientJoin', '\c2Now leaving the mission area!');
}

function GameConnection::onEnterMissionArea(%this)
{
   // The control objects invoke this method when they
   // move back into the mission area.

   messageClient(%this, 'MsgClientJoin', '\c2Now entering the mission area.');
}

//-----------------------------------------------------------------------------

function GameConnection::onDeath(%this, %sourceObject, %sourceClient, %damageType, %damLoc)
{
   game.onDeath(%this, %sourceObject, %sourceClient, %damageType, %damLoc);
}

function restartLevel()
{

  if( isObject( LocalClientConnection.checkPointShape ) )
     LocalClientConnection.checkPointShape.stopThread( 0 );

  LocalClientConnection.checkPoint = 0;
  LocalClientConnection.checkPointPowerUp = 0;
  LocalClientConnection.checkPointShape = 0;
  LocalClientConnection.CheckPointGemCount = 0;
  LocalClientConnection.checkPointBlastEnergy = 0.0;
  LocalClientConnection.checkPointGemConfirm = 0;
  LocalClientConnection.respawnPlayer();

  // resumeGame();
  // LocalClientConnection.respawnPlayer();
  
   // Perform cleanup to reset the game.
   if (isEventPending($Game::cycleSchedule))
      cancel($Game::CycleSchedule);
      
   if (isEventPending($finishPointSchedule))
      cancel($finishPointSchedule);
      
   if (isEventPending($Game::BlockJIPSchedule))
      cancel($Game::BlockJIPSchedule);
  
   onMissionReset();
   LocalClientConnection.player.setPowerUp(0, true);
   setGameState("start");
   LocalClientConnection.player.setMode(Start);
   LocalClientConnection.player.setMarbleTime(0);
}

function GameConnection::onFoundGem(%this,%amount,%gem,%points)
{
   if (MissionInfo.gameMode $= "Scrum")
   {
      %gem.deleted = true;

      schedule(0, 0, "removeGem", %gem);

      %oldPoints = %this.points;
      %this.points += %points;

      // send message to client telling him he scored (true parameter means msg recipient is scoring client)
      %this.play2D(GotGemSfx);
      %count = ClientGroup.getCount();
      for(%cl= 0; %cl < %count; %cl++)
      {
         %recipient = ClientGroup.getObject(%cl);
         if(%recipient != %this)
            %recipient.play3D(OpponentGotGemSfx, %this.getControlObject().getTransform());
      }

      messageClient(%this, 'MsgClientScoreChanged', "", %this, true, %this.points, %oldPoints);
      // send message to everybody except client telling them that he scored
      messageAllExcept(%this, -1, 'MsgClientScoreChanged', "", %this, false, %this.points, %oldPoints);

      //commandToClient(%this, 'setPoints', %this, %this.points);
      //if (%points == 1)
      //   messageClient(%this, 'MsgItemPickup', $Text::Tagged::GemPickupOnePoint);
      //else
      //   messageClient(%this, 'MsgItemPickup', $Text::Tagged::GemPickupNPoints, %points);

      return;
   }

   $Game::GemsFound += %amount;
   %this.gemCount += %amount;

   if (MissionInfo.gameMode $= "Hunt")
      %remaining = $Game::gemCount - $Game::GemsFound;
   else
      %remaining = $Game::gemCount - %this.gemCount;

   if (%remaining <= 0) {
      //if (MissionInfo.gameType $= "Multiplayer")
      if(MissionInfo.gameMode $= "Hunt")
      {
         // Rank the players
         buildRanks();

         doRankNotifications();

         setGameState("end");
      }
      else
      {
         messageClient(%this, 'MsgHaveAllGems', $Text::Tagged::HaveAllGems );
         %this.play2d(GotAllGemsSfx);
         %this.gemCount = $Game::GemCount;
      }
   }
   else
   {
      if(%remaining == 1)
         %msg = $Text::Tagged::OneGemLeft;
      else
         %msg = $Text::Tagged::NGemsLeft;

      messageClient(%this, 'MsgItemPickup', %msg, %remaining);
      %this.play2d(GotGemSfx);
   }

   // If we are in single player mode, keep track of the gems we have found
   // with regard to checkpoint status so you can't cheat with checkpoints
   //if( MissionInfo.gameType !$= "MultiPlayer" && isObject( %this.checkPoint ) )
   if (MissionInfo.gameMode !$= "Hunt" && MissionInfo.gameMode !$= "Scrum" && isObject( %this.checkPoint ))
   {
      %gem.checkPointConfirmationNumber = %this.checkPointGemConfirm + 1;
   }

   commandToClient(%this,'setGemCount',%this.gemCount,$Game::GemCount);
}


function GameConnection::onOutOfBounds(%this)
{
   if ($Game::State $= "End" || $Game::State $= "wait")
      return;

   // Reset the player back to the last checkpoint
   commandToClient(%this,'setMessage',"outOfBounds",2000);
   %this.play2d(OutOfBoundsVoiceSfx);
   %this.player.setOOB(true);
   if(!isEventPending(%this.respawnSchedule))
      %this.respawnSchedule = %this.schedule(2500, respawnPlayer);
}

function Marble::onOOBClick(%this)
{
   cancel(%this.client.respawnSchedule);
   if($Game::State $= "play" || $Game::State $= "go")
      %this.client.respawnPlayer();
}

//-----------------------------------------------------------------------------

function GameConnection::spawnPlayer(%this)
{
   // Combination create player and drop him somewhere
   %this.checkPoint = 0; // clear any checkpoints
   %this.checkPointPowerUp = 0;
   %this.checkPointShape = 0;
   %this.checkPointGemCount = 0;
   %this.checkPointBlastEnergy = 0.0;
   %this.checkPointGemConfirm = 0;

   %spawnPoint = pickSpawnPoint();
   %this.createPlayer(%spawnPoint);

   serverPlay3d(spawnSfx, %this.player.getTransform());
}

//function GameConnection::respawnPlayer(%this)
//{
   //cancel(%this.respawnSchedule);
   //
   //// clear any messages being displayed to client
   //commandToClient(%this, 'setMessage',"");
   //
   //if (!isObject(%this.checkPoint))
   //{
      //onMissionReset();
   //}
   //
   //%spawnPoint = pickSpawnPoint();
   //
   //%this.player.setVelocity("0 0 0");
   //%this.player.setPosition(%spawnPoint, 0.45);
   //
   //commandToClient(%this, 'setGemCount', 0, $Game::GemCount);
//}

function GameConnection::respawnPlayer(%this)
{
   // Reset the player back to the last checkpoint
   cancel(%this.respawnSchedule);

   // clear any messages being displayed to client
   commandToClient(%this, 'setMessage',"");
   
   %spmode = MissionInfo.gameMode !$= "Hunt" && MissionInfo.gameMode !$= "Scrum";

   if (%spmode)
   {
      // if the player has no checkpoint, assume a full mission restart is occuring
      if (!isObject(%this.checkPoint))
      {
         onMissionReset();
         setGameState("start");
         %this.player.setMode(Start);
         %this.player.setMarbleTime(0);
         %this.gemCount = 0;
         commandToClient(%this, 'setGemCount', %this.gemCount, $Game::GemCount);
      }
      else
      {
         // Reset the moving platforms ONLY
         if( isObject( ServerGroup ) )
            ServerGroup.onMissionReset( %this.checkPointGemConfirm );

         // This animation is fucked up -pw
         //%this.checkPointShape.stopThread( 1 );
         //%this.checkPointShape.schedule( 200, "playThread", 1, "respawn" );
         %this.gemCount = %this.checkPointGemCount;
         commandToClient(%this, 'setGemCount', %this.gemCount, $Game::GemCount);

         %this.player.setBlastEnergy( %this.checkPointBlastEnergy );
      }
   }

   %this.player.setMarbleBonusTime(0);

   %powerUp = %this.player.getPowerUp();
   %this.player.setPowerUp(0, true);
   //if ($Server::ServerType $= "MultiPlayer")
   if (MissionInfo.gameMode $= "Hunt" || MissionInfo.gameMode $= "Scrum")
      %this.player.setPowerUp(%powerUp, true);
   else if( %spmode && isObject( %this.checkPoint ) && isObject( %this.checkPointPowerUp ) )
      %this.player.setPowerUp( %this.checkPointPowerUp );

   %this.player.setOOB(false);
   %this.player.setVelocity("0 0 0");
   %this.player.setVelocityRot("0 0 0");

   if ((%spmode || MissionInfo.gameMode $= "Race") && isObject(%this.checkPoint))
   {
      // Check and if there is a checkpoint shape, use that instead so the marble gets centered
      // on it properly. Mantis bug: 0000819
      if( isObject( %this.checkPointShape ) )
         %spawnPoint = %this.checkPointShape;
      else
         %spawnPoint = %this.checkPoint;
   }
   else
   {
      // in multiplayer mode, try to use the spawn point closest to the
      // marble's last contact position.  if this position is invalid
      // the findClosest function will defer to pickSpawnPoint()
      //if ($Server::ServerType $= "MultiPlayer")
      if (MissionInfo.gameMode $= "Hunt" || MissionInfo.gameMode $= "Scrum")
         %spawnPoint = findClosestSpawnPoint(%this.player.getLastContactPosition());
      else
      {
         // If we restarted the level, reset the blast energy
         %this.player.setBlastEnergy( 0.0 );

         %spawnPoint = pickSpawnPoint();
      }
   }

   %spawnPos = getSpawnPosition(%spawnPoint);
   %this.player.setPosition(%spawnPos, 0.45);
   setGravity(%this.player, %spawnPoint);

   faceGems( %this.player );   
   
   //%this.player.setGravityDir("1 0 0 0 -1 0 0 0 -1",true);

   serverPlay3d(spawnSfx, %this.player.getTransform());
}

//-----------------------------------------------------------------------------

//function GameConnection::createPlayer(%this, %spawnPoint)
//{
   //if (%this.player > 0)  {
      //// The client should not have a player currently
      //// assigned.  Assigning a new one could result in
      //// a player ghost.
      //error( "Attempting to create an angus ghost!" );
   //}
   //
   //if (%this.marbleIndex $= "" || %this.marbleIndex > $Const::OfficialMarbleCount || %this.marbleIndex < 1)
      //%this.marbleIndex = "1";
//
   //echo("Creating marble with Index: " @ %this.marbleIndex);
//
   //// Create the player object
   //%player = new Marble() {
      //dataBlock = "Marble" @ (%this.marbleIndex);
      //client = %this;
   //};
   //MissionCleanup.add(%player);
//
   //// Player setup...
   ////%player.setTransform(%spawnPoint);
   //%player.setPosition(%spawnPoint, 0.45);
   //%player.setEnergyLevel(60);
   //%player.setShapeName(%this.objectName);
//
   //// Update the camera to start with the player
   //%this.camera.setTransform(%player.getEyeTransform());
//
   //// Give the client control of the player
   //%this.player = %player;
   //%this.setControlObject(%player);
//}


//-----------------------------------------------------------------------------
// Manage game state
//-----------------------------------------------------------------------------

// This is used to inform all of the clients that the
// server is in a "wait" state
function setWaitState(%client)
{
   commandToClient(%client, 'setPowerup',"");
   commandToClient(%client, 'setGemCount', 0, $Game::GemCount);
   commandToClient(%client, 'setTimer', "reset");
   commandToClient(%client, 'setMessage', "");
   commandToClient(%client, 'setGameDuration', 0, 0);
}

// This is used to inform all of the clients that the
// server is in a "start" state
function setStartState(%client)
{
   commandToClient(%client, 'setPowerup',"");
   commandToClient(%client, 'setGemCount', 0, $Game::GemCount);
   %help = MissionInfo.startHelpText;
   if (%help !$= "")
      commandToClient(%client, 'setHelpLine', %help);
   commandToClient(%client, 'setMessage',"");

   commandToClient(%client, 'setTimer', "reset");
   if ($Game::Duration)
      commandToClient(%client, 'setGameDuration', $Game::Duration, 0);
   else
      commandToClient(%client, 'setGameDuration', 0, 0);

   if (isObject($timeKeeper) && %client == $timeKeeper)
      %client.player.setMarbleTime(0);
   else if (isObject($timeKeeper))
      %client.player.setMarbleTime($timeKeeper.player.getMarbleTime());

   %client.player.setMode(Start);
}

// This is used to inform all of the clients that the
// server is in a "ready" state
function setReadyState(%client)
{
   commandToClient(%client, 'setMessage', "ready");
   if ($Game::Duration)
      commandToClient(%client, 'setGameDuration', $Game::Duration, 0);
   if (MissionInfo.gameMode $= "Scrum" || MissionInfo.gameMode $= "Hunt" || MissionInfo.gameMode $= "Sumo")
      %client.play2D(ReadyVoiceSfx);

   // Sync with the "first" marble
   if (isObject($timeKeeper) && %client != $timeKeeper)
      %client.player.setMarbleTime($timeKeeper.player.getMarbleTime());
}

// This is used to inform all of the clients that the
// server is in a "set" state
function setSetState(%client)
{
   commandToClient(%client, 'setMessage', "set");
   //%this.play2D(SetVoiceSfx);

   // Sync with the "first" marble
   if (isObject($timeKeeper) && %client != $timeKeeper)
      %client.player.setMarbleTime($timeKeeper.player.getMarbleTime());
}

// This is used to inform all of the clients that the
// server is in a "go" state
function setGoState(%client)
{
   commandToClient(%client, 'setMessage', "go");
   if (MissionInfo.gameMode $= "Scrum" || MissionInfo.gameMode $= "Hunt" || MissionInfo.gameMode $= "Sumo")
      %client.play2D(GetRollingVoiceSfx);
   commandToClient(%client, 'setTimer', "start");
   if ($Game::Duration)
      commandToClient(%client, 'setGameDuration', $Game::Duration, 0);

   //if (MissionInfo.gameType !$= "MultiPlayer")
   if (MissionInfo.gameMode !$= "Hunt" && MissionInfo.gameMode !$= "Scrum")
      %client.player.setPad($Game::EndPad);

   %client.player.setMode(Normal);

   // Sync with the "first" marble
   if (isObject($timeKeeper) && %client != $timeKeeper)
      %client.player.setMarbleTime($timeKeeper.player.getMarbleTime());
}

// This is used to inform all of the clients that the
// server is in a "play" state
function setPlayState(%client)
{
   commandToClient(%client, 'setMessage', "");

   if ($Game::Duration)
      commandToClient(%client, 'setGameDuration', $Game::Duration, $Server::PlayStart);

   // Sync with the "first" marble
   if (isObject($timeKeeper) && %client != $timeKeeper)
      %client.player.setMarbleTime($timeKeeper.player.getMarbleTime());
}

// This is used to inform all of the clients that the
// server is in a "end" state
function setEndState(%client, %winner)
{
   commandToClient(%client, 'setTimer', "stop");
   commandToClient(%client, 'setEndTime', $Server::PlayEnd);
}

// The server maintains the state and transitions to other states
function setGameState(%state)
{
   // Skip out of the current transitions
   if (isEventPending($stateSchedule))
      cancel($stateSchedule);

   $stateSchedule = 0;

   if (%state $= "play")
      $Server::PlayStart = getSimTime();
   if (%state $= "end")
      $Server::PlayEnd = getSimTime();

   // Update client states
   for (%clientIndex = 0; %clientIndex < ClientGroup.getCount(); %clientIndex++)
   {
      %cl = ClientGroup.getObject( %clientIndex );

      switch$ (%state)
      {
         case "wait"  :
            setWaitState(%cl);
         case "start" :
            setStartState(%cl);
         case "ready" :
            setReadyState(%cl);
         case "go"    :
            setGoState(%cl);
         case "play"  :
            setPlayState(%cl);
         case "end"   :
            setEndState(%cl);
      }

      commandToClient(%cl, 'setGameState', %state);
   }

   // schedule the transition to the next state
   switch$ (%state)
   {
      case "start" :
         $stateSchedule = schedule(500, 0, "setGameState", "ready");
      case "ready" :
         $stateSchedule = schedule(3000, 0, "setGameState", "go");
//      case "set"   :
//         $stateSchedule = schedule(1500, 0, "setGameState", "go");
      case "go"    :
         $stateSchedule = schedule(2000, 0, "setGameState", "play");
         if ($Game::Duration)
         {
            $Game::CycleSchedule = schedule($Game::Duration + 90, 0, "onGameDurationEnd" );
            if ($Server::ServerType $= "MultiPlayer")
            {
               // block join in progress in last minute in all games.  it may be already blocked
               // (e.g. in ranked games)
               %blockAt = $Game::Duration - 60 * 1000;
               if (%blockAt > 0)
               {
                  $Game::BlockJIPSchedule = schedule(%blockAt, 0, "serverSetJIPEnabled", false);
               }
            }
         }
      case "end"   :
         $stateSchedule = schedule( 5000, 0, "endGame");
   }

   $Game::State = %state;
}

// This is used to "catch up" newly joining clients
function GameConnection::updateGameState(%this)
{
   switch$ ($Game::State)
   {
      case "wait"  :
         setWaitState(%this);
      case "start" :
         setStartState(%this);
      case "ready" :
         setReadyState(%this);
      case "set"   :
         setSetState(%this);
      case "go"    :
         setGoState(%this);
      case "play"  :
         setPlayState(%this);
      case "end"   :
         setEndState(%this);
   }

   commandToClient(%this, 'setGameState', $Game::State);
}

function GameConnection::resetStats(%this)
{
   // Reset game stats
   %this.gemCount = 0;
   %this.points = 0;
   %this.rank = 0;
   %this.finishTime = 0;
   %spmode = MissionInfo.gameMode !$= "Hunt" && MissionInfo.gameMode !$= "Scrum";
   if (isObject(%this.player) && %spmode)
      %this.player.setMarbleTime(0);

   // Reset the checkpoint, except in single player modes
   if (!%spmode && isObject(%this.checkPoint))
   {
      %this.checkPoint = 0;
      %this.checkPointPowerUp = 0;
      %this.checkPointShape = 0;
      %this.checkPointGemCount = 0;
      %this.checkPointBlastEnergy = 0.0;
      %this.checkPointGemConfirm = 0;
   }
}

//-----------------------------------------------------------------------------

function GameConnection::onEnterPad(%this)
{
   // Handle level ending
   //if (MissionInfo.gameType !$= "MultiPlayer")
   if (MissionInfo.gameMode !$= "Hunt" && MissionInfo.gameMode !$= "Scrum")
   {
      if (%this.player.getPad() == $Game::EndPad) {

         if ($Game::GemCount && %this.gemCount < $Game::GemCount) {
            %this.play2d(MissingGemsSfx);
            messageClient(%this, 'MsgMissingGems', $Text::Tagged::NotAllGems );
         }
         else
         {
            if ($pref::Marble::finishShake)
               %this.player.shakeCamera(2.0, 10.0, 10.0, 1.0);
            %this.player.setMode(Victory);
            messageClient(%this, 'MsgRaceOver', $Text::Tagged::Congratulations );
            serverplay2d(WonRaceSfx);
            setGameState("end");
         }
      }
   }
}

function GameConnection::onLeavePad(%this)
{
   // Don't care if the leave
}

function GameConnection::onFinishPoint(%this)
{
   error("onFinishPoint called, but shouldn't be?  (Race mode only");

   // Freeze the player
   %this.player.setMode(Victory);

   // Grab the marble time (which should stop when the mode is set to Victory)
   %this.finishTime = %this.player.getMarbleTime();

   // A new person finished
   $Game::ClientsFinished++;

   // If this is the first person to finish set a timer to end the whole race
   if (!isEventPending($finishPointSchedule))
      $finishPointSchedule = schedule(10000, 0, "endFinishPoint");

   // Notify them of what palce they came in
//   if ($Game::ClientsFinished == 1)
//      %msg = $Text::Tagged::WonMP;
//   else if ($Game::ClientsFinished == 2)
//      %msg = $Text::Tagged::SecondMP;
//   else if ($Game::ClientsFinished == 3)
//      %msg = $Text::Tagged::ThirdMP;
//   else
//      %msg = $Text::Tagged::NthMP;

   // JMQ: we aren't using race mode anymore so just send down "game over"
   %msg = $Text::Tagged::GameOver;
   messageClient(%this, 'MsgFinish', %msg, $Game::ClientsFinished);

   // If everyone has finished end the game
   if ($Game::ClientsFinished >= ClientGroup.getCount())
      endFinishPoint();
}

function endFinishPoint()
{
   cancel($finishPointSchedule);

   // Rank the players
   buildRanks();

   setGameState("end");
}

//-----------------------------------------------------------------------------
// Support functions
//-----------------------------------------------------------------------------

function spawnDummyMarble(%spawnPoint)
{
   if (%spawnPoint $= "")
      %spawnPoint = pickSpawnPoint();

   %spawnPos = getSpawnPosition(%spawnPoint);

   %marbleIndex = getRandom(1, 37);

   %dummy = new Marble() {
      dataBlock = "Marble" @ (%marbleIndex);
   };
   MissionCleanup.add(%dummy);

   %dummy.setPosition(%spawnPos, 0.45);
   setGravity(%dummy, %spawnPos);
}

function GameConnection::createPlayer(%this, %spawnPoint)
{
   if (%this.player > 0)  {
      // The client should not have a player currently
      // assigned.  Assigning a new one could result in
      // a player ghost.
      error( "Attempting to create an angus ghost!" );
   }

   if (%this.marbleIndex $= "" || %this.marbleIndex > $Const::OfficialMarbleCount || %this.marbleIndex < 1)
      %this.marbleIndex = "1";

   echo("Creating marble with Index: " @ %this.marbleIndex);

   // Create the player object
   %player = new Marble() {
      dataBlock = "Marble" @ (%this.marbleIndex);
      client = %this;
   };
   MissionCleanup.add(%player);

   // Player setup...
   %spawnPos = getSpawnPosition(%spawnPoint);
   %player.setPosition(%spawnPos, 0.45);
   %player.setEnergyLevel(60);
   %player.setShapeName(%this.playerName);
   %player.client.status = 1;
   //if ($Server::ServerType $= "MultiPlayer")
   if (MissionInfo.gameMode $= "Hunt" || MissionInfo.gameMode $= "Scrum")
      %player.setUseFullMarbleTime(true);
   setGravity(%player, %spawnPoint);
   //%player.setGravityDir("1 0 0 0 -1 0 0 0 -1",true);

   // Update the camera to start with the player
   if (isObject(%this.camera))
      %this.camera.setTransform(%player.getEyeTransform());

   // In multi-player, set the position, and face gems
   faceGems( %player );

   // Give the client control of the player
   %this.player = %player;
   %this.setControlObject(%player);
}


//-----------------------------------------------------------------------------

function GameConnection::setCheckpoint(%this, %checkPoint)
{
   // The CheckPoint triggers should have a sequence field that can
   // help us keep them in order
   if (%checkPoint.sequence >= %this.checkPoint.sequence && gravityIsEqual( %this.player, %checkpoint ) )
   {
      %sameOne = %checkPoint == %this.checkPoint;
      
      if (!%sameOne)
      {
         %this.checkPointGemConfirm++;
         //echo("checkpoint: " @ %this.checkPointGemConfirm);

         %this.checkPoint = %checkPoint;
         %this.checkPointPowerUp = %this.player.getPowerUp();
         %this.checkPointGemCount = %this.gemCount;
         %this.checkPointBlastEnergy = %this.player.getBlastEnergy();

         %chkGrp = %checkPoint.getGroup();

         for(%i = 0; ( %chkShape = %chkGrp.getObject( %i ) ) != -1; %i++ )
         {
            // This looks wonkey, however "checkpointShape" is the name of the datablock which
            // the shapes use. -pw
            if( %chkShape.getClassName() $= "StaticShape" && %chkShape.getDatablock().getId() == checkpointShape.getId() )
            {
               if( isObject( %this.checkPointShape ) )
                  %this.checkPointShape.stopThread( 0 );

               %this.checkPointShape = %chkShape;
               break;
            }
         }

         serverplay2d(CheckpointSfx);
         %this.checkPointShape.stopThread( 0 );
         %this.checkPointShape.playThread( 0, "activate" );
         commandToClient(%this, 'CheckpointHit', %checkPoint.sequence );
      }
   }
}

function setGravity(%player, %spawnobj)
{
   %rotation = getWords(%spawnobj.getTransform(), 3);
   %rotation = getWords(%rotation, 0, 2) SPC mRadToDeg(getWord(%rotation, 3));
   %ortho = vectorOrthoBasis(%rotation);
   %orthoinv = getWords(%ortho, 0, 5) SPC VectorScale(getWords(%ortho, 6), -1);

   //echo(%spawnobj.getTransform() SPC "|" SPC %orthoinv);
   
   %player.setGravityDir(%orthoinv,true);
   //%player.setGravityDir(%rotation,true);
}

function gravityIsEqual( %shapebaseA, %shapebaseB )
{
   // Hack for now
   return true;

   %rotationA = getWords( %shapebaseA.getTransform(), 3 );
   %orthoA = vectorOrthoBasis( %rotationA );
   %orthoinvA = getWords( %orthoA, 0, 5 ) SPC VectorScale( getWords( %orthoA, 6 ), -1 );

   %rotationB = getWords( %shapebaseB.getTransform(), 3 );
   %orthoB = vectorOrthoBasis( %rotationB );
   %orthoinvB = getWords( %orthoB, 0, 5 ) SPC VectorScale( getWords( %orthoB, 6 ), -1 );

   echo( "ShapeBaseA (" @ %shapebaseA.getName() @ "): " @ %orthoinvA );
   echo( "ShapeBaseB (" @ %shapebaseB.getName() @ "): " @ %orthoinvB );

   return %orthoinvA $= %orthoinvB;
}

function getSpawnPosition(%spawnobj)
{
   if (!isObject(%spawnobj))
      return "0 0 0";

   %pos = %spawnobj.getWorldBoxCenter();
   %rotation = getWords(%spawnobj.getTransform(), 3);

   //if (isSinglePlayerMode())
   //   %offset = MatrixMulVector(%spawnobj.getTransform(), "0 0 1.5");
   //else
      %offset = MatrixMulVector(%spawnobj.getTransform(), "0 0 0.5");

   %pos = VectorAdd(%pos, %offset);

   return %pos SPC %rotation;
}

$Server::PlayerSpawnMinDist = 3.0;

function findClosestSpawnPoint(%position)
{
   %spawn = 0;

   %group = nameToID("MissionGroup/SpawnPoints");
   if (%position !$= "" && %group != -1)
   {
      %count = %group.getCount();

      if (%count > 0)
      {
         %closest = 0;
         %closestDist = 0;
         for (%i = 0; %i < %count; %i++)
         {
            %spawn = %group.getObject(%i);
            %spawnpos = %spawn.getPosition();

            %dist = VectorDist(%spawnpos, %position);

            if (%closest == 0 || %dist < %closestDist)
            {
               %closest = %spawn;
               %closestDist = %dist;
            }
         }
         %spawn = %closest;
      }
   }

   if (!isObject(%spawn))
   {
      // defer to pickSpawnPoint()
      error("Unable to find closest spawn point to position, defering to pickSpawnPoint().  Input position:" SPC %position);
      %spawn = pickSpawnPoint();
   }
   else
   {
      // Use the container system to iterate through all the objects
      // within the spawn radius.
      InitContainerRadiusSearch(%spawn.getPosition(), $Server::PlayerSpawnMinDist, $TypeMasks::PlayerObjectType);

      if (containerSearchNext())
      {
         echo("Player spawn point occupied, picking random spawn");
         // spawn point occupied, get a random one
         %spawn = pickSpawnPoint();
      }
   }

   return %spawn;
}

// test function
function echoClosestMarble()
{
   if (!isObject(LocalClientConnection.player))
   {
      echo("can't find player");
      return;
   }
   InitContainerRadiusSearch(LocalClientConnection.player.getPosition(), 1000, $TypeMasks::PlayerObjectType);
   %marble = containerSearchNext();
   if (%marble.getId() == LocalClientConnection.player.getId())
      %marble = containerSearchNext();
   if (isObject(%marble))
   {
      %dist = VectorDist(%marble.getPosition(), LocalClientConnection.player.getPosition());
      echo("closest marble is" SPC %dist SPC "units");
   }
   else
   {
      echo("no other marbles found in radius");
   }
}

function pickSpawnPoint(%spawnGroup)
{
   if (%spawnGroup $= "")
      %spawnGroup = "MissionGroup/SpawnPoints";
   %group = nameToID(%spawnGroup);

   if (%group != -1)
   {
      %count = %group.getCount();

      if (%count > 0)
      {
         for (%i = 0; %i < %count; %i++)
         {
            %index = getRandom(%count-1);
            %spawn = %group.getObject(%index);
            %spawnpos = %spawn.getPosition();

            // Use the container system to iterate through all the objects
            // within the spawn radius.
            InitContainerRadiusSearch(%spawnpos, $Server::PlayerSpawnMinDist, $TypeMasks::PlayerObjectType);

            if (!containerSearchNext())
               return %spawn;
         }

         // Unable to find an empty pad so spawn at a random one
         %index = getRandom(%count-1);
         %spawn = %group.getObject(%index);

         return %spawn;
      }
      else
         error("No spawn points found in SpawnPoints SimGroup");
	}
	else
   {
		error("Missing SpawnPoints SimGroup");

      %missionGroup = GameMissionInfo.getCurrentMission().missionGroup;

      if (isObject(%missionGroup))
         return GameMissionInfo.getCurrentMission().cameraPoint;
   }
   
   if (%spawnGroup $= "MissionGroup/SpawnPoints")
      return pickSpawnPoint("MissionGroup/PlayerDropPoints");

	// Could be no spawn points, in which case we'll stick the
	// player at the center of the world.
   return 0;
}

//function pickSpawnPoint()
//{
   //%groupName = "MissionGroup/PlayerDropPoints";
   //%group = nameToID(%groupName);
//
   //if (%group != -1) {
      //%count = %group.getCount();
      //if (%count != 0) {
         //%index = getRandom(%count-1);
         //%spawn = %group.getObject(%index);
         //return %spawn.getTransform();
      //}
      //else
         //error("No spawn points found in " @ %groupName);
   //}
   //else
      //error("Missing spawn points group " @ %groupName);
//
   //// Could be no spawn points, in which case we'll stick the
   //// player at the center of the world.
   //return "0 0 300 1 0 0 0";
//}

function getGem(%gemDB)
{
   if (!isObject(%gemDB))
   {
      // ugh
      error("error, invalid gem db passed to getGem()");
      return 0;
   }

   %gemDB = %gemDB.getId();

   %freeGroup = nameToId("FreeGems" @ %gemDB);
   if (isObject(%freeGroup) && %freeGroup.getCount() > 0)
   {
      //echo("allocating gem from pool");
      %gem = %freeGroup.getObject(0);
      %freeGroup.remove(%gem);
      %gem.setHidden(false);
      %gem.schedule(100, startFade, "1000", 0, false);
      return %gem;
   }

   //echo("allocating new gem");
   // spawn new Gem
   %gem = new Item() {
      dataBlock = %gemDB;
      collideable = "0";
      static = "1";
      rotate = "1";
      permanent = "0";
      deleted = "0";
   };
   return %gem;
}

function freeGem(%gem)
{
   if (!isObject(%gem))
      return;

   if (!isObject(FreeGemGroups))
      new SimGroup(FreeGemGroups);

   %gemDB = %gem.getDatablock();
   %gemDB = %gemDB.getId();

   %freeGroupName = "FreeGems" @ %gemDB;
   %freeGroup = nameToId(%freeGroupName);
   if (!isObject(%freeGroup))
   {
      %freeGroup = new SimGroup(%freeGroupName);
      FreeGemGroups.add(%freeGroup);
   }

   //echo("freeing gem to pool");
   %freeGroup.add(%gem);
   %gem.setHidden(true);
}

function removeGem(%gem)
{
   freeGem(%gem);

   // refill gem groups if necessary
   //if ($Server::ServerType $= "MultiPlayer")
   if (MissionInfo.gameMode $= "Hunt" || MissionInfo.gameMode $= "Scrum")
      refillGemGroups();
}

// due to gem groups, this function isn't used anymore.  use spawnGemAt() instead
//function spawnGem()
//{
//   if (!isObject(TargetGems))
//   {
//      %targetGroup = new SimGroup(TargetGems);
//
//      MissionGroup.add(%targetGroup);
//   }
//
//   if (!isObject(TargetGemsLights))
//   {
//      %targetGroup = new SimGroup(TargetGemsLights);
//
//      MissionGroup.add(%targetGroup);
//   }
//
//   %group = nameToID("MissionGroup/GemSpawns");
//
//   if (%group != -1)
//   {
//      %count = %group.getCount();
//
//      if (%count > 0)
//      {
//         %spawnpos = "0 0 0";
//
//         for (%i = 0; %i < 6; %i++)
//         {
//            %index = getRandom(%count - 1);
//            %spawn = %group.getObject(%index);
//            %spawnpos = %spawn.getTransform();
//
//            %badSpawn = false;
//            for (%j = 0; %j < TargetGems.getCount(); %j++)
//            {
//               %obj = TargetGems.getObject(%j);
//
//               if (%obj.deleted)
//                  continue;
//
//               %objpos = %obj.getPosition();
//               if (VectorDist(%objpos, %spawnpos) < 3.0)
//               {
//                  %badSpawn = true;
//                  break;
//               }
//            }
//
//            if (!%badSpawn)
//               break;
//         }
//
//         %gem = new Item() {
//            dataBlock = "GemItem";
//            collideable = "0";
//            static = "1";
//            rotate = "1";
//            permanent = "0";
//            deleted = "0";
//         };
//
//         %gem.setTransform(%spawnpos);
//
//         TargetGems.add(%gem);
//
//         %light = new Item() {
//            dataBlock = "GemLightShaft";
//         };
//
//         %light.setTransform(%spawnpos);
//
//         TargetGemsLights.add(%light);
//
//         %gem.gemLight = %light;
//
//         return %gem;
//      }
//      else
//         error("No spawn points found in GemSpawns SimGroup");
//	}
//	else
//		error("Missing GemSpawns SimGroup");
//}

function spawnGemAt(%spawnPoint, %includeLight)
{
   if (!isObject(%spawnPoint))
   {
      error("Unable to spawn gem at specified point: spawn point does not exist" SPC %spawnPoint);
      return 0;
   }

   // if it has a gem on it, that is not good
   if (isObject(%spawnPoint.gem) && !%spawnPoint.gem.isHidden())
   {
      error("Gem spawn point already has an active gem on it");
      return %spawnPoint.gem;
   }

   // see if the spawn point has a custom gem datablock
   %gemDB = %spawnPoint.getGemDataBlock();
   if (!isObject(%gemDB))
      %gemDB = "GemItem";

   %gem = getGem(%gemDB);
   %gem.setBuddy(%includeLight);

   %gem.setTransform(%spawnPoint.getTransform());

   // point the gem on the spawn point
   %spawnPoint.gem = %gem;

   return %gem;
}

function getRandomObject(%groupName)
{
   %group = nameToID(%groupName);

   %object = 0;

   if (%group != -1)
   {
      %count = %group.getCount();

      if (%count > 0)
      {
         %index = getRandom(%count - 1);
         %object = %group.getObject(%index);
      }
   }
   return %object;
}

// returns a random gem spawn point
function findGemSpawn()
{
   return getRandomObject("MissionGroup/GemSpawns");
}

// returns a random gem spawn group
function findGemSpawnGroup()
{
   return getRandomObject("GemSpawnGroups");
}

// test function
function gemSpawnReport()
{
   %group = nameToId("MissionGroup/GemSpawns");
   for (%i = 0; %i < %group.getCount(); %i++)
   {
      %spawn = %group.getObject(%i);
      if (isObject(%spawn.gem))
         echo(%spawn.gem.getDatablock().getName() SPC "object found on gem spawn point");
   }
}

// test function
function deleteSomeGemSpawnGroups()
{
   for (%i = GemSpawnGroups.getCount() - 1; %i > 0; %i--)
   {
      %x = GemSpawnGroups.getObject(%i);
      GemSpawnGroups.remove(%x);
      %x.delete();
   }
}

// returns a random gem spawn group that is sufficiently far from other active gem groups that still
// have gems in them.
function pickGemSpawnGroup()
{
   if (!isObject(ActiveGemGroups))
   {
      error("ActiveGemGroups is not an object");
      return findGemSpawnGroup();
   }

   %gemGroupRadius = $Server::GemGroupRadius;
   // allow mission to override radius
   if (MissionInfo.gemGroupRadius > 0)
      %gemGroupRadius = MissionInfo.gemGroupRadius;
   echo("PickGemSpawnGroup: using radius" SPC %gemGroupRadius);

   // double the radius to make it that much more unlikely that we'll pick a group that is too close
   // to another active group
   %gemGroupRadius *= 2;

   // we'll make 6 attempts to find a group that is sufficiently far from other groups
   // after that we give up and use a random group
   %group = 0;
   for (%attempt = 0; %attempt < 6; %attempt++)
   {
      %group = findGemSpawnGroup();
      if (!isObject(%group))
      {
         error("findGemSpawnGroup returned non-object");
         return 0;
      }
      if (%group.getCount() == 0)
      {
         error("gem spawn group contains no spawn points");
         continue;
      }

      %groupSpawn = %group.getObject(0);

      // see if the spawn group is far enough from the primary spawn point of point of the active
      // gem groups
      %ok = true;
      for (%i = 0; %i < ActiveGemGroups.getCount(); %i++)
      {
         %active = ActiveGemGroups.getObject(%i);
         if (%active.getCount() == 0)
            continue;
         %gem = %active.getObject(0);
         if (VectorDist(%gem.getTransform(), %groupSpawn.getTransform()) < %gemGroupRadius)
         {
            %ok = false;
            break;
         }
      }

      if (%ok)
         return %group;
   }

   // if we get here we did not find an appropriate group, just use a random one
   error("unable to find a spawn group that works with active gem groups, using random");
   return findGemSpawnGroup();
}

// re-fill any gem groups that are empty
function refillGemGroups()
{
   if (!isObject(ActiveGemGroups))
   {
      error("ActiveGemGroups does not exist, can't refill gem groups");
      return;
   }

   for (%i = 0; %i < ActiveGemGroups.getCount(); %i++)
   {
      %gemGroup = ActiveGemGroups.getObject(%i);

      if (%gemGroup.getCount() == 0)
      {
         // pick a new spawnGroup for the group
         %spawnGroup = pickGemSpawnGroup();
         if (!isObject(%spawnGroup))
         {
            error("Unable to locate gem spawn group, aborting");
            break;
         }
         %gemGroup.spawnGroup = %spawnGroup;
         fillGemGroup(%gemGroup);
      }
   }
}

// fill the specified gem group with gems using spawn points from its spawnGroup
function fillGemGroup(%gemGroup)
{
   %start = getRealTime();

   if (!isObject(%gemGroup))
   {
      error("Can't fill gem group, supplied group is not an object:" SPC %gemGroup);
      return;
   }

   %spawnGroup = %gemGroup.spawnGroup;
   if (!isObject(%spawnGroup))
   {
      error("Can't fill gem group, it does not contain a valid spawn group:" SPC %gemGroup);
      return;
   }

   // it should be empty already but clear it out anyway
   %gemGroup.clear();

   // refill it using spawn points from its spawn group
   for (%i = 0; %i < %spawnGroup.getCount(); %i++)
   {
      %spawn = %spawnGroup.getObject(%i);

      // don't spawn duplicate gems
      if (isObject(%spawn.gem) && !%spawn.gem.isHidden())
         continue;

      // spawn a gem and light at the spawn point
      %gem = spawnGemAt(%spawn,true);

      // add gem to gemGroup
      %gemGroup.add(%gem);
   }

   echo("Took" SPC (getRealTime() - %start) @ "ms to fill gem groups");
   
   $LastFilledGemSpawnGroup = %gemGroup;
}

// setup the gem groups
function SetupGems(%numGemGroups)
{
   %start = getRealTime();

   // delete any active gem groups and their associated gems
   if (isObject(ActiveGemGroups))
      ActiveGemGroups.delete();

   // get gem group configuration params
   %gemGroupRadius = $Server::GemGroupRadius;
   %maxGemsPerGroup = $Server::MaxGemsPerGroup;

   // allow mission to override
   if (MissionInfo.gemGroupRadius > 0)
      %gemGroupRadius = MissionInfo.gemGroupRadius;
   if (MissionInfo.maxGemsPerGroup > 0)
      %maxGemsPerGroup = MissionInfo.maxGemsPerGroup;
   echo("SetupGems: using radius" SPC %gemGroupRadius SPC "and max gems per group" SPC %maxGemsPerGroup);

   // clear the "gem" field on each spawn point.  these may contain bogus but valid object ids that were
   // persisted by the mission editor
   %group = nameToId("MissionGroup/GemSpawns");
   for (%i = 0; %i < %group.getCount(); %i++)
      %group.getObject(%i).gem = 0;

   // set up the gem spawn groups (this is done in engine code to improve performance).
   // it will create a GemSpawnGroups object that contains groups of clustered spawn points
   SetupGemSpawnGroups(%gemGroupRadius, %maxGemsPerGroup);

   // ActiveGemGroups contains groups of spawned gems.  the groups are populated using the spawn point
   // information from the ActiveGemGroups object.  when a group is empty, a new gem spawn group is
   // selected for it, and then it is refilled with gems
   new SimGroup(ActiveGemGroups);
   MissionCleanup.add(ActiveGemGroups);

   // create the active gem groups
   for (%i = 0; %i < %numGemGroups; %i++)
   {
      %gemGroup = new SimGroup();
      ActiveGemGroups.add(%gemGroup);
   }

   // fill them up
   refillGemGroups();

   echo("Took" SPC (getRealTime() - %start) @ "ms to set up gems for mission");
}

//-----------------------------------------------------------------------------

function countGems(%group)
{
   // Count up all gems out there are in the world
   %gems = 0;
   for (%i = 0; %i < %group.getCount(); %i++)
   {
      %object = %group.getObject(%i);
      %type = %object.getClassName();
      if (%type $= "SimGroup")
         %gems += countGems(%object);
      else
         if (%type $= "Item" &&
               %object.getDatablock().classname $= "Gem")
            %gems++;
   }
   return %gems;
}

//-----------------------------------------------------------------------------
// Averagae player count code -- track integral of # of players over time
//-----------------------------------------------------------------------------

function updateAvgPlayerCount()
{
   if (isObject($timeKeeper))
   {
      // update player count averages
      echo("Updating average player count...");
      %time = $timeKeeper.player.getMarbleTime()/1000;
      %delta = %time-$Game::avgPlayerCountTime;
      echo("   Previous count was" SPC $Game::avgPlayerCount SPC "for" SPC $Game::avgPlayerCountTime SPC "seconds.");
      if (%delta<0)
      {
         // safety...
         error("updateAvgPlayerCount: negative time...");
         %delta=0;
      }
      echo("   Adding in" SPC $Game::lastPlayerCount SPC "players over an additional" SPC %delta SPC "seconds.");
      $Game::avgPlayerCount = $Game::avgPlayerCount * $Game::avgPlayerCountTime + $Game::lastPlayerCount * %delta;
      if (%time>0.0001)
         $Game::avgPlayerCount /= %time;
      $Game::avgPlayerCountTime = %time;
      $Game::lastPlayerCount =    $Server::PlayerCount;
      echo("   New average player count is" SPC $Game::avgPlayerCount SPC "and there are currently" SPC $Game::lastPlayerCount SPC "players in the game");
   }
   else
   {
      // Not nessecarily an error anymore.
      echo("No time keeper marble. Oooh, is this one of those exciting new level previews!?");
      resetAvgPlayerCount();
   }
}

function resetAvgPlayerCount()
{
   $Game::avgPlayerCount = $Server::PlayerCount;
   $Game::avgPlayerCountTime = 0;
   $Game::lastPlayerCount = 0;
}

function getAvgPlayerCount()
{
    return $Game::avgPlayerCount;
}

// ----------------------------------------------------------------------------
// weapon HUD
// ----------------------------------------------------------------------------
function GameConnection::setAmmoAmountHud(%client, %amount, %amountInClips )
{
   commandToClient(%client, 'SetAmmoAmountHud', %amount, %amountInClips);
}

function GameConnection::RefreshWeaponHud(%client, %amount, %preview, %ret, %zoomRet, %amountInClips)
{
   commandToClient(%client, 'RefreshWeaponHud', %amount, %preview, %ret, %zoomRet, %amountInClips);
}
