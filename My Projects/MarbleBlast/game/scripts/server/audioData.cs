//-----------------------------------------------------------------------------
// Ready - Set - Get Rolling

datablock SFXProfile(pauseSfx)
{
   filename    = "art/sound/level_text.wav";
   description = Audio2D;
   preload = true;
};

datablock SFXProfile(easterNewSfx)
{
   filename    = "art/sound/easter_egg.wav";
   description = Audio2D;
   preload = true;
};

datablock SFXProfile(easterNotNewSfx)
{
   filename    = "art/sound/pu_easter.wav";
   description = Audio2D;
   preload = true;
};

datablock SFXProfile(spawnSfx)
{
   filename    = "art/sound/spawn_alternate.wav";
   description = AudioClose3d;
   preload = true;
};

datablock SFXProfile(pickupSfx)
{
   filename    = "art/sound/pickup.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock SFXProfile(HelpDingSfx)
{
   filename    = "art/sound/InfoTutorial.wav";
   description = Audio2D;
   preload = true;
};

datablock SFXProfile(ReadyVoiceSfx)
{
   filename    = "art/sound/ready.wav";
   description = Audio2D;
   preload = true;
};

datablock SFXProfile(SetVoiceSfx)
{
   filename    = "art/sound/set.wav";
   description = Audio2D;
   preload = true;
};

datablock SFXProfile(CheckpointSfx)
{
   filename = "art/sound/checkpoint.wav";
   description = Audio2D;
   preload = true;
};

datablock SFXProfile(GetRollingVoiceSfx)
{
   filename    = "art/sound/go.wav";
   description = Audio2D;
   preload = true;
};

datablock SFXProfile(WonRaceSfx)
{
   filename    = "art/sound/finish.wav";
   description = Audio2D;
   volume = 0.55;
   preload = true;
};

datablock SFXProfile(MissingGemsSfx)
{
   filename    = "art/sound/missingGems.wav";
   description = Audio2D;
   preload = true;
};

datablock SFXProfile(jumpSfx)
{
   filename    = "art/sound/bounce.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock SFXProfile(bounceSfx)
{
   filename    = "art/sound/bounce.wav";
   description = AudioDefault3d;
   preload = true;
};

//-----------------------------------------------------------------------------
// Multiplayer

datablock SFXProfile(PlayerJoinSfx)
{
   filename    = "art/sound/spawn_alternate.wav";
   description = Audio2D;
   volume = 0.5;
   preload = true;
};

datablock SFXProfile(PlayerDropSfx)
{
   filename    = "art/sound/InfoTutorial.wav";
   volume = 0.5;
   description = Audio2D;
   preload = true;
};

//-----------------------------------------------------------------------------
// Misc

datablock SFXProfile(PenaltyVoiceSfx)
{
   filename    = "art/sound/penalty.wav";
   description = AudioDefault3d;
   preload = true;
};

// IGC HACK -pw
// datablock SFXProfile(OutOfBoundsVoiceSfx)
// {
   // filename    = "art/sound/whoosh.wav";
   // description = Audio2D;
   // preload = true;
// };

datablock SFXProfile(DestroyedVoiceSfx)
{
   filename    = "art/sound/destroyedVoice.wav";
   description = AudioDefault3d;
   preload = true;
};