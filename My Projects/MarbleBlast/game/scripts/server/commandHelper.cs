function Marble::shakeCamera(%this, %amplifier, %freq, %falloff, %duration)
{
   commandToClient(%this.client, 'ShakeCamera', %this.client.getGhostID(%this), %amplifier, %freq, %falloff, %duration);
}
