datablock MissionMarkerData(GemSpawnSphereMarker)
{
   category = "Misc";
   shapeFile = "core/art/shapes/octahedron.dts";
};

function SpawnSphere::getGemDataBlock(%obj)
{
   return %obj.gemDataBlock;
}

datablock MissionMarkerData(CameraSpawnSphereMarker)
{
   category = "Misc";
   shapeFile = "core/art/shapes/octahedron.dts";
};
