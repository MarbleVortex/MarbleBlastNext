//-----------------------------------------------------------------------------
// Torque Game Engine
//
// Copyright (c) 2001 GarageGames.Com
// Portions Copyright (c) 2001 by Sierra Online, Inc.
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------

$Const::OfficialMarbleCount = 37;

datablock ParticleData(BounceParticle)
{
   textureName          = "art/particles/burst";
   dragCoeffiecient     = 0.5;
   gravityCoefficient   = -0.1;
   windCoefficient      = 0;
   inheritedVelFactor   = 0;
   constantAcceleration = -2;
   lifetimeMS           = 400;
   lifetimeVarianceMS   = 50;
   useInvAlpha =  false;
   spinSpeed     = 90;
   spinRandomMin = -90.0;
   spinRandomMax =  90.0;

   colors[0]     = "0.5 0.5 0.5 0.3";
   colors[1]     = "0.3 0.3 0.2 0.1";
   colors[2]     = "0.2 0.2 0.1 0.0";

   sizes[0]      = 0.8;
   sizes[1]      = 0.4;
   sizes[2]      = 0.2;

   times[0]      = 0;
   times[1]      = 0.75;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(MarbleBounceEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;
   ejectionVelocity = 6.0;
   velocityVariance = 0.25;
   thetaMin         = 80.0;
   thetaMax         = 90.0;
   lifetimeMS       = 250;
   particles = "BounceParticle";
};

//-----------------------------------------------------------------------------

datablock ParticleData(TrailParticle)
{
   textureName          = "art/particles/burst";
   dragCoeffiecient     = 1.0;
   gravityCoefficient   = 0;
   windCoefficient      = 0;
   inheritedVelFactor   = 1;
   constantAcceleration = 0;
   lifetimeMS           = 100;
   lifetimeVarianceMS   = 10;
   useInvAlpha =  true;
   spinSpeed     = 0;

   colors[0]     = "1 1 0 0.0";
   colors[1]     = "1 1 0 1";
   colors[2]     = "1 1 1 0.0";

   sizes[0]      = 0.7;
   sizes[1]      = 0.4;
   sizes[2]      = 0.1;

   times[0]      = 0;
   times[1]      = 0.15;
   times[2]      = 1.0;
};

//datablock ParticleData(TrailParticle2)
//{
//   textureName          = "art/particles/star";
//   dragCoeffiecient     = 1.0;
//   gravityCoefficient   = 0;
//   windCoefficient      = 0;
//   inheritedVelFactor   = 0;
//   constantAcceleration = -2;
//   lifetimeMS           = 400;
//   lifetimeVarianceMS   = 100;
//   useInvAlpha =  true;
//   spinSpeed     = 90;
//   spinRandomMin = -90.0;
//   spinRandomMax =  90.0;
//
//   colors[0]     = "0.9 0.0 0.0 0.9";
//   colors[1]     = "0.9 0.9 0.0 0.9";
//   colors[2]     = "0.9 0.9 0.9 0.0";
//
//   sizes[0]      = 0.165;
//   sizes[1]      = 0.165;
//   sizes[2]      = 0.165;
//
//   times[0]      = 0;
//   times[1]      = 0.55;
//   times[2]      = 1.0;
//};

datablock ParticleData(TrailParticle2)
{
   textureName          = "art/particles/burst";
   dragCoefficient     = 0.0;
   gravityCoefficient   = 0;   
   inheritedVelFactor   = 0.00;
   lifetimeMS           = 2000;
   lifetimeVarianceMS   = 0;
   useInvAlpha = false;
   spinRandomMin = -90.0;
   spinRandomMax = 90.0;

   colors[0]     = "0.5 0.3 0.2 1.0";
   colors[1]     = "0.5 0.3 0.2 1.0";
   colors[2]     = "0.2 0.0 0.0 0.0";

   sizes[0]      = 0.6;
   sizes[1]      = 0.5;
   sizes[2]      = 0.1;

   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(MarbleTrailEmitter)
{
   ejectionPeriodMS = 9;
   periodVarianceMS = 0;
   ejectionVelocity = 3.0;
   velocityVariance = 0.25;
   thetaMin         = 60.0;
   thetaMax         = 90.0;
   lifetimeMS       = 1000000;
   particles = TrailParticle2;
};

//-----------------------------------------------------------------------------
// ActivePowerUp
// 0 - no active powerup
// 1 - Super Jump
// 2 - Super Speed
// 3 - Super Bounce
// 4 - Indestructible

datablock SFXProfile(Bounce1Sfx)
{
   filename    = "art/sound/bouncehard1.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock SFXProfile(Bounce2Sfx)
{
   filename    = "art/sound/bouncehard2.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock SFXProfile(Bounce3Sfx)
{
   filename    = "art/sound/bouncehard3.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock SFXProfile(Bounce4Sfx)
{
   filename    = "art/sound/bouncehard4.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock SFXProfile(MegaBounce1Sfx)
{
   filename    = "art/sound/mega_bouncehard1.wav";
   description = AudioClose3d;
   preload = true;
};

datablock SFXProfile(MegaBounce2Sfx)
{
   filename    = "art/sound/mega_bouncehard2.wav";
   description = AudioClose3d;
   preload = true;
};

datablock SFXProfile(MegaBounce3Sfx)
{
   filename    = "art/sound/mega_bouncehard3.wav";
   description = AudioClose3d;
   preload = true;
};

datablock SFXProfile(MegaBounce4Sfx)
{
   filename    = "art/sound/mega_bouncehard4.wav";
   description = AudioClose3d;
   preload = true;
};

datablock SFXProfile(JumpSfx)
{
   filename    = "art/sound/Jump.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock SFXProfile(RollingHardSfx)
{
   filename    = "art/sound/Rolling_Hard.wav";
   description = AudioClosestLoop3D;
   volume = 0.91;
   preload = true;
};

datablock SFXProfile(RollingMegaSfx)
{
   filename    = "art/sound/mega_roll.wav";
   description = AudioCloseLoop3d;
   volume = 0.91 * 0.85;
   preload = true;
};

datablock SFXProfile(RollingIceSfx)
{
   filename    = "art/sound/ice_roll.wav";
   description = AudioClosestLoop3d;
   volume = 0.91 * 0.85;
   preload = true;
};

datablock SFXProfile(SlippingSfx)
{
   filename    = "art/sound/Sliding.wav";
   description = AudioClosestLoop3d;
   volume = 0.60;
   preload = true;
};

datablock MarbleData(DefaultMarble)
{
   shadowEnable = true;
   shadowCanMove = true;
   shadowSize = 256;
   // both of the following need to be true to see shadows on the start pad.  but it gets really slow and 
   // their are render artifacts
   shadowSelfShadow = false; 
   shadowDTSShadow = false;
   
   shapeFile = "art/shapes/marbles/marble32.dts";
   emap = true;
   renderFirstPerson = true;
// maxRollVelocity = 55;
// angularAcceleration = 120;
   maxRollVelocity = 15;
   angularAcceleration = 75;
   brakingAcceleration = 30;
   gravity = 20; //20;
   staticFriction = 1.1;
   kineticFriction = 0.7;
   bounceKineticFriction = 0.2;
   maxDotSlide = 0.5;
   bounceRestitution = 0.5;
   jumpImpulse = 7.5;
   maxForceRadius = 50;
   size = 1.5;
   megaSize = 1.5 * 2.25;
   mass = 1;
   
   blastRechargeTime = 36000;
   maxNaturalBlastRecharge = 30000;

   bounce1 = Bounce1Sfx;
   bounce2 = Bounce2Sfx;
   bounce3 = Bounce3Sfx;
   bounce4 = Bounce4Sfx;
   megabounce1 = MegaBounce1Sfx;
   megabounce2 = MegaBounce2Sfx;
   megabounce3 = MegaBounce3Sfx;
   megabounce4 = MegaBounce4Sfx;

   rollHardSound = RollingHardSfx;
   rollMegaSound = RollingMegaSfx;
   rollIceSound = RollingIceSfx;
   slipSound = SlippingSfx;
   jumpSound = JumpSfx;
   
   // Emitters
   minTrailSpeed = 15;            // Trail threshold
   trailEmitter = MarbleTrailEmitter;
   
   minBounceSpeed = 3;           // Bounce threshold
   bounceEmitter = MarbleBounceEmitter;
   
   powerUps = PowerUpDefs;
   
   powerUpEmitter[1] = MarbleSuperJumpEmitter; 		// Super Jump
   powerUpEmitter[2] = MarbleSuperSpeedEmitter; 	// Super Speed
// powerUpEmitter[3] = MarbleSuperBounceEmitter; 	// Super Bounce
// powerUpEmitter[4] = MarbleShockAbsorberEmitter; 	// Shock Absorber
// powerUpEmitter[5] = MarbleHelicopterEmitter; 	// Helicopter
   
   // Power up timouts. Timeout on the speed and jump only affect
   // the particle trail
   powerUpTime[1] = 1000;	// Super Jump
   powerUpTime[2] = 1000; 	// Super Speed
   powerUpTime[3] = 5000; 	// Super Bounce
   powerUpTime[4] = 5000; 	// Shock Absorber
   powerUpTime[5] = 5000; 	// Helicopter

   // Allowable Inventory Items
   maxInv[SuperJumpItem] = 20;
   maxInv[SuperSpeedItem] = 20;
   maxInv[SuperBounceItem] = 20;
   maxInv[IndestructibleItem] = 20;
   maxInv[TimeTravelItem] = 20;
// maxInv[GoodiesItem] = 10;

   dynamicReflection = true;
};

// Anthony: Eval may be evil, but could it possibly be more evil than what what was going on here before?
// I doubt it, and I see no other solution, other than to write out around 100 different datablocks for every marble seperately.
// Or just use a supermarble like MBG, but we can't do that.
for ($i=1;$i<=$Const::OfficialMarbleCount;$i++) {
   eval("datablock MarbleData(Marble" @ $i @ " : DefaultMarble)\n{\nshapeFile = \"art/shapes/marbles/marble" @ $i @ ".dts\";\n};");
}

function MarbleData::processTick(%this, %obj, %clientSide)
{
   if (!%clientSide)
   {
      PlayGui.updateDebug();
   }
}

function MarbleData::onAdd(%this, %obj)
{
   echo("New Marble: " @ %obj);
   
   %obj.static = false;
   %obj.disableRotation = false;
   %obj.disablePhysics = false;
   %obj.disableCollision = false;
}

function MarbleData::onTrigger(%this, %obj, %triggerNum, %val)
{
   
}

//-----------------------------------------------------------------------------

function MarbleData::onCollision(%this,%obj,%col)
{
   // JMQ: workaround: skip hidden objects  
   if (%col.isHidden())
      return;
  
   // Try and pickup all items
   if (%col.getClassName() $= "Item")
      %obj.pickup(%col,1);
}

//-----------------------------------------------------------------------------
// The following event callbacks are punted over to the connection
// for processing

function MarbleData::onEnterPad(%this,%object)
{
   %object.client.onEnterPad();
}

function MarbleData::onLeavePad(%this, %object)
{
   %object.client.onLeavePad();
}

function MarbleData::onStartPenalty(%this, %object)
{
   %object.client.onStartPenalty();
}

function MarbleData::onOutOfBounds(%this, %object)
{
   if (isObject(%object) && isObject(%object.client))
      %object.client.onOutOfBounds();
}

function MarbleData::setCheckpoint(%this, %object, %check)
{
   %object.client.setCheckpoint(%check);
}

function MarbleData::onFinishPoint(%this, %object)
{
   %object.client.onFinishPoint();
}

function MarbleData::onEnterLiquid(%this, %obj, %coverage, %type)
{
   
}

function MarbleData::onUpdateLiquid(%this, %obj, %coverage, %type)
{
   if (MissionInfo.liquidOOB && %coverage >= 0.8 && !%obj.isOOB() && !%obj.blockLiquidOOB)
   {
      %obj.blockLiquidOOB = true;
      %this.onOutOfBounds(%obj);
   }
}

function MarbleData::onLeaveLiquid(%this, %obj, %type)
{
   %obj.blockLiquidOOB = false;
}

//-----------------------------------------------------------------------------
// Marble object
//-----------------------------------------------------------------------------

function Marble::setPowerUp(%this,%item,%reset)
{
   if (%item.powerUpId != 6)
   {
      // Hack: don't set powerup gui if blast item
      commandToClient(%this.client,'setPowerup',%item.bmpFile);
      %this.powerUpData = %item;
   }
   %this.setPowerUpId(%item.powerUpId,%reset);
}

function Marble::getPowerUp(%this)
{
   return %this.powerUpData;
}

function Marble::onPowerUpUsed(%this)
{
   commandToClient(%this.client,'setPowerup',"");
   %this.playAudio(0, %this.powerUpData.activeAudio);
   %this.powerUpData = "";
}

function Marble::onBlastUsed(%this)
{
   %this.playAudio(0, doBlastSfx);
   if ($pref::Marble::blastShake)
      %this.shakeCamera(2.0, 10.0, 10.0, 0.75);
}

function Marble::onTimeFreeze(%this,%timeFreeze)
{
   %this.client.incBonusTime(%timeFreeze);
}

function Marble::onPowerUpExpired(%this, %powerUpId )
{
   if( %powerUpId == 7 )
      %this.playAudio( 0, ShrinkMegaMarbleSfx );
}

//-----------------------------------------------------------------------------

function marbleVel()
{
   return $MarbleVelocity;
}

function metricsMarble()
{
   Canvas.pushDialog(FrameOverlayGui, 1000);
   TextOverlayControl.setValue("$MarbleVelocity");
}
