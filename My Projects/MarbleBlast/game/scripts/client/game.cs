//-----------------------------------------------------------------------------
// Copyright (c) 2012 GarageGames, LLC
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

$Game::StartPad = 0;

//----------------------------------------------------------------------------
// Game start / end events sent from the server
//----------------------------------------------------------------------------

function clientCmdGameStart(%seq)
{
   PlayerListGui.zeroScores();
}

function clientCmdGameEnd(%seq)
{
   // Stop local activity... the game will be destroyed on the server
   sfxStopAll();
   
   if ((!EditorIsActive() && !GuiEditorIsActive()))
   {
      %gemHunt = isObject(MissionInfo) && (MissionInfo.gameMode $= "Scrum" || MissionInfo.gameMode $= "Hunt" || MissionInfo.gameMode $= "Sumo");
      %joined = $Server::ServerType $= "" && !$Server::Hosting && $Client::missionRunning;
      %multiplayer = %gemHunt || %joined;
      if (%multiplayer)
      {
         // Copy the current scores from the player list into the
         // end game gui (bit of a hack for now).
         EndGameGuiList.clear();
         for (%i = 0; %i < PlayerListGuiList.rowCount(); %i++)
         {
            %text = PlayerListGuiList.getRowText(%i);
            %id = PlayerListGuiList.getRowId(%i);
            EndGameGuiList.addRow(%id, %text);
         }
         EndGameGuiList.sortNumerical(1, false);
      } else {
         EndGameGuiList.clear();
         %playerName = $pref::Player::Name;
         %time = PlayGui.elapsedTime;
         %score = formatTime(%time);
         EndGameGuiList.addRow(0, %playerName TAB %score);
      }

      // Display the end-game screen
      Canvas.setContent(EndGameGui);
   }
}

function clientCmdSetGemCount(%gems,%maxGems)
{
   PlayGui.setGemCount(%gems);
   PlayGui.setMaxGems(%maxGems);
}

function clientCmdSetGameDuration(%duration, %playStart)
{
   PlayGui.gameDuration = %duration;
   PlayGui.setTimer(0);

   if (%playStart > 0)
      $Client::LastGamePlayStart = %playStart;
}

function clientCmdSetEndTime(%playEnd)
{
   $Client::LastGamePlayEnd = %playEnd;
}

function clientCmdSetPowerup(%shapeFile)
{
   PlayGui.setPowerUp(%shapeFile);
}

function clientCmdSetMessage(%message,%time)
{
   PlayGui.setMessage(%message,%time);

   if( %message $= "ready" && isObject($Game::StartPad) )
   {
      $Game::StartPad.stopThread( 0 );
      $Game::StartPad.playThread( 0, "start" );
   }
}

function clientCmdInitStartPad(%pad)
{
   %obj = ServerConnection.resolveGhostID(%pad);
   $Game::StartPad = %obj;
   %obj.playThread(0,"ambient");
}

function clientCmdSetHelpLine(%helpLine,%beep)
{
   addHelpLine(%helpLine,%beep);
}

//function clientCmdRemoveHelpLine(%instant)
//{
   //removeHelpLine(%instant);
//}

function clientCmdAddStartMessage(%message,%isOptional)
{
   if (!%isOptional || $pref::displayMPHelpText)
      addStartMessage(%message);
}

function clientCmdSetTimer(%cmd,%time)
{
   switch$ (%cmd)
   {
      case "reset":
         PlayGui.resetTimer();
      case "start":
         PlayGui.startTimer();
      case "stop":
         PlayGui.stopTimer();
      case "set":
         PlayGui.setTimer(%time);
   }
}

function clientCmdSetGameState(%state, %data)
{
   echo("@@@@@@@@@@@@ got" SPC %state SPC "state");
}
