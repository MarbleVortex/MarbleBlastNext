function clientCmdShakeCamera(%ghostID, %amplifier, %freq, %falloff, %duration)
{
   %player = ServerConnection.resolveGhostID(%ghostID);
   
   if (isObject(%player))
      %player.applyCameraShake(%amplifier, %freq, %falloff, %duration);
}