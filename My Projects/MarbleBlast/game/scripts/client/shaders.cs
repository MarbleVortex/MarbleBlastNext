//-----------------------------------------------------------------------------
// Copyright (c) 2012 GarageGames, LLC
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

//*****************************************************************************
// Shaders  ( For Custom Materials )
//*****************************************************************************

// Matt: Ported this up from MBU/O
new ShaderData( NoiseTile )
{
   DXVertexShaderFile   = "shaders/common/noiseTileV.hlsl";
   DXPixelShaderFile    = "shaders/common/noiseTileP.hlsl";
   //OGLVertexShaderFile  = "shaders/common/gl/noiseTileV.glsl";
   //OGLPixelShaderFile   = "shaders/common/gl/noiseTileP.glsl";
   samplerNames[0] = "$diffuseMap";
   samplerNames[1] = "$bumpMap";
   samplerNames[2] = "$noiseMap";
   pixVersion = 2.0;
};

// Marbles

//new ShaderData( BumpMarb )
//{
   //DXVertexShaderFile 	= "shaders/common/marbles/marbBumpCubeDiffuseV.hlsl";
   //DXPixelShaderFile 	= "shaders/common/marbles/marbBumpCubeDiffuseP.hlsl";
   //pixVersion = 2.0;
//};
//
//new ShaderData( ClassicMarb )
//{
   //DXVertexShaderFile 	= "shaders/common/marbles/marbClassicV.hlsl";
   //DXPixelShaderFile 	= "shaders/common/marbles/marbClassicP.hlsl";
   //pixVersion = 2.0;
//};
//
//new ShaderData( ClassicRefRefMarb )
//{
   //DXVertexShaderFile 	= "shaders/common/marbles/marbClassicGlassV.hlsl";
   //DXPixelShaderFile 	= "shaders/common/marbles/marbClassicGlassP.hlsl";
   //pixVersion = 2.0;
//};
//
//new ShaderData( DiffMarb )
//{
   //DXVertexShaderFile 	= "shaders/common/marbles/marbDiffCubeV.hlsl";
   //DXPixelShaderFile 	= "shaders/common/marbles/marbDiffCubeP.hlsl";
   //pixVersion = 2.0;
//};
//
//new ShaderData( ClassicMarb2 )
//{
   //DXVertexShaderFile 	= "shaders/common/marbles/marbClassic2V.hlsl";
   //DXPixelShaderFile 	= "shaders/common/marbles/marbClassic2P.hlsl";
   //pixVersion = 2.0;
//};
//
//new ShaderData( ClassicMarb2PureSphere )
//{
   //DXVertexShaderFile 	= "shaders/common/marbles/marbClassic2PureSphereV.hlsl";
   //DXPixelShaderFile 	= "shaders/common/marbles/marbClassic2PureSphereP.hlsl";
   //pixVersion = 2.0;
//};
//
//new ShaderData( ClassicMarbGlass )
//{
   //DXVertexShaderFile 	= "shaders/common/marbles/marbClassicGlassPerturbV.hlsl";
   //DXPixelShaderFile 	= "shaders/common/marbles/marbClassicGlassPerturbP.hlsl";
   //pixVersion = 2.0;
//};
//
//new ShaderData( ClassicMarbGlassPureSphere )
//{
   //DXVertexShaderFile 	= "shaders/common/marbles/marbClassicGlassPerturbPureSphereV.hlsl";
   //DXPixelShaderFile 	= "shaders/common/marbles/marbClassicGlassPerturbPureSphereP.hlsl";
   //pixVersion = 2.0;
//};
//
//new ShaderData( ClassicMarbMetal )
//{
   //DXVertexShaderFile 	= "shaders/common/marbles/marbClassicMetalPerturbV.hlsl";
   //DXPixelShaderFile 	= "shaders/common/marbles/marbClassicMetalPerturbP.hlsl";
   //pixVersion = 2.0;
//};
//
//new ShaderData( ClassicMarbGlass2 )
//{
   //DXVertexShaderFile 	= "shaders/common/marbles/marbClassicGlassPerturb2V.hlsl";
   //DXPixelShaderFile 	= "shaders/common/marbles/marbClassicGlassPerturb2P.hlsl";
   //pixVersion = 2.0;
//};
//
//new ShaderData( CrystalMarb )
//{
   //DXVertexShaderFile 	= "shaders/common/marbles/marbCrystalV.hlsl";
   //DXPixelShaderFile 	= "shaders/common/marbles/marbCrystalP.hlsl";
   //pixVersion = 2.0;
//};
//
//new ShaderData( ClassicMarb3 )
//{
   //DXVertexShaderFile 	= "shaders/common/marbles/marbClassic3V.hlsl";
   //DXPixelShaderFile 	= "shaders/common/marbles/marbClassic3P.hlsl";
   //pixVersion = 2.0;
//};