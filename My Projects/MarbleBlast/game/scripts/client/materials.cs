//function defineBasicMarbleMaterial(%id, %shader, %bump, %diff, %mapTo, %specular, %specPow)
//{
   //%tmp = "singleton CustomMaterial(Material_Marble" @ %id @ ") " @
        //"{\n" @
        //"    mapTo = \"" @ %mapTo @ "\";\n" @
        //"    texture[0] = \"art/shapes/marbles/" @ %bump @ "\";\n" @
        //"    texture[1] = \"art/shapes/marbles/" @ %diff @ "\";\n" @
        //"    texture[3] = \"$dynamicCubemap\";\n" @
        //"    specular[0] = \"" @ %specular @ "\";\n" @
        //"    specularPower[0] = " @ %specPow @ ";\n" @
        //"    dynamicCubemap = true;\n" @ 
        ////"    cubemap = \"marbleCubemap3\";" @
        //"    shader = \"" @ %shader @ "\";\n" @
        //"    version = 2.0;\n" @ 
        //"};\n";
        //
////   echo(%tmp); // <-- I'm good for debugging.
   //eval(%tmp);
//}
//
//defineBasicMarbleMaterial("01", "ClassicMarbGlassPureSphere", "marble1.normal",  "marble1.skin",  "marble1.skin",  "0.6 0.6 0.6 0.6", 12 );
//defineBasicMarbleMaterial("02", "ClassicMarbGlassPureSphere", "marble1.normal",  "marble2.skin",  "marble2.skin",  "0.6 0.6 0.6 0.6", 12 );
//defineBasicMarbleMaterial("03", "ClassicMarbGlassPureSphere", "marble1.normal",  "marble3.skin",  "marble3.skin",  "0.6 0.6 0.6 0.6", 12 );
//defineBasicMarbleMaterial("04", "ClassicMarbGlassPureSphere", "marble1.normal",  "marble4.skin",  "marble4.skin",  "0.6 0.6 0.6 0.6", 12 );
//defineBasicMarbleMaterial("05", "ClassicMarbGlassPureSphere", "marble1.normal",  "marble5.skin",  "marble5.skin",  "0.6 0.6 0.6 0.6", 12 );
//defineBasicMarbleMaterial("06", "ClassicMarbGlassPureSphere", "marble1.normal",  "marble6.skin",  "marble6.skin",  "0.6 0.6 0.6 0.6", 12 );
//defineBasicMarbleMaterial("07", "ClassicMarbGlassPureSphere", "marble1.normal",  "marble7.skin",  "marble7.skin",  "0.6 0.6 0.6 0.6", 12 );
//defineBasicMarbleMaterial("08", "ClassicMarbGlassPureSphere", "marble1.normal",  "marble8.skin",  "marble8.skin",  "0.6 0.6 0.6 0.6", 12 );
//defineBasicMarbleMaterial("09", "CrystalMarb",                "marble9.normal",  "marble9.skin",  "marble9.skin",  "0.6 0.6 0.6 0.6", 12 );
//defineBasicMarbleMaterial("10", "CrystalMarb",                "marble9.normal",  "marble10.skin", "marble10.skin", "0.6 0.6 0.6 0.6", 12 );
//defineBasicMarbleMaterial("11", "CrystalMarb",                "marble9.normal",  "marble11.skin", "marble11.skin", "0.6 0.6 0.6 0.6", 12 );
//defineBasicMarbleMaterial("12", "CrystalMarb",                "marble9.normal",  "marble12.skin", "marble12.skin", "0.6 0.6 0.6 0.6", 12 );
//defineBasicMarbleMaterial("13", "CrystalMarb",                "marble9.normal",  "marble13.skin", "marble13.skin", "0.6 0.6 0.6 0.6", 12 );
//defineBasicMarbleMaterial("14", "CrystalMarb",                "marble9.normal",  "marble14.skin", "marble14.skin", "0.6 0.6 0.6 0.6", 12 );
//defineBasicMarbleMaterial("15", "ClassicMarbMetal",           "marble16.normal", "marble15.skin", "marble15.skin", "0.6 0.6 0.6 0.6", 12 );
//defineBasicMarbleMaterial("16", "ClassicMarbGlass",           "marble16.normal", "marble16.skin", "marble16.skin", "0.6 0.6 0.6 0.6", 12 );
//defineBasicMarbleMaterial("17", "ClassicMarbGlass",           "marble17.normal", "marble17.skin", "marble17.skin", "0.6 0.6 0.6 0.6", 12 );
//defineBasicMarbleMaterial("18", "ClassicMarbMetal",           "marble16.normal", "marble18.skin", "marble18.skin", "0.6 0.6 0.6 0.6", 12 );
//defineBasicMarbleMaterial("19", "ClassicMarb2",               "marble.normal",            "marble19.skin", "marble19.skin", "0.6 0.6 0.6 0.6", 12 );
//defineBasicMarbleMaterial("20", "ClassicMarb3",               "marble.normal",            "marble20.skin", "marble20.skin", "0.6 0.6 0.6 0.6", 12 );
//defineBasicMarbleMaterial("21", "ClassicMarb3",               "marble.normal",            "marble21.skin", "marble21.skin", "0.6 0.6 0.6 0.6", 12 );
//defineBasicMarbleMaterial("22", "ClassicMarb3",               "marble.normal",            "marble22.skin", "marble22.skin", "0.6 0.6 0.6 0.6", 12 );
//defineBasicMarbleMaterial("23", "ClassicMarb3",               "marble.normal",            "marble23.skin", "marble23.skin", "0.6 0.6 0.6 0.6", 12 );
//defineBasicMarbleMaterial("24", "ClassicMarb3",               "marble.normal",            "marble24.skin", "marble24.skin", "0.6 0.6 0.6 0.6", 12 );
//defineBasicMarbleMaterial("25", "ClassicMarb3",               "marble.normal",            "marble25.skin", "marble25.skin", "0.6 0.6 0.6 0.6", 12 );
//defineBasicMarbleMaterial("26", "ClassicMarb3",               "marble.normal",            "marble26.skin", "marble26.skin", "0.6 0.6 0.6 0.6", 12 );
//defineBasicMarbleMaterial("27", "ClassicMarb3",               "marble.normal",            "marble27.skin", "marble27.skin", "0.6 0.6 0.6 0.6", 12 );
//defineBasicMarbleMaterial("28", "ClassicMarb3",               "marble.normal",            "marble28.skin", "marble28.skin", "0.6 0.6 0.6 0.6", 12 );
//defineBasicMarbleMaterial("29", "ClassicMarb3",               "marble.normal",            "marble29.skin", "marble29.skin", "0.6 0.6 0.6 0.6", 12 );
//defineBasicMarbleMaterial("30", "ClassicMarb3",               "marble.normal",            "marble30.skin", "marble30.skin", "0.3 0.3 0.3 0.3", 24 );
//defineBasicMarbleMaterial("31", "ClassicMarb3",               "marble.normal",            "marble31.skin", "marble31.skin", "0.6 0.6 0.6 0.6", 12 );
//// MBG-Style Default Rainbow Marble
//defineBasicMarbleMaterial("32", "ClassicMarb",                "marblefx.normal",          "marble32.skin", "marble32.skin", "0.6 0.6 0.6 0.6", 12 );
//defineBasicMarbleMaterial("33", "ClassicMarb2",               "marble.normal",            "marble33.skin", "marble33.skin", "0.6 0.6 0.6 0.6", 12 );
//defineBasicMarbleMaterial("34", "ClassicMarb3",               "marble.normal",            "marble34.skin", "marble34.skin", "0.2 0.2 0.2 0.2", 6  );
//defineBasicMarbleMaterial("35", "ClassicMarb3",               "marble.normal",            "marble35.skin", "marble35.skin", "0.6 0.6 0.6 0.6", 12 );
//defineBasicMarbleMaterial("36", "ClassicMarb3",               "marble36.normal", "marble36.skin", "marble36.skin", "0.6 0.6 0.6 0.6", 12 );
//defineBasicMarbleMaterial("37", "ClassicMarbGlassPureSphere", "marble.normal",            "marble37.skin", "marble37.skin", "0.6 0.6 0.6 0.6", 12 );
