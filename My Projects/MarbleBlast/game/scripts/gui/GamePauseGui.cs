function GamePauseGui::onWake(%this)
{
   pauseGame();
   
   GamePauseGuiText.setText("<font:Arial:18><just:center>Would you like to exit this level?");
   GamePauseGuiText.forceReflow();
}

function GamePauseGui::onExitLevel(%this)
{
   %this.hide();
   $Client::SelectNextLevel = false;
   disconnect();
}

function GamePauseGui::onCancel(%this)
{
   %this.hide();
   resumeGame();
}

function GamePauseGui::onRestart(%this)
{
   %this.hide();
   resumeGame();
   restartLevel();
}

function GamePauseGui::show(%this)
{
   $GameCanvas.pushDialog(GamePauseGui);
}

function GamePauseGui::hide(%this)
{
   $GameCanvas.popDialog(GamePauseGui);
}