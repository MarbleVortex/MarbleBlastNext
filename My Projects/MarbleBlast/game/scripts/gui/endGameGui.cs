function EndGameGui::onWake(%this)
{
   //%active = $Server::Hosting || $Server::ServerType $= "SinglePlayer";
   %joined = $Server::ServerType $= "" && !$Server::Hosting && $Client::missionRunning;
   
   EndGameGuiReplayButton.setActive($Server::ServerType !$= "MultiPlayer" && !%joined);   
   EndGameGuiDoneButton.setText(%joined ? "Disconnect" : "Done");
}

function EndGameGui::onDone(%this)
{
   $Client::SelectNextLevel = true;
   disconnect();
}

function EndGameGui::onReplay(%this)
{
   Canvas.setContent(PlayGui);
   restartLevel();
}