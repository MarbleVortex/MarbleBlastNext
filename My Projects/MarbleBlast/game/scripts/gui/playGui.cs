//-----------------------------------------------------------------------------
// Copyright (c) 2012 GarageGames, LLC
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PlayGui is the main TSControl through which the game is viewed.
// The PlayGui also contains the hud controls.
//-----------------------------------------------------------------------------

function PlayGui::onWake(%this)
{
   // Turn off any shell sounds...
   // sfxStop( ... );

   $enableDirectInput = "1";
   activateDirectInput();
   
   %this.resizeGemSlots();
   %this.setGemCount(%this.gemCount);
   %this.setMaxGems(%this.maxGems);
   %this.scaleGemArrows();

   // recenter center things
   reCenter(TimeBox, 0);
   reCenter(HelpTextBox, 0);
   reCenter(ChatTextBox, 0);
   
   %multiplayer = $Server::ServerType $= "MultiPlayer" || ($Server::ServerType $= "" && !$Server::Hosting && $Client::missionRunning);
   if (isObject(PlayerListGui) && %multiplayer)
   {
      Canvas.pushDialog(PlayerListGui);
   }

   // Message hud dialog
   //if ( isObject( MainChatHud ) )
   //{
   //   Canvas.pushDialog( MainChatHud );
   //   chatHud.attach(HudMessageVector);
   //}      
   
   // just update the action map here
   moveMap.push();

   // hack city - these controls are floating around and need to be clamped
   if ( isFunction( "refreshCenterTextCtrl" ) )
      schedule(0, 0, "refreshCenterTextCtrl");
   if ( isFunction( "refreshBottomTextCtrl" ) )
      schedule(0, 0, "refreshBottomTextCtrl");
}

function PlayGui::onSleep(%this)
{
   if ( isObject( MainChatHud ) )
      Canvas.popDialog( MainChatHud );
   
   // pop the keymaps
   moveMap.pop();
}

//-----------------------------------------------------------------------------
function PlayGui::setMessage(%this,%text,%timer)
{
   if (%text $= "ready")
   {
      // Handle controler removed
      if( $ControlerRemovedDuringLoad )
      {
         lockedControlerRemoved();
      }

      // Handle network cable pull
      if( $NetworkPulledDuringLoad )
      {
         $NetworkPulledDuringLoad = false;
         NetworkDisconnectGui.onNetworkDisconnect();
      }
   }
   
   %isHuntLevel = $Client::connectedMultiplayer || MissionInfo.gameMode $= "Scrum" || MissionInfo.gameMode $= "Hunt" || MissionInfo.gameMode $= "race";

   switch$ (%text)
   {
      case "ready":
         %text = %isHuntLevel ? "Ready" : "";
      case "go":
         %text = %isHuntLevel ? "Go!" : "";
      case "outOfBounds":
         %text = "Out of Bounds";
   }

   // Set the center message text
   if (%text !$= "")  {
      CenterMessageDlgText.setText(%text);
      CenterMessageDlg.setVisible(true);
      cancel(CenterMessageDlg.timer);
      if (%timer)
         CenterMessageDlg.timer = CenterMessageDlg.schedule(%timer,"setVisible",false);
   }
   else
      CenterMessageDlg.setVisible(false);
}

//-----------------------------------------------------------------------------
function PlayGui::setPowerUp(%this,%bmpFile)
{
   // Update the power up hud control
   if (%bmpFile $= "")
   {
      %oldBitmap = %this.currentBitmap;
      %baseBitmap = "art/gui/game/powerup.png";
      if (%oldBitmap $= "")
         %oldBitmap = %baseBitmap;
      HUD_ShowPowerUp.setBitmap(%baseBitmap);
      %this.currentBitmap = %baseBitmap;
      
      HUD_ShowPowerUp.removeEffect("Spin");
      if (%oldBitmap !$= %baseBitmap && $pref::GUI::EffectsEnabled)
         HUD_ShowPowerUp.addEffect("Push");
   }
   else
   {
      HUD_ShowPowerUp.setBitmap("art/gui/game/" @ %bmpFile);
      %this.currentBitmap = %bmpFile;
      if ($pref::GUI::EffectsEnabled)
         HUD_ShowPowerUp.addEffect("Pop");
   }
   MinSec_Colon.setBitmap("art/gui/game/numbers/colon.png");
   MinSec_Point.setBitmap("art/gui/game/numbers/point.png");
   PG_NegSign.setBitmap("art/gui/game/numbers/dash.png");
   HUD_ShowGem.setBitmap("art/gui/game/gem.png");
   GemsSlash.setBitmap("art/gui/game/numbers/slash.png");
   TimeBox.setBitmap("art/gui/game/timebackdrop0");
   BoostBarFG.setBitmap("art/gui/game/powerbar.png");
}

function HUD_ShowPowerUp::onEffectDone(%this, %effect)
{
   if (!$pref::GUI::EffectsEnabled)
      return;
      
   if (%effect $= "Pop")
   {
      // TODO: Make new hud images to make this look better
      //%this.addEffect("Spin");
   }
}

function PlayGui::resizeGemSlots(%this)
{
   // These control gemCount/Max number placement
   $gemsVertPos = 4;
   $showGemVertPos = 2;

   // resize the position variables
   %newExt = GemBox.extent;
   %oldExt = GemBox.normextent;
   %horzScale = getWord(%newExt,0)/getWord(%oldExt,0);

   $gemsFoundFirstSlot  = 20*%horzScale;
   $gemsFoundSecondSlot = 38*%horzScale;
   $gemsFoundThirdSlot  = 56*%horzScale;

   $gemsSlashPos = 73*%horzScale;

   $gemsTotalFirstSlot  = 89*%horzScale;
   $gemsTotalSecondSlot = 107*%horzScale;
   $gemsTotalThirdSlot  = 125*%horzScale;

   $showGemFirstSlot = 107*%horzScale;
   $showGemSecondSlot = 125*%horzScale;
   $showGemThirdSlot = 144*%horzScale;
}

function PlayGui::setMaxGems(%this,%count)
{
   %this.maxGems = %count;
   %one = %count % 10;
   %ten = (%count - %one) % 100;
   %hundred = (%count - %ten - %one) % 1000;
   %ten /= 10;
   %hundred /= 100;
   GemsTotalHundred.setNumber(%hundred, true);
   GemsTotalTen.setNumber(%ten, true);
   GemsTotalOne.setNumber(%one, true);
   %visible = %count != 0;

   // Decide which of these should be visible and
   // where they should be positioned
   if (%count < 10)
   {
      GemsFoundOne.setVisible(%visible);
      GemsTotalOne.setVisible(%visible);
      GemsFoundTen.setVisible(false);
      GemsTotalTen.setVisible(false);
      GemsFoundHundred.setVisible(false);
      GemsTotalHundred.setVisible(false);

      GemsFoundOne.position = $gemsFoundThirdSlot - 48 SPC $gemsVertPos;
      GemsTotalOne.position = $gemsTotalFirstSlot - 48 SPC $gemsVertPos;

      GemsSlash.position = $gemsSlashPos - 48 SPC $gemsVertPos;

      HUD_ShowGem.setVisible(%visible);
      HUD_ShowGem.position = $showGemFirstSlot - 48 SPC $showGemVertPos;
   }
   else if (%count < 100)
   {
      GemsFoundOne.setVisible(%visible);
      GemsTotalOne.setVisible(%visible);
      GemsFoundTen.setVisible(%visible);
      GemsTotalTen.setVisible(%visible);
      GemsFoundHundred.setVisible(false);
      GemsTotalHundred.setVisible(false);

      GemsFoundOne.position = $gemsFoundThirdSlot - 24 SPC $gemsVertPos;
      GemsTotalOne.position = $gemsTotalSecondSlot - 24 SPC $gemsVertPos;
      GemsFoundTen.position = $gemsFoundSecondSlot - 24 SPC $gemsVertPos;
      GemsTotalTen.position = $gemsTotalFirstSlot - 24 SPC $gemsVertPos;

      GemsSlash.position = $gemsSlashPos - 24 SPC $gemsVertPos;

      HUD_ShowGem.setVisible(%visible);
      HUD_ShowGem.position = $showGemSecondSlot - 24 SPC $showGemVertPos;
   }
   else
   {
      GemsFoundOne.setVisible(%visible);
      GemsTotalOne.setVisible(%visible);
      GemsFoundTen.setVisible(%visible);
      GemsTotalTen.setVisible(%visible);
      GemsFoundHundred.setVisible(%visible);
      GemsTotalHundred.setVisible(%visible);

      GemsFoundOne.position = $gemsFoundThirdSlot SPC $gemsVertPos;
      GemsTotalOne.position = $gemsTotalThirdSlot SPC $gemsVertPos;
      GemsFoundTen.position = $gemsFoundSecondSlot SPC $gemsVertPos;
      GemsTotalTen.position = $gemsTotalSecondSlot SPC $gemsVertPos;
      GemsFoundHundred.position = $gemsFoundFirstSlot SPC $gemsVertPos;
      GemsTotalHundred.position = $gemsTotalFirstSlot SPC $gemsVertPos;

      GemsSlash.position = $gemsSlashPos SPC $gemsVertPos;

     %width = getWord(Canvas.getVideoMode(), 0);
     if (%width > 640)
        HUD_ShowGem.setVisible(%visible);
     else
        HUD_ShowGem.setVisible(false);
     HUD_ShowGem.position = $showGemThirdSlot SPC $showGemVertPos;
   }

   GemsSlash.setVisible(%visible);
   SUMO_NegSign.setVisible(false);

   //HUD_ShowGem.setModel("art/shapes/items/gem.dts","");
}

function PlayGui::setGemCount(%this,%count)
{
   %oldCount = %this.gemCount;
   %this.gemCount = %count;
   %one = %count % 10;
   %ten = (%count - %one) % 100;
   %hundred = (%count - %ten - %one) % 1000;
   %ten /= 10;
   %hundred /= 100;
   GemsFoundHundred.setNumber(%hundred, true);
   GemsFoundTen.setNumber(%ten, true);
   GemsFoundOne.setNumber(%one, true);
   
   %effect = "";
   if (%count > %oldCount)
   {
      %effect = "Pop";
   } else if (%count < %oldCount)
   {
      %effect = "Push";
   }
   
   if (%effect !$= "" && $pref::GUI::EffectsEnabled)
   {
      SUMO_NegSign.addEffect(%effect);
      GemsFoundHundred.addEffect(%effect);
      GemsFoundTen.addEffect(%effect);
      GemsFoundOne.addEffect(%effect);
      GemsSlash.addEffect(%effect);
      GemsTotalHundred.addEffect(%effect);
      GemsTotalTen.addEffect(%effect);
      GemsTotalOne.addEffect(%effect);
      HUD_ShowGem.addEffect(%effect);
   }
}

//-----------------------------------------------------------------------------
function PlayGui::setPoints(%this, %points)//, %clientid, %points)
{
   %pts = %points;

   %drawNeg = false;
   %negOffset = 0;
   if(%pts < 0)
   {
      %pts = - %pts;
      %drawNeg = true;
      %negOffset = 24;
   }

   %one = %pts % 10;
   %ten = (%pts - %one) % 100;
   %hundred = (%pts - %ten - %one) % 1000;
   %ten /= 10;
   %hundred /= 100;
   GemsFoundHundred.setNumber(%hundred, true);
   GemsFoundTen.setNumber(%ten, true);
   GemsFoundOne.setNumber(%one, true);

   %visible = %pts != 0;

   GemsSlash.setVisible(false);
   HUD_ShowGem.setVisible(false);
   GemsTotalOne.setVisible(false);
   GemsTotalTen.setVisible(false);
   GemsTotalHundred.setVisible(false);

   SUMO_NegSign.setVisible(%drawNeg);

   // Decide which of these should be visible and
   // where they should be positioned
   if (%pts < 10)
   {
      GemsFoundOne.setVisible(%visible);
      GemsFoundTen.setVisible(false);
      GemsFoundHundred.setVisible(false);

      GemsFoundOne.position = $gemsFoundThirdSlot - %negOffset SPC $gemsVertPos;
      
      GemsFoundOne.addEffect("Pop");
   }
   else if (%pts < 100)
   {
      GemsFoundOne.setVisible(%visible);
      GemsFoundTen.setVisible(%visible);
      GemsFoundHundred.setVisible(false);

      GemsFoundOne.position = $gemsFoundThirdSlot SPC $gemsVertPos;
      GemsFoundTen.position = $gemsFoundSecondSlot SPC $gemsVertPos;
      
      GemsFoundOne.addEffect("Pop");
      GemsFoundTen.addEffect("Pop");
   }
   else
   {
      GemsFoundOne.setVisible(%visible);
      GemsFoundTen.setVisible(%visible);
      GemsFoundHundred.setVisible(%visible);

      GemsFoundOne.position = $gemsFoundThirdSlot + %negOffset SPC $gemsVertPos;
      GemsFoundTen.position = $gemsFoundSecondSlot + %negOffset SPC $gemsVertPos;
      GemsFoundHundred.position = $gemsFoundFirstSlot + %negOffset SPC $gemsVertPos;
      
      GemsFoundOne.addEffect("Pop");
      GemsFoundTen.addEffect("Pop");
      GemsFoundHundred.addEffect("Pop");
   }
}

function PlayGui::updateControls(%this)
{
   %lineSpacingHack = "<font:Arial Bold:24> <font:Arial Bold:20>";
   %biggerFont = "<font:Arial Bold:24>";
	
   if (PlayGui.gameDuration)
      %et = PlayGui.gameDuration - %this.elapsedTime;
   else
      %et = %this.elapsedTime;
   %drawNeg = false;
   if(%et < 0)
   {
      %et = - %et;
      %drawNeg = true;
   }
   %hundredth = mFloor((%et % 1000) / 10);
   %totalSeconds = mFloor(%et / 1000);
   %seconds = %totalSeconds % 60;
   %minutes = (%totalSeconds - %seconds) / 60;
   %secondsOne      = %seconds % 10;
   %secondsTen      = (%seconds - %secondsOne) / 10;
   %minutesOne      = %minutes % 10;
   %minutesTen      = (%minutes - %minutesOne) / 10;
   %hundredthOne    = %hundredth % 10;
   %hundredthTen    = (%hundredth - %hundredthOne) / 10;
   // Update the controls
   Min_Ten.setNumber(%minutesTen);
   Min_One.setNumber(%minutesOne);
   Sec_Ten.setNumber(%secondsTen);
   Sec_One.setNumber(%secondsOne);
   Sec_Tenth.setNumber(%hundredthTen, true);
   Sec_Hundredth.setNumber(%hundredthOne, true);
   PG_NegSign.setVisible(%drawNeg);

   if (%this.lastHundredth != %hundredth)
   {
      TimeBox.animBitmap("timebackdrop");
      %this.lastHundredth = %hundredth;
   }
}

function PlayGui::scaleGemArrows(%this)
{
   /*
	// defaults
// 	%ellipseScreenFractionX = "0.790000";
// 	%ellipseScreenFractionY = "0.990000";
	%fullArrowLength = "60";
	%fullArrowWidth = "40";
// 	%maxArrowAlpha = "0.6";
// 	%maxTargetAlpha = "0.4";
 	%minArrowFraction = "0.4";
	
	// defaults based on 640x480 scale up accordingly
// 	%xscale = (getWord(RootShapeNameHud.getExtent(), 0)/640);
	%yscale = (getWord(RootShapeNameHud.getExtent(), 1)/480);
	
//  	RootShapeNameHud.ellipseScreenFraction = (%ellipseScreenFractionX*%xscale) SPC (%ellipseScreenFractionY*%yscale);
 	RootShapeNameHud.fullArrowLength = %fullArrowLength*%yscale;
 	RootShapeNameHud.fullArrowWidth = %fullArrowWidth*%yscale;
//   	RootShapeNameHud.minArrowFraction = %minArrowFraction*%yscale;
*/
}

function PlayGui::clearHud( %this )
{
   Canvas.popDialog( MainChatHud );

   while ( %this.getCount() > 0 )
      %this.getObject( 0 ).delete();
}

//-----------------------------------------------------------------------------
// Elapsed Timer Display
function PlayGui::setTimer(%this,%dt)
{
   %this.elapsedTime = %dt;
   %this.updateControls();
}

function PlayGui::resetTimer(%this)
{
   %this.elapsedTime = 0;
   %this.stopTimer();
   %this.updateControls();
}

function PlayGui::startTimer(%this)
{
}

function PlayGui::stopTimer(%this)
{
   if($BonusSfx !$= "")
   {
      sfxStop($BonusSfx);
      $BonusSfx = "";
   }
}

function PlayGui::updateTimer(%this, %time, %bonusTime)
{
   %startTime = $Sim::Time;
   %delta = (%startTime - $LastTime) * 1000;
   $LastTime = %startTime;
   
   if (%bonusTime && $BonusSfx $= "")
   {
      $BonusSfx = sfxPlay(TimeTravelLoopSfx);
   }
   else if (%bonusTime == 0 && $BonusSfx !$= "")
   {
      sfxStop($BonusSfx);
      $BonusSfx = "";
   }
   
   if (%bonusTime && $pref::GUI::EffectsEnabled)
   {
      %effect = "Push";
      Min_Ten.addEffect(%effect);
      Min_One.addEffect(%effect);
      Sec_Ten.addEffect(%effect);
      Sec_One.addEffect(%effect);
      Sec_Tenth.addEffect(%effect);
      Sec_Hundredth.addEffect(%effect);
      MinSec_Colon.addEffect(%effect);
      MinSec_Point.addEffect(%effect);
      //PG_NegSign.addEffect(%effect);
      //TimeBox.addEffect(%effect);
   }

   %this.elapsedTime = %time;

   // Some sanity checking
   if (%this.elapsedTime > 3600000)
      %this.elapsedTime = 3599999;
   %this.updateControls();
}

//-----------------------------------------------------------------------------

function GuiBitmapCtrl::setNumber(%this,%number)
{
   %this.setBitmap($Con::Root @ "art/gui/game/numbers/" @ %number @ ".png");
   %this.setBitmap("art/gui/game/numbers/" @ %number @ ".png");
}

function GuiFancyBitmapCtrl::setNumber(%this,%number, %disableEffect)
{
   %oldNumber = %this.number;
   %this.number = %number;
   %this.setBitmap($Con::Root @ "art/gui/game/numbers/" @ %number @ ".png");
   %this.setBitmap("art/gui/game/numbers/" @ %number @ ".png");
   
   if (%this.number != %oldNumber && !%disableEffect && $pref::GUI::EffectsEnabled)
   {
      // Debugging FTW
      //if (%this.objectName $= "Min_One")
      //{
         //echo("BLABLABLA");
      //}
      %this.addEffect("Pop");
   }
   
   // Matt: This looks odd on the timer
   // Commenting For now...
   //if (!%disableEffect)
   //{
      //if (%number > %oldNumber)
      //{
         //%this.addEffect("Pop");
      //} else if (%number < %oldNumber)
      //{
         //%this.addEffect("Push");
      //}
   //}
}

function GuiBitmapCtrl::animBitmap(%this,%bitmapPrefix)
{
   %this.setBitmap($Con::Root @ "art/gui/game/" @ %bitmapPrefix @ %this.animCurrent @ ".png");
   %this.setBitmap("art/gui/game/" @ %bitmapPrefix @ %this.animCurrent @ ".png");
   %this.animCurrent = (%this.animCurrent >= %this.animMax) ? 0 : %this.animCurrent++;
}

function GuiFancyBitmapCtrl::animBitmap(%this,%bitmapPrefix)
{
   %this.setBitmap($Con::Root @ "art/gui/game/" @ %bitmapPrefix @ %this.animCurrent @ ".png");
   %this.setBitmap("art/gui/game/" @ %bitmapPrefix @ %this.animCurrent @ ".png");
   %this.animCurrent = (%this.animCurrent >= %this.animMax) ? 0 : %this.animCurrent++;
}

//-----------------------------------------------------------------------------

function refreshBottomTextCtrl()
{
   BottomPrintText.position = "0 0";
}

function refreshCenterTextCtrl()
{
   CenterPrintText.position = "0 0";
}

//-----------------------------------------------------------------------------

function PlayGui::updateDebug(%this)
{
   PG_Debug_Text.setText("Cam: " @ $camInfo);
}
