function c()
{
   if (isObject(LocalClientConnection))
   {
      return LocalClientConnection;
   } else if (isObject(ServerConnection))
   {
      return ServerConnection;
   } else {
      return 0;
   }
}

function p()
{
   if (isObject(LocalClientConnection) && isObject(LocalClientConnection.player))
   {
      return LocalClientConnection.player;
   } else {
      return 0;
   }
}

function getGravity()
{
   %player = p();
   if (isObject(%player))
   {
      return %player.getGravityDir();
   } else {
      return 0;
   }
}