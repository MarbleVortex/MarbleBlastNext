#ifndef _GUITABLISTCTRL_H_
#define _GUITABLISTCTRL_H_

#include "gui/core/guiControl.h"

class GuiTabListCtrl : public GuiControl
{
	typedef GuiControl Parent;

public:
	enum Orientation
	{
		Top = 0,
		Bottom//,
		//Left,
		//Right
	};

protected:

	Orientation mOrientation;
	S32 mTabHeight;
	S32 mTabWidth;
	S32 mFrontPadding;
	bool mHasTexture;
	RectI* mBitmapBounds;

	enum
	{
		TabSelected = 0,     ///< Index of selected tab texture
		TabNormal,           ///< Index of normal tab texture
		TabHover,            ///< Index of hover tab texture
		TabEnds,             ///< Index of end lines for horizontal tabs
		NumBitmaps           ///< Number of bitmaps in this array
	};

	struct Tab
	{
		String text;
		String data;
	};

	Vector<Tab> mTabs;
	S32 mSelectedIndex;
	S32 mHoverIndex;

	S32 findHitTab(Point2I hitPoint);
	RectI getTabRect(S32 index);

public:
	GuiTabListCtrl();
	~GuiTabListCtrl();

	static void initPersistFields();

	bool onWake();
	void onRender(Point2I offset, const RectI &updateRect);

	void renderTab(Point2I offset, RectI rect, S32 indexMultiplier, String text);

	virtual void onMouseMove(const GuiEvent &event);
	virtual void onMouseDown(const GuiEvent &event);
	virtual void onMouseLeave(const GuiEvent &event);

	void clearTabs();
	bool addTab(String text, String data);
	bool containsTab(String text, String data = "");
	S32 getTabCount();
	String getTabText(S32 index);
	String getTabData(S32 index);
	S32 getSelectedIndex();
	String getSelectedText();
	String getSelectedData();
	void selectTabByIndex(S32 index);
	void selectTabByText(String text);
	void selectTabByData(String data);

	void setOrientation(Orientation orientation);
	Orientation getOrientation();

	DECLARE_CONOBJECT(GuiTabListCtrl);

	DECLARE_CALLBACK(void, onTabSelected, (String text, S32 index));
};

typedef GuiTabListCtrl::Orientation TabOrientation;
DefineEnumType(TabOrientation);

#endif // _GUITABLISTCTRL_H_