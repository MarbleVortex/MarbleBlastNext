#include "gui/controls/guiTabListCtrl.h"

#include "console/engineAPI.h"
#include "gfx/gfxDevice.h"
#include "gfx/gfxDrawUtil.h"
#include "gui/core/guiDefaultControlRender.h"

// Temporary
#include "windowManager/platformWindowMgr.h"

IMPLEMENT_CONOBJECT(GuiTabListCtrl);

IMPLEMENT_CALLBACK(GuiTabListCtrl, onTabSelected, void, (String text, S32 index), (text, index),
	"Called when a tab is selected.");

ImplementEnumType(TabOrientation, "The orientation for the Tabs")
	{ GuiTabListCtrl::Top, "Top", "Display on top." },
	{ GuiTabListCtrl::Bottom, "Bottom", "Display on bottom." },
	//{ GuiTabListCtrl::Left, "Left", "Display on left." },
	//{ GuiTabListCtrl::Right, "Right", "Display on right." },
EndImplementEnumType;

// -----------------------------------------------------------------------

GuiTabListCtrl::GuiTabListCtrl()
{
	this->mOrientation = GuiTabListCtrl::Top;
	this->mTabHeight = 25;
	this->mTabWidth = 75;
	this->mFrontPadding = 0;
	this->mHasTexture = false;
	this->mBitmapBounds = NULL;

	this->mSelectedIndex = 0;
	this->mHoverIndex = -1;
}

GuiTabListCtrl::~GuiTabListCtrl()
{
	this->mTabs.clear();
}

void GuiTabListCtrl::initPersistFields()
{
	addField("orientation", TYPEID<TabOrientation>(), Offset(mOrientation, GuiTabListCtrl));

	Parent::initPersistFields();
}

bool GuiTabListCtrl::onWake()
{
	if (!Parent::onWake())
		return false;

	mHasTexture = mProfile->constructBitmapArray() > 0;
	if (mHasTexture)
	{
		mBitmapBounds = mProfile->mBitmapArrayRects.address();
		mTabHeight = mBitmapBounds[TabSelected].extent.y;
	}

	return true;
}

// -----------------------------------------------------------------------

void GuiTabListCtrl::onMouseDown(const GuiEvent &event)
{
	Point2I localMouse = globalToLocalCoord(event.mousePoint);

	S32 tabIndex = findHitTab(localMouse);
	if (tabIndex >= 0)
	{
		selectTabByIndex(tabIndex);
	}
}

// -----------------------------------------------------------------------

void GuiTabListCtrl::onMouseMove(const GuiEvent &event)
{
	Point2I localMouse = globalToLocalCoord(event.mousePoint);

	S32 tabIndex = findHitTab(localMouse);
	if (tabIndex >= 0 && tabIndex != this->mHoverIndex)
		this->mHoverIndex = tabIndex;
	else if (tabIndex < 0)
		this->mHoverIndex = -1;

	Parent::onMouseMove(event);
}

// -----------------------------------------------------------------------

void GuiTabListCtrl::onMouseLeave(const GuiEvent &event)
{
	Parent::onMouseLeave(event);

	this->mHoverIndex = -1;
}

// -----------------------------------------------------------------------

S32 GuiTabListCtrl::findHitTab(Point2I hitPoint)
{
	if (mTabs.empty() || mTabHeight <= 0)
		return -1;

	for (S32 i = 0; i < mTabs.size(); i++)
	{
		if (getTabRect(i).pointInRect(hitPoint))
			return i;
	}
	return -1;
}

RectI GuiTabListCtrl::getTabRect(S32 index)
{
	S32 tabOffset = (index * mTabWidth) + mFrontPadding;
	return RectI(tabOffset, 0, mTabWidth, mTabHeight);
}

// -----------------------------------------------------------------------

void GuiTabListCtrl::clearTabs()
{
	this->mTabs.clear();
}

// -----------------------------------------------------------------------

bool GuiTabListCtrl::addTab(String text, String data)
{
	if (this->containsTab(text, data))
		return false;

	Tab tab;
	tab.text = text;
	if (data.isEmpty())
		tab.data = text;
	else
		tab.data = data;

	this->mTabs.push_back(tab);
	return true;
}

// -----------------------------------------------------------------------

bool GuiTabListCtrl::containsTab(String text, String data)
{
	for (S32 i = 0; i < this->mTabs.size(); i++)
	{
		String txt = this->mTabs[i].text;
		if (text == txt)
			return true;
		if (data.isNotEmpty())
		{
			String dta = this->mTabs[i].data;
			if (data == dta)
				return true;
		}
	}

	return false;
}

// -----------------------------------------------------------------------

S32 GuiTabListCtrl::getTabCount()
{
	return this->mTabs.size();
}

// -----------------------------------------------------------------------

String GuiTabListCtrl::getTabText(S32 index)
{
	if (index < 0 || index >= this->mTabs.size())
	{
		Con::warnf("GuiTabListCtrl::getTabText: Index out of range!");
		return StringTable->insert("");
	}

	return this->mTabs[index].text;
}

// -----------------------------------------------------------------------

String GuiTabListCtrl::getTabData(S32 index)
{
	if (index < 0 || index >= this->mTabs.size())
	{
		Con::warnf("GuiTabListCtrl::getTabText: Index out of range!");
		return StringTable->insert("");
	}

	return this->mTabs[index].data;
}

// -----------------------------------------------------------------------

S32 GuiTabListCtrl::getSelectedIndex()
{
	return this->mSelectedIndex;
}

// -----------------------------------------------------------------------

String GuiTabListCtrl::getSelectedText()
{
	if (this->mSelectedIndex < 0 || this->mSelectedIndex >= this->mTabs.size())
		return StringTable->insert("");

	return this->mTabs[this->mSelectedIndex].text;
}

// -----------------------------------------------------------------------

String GuiTabListCtrl::getSelectedData()
{
	if (this->mSelectedIndex < 0 || this->mSelectedIndex >= this->mTabs.size())
		return StringTable->insert("");

	return this->mTabs[this->mSelectedIndex].data;
}

// -----------------------------------------------------------------------

void GuiTabListCtrl::selectTabByIndex(S32 index)
{
	if (index < 0 || index >= this->mTabs.size())
	{
		Con::warnf("GuiTabListCtrl::selectTabByIndex: Index out of range!");
		return;
	}

	// Already Selected?
	if (index == this->mSelectedIndex)
		return;

	this->mSelectedIndex = index;

	this->onTabSelected_callback(this->mTabs[index].data, index);
}

// -----------------------------------------------------------------------

void GuiTabListCtrl::selectTabByText(String text)
{
	for (S32 i = 0; i < this->mTabs.size(); i++)
	{
		String tabText = this->mTabs[i].text;
		if (tabText == text)
		{
			selectTabByIndex(i);
			break;
		}
	}
}

// -----------------------------------------------------------------------

void GuiTabListCtrl::selectTabByData(String data)
{
	for (S32 i = 0; i < this->mTabs.size(); i++)
	{
		String tabData = this->mTabs[i].data;
		if (tabData == data)
		{
			selectTabByIndex(i);
			break;
		}
	}
}

// -----------------------------------------------------------------------

void GuiTabListCtrl::onRender(Point2I offset, const RectI &updateRect)
{
	// Temporarily disable clipping for testing
	Point2I res = PlatformWindowManager::get()->getDesktopResolution();
	RectI newClip = RectI(0, 0, res.x, res.y);
	GFX->setClipRect(newClip);

	// We're so nice we'll store the old modulation before we clear it for our rendering! :)
	ColorI oldModulation;
	GFX->getDrawUtil()->getBitmapModulation(&oldModulation);

	// Wipe it out
	GFX->getDrawUtil()->clearBitmapModulation();

	Parent::onRender(offset, updateRect);

	if (this->mTabs.empty())
		return;

	for (S32 i = 0; i < this->mTabs.size(); i++)
	{
		StringTableEntry text = this->mTabs[i].text;
		ColorI oldColor;

		GFX->getDrawUtil()->getBitmapModulation(&oldColor);

		S32 tabOffset = (i * mTabWidth) + mFrontPadding;

		RectI tabRect(tabOffset, 0, mTabWidth, mTabHeight);
		tabRect.point += offset;

		// Is this a skinned control?
		if (mHasTexture && mProfile->mBitmapArrayRects.size() >= 9)
		{
			S32 indexMultiplier = 1;
			if (i == this->mSelectedIndex)
				indexMultiplier += TabSelected;
			else if (i == this->mHoverIndex)
				indexMultiplier += TabHover;
			else
				indexMultiplier += TabNormal;

			RectI newTabRect(tabOffset, 0, mTabWidth, mTabHeight);

			this->renderTab(offset, newTabRect, indexMultiplier, text);
		}
		else
		{
			if (i == this->mSelectedIndex)
				GFX->getDrawUtil()->drawRectFill(tabRect, mProfile->mFillColor);
			else if (i == this->mHoverIndex)
				GFX->getDrawUtil()->drawRectFill(tabRect, mProfile->mFillColorHL);
			else
				GFX->getDrawUtil()->drawRectFill(tabRect, mProfile->mFillColorNA);
		}

		GFX->getDrawUtil()->setBitmapModulation(mProfile->mFontColor);
		
		renderJustifiedText(tabRect.point, tabRect.extent, text);

		GFX->getDrawUtil()->setBitmapModulation(oldColor);

		// If we're on the last tab, draw the nice end piece
		if (i + 1 == mTabs.size())
		{
			Point2I tabEndPoint = Point2I(tabRect.point.x + tabRect.extent.x + offset.x, tabRect.point.y + offset.y);
			Point2I tabEndExtent = Point2I((tabRect.point.x + tabRect.extent.x) - tabEndPoint.x, tabRect.extent.y);
			RectI tabEndRect = RectI(tabEndPoint, tabEndExtent);

			GFX->setClipRect(tabEndRect);

			// As it turns out the last tab can be outside the viewport in which
			// case trying to render causes a DX assert. Could be better if 
			// setClipRect returned a bool.
			if (GFX->getViewport().isValidRect())
				renderFixedBitmapBordersFilled(tabEndRect, TabEnds + 1, mProfile);
		}
	}

	// Restore old modulation
	GFX->getDrawUtil()->setBitmapModulation(oldModulation);
}

void GuiTabListCtrl::renderTab(Point2I offset, RectI rect, S32 indexMultiplier, String text)
{
	if (this->mOrientation == GuiTabListCtrl::Top)
	{
		RectI rect2 = rect;
		rect2.point += offset;
		renderFixedBitmapBordersFilled(rect2, indexMultiplier, mProfile);
	}
	else if (this->mOrientation == GuiTabListCtrl::Bottom)
	{
		Point2I center;
		center.x = (S32)(rect.point.x + rect.extent.x / 2.0f);
		center.y = (S32)(rect.point.y + rect.extent.y / 2.0f);
		
		GFX->pushWorldMatrix();

		MatrixF mat = GFX->getWorldMatrix();
		AngAxisF rot(Point3F(0.0f, 0.0f, -1.0f), mDegToRad(180.0f));
		rot.setMatrix(&mat);
		mat.setPosition(Point3F(offset.x + center.x, offset.y + center.y, 0.0f));

		GFX->multWorld(mat);

		RectI rect2 = rect;
		rect2.point -= center;
		renderFixedBitmapBordersFilled(rect2, indexMultiplier, mProfile);

		GFX->popWorldMatrix();
	}

	//if (this->mOrientation == GuiTabListCtrl::Top)
	//	renderFixedBitmapBordersFilled(rect, indexMultiplier, mProfile);
	//else if (this->mOrientation == GuiTabListCtrl::Bottom)
	//{
	//	GFX->pushWorldMatrix();

	//	MatrixF mat = GFX->getWorldMatrix();
	//	//Point3F pos = mat.getPosition();
	//	AngAxisF rot(Point3F(0.0f, 0.0f, -1.0f), mDegToRad(10.0f));
	//	rot.setMatrix(&mat);
	//	mat.setPosition(Point3F(mTabWidth / 2.0f, mTabHeight / 2.0f, 0.0f));
	//	//mat.setPosition(pos);

	//	//MatrixF rotMat;
	//	//AngAxisF rot(Point3F(0.0f, 0.0f, -1.0f), mDegToRad(90.0f));
	//	//rot.setMatrix(&rotMat);

	//	//mat.setPosition(pos);
	//	//mat.mul(rotMat);

	//	GFX->setWorldMatrix(mat);

	//	rect.point -= Point2I(mTabWidth / 2, mTabHeight / 2);
	//	renderFixedBitmapBordersFilled(rect, indexMultiplier, mProfile);
	//	rect.point += Point2I(mTabWidth / 2, mTabHeight / 2);

	//	GFX->popWorldMatrix();
	//}
}

void GuiTabListCtrl::setOrientation(Orientation orientation)
{
	this->mOrientation = orientation;
}

GuiTabListCtrl::Orientation GuiTabListCtrl::getOrientation()
{
	return this->mOrientation;
}

// -----------------------------------------------------------------------

DefineEngineMethod(GuiTabListCtrl, clearTabs, void, (),,
	"Removes all tabs from the control.")
{
	object->clearTabs();
}

// -----------------------------------------------------------------------

DefineEngineMethod(GuiTabListCtrl, addTab, bool, (String text, String data), ("", ""),
	"Adds a tab to the control.\n\n"
	"@param text Text for the tab.\n\n"
	"@param data Data for the tab (will be the same as text if left blank)\n\n"
	"@return whether or not the tab was added.")
{
	return object->addTab(text, data);
}

// -----------------------------------------------------------------------

DefineEngineMethod(GuiTabListCtrl, containsTab, bool, (String text), (""),
	"Checks if a tab with the specified text already exists.\n\n"
	"@param text Text to check for.\n\n"
	"@return whether or not the tab exists.")
{
	return object->containsTab(text);
}

// -----------------------------------------------------------------------

DefineEngineMethod(GuiTabListCtrl, getTabCount, S32, (),,
	"Get the number of tabs.\n\n"
	"@return The number of tabs.")
{
	return object->getTabCount();
}

// -----------------------------------------------------------------------

DefineEngineMethod(GuiTabListCtrl, getTabText, String, (S32 index), ,
	"Get the text of the specified tab.\n\n"
	"@param index Index of the tab to retrieve text from.\n\n"
	"@return The text of the specified tab.")
{
	return object->getTabText(index);
}

// -----------------------------------------------------------------------

DefineEngineMethod(GuiTabListCtrl, getTabData, String, (S32 index), ,
	"Get the data of the specified tab.\n\n"
	"@param index Index of the tab to retrieve data from.\n\n"
	"@return The data of the specified tab.")
{
	return object->getTabData(index);
}

// -----------------------------------------------------------------------

DefineEngineMethod(GuiTabListCtrl, getSelectedIndex, S32, (), ,
	"Get the index of the selected tab.\n\n"
	"@return The index of the selected tab.")
{
	return object->getSelectedIndex();
}

// -----------------------------------------------------------------------

DefineEngineMethod(GuiTabListCtrl, getSelectedText, String, (), ,
	"Get the text of the selected tab.\n\n"
	"@return The text of the selected tab.")
{
	return object->getSelectedText();
}

// -----------------------------------------------------------------------

DefineEngineMethod(GuiTabListCtrl, getSelectedData, String, (), ,
	"Get the data of the selected tab.\n\n"
	"@return The data of the selected tab.")
{
	return object->getSelectedData();
}

// -----------------------------------------------------------------------

DefineEngineMethod(GuiTabListCtrl, selectTabByIndex, void, (S32 index), ,
	"Select a tab by it's index.\n\n"
	"@param index Index of the tab to select.")
{
	object->selectTabByIndex(index);
}

// -----------------------------------------------------------------------

DefineEngineMethod(GuiTabListCtrl, selectTabByText, void, (String text), ,
	"Select a tab by the text.\n\n"
	"@param text Text of the tab to select.")
{
	object->selectTabByText(text);
}

// -----------------------------------------------------------------------

DefineEngineMethod(GuiTabListCtrl, selectTabByData, void, (String data), ,
	"Select a tab by the text.\n\n"
	"@param data Data of the tab to select.")
{
	object->selectTabByData(data);
}

// -----------------------------------------------------------------------

DefineEngineMethod(GuiTabListCtrl, setOrientation, void, (TabOrientation orientation), ,
	"Set the orientation of this control.\n\n"
	"@param orientation Orientation to set this control to.")
{
	object->setOrientation(orientation);
}

// -----------------------------------------------------------------------

DefineEngineMethod(GuiTabListCtrl, getOrientation, TabOrientation, (), ,
	"Get the orientation of this control.\n\n"
	"@return The orientation of this control.")
{
	return object->getOrientation();
}