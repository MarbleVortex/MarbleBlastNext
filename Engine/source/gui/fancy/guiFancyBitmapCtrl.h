#ifndef __GUIFANCYBITMAPCTRL_H_
#define __GUIFANCYBITMAPCTRL_H_

#include "gui/fancy/guiFancyControl.h"
#include "gui/shiny/guiTickCtrl.h"

class GuiFancyBitmapCtrl : public GuiFancyControl
{
public:
	typedef GuiFancyControl Parent;

protected:

	/// Name of the bitmap file.  If this is 'texhandle' the bitmap is not loaded
	/// from a file but rather set explicitly on the control.
	String mBitmapName;

	/// Loaded texture.
	GFXTexHandle mTextureObject;

	Point2I mStartPoint;

	/// If true, bitmap tiles inside control.  Otherwise stretches.
	bool mWrap;

	static bool setBitmapName(void *object, const char *index, const char *data);
	static const char *getBitmapName(void *obj, const char *data);

public:
	GuiFancyBitmapCtrl();
	static void initPersistFields();

	void setBitmap(const char *name, bool resize = false);
	void setBitmapHandle(GFXTexHandle handle, bool resize = false);

	// GuiControl.
	bool onWake();
	void onSleep();
	void inspectPostApply();

	void updateSizing();

	void onRender(Point2I offset,const RectI& updateRect);
	void setValue(S32 x, S32 y);

	DECLARE_CONOBJECT(GuiFancyBitmapCtrl);
};

#endif // __GUIFANCYBITMAPCTRL_H_