#include "gui/fancy/guiFancyBitmapCtrl.h"

#include "gfx/gfxDrawUtil.h"
#include "console/engineAPI.h"
#include "windowManager/platformWindowMgr.h"

IMPLEMENT_CONOBJECT(GuiFancyBitmapCtrl);

GuiFancyBitmapCtrl::GuiFancyBitmapCtrl() : mBitmapName(), mStartPoint(0, 0), mWrap(false)
{

}

bool GuiFancyBitmapCtrl::setBitmapName(void *object, const char *index, const char *data)
{
	// Prior to this, you couldn't do bitmap.bitmap = "foo.jpg" and have it work.
	// With protected console types you can now call the setBitmap function and
	// make it load the image.
	static_cast<GuiFancyBitmapCtrl *>(object)->setBitmap(data);

	// Return false because the setBitmap method will assign 'mBitmapName' to the
	// argument we are specifying in the call.
	return false;
}

void GuiFancyBitmapCtrl::initPersistFields()
{
	addGroup("Bitmap");

	addProtectedField("bitmap", TypeImageFilename, Offset(mBitmapName, GuiFancyBitmapCtrl),
		&setBitmapName, &defaultProtectedGetFn,
		"The bitmap file to display in the control.");
	addField("wrap", TypeBool, Offset(mWrap, GuiFancyBitmapCtrl),
		"If true, the bitmap is tiled inside the control rather than stretched to fit.");

	endGroup("Bitmap");

	Parent::initPersistFields();
}

bool GuiFancyBitmapCtrl::onWake()
{
	if (!Parent::onWake())
		return false;
	setActive(true);
	setBitmap(mBitmapName);
	return true;
}

void GuiFancyBitmapCtrl::onSleep()
{
	if (!mBitmapName.equal("texhandle", String::NoCase))
		mTextureObject = NULL;

	Parent::onSleep();
}

//-------------------------------------
void GuiFancyBitmapCtrl::inspectPostApply()
{
	// if the extent is set to (0,0) in the gui editor and appy hit, this control will
	// set it's extent to be exactly the size of the bitmap (if present)
	Parent::inspectPostApply();

	if (!mWrap && (getExtent().x == 0) && (getExtent().y == 0) && mTextureObject)
	{
		setExtent(mTextureObject->getWidth(), mTextureObject->getHeight());
	}
}

void GuiFancyBitmapCtrl::setBitmap(const char *name, bool resize)
{
	mBitmapName = name;
	if (!isAwake())
		return;

	if (mBitmapName.isNotEmpty())
	{
		if (!mBitmapName.equal("texhandle", String::NoCase))
			mTextureObject.set(mBitmapName, &GFXDefaultGUIProfile, avar("%s() - mTextureObject (line %d)", __FUNCTION__, __LINE__));

		// Resize the control to fit the bitmap
		if (mTextureObject && resize)
		{
			setExtent(mTextureObject->getWidth(), mTextureObject->getHeight());
			updateSizing();
		}
	}
	else
		mTextureObject = NULL;

	setUpdate();
}

void GuiFancyBitmapCtrl::updateSizing()
{
	if (!getParent())
		return;
	// updates our bounds according to our horizSizing and verSizing rules
	RectI fakeBounds(getPosition(), getParent()->getExtent());
	parentResized(fakeBounds, fakeBounds);
}

void GuiFancyBitmapCtrl::setBitmapHandle(GFXTexHandle handle, bool resize)
{
	mTextureObject = handle;

	mBitmapName = String("texhandle");

	// Resize the control to fit the bitmap
	if (resize)
	{
		setExtent(mTextureObject->getWidth(), mTextureObject->getHeight());
		updateSizing();
	}
}

void GuiFancyBitmapCtrl::onRender(Point2I offset, const RectI& updateRect)
{
	// Ignore Clipping Boundaries in-case the image becomes larger during the effects
	Point2I res = PlatformWindowManager::get()->getDesktopResolution();
	RectI newClip = RectI(0, 0, res.x, res.y);
	GFX->setClipRect(newClip);

	// Get the center point
	Point2F center;
	center.x = getExtent().x / 2.0f;
	center.y = getExtent().y / 2.0f;

	// Get the world matrix to apply effects
	GFX->pushWorldMatrix();
	MatrixF world = GFX->getWorldMatrix();
	MatrixF mat;

	// Apply Rotation
	AngAxisF rot(Point3F(0.0f, 0.0f, -1.0f), mDegToRad(mSpinTimer));
	rot.setMatrix(&mat);
	mat.setPosition(Point3F(offset.x + center.x, offset.y + center.y, 0.0f));
	world.mul(mat);

	// Apply Scale
	F32 scaleAmount = 0.005f;
	//if (this->mCurrentEffect == Push)
	if (this->mEffects.contains(Push))
		scaleAmount *= -1;
	F32 scaleNum = ((mRadToDeg(mSin(mDegToRad(mScaleTimer)))) * scaleAmount) + 1;
	Point3F scale(scaleNum, scaleNum, scaleNum);
	world.scale(scale);

	// Apply the effects to the world matrix
	GFX->setWorldMatrix(world);

	// Render the GuiBitmapCtrl
	// ----------
	Point2I newOffset(0.0f - center.x, 0.0f - center.y);

	if (mTextureObject)
	{
		GFX->getDrawUtil()->clearBitmapModulation();
		if (mWrap)
		{
			// We manually draw each repeat because non power of two textures will 
			// not tile correctly when rendered with GFX->drawBitmapTile(). The non POT
			// bitmap will be padded by the hardware, and we'll see lots of slack
			// in the texture. So... lets do what we must: draw each repeat by itself:
			GFXTextureObject* texture = mTextureObject;
			RectI srcRegion;
			RectI dstRegion;
			F32 xdone = ((F32)getExtent().x / (F32)texture->mBitmapSize.x) + 1;
			F32 ydone = ((F32)getExtent().y / (F32)texture->mBitmapSize.y) + 1;

			S32 xshift = mStartPoint.x%texture->mBitmapSize.x;
			S32 yshift = mStartPoint.y%texture->mBitmapSize.y;
			for (S32 y = 0; y < ydone; ++y)
				for (S32 x = 0; x < xdone; ++x)
				{
					srcRegion.set(0, 0, texture->mBitmapSize.x, texture->mBitmapSize.y);
					dstRegion.set(((texture->mBitmapSize.x*x) + newOffset.x) - xshift,
						((texture->mBitmapSize.y*y) + newOffset.y) - yshift,
						texture->mBitmapSize.x,
						texture->mBitmapSize.y);
					GFX->getDrawUtil()->drawBitmapStretchSR(texture, dstRegion, srcRegion, GFXBitmapFlip_None, GFXTextureFilterLinear);
				}

		}
		else
		{
			RectI rect(newOffset, getExtent());
			GFX->getDrawUtil()->drawBitmapStretch(mTextureObject, rect, GFXBitmapFlip_None, GFXTextureFilterLinear, false);
		}
	}

	if (mProfile->mBorder || !mTextureObject)
	{
		RectI rect(newOffset.x, newOffset.y, getExtent().x, getExtent().y);
		GFX->getDrawUtil()->drawRect(rect, mProfile->mBorderColor);
	}
	// ----------

	renderChildControls(offset, updateRect);

	// Restore the original world matrix
	GFX->popWorldMatrix();
}

void GuiFancyBitmapCtrl::setValue(S32 x, S32 y)
{
	if (mTextureObject)
	{
		x += mTextureObject->getWidth() / 2;
		y += mTextureObject->getHeight() / 2;
	}
	while (x < 0)
		x += 256;
	mStartPoint.x = x % 256;

	while (y < 0)
		y += 256;
	mStartPoint.y = y % 256;
}

DefineEngineMethod(GuiFancyBitmapCtrl, setValue, void, (S32 x, S32 y), ,
	"Set the offset of the bitmap within the control.\n"
	"@param x The x-axis offset of the image.\n"
	"@param y The y-axis offset of the image.\n")
{
	object->setValue(x, y);
}

static ConsoleDocFragment _sGuiFancyBitmapCtrlSetBitmap1(
	"@brief Assign an image to the control.\n\n"
	"Child controls with resize according to their layout settings.\n"
	"@param filename The filename of the image.\n"
	"@param resize Optional parameter. If true, the GUI will resize to fit the image.",
	"GuiBitmapCtrl", // The class to place the method in; use NULL for functions.
	"void setBitmap( String filename, bool resize );"); // The definition string.

static ConsoleDocFragment _sGuiFancyBitmapCtrlSetBitmap2(
	"@brief Assign an image to the control.\n\n"
	"Child controls will resize according to their layout settings.\n"
	"@param filename The filename of the image.\n"
	"@param resize A boolean value that decides whether the ctrl refreshes or not.",
	"GuiBitmapCtrl", // The class to place the method in; use NULL for functions.
	"void setBitmap( String filename );"); // The definition string.


//"Set the bitmap displayed in the control. Note that it is limited in size, to 256x256."
DefineConsoleMethod(GuiFancyBitmapCtrl, setBitmap, void, (const char * fileRoot, bool resize), (false),
	"( String filename | String filename, bool resize ) Assign an image to the control.\n\n"
	"@hide")
{
	char filename[1024];
	Con::expandScriptFilename(filename, sizeof(filename), fileRoot);
	object->setBitmap(filename, resize);
}
