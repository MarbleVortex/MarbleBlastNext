#include "gui/fancy/guiFancyControl.h"

#include "gui/core/guiDefaultControlRender.h"
#include "gfx/gfxDrawUtil.h"
#include "console/engineAPI.h"
#include "windowManager/platformWindowMgr.h"

IMPLEMENT_CONOBJECT(GuiFancyControl);

ImplementEnumType(GuiFancyEffect, "Fancy Effect Types for GuiFancy*")
	{ GuiFancyControl::Pop, "Pop", "Makes Gui Elements Pop" },
	{ GuiFancyControl::Push, "Push", "Makes Gui Elements Push" },
	{ GuiFancyControl::Spin, "Spin", "Makes Gui Elements Spin" }
EndImplementEnumType;

IMPLEMENT_CALLBACK(GuiFancyControl, onEffectAdded, void, (GuiFancyEffect effect), (effect), "");
IMPLEMENT_CALLBACK(GuiFancyControl, onEffectRemoved, void, (GuiFancyEffect effect), (effect), "");
IMPLEMENT_CALLBACK(GuiFancyControl, onEffectsCleared, void, (), (), "");
IMPLEMENT_CALLBACK(GuiFancyControl, onEffectDone, void, (GuiFancyEffect effect), (effect), "");

GuiFancyControl::GuiFancyControl()
{
	mEffects = Vector<EffectType>();

	mSpinTimer = 0.0f;
	mScaleTimer = 0.0f;
}

GuiFancyControl::~GuiFancyControl()
{

}

void GuiFancyControl::initPersistFields()
{
	Parent::initPersistFields();
}

void GuiFancyControl::resetVariables(EffectType effect)
{
	// Reset Variables
	switch (effect)
	{
	case Pop:
	case Push:
		this->mScaleTimer = 0.0f;
		break;
	case Spin:
		this->mSpinTimer = 0.0f;
		break;
	default:
		break;
	}
}

void GuiFancyControl::resetAllVariables()
{
	this->mScaleTimer = 0.0f;
	this->mSpinTimer = 0.0f;
}

void GuiFancyControl::advanceTime(F32 timeDelta)
{
	// Update the effect
	this->updateEffect(timeDelta);
}

void GuiFancyControl::updateEffect(F32 dt)
{
	for (S32 i = 0; i < this->mEffects.size(); i++)
	{
		EffectType type = this->mEffects[i];
		switch (type)
		{
		case Pop:
		case Push:
			// Update Pop/Push Effect
			mScaleTimer += 1800 * dt;
			if (mScaleTimer >= 180)
			{
				// Only do the effect once
				this->removeEffect(type, false);
				this->onEffectDone_callback(type);
			}
			break;
		case Spin:
			// Update Spin Effect
			mSpinTimer += 100 * dt;
			while (mSpinTimer >= 360)
				mSpinTimer -= 360;
			break;
			//case None:
		default:
			break;
		}
	}
}

void GuiFancyControl::addEffect(EffectType effect, bool doCallback)
{
	if (this->mEffects.contains(effect))
		return;

	this->mEffects.push_back(effect);

	this->resetVariables(effect);

	// Notify Script
	if (doCallback)
		this->onEffectAdded_callback(effect);
}

void GuiFancyControl::removeEffect(EffectType effect, bool doCallback)
{
	if (!this->mEffects.contains(effect))
		return;

	this->mEffects.remove(effect);

	this->resetVariables(effect);

	// Notify Script
	if (doCallback)
		this->onEffectRemoved_callback(effect);
}

void GuiFancyControl::clearEffects(bool doCallback)
{
	this->mEffects.clear();

	this->resetAllVariables();

	// Notify Script
	if (doCallback)
		this->onEffectsCleared_callback();
}

void GuiFancyControl::renderChildControls(Point2I offset, const RectI &updateRect)
{
	// Save the current clip rect 
	// so we can restore it at the end of this method.
	RectI savedClipRect = GFX->getClipRect();

	// offset is the upper-left corner of this control in screen coordinates
	// updateRect is the intersection rectangle in screen coords of the control
	// hierarchy.  This can be set as the clip rectangle in most cases.
	//RectI clipRect = updateRect;

	iterator i;
	for (i = begin(); i != end(); i++)
	{
		GuiControl *ctrl = static_cast<GuiControl *>(*i);
		if (ctrl->isVisible())
		{
			Point2I childPosition = offset + ctrl->getPosition();
			RectI childClip(childPosition, ctrl->getExtent() + Point2I(1, 1));

			//if (childClip.intersect(clipRect))
			//{
				//GFX->setClipRect(childClip);
				GFX->setStateBlock(mDefaultGuiSB);
				ctrl->onRender(childPosition, childClip);
			//}
		}
	}

	// Restore the clip rect to what it was at the start
	// of this method.
	GFX->setClipRect(savedClipRect);
}

void GuiFancyControl::onRender(Point2I offset, const RectI& updateRect)
{
	// Ignore Clipping Boundaries in-case the image becomes larger during the effects
	Point2I res = PlatformWindowManager::get()->getDesktopResolution();
	RectI newClip = RectI(0, 0, res.x, res.y);
	GFX->setClipRect(newClip);

	// Get the center point
	Point2F center;
	center.x = getExtent().x / 2.0f;
	center.y = getExtent().y / 2.0f;

	// Get the world matrix to apply effects
	GFX->pushWorldMatrix();
	MatrixF world = GFX->getWorldMatrix();
	MatrixF mat;

	// Apply Rotation
	AngAxisF rot(Point3F(0.0f, 0.0f, -1.0f), mDegToRad(mSpinTimer));
	rot.setMatrix(&mat);
	mat.setPosition(Point3F(offset.x + center.x, offset.y + center.y, 0.0f));
	world.mul(mat);

	// Apply Scale
	F32 scaleAmount = 0.005f;
	if (this->mEffects.contains(Push))
		scaleAmount *= -1;
	F32 scaleNum = ((mRadToDeg(mSin(mDegToRad(mScaleTimer)))) * scaleAmount) + 1;
	Point3F scale(scaleNum, scaleNum, scaleNum);
	world.scale(scale);

	// Apply the effects to the world matrix
	GFX->setWorldMatrix(world);

	// Render the GuiControl
	// -------------
	Point2I newOffset(0.0f - center.x, 0.0f - center.y);
	RectI ctrlRect(newOffset, getExtent());

	//if opaque, fill the update rect with the fill color
	if (mProfile->mOpaque)
		GFX->getDrawUtil()->drawRectFill(ctrlRect, mProfile->mFillColor);

	//if there's a border, draw the border
	if (mProfile->mBorder)
		renderBorder(ctrlRect, mProfile);

	// Render Children
	renderChildControls(offset, updateRect);
	// -------------

	//Parent::onRender(Point2I(0.0f - center.x, 0.0f - center.y), updateRect);

	// Restore the original world matrix
	GFX->popWorldMatrix();
}

DefineEngineMethod(GuiFancyControl, addEffect, void, (GuiFancyEffect effect, bool notify), (false), "addEffect(GuiFancyEffect, [notify])")
{
	object->addEffect(effect, notify);
}

DefineEngineMethod(GuiFancyControl, removeEffect, void, (GuiFancyEffect effect, bool notify), (false), "removeEffect(GuiFancyEffect, [notify])")
{
	object->removeEffect(effect, notify);
}

DefineEngineMethod(GuiFancyControl, clearEffects, void, (bool notify), (false), "clearEffects([notify])")
{
	object->clearEffects(notify);
}
