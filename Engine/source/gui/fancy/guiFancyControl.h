#ifndef __GUIFANCYCONTROL_H_
#define __GUIFANCYCONTROL_H_

#include "gui/core/guiControl.h"
#include "gui/shiny/guiTickCtrl.h"

class GuiFancyControl : public GuiControl, public virtual ITickable
{
	typedef GuiControl Parent;

public:
	enum EffectType
	{
		Pop,
		Push,
		Spin
	};

protected:
	Vector<EffectType> mEffects;

	F32 mScaleTimer;
	F32 mSpinTimer;

public:
	GuiFancyControl();
	~GuiFancyControl();

	virtual void interpolateTick(F32 delta) { }
	virtual void processTick() { }
	virtual void advanceTime(F32 timeDelta);

	void resetVariables(EffectType effect);
	void resetAllVariables();

	void updateEffect(F32 dt);
	void addEffect(EffectType effect, bool doCallback = true);
	void removeEffect(EffectType effect, bool doCallback = true);
	void clearEffects(bool doCallback = true);

	void renderChildControls(Point2I offset, const RectI &updateRect);
	void onRender(Point2I offset, const RectI& updateRect);
	static void initPersistFields();

	DECLARE_CONOBJECT(GuiFancyControl);

	DECLARE_CALLBACK(void, onEffectAdded, (EffectType effect));
	DECLARE_CALLBACK(void, onEffectRemoved, (EffectType effect));
	DECLARE_CALLBACK(void, onEffectsCleared, ());
	DECLARE_CALLBACK(void, onEffectDone, (EffectType effect));
};

typedef GuiFancyControl::EffectType GuiFancyEffect;
DefineEnumType(GuiFancyEffect);

#endif // __GUIFANCYCONTROL_H_