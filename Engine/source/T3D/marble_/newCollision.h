#ifndef __NEW_COLLISION_H_
#define __NEW_COLLISION_H_

#include "math/mMath.h"

namespace NewCollision
{
	extern F32 closestPtSegmentSegment(Point3F p1, Point3F q1, Point3F p2, Point3F q2, F32* s, F32* t, Point3F& c1, Point3F& c2);
	extern bool intersectSegmentCapsule(Point3F segStart, Point3F segEnd, Point3F capStart, Point3F capEnd, F32 radius, F32* tSeg, F32* tCap);
	extern bool intersectMovingSphereTriangle(Point3F startCenter, Point3F endCenter, F32 radius, Point3F p0, Point3F p1, Point3F p2, Point3F normal, F32& t, Point3F& closest);
	extern bool pointInTriangle(Point3F pnt, Point3F a, Point3F b, Point3F c);
	extern Point3F closestPtPointTriangle(Point3F p, Point3F a, Point3F b, Point3F c);
	extern bool closestPtPointTriangle(Point3F pt, float radius, Point3F p0, Point3F p1, Point3F p2, Point3F normal, Point3F& closest);
}

#endif // __NEW_COLLISION_H_