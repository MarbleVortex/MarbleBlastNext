#ifndef _MARBLE_H_
#define _MARBLE_H_

#ifndef _SHAPEBASE_H_
#include "T3D/shapeBase.h"
#endif

#ifndef _H_PARTICLE_EMITTER
#include "T3D\fx\particleEmitter.h"
#endif

#ifndef _RIGID_H_
#include "T3D/rigid.h"
#endif

#ifndef _SFXSYSTEM_H_
#include "sfx/sfxSystem.h"
#endif

#ifndef _SFXSOURCE_H_
#include "sfx/sfxSource.h"
#endif

#ifndef _MARBLECOLLISION_H_
#include "T3D/marble/marbleCollision.h"
#endif

class Marble;

class MarbleData : public ShapeBaseData
{
	typedef ShapeBaseData Parent;

	friend class Marble;

protected:
	F32 mMaxRollVelocity;
	F32 mAngularAcceleration;
	F32 mBrakingAcceleration;
	F32 mStaticFriction;
	F32 mKineticFriction;
	F32 mBounceKineticFriction;
	F32 mGravity;
	F32 mMaxDotSlide;
	F32 mBounceRestitution;
	F32 mAirAccel;
	F32 mEnergyRechargeRate;
	F32 mJumpImpulse;
	F32 mMaxForceRadius;
	F32 mCameraDistance;
	F32 mMinBounceVel;
	F32 mMinTrailSpeed;
	F32 mMinBounceSpeed;
	F32 mSize;

	ParticleEmitterData* mBounceEmitter;
	ParticleEmitterData* mTrailEmitter;
	ParticleEmitterData* mPowerUpEmitter;

	S32 mPowerUpTime;

	enum Sounds {
		RollHardSound,
		SlipSound,
		Bounce1,
		Bounce2,
		Bounce3,
		Bounce4,
		JumpSound,
		MaxSounds
	};

	SFXTrack* sound[MaxSounds];

public:

	MarbleData();
	~MarbleData();

	static void initPersistFields();
	bool preload(bool, String &errorStr);
	virtual void packData(BitStream* stream);
	virtual void unpackData(BitStream* stream);

	DECLARE_CONOBJECT(MarbleData);
};

class Marble : public ShapeBase
{
	typedef ShapeBase Parent;

private:

	MarbleData* mDataBlock;

	enum MaskBits {
		MoveMask = Parent::NextFreeMask,
		NextFreeMask = Parent::NextFreeMask << 1
	};

	enum MarbleMode {
		Start,
		Normal,
		Victory
	};

	bool mStatic;
	bool mDisablePhysics;
	bool mDisableRotation;
	bool mDisableCollision;
	bool mIgnoreMarbles;

	F32 mRollVolume;
	SFXSource* mRollHandle;

	Point3F mVelocity;
	Point3F mRotVelocity;

	F32 mRadius;
	bool mControllable;
	bool mBounceYet;
	F32 mSlipAmount;
	F32 mContactTime;
	F32 mTotalTime;
	Point3F mBouncePos;
	Point3F mBounceNormal;
	F32 mBounceSpeed;

	F32 mElasticity;
	F32 mFriction;
	F32 mVisibilityLevel;
	Point3F mGravity;

	F32 mCameraY;
	F32 mCameraX;

	Point3F mStartPos;

	const Move* mMove;

	F32 dtx1;
	F32 dtx2;
	Point3F posx0;
	Point3F posx1;
	Point3F posx2;

	Vector<MarbleCollision::CollisionInfo> mContacts;

	void updateAnimation(F32 dt);
	void updateIntegration(F32 dt, Point3F& pos, QuatF& rot, Point3F& vel, Point3F& omega);

	void updateBounceSound(Point3F pos);
	void updateRollSound(Point3F pos);

	bool computeMoveForces(Point3F omega, Point3F* aControl, Point3F* desiredOmega);
	void applyContactForces(float dt, bool isCentered, Point3F aControl, Point3F desiredOmega, Point3F& vel, Point3F& omega, Point3F& A, Point3F* a);
	Point3F getExternalForces(float dt);
	void velocityCancel(Point3F& vel, Point3F& omega, bool surfaceSlide, bool noBounce);
	void getMarbleAxis(Point3F& sideDir, Point3F& motionDir, Point3F& upDir);
	void reportBounce(Point3F pos, Point3F normal, float speed);
	F32 updateCollision(Point3F& pos1, Point3F pos0, QuatF& rot1, QuatF rot0, float dt);
	void findContacts(Point3F pos, QuatF rot);
	F32 findMarbleCollisionTime(Point3F pos0, Point3F pos1, F32 dt, F32 themRadius);
	void addMarbleContact(Point3F pos, F32 radius, Vector<MarbleCollision::CollisionInfo>& contacts);

	void resolveCollision(Point3F pos, Point3F norm);
	void resolveCollisions(Vector<MarbleCollision::CollisionInfo>& list);

public:

	static void initPersistFields();
	static void consoleInit();

	Marble();
	~Marble();

	bool onAdd();
	void onRemove();

	void setVelocity(Point3F velocity);
	Point3F getVelocity();
	AngAxisF getRotation();

	void setPosition(const Point3F& pos, const AngAxisF& rot, float rot1);
	void setPosition(const Point3F& pos);
	void Marble::setPosition(const Point3F& pos, const QuatF& rot);
	void Marble::setTransform(const MatrixF& newMat);
	const MatrixF getTransform();

	bool onNewDataBlock(GameBaseData* dptr, bool reload);
	void getCameraTransform(F32* pos, MatrixF* mat);
	void processTick(const Move *move);
	void advanceTime(F32 dt);
	void updateMove(float dt, Point3F& pos, QuatF& rot, Point3F& vel, Point3F& omega);

	void writePacketData(GameConnection * conn, BitStream *stream);
	void readPacketData(GameConnection * conn, BitStream *stream);
	U32  packUpdate(NetConnection * conn, U32 mask, BitStream *stream);
	void unpackUpdate(NetConnection * conn, BitStream *stream);



	void collisionCallback(SceneObject* so, void* key);
	void findContactCallback(SceneObject* so, void* key);

	DECLARE_CONOBJECT(Marble);
};

#endif // _MARBLE_H_
