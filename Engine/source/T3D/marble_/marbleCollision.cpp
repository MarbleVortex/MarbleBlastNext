#include "T3D/marble/marbleCollision.h"
#include "T3D/marble/newCollision.h"

#include "collision/concretePolyList.h"

namespace MarbleCollision
{
	void fixList(ConcretePolyList list, Vector<Triangle>& triList, Vector<Point3F>& pointList)//, Vector<Point3F>& normList)
	{
		Vector<Point3F> points = list.mVertexList;
		Vector<U32> indices = list.mIndexList;
		for (int i = 0; i < indices.size(); i++)
		{
			pointList.push_back(points[indices[i]]);
		}
		for (int i = 0; i < list.mPolyList.size(); i++)
		{
			ConcretePolyList::Poly poly = list.mPolyList[i];

			if (poly.vertexCount == 3)
			{
				Triangle tri = Triangle();
				tri.v1 = pointList[poly.vertexStart + 0];
				tri.v2 = pointList[poly.vertexStart + 1];
				tri.v3 = pointList[poly.vertexStart + 2];
				tri.normal = poly.plane;
				triList.push_back(tri);
			}
			else if (poly.vertexCount == 4)
			{
				Triangle tri1 = Triangle();
				tri1.v1 = pointList[poly.vertexStart + 0];
				tri1.v2 = pointList[poly.vertexStart + 1];
				tri1.v3 = pointList[poly.vertexStart + 2];
				tri1.normal = poly.plane;
				triList.push_back(tri1);

				Triangle tri2 = Triangle();
				tri2.v1 = pointList[poly.vertexStart + 2];
				tri2.v2 = pointList[poly.vertexStart + 0];
				tri2.v3 = pointList[poly.vertexStart + 3];
				tri2.normal = poly.plane;
				triList.push_back(tri2);
			}
			else if (poly.vertexCount == 5)
			{
				Triangle tri1 = Triangle();
				tri1.v1 = pointList[poly.vertexStart + 0];
				tri1.v2 = pointList[poly.vertexStart + 1];
				tri1.v3 = pointList[poly.vertexStart + 2];
				tri1.normal = poly.plane;
				triList.push_back(tri1);

				Triangle tri2 = Triangle();
				tri2.v1 = pointList[poly.vertexStart + 2];
				tri2.v2 = pointList[poly.vertexStart + 3];
				tri2.v3 = pointList[poly.vertexStart + 4];
				tri2.normal = poly.plane;
				triList.push_back(tri2);

				Triangle tri3 = Triangle();
				tri3.v1 = pointList[poly.vertexStart + 4];
				tri3.v2 = pointList[poly.vertexStart + 0];
				tri3.v3 = pointList[poly.vertexStart + 2];
				tri3.normal = poly.plane;
				triList.push_back(tri3);
			}
			else if (poly.vertexCount == 7)
			{
				Triangle tri1 = Triangle();
				tri1.v1 = pointList[poly.vertexStart + 0];
				tri1.v2 = pointList[poly.vertexStart + 1];
				tri1.v3 = pointList[poly.vertexStart + 2];
				tri1.normal = poly.plane;
				triList.push_back(tri1);

				Triangle tri2 = Triangle();
				tri2.v1 = pointList[poly.vertexStart + 2];
				tri2.v2 = pointList[poly.vertexStart + 3];
				tri2.v3 = pointList[poly.vertexStart + 4];
				tri2.normal = poly.plane;
				triList.push_back(tri2);

				Triangle tri3 = Triangle();
				tri3.v1 = pointList[poly.vertexStart + 4];
				tri3.v2 = pointList[poly.vertexStart + 5];
				tri3.v3 = pointList[poly.vertexStart + 6];
				tri3.normal = poly.plane;
				triList.push_back(tri3);

				Triangle tri4 = Triangle();
				tri4.v1 = pointList[poly.vertexStart + 6];
				tri4.v2 = pointList[poly.vertexStart + 0];
				tri4.v3 = pointList[poly.vertexStart + 2];
				tri4.normal = poly.plane;
				triList.push_back(tri4);

				Triangle tri5 = Triangle();
				tri5.v1 = pointList[poly.vertexStart + 6];
				tri5.v2 = pointList[poly.vertexStart + 2];
				tri5.v3 = pointList[poly.vertexStart + 4];
				tri5.normal = poly.plane;
				triList.push_back(tri5);
			}
			else {
				Con::warnf("Unsupported Polygon (%d vertices)", poly.vertexCount);
				//Con::warnf("Don't know how to deal with polygons with more than 4 vertices!");
			}
		}
	}

	F32 findIntersectionTime(SceneObject* obj, Point3F pos0, Point3F pos1, F32 radius)
	{
		Box3F searchBox = Box3F();
		float bigRad = radius * 1.05f;
		Point3F radial = Point3F(bigRad, bigRad, bigRad);
		searchBox.minExtents = (searchBox.maxExtents = pos0 - radial);
		searchBox.extend(pos0 + radial);
		searchBox.extend(pos1 - radial);
		searchBox.extend(pos1 + radial);
		Vector<Triangle> triList = Vector<Triangle>();
		Vector<Point3F> pointList = Vector<Point3F>();
		//Vector<Point3F> normList = Vector<Point3F>();
		ConcretePolyList list = ConcretePolyList();
		obj->buildPolyList(PLC_Collision, &list, searchBox, SphereF(searchBox.minExtents + radial, bigRad));
		fixList(list, triList, pointList);//, normList);
		// TEMP
		//this->findPolys(searchBox, triList, pointList, normList);
		float tmin = 2.0f;
		for (Triangle tri : triList)
		{
			//Point3F v0 = pointList[tri.idx0];
			//Point3F v1 = pointList[tri.idx1];
			//Point3F v2 = pointList[tri.idx2];
			//Point3F norm = normList[tri.normalIdx];
			Point3F v0 = tri.v1;
			Point3F v1 = tri.v2;
			Point3F v2 = tri.v3;
			Point3F norm = tri.normal;
			float t = 0.0f;
			Point3F closest = Point3F(0, 0, 0);
			if (NewCollision::intersectMovingSphereTriangle(pos0, pos1, radius, v0, v1, v2, norm, t, closest) && t < tmin)
			{
 				norm = pos0 + t * (pos1 - pos0) - closest;
				if (norm.lenSquared() > 1E-05f)
				{
					norm.normalize();
					float dir = mDot(norm, pos1 - pos0);
					if (dir < -0.0001f)
					{
						tmin = t;
					}
				}
			}
		}
		return tmin;
	}

	void addPlatformContact(SceneObject* obj, Point3F pos, float radius, Vector<CollisionInfo>& contacts)
	{
		Box3F searchBox = Box3F();
		float bigRad = radius * 1.1f;
		Point3F radial = Point3F(bigRad, bigRad, bigRad);
		searchBox.minExtents = (searchBox.maxExtents = pos - radial);
		searchBox.extend(pos + radial);
		Vector<Triangle> triList = Vector<Triangle>();
		Vector<Point3F> pointList = Vector<Point3F>();
		//Vector<Point3F> normList = Vector<Point3F>();
		ConcretePolyList list = ConcretePolyList();
		obj->buildPolyList(PLC_Collision, &list, searchBox, SphereF(searchBox.minExtents + radial, bigRad));
		fixList(list, triList, pointList);//, normList);
		// TEMP
		//this->findPolys(searchBox, triList, pointList, normList);
		for (Triangle tri : triList)
		{
			//Point3F v0 = pointList[tri.idx0];
			//Point3F v1 = pointList[tri.idx1];
			//Point3F v2 = pointList[tri.idx2];
			Point3F v0 = tri.v1;
			Point3F v1 = tri.v2;
			Point3F v2 = tri.v3;
			Point3F closest = Point3F(0, 0, 0);
			//if (NewCollision::closestPtPointTriangle(pos, radius, v0, v1, v2, normList[tri.normalIdx], closest) && (pos - closest).lenSquared() < radius * radius)
			if (NewCollision::closestPtPointTriangle(pos, radius, v0, v1, v2, tri.normal, closest) && (pos - closest).lenSquared() < radius * radius)
			{
				Point3F normal = (pos - closest);
				//if (mDot(pos - closest, normList[tri.normalIdx]) > 0.0f)
				if (mDot(pos - closest, tri.normal) > 0.0f)
				{
					normal.normalize();
					CollisionInfo info = CollisionInfo();
					info.normal = normal;
					info.point = closest;
					info.penetration = radius - mDot(pos - closest, info.normal);
					info.restitution = 1.0f;
					info.friction = 1.0f;
					info.hasCollider = false;
					info.obj = obj;
					info.velocity = Point3F(0, 0, 0);
					contacts.push_back(info);
				}
			}
			//else if (NewCollision::closestPtPointTriangle(pos, radius, v0, v1, v2, normList[tri.normalIdx], closest))
			else if (NewCollision::closestPtPointTriangle(pos, radius, v0, v1, v2, tri.normal, closest))
			{
				AssertFatal((pos - closest).len() >= radius, "should have found contact");
			}
		}
	}

	/*int addPoint(Vector<Point3F>& list, Point3F pnt)
	{
		int start = getMax(0, list.size() - 500);
		for (int i = start; i < list.size(); i++)
		{
			Point3F pnti = list[i];
			if (mFabs(pnti.x - pnt.x) < MarblePointEpsilon && mFabs(pnti.y - pnt.y) < MarblePointEpsilon && mFabs(pnti.z - pnt.z) < MarblePointEpsilon)
			{
				return i;
			}
		}
		list.push_back(pnt);
		return list.size() - 1;
	}

	int addNormal(Vector<Point3F>& list, Point3F normal)
	{
		int start = getMax(0, list.size() - 500);
		for (int i = start; i < list.size(); i++)
		{
			if (mDot(list[i], normal) > 1.0f - MarbleDotEpsilon)
			{
				return i;
			}
		}
		list.push_back(normal);
		return list.size() - 1;
	}*/

	/*void SceneObject::findPolys(Box3F box, Vector<Triangle>& triList, Vector<Point3F>& pointList, Vector<Point3F>& normList)
	{
		if (!this->prevQuery.isContained(box.maxExtents) || !this->prevQuery.isContained(box.minExtents))
		{
			this->searchId++;
			this->foundPolys.clear();
			Point3F extent = 0.55f * (box.maxExtents - box.minExtents);
			Point3F center = 0.5f * (box.maxExtents + box.minExtents);
			box.minExtents = (box.maxExtents = center);
			box.minExtents -= extent;
			box.maxExtents += extent;
			this->prevQuery = box;
			int count = 0;
			int found = 0;
			this->leaves.clear();
			this->tree.QueryBox(box.minExtents, box.maxExtents, true, this->leaves);
			//using (List<int>.Enumerator enumerator = StaticGeometryComponent._leaves.GetEnumerator())
			//{
			for (int i = 0; i < this->leaves.size(); i++)
			{
				//while (enumerator.MoveNext())
				//{
				int leaf = this->leaves[i];
				for (TriangleNode* cur = this->tree.GetNodeData(leaf); cur != NULL; cur++)
				{
					if (cur->tri->searchId != this->searchId)
					{
						cur->tri->searchId = this->searchId;
						count++;
						Box3F triBox = Box3F(Point3F(0, 0, 0), Point3F(0, 0, 0));
						triBox.minExtents = (triBox.maxExtents = this->points[cur->tri->idx0]);
						triBox.extend(this->points[cur->tri->idx1]);
						triBox.extend(this->points[cur->tri->idx2]);
						if (triBox.isOverlapped(box))
						{
							this->foundPolys.push_back(*cur->tri);
							found++;
						}
					}
				}
				//}

			}
			goto IL_208;
			//}
		}
		box = this->prevQuery;
	IL_208:
	#ifdef _DEBUG
		Vector<Triangle> _foundPolys2 = Vector<Triangle>();
		for (Triangle tri : this->triangles)
		{
			Box3F triBox2 = Box3F();
			triBox2.minExtents = (triBox2.maxExtents = this->points[tri.idx0]);
			triBox2.extend(this->points[tri.idx1]);
			triBox2.extend(this->points[tri.idx2]);
			if (triBox2.isOverlapped(box))
			{
				_foundPolys2.push_back(tri);
			}
		}
		AssertFatal(this->foundPolys.size() == _foundPolys2.size(), "Wrong number of tris found");
		for (int i = 0; i < this->foundPolys.size(); i++)
		{
			bool found2 = false;
			for (int j = 0; j < _foundPolys2.size(); j++)
			{
				if (this->foundPolys[i].idx0 == _foundPolys2[j].idx0 && this->foundPolys[i].idx1 == _foundPolys2[j].idx1 && this->foundPolys[i].idx2 == _foundPolys2[j].idx2 && this->foundPolys[i].normalIdx == _foundPolys2[j].normalIdx)
				{
					found2 = true;
					break;
				}
			}
			AssertFatal(found2, "poly missing");
		}
	#endif
		triList = this->foundPolys;
		pointList = this->points;
		normList = this->normals;
	}*/
}