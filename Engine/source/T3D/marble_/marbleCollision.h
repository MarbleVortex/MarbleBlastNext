#ifndef _MARBLECOLLISION_H_
#define _MARBLECOLLISION_H_

#ifndef _SCENEOBJECT_H_
#include "scene/sceneObject.h"
#endif

#ifndef _ABSTRACTPOLYLIST_H_
#include "collision/abstractPolyList.h"
#endif

namespace MarbleCollision
{

	struct CollisionInfo
	{
		Point3F point;
		Point3F normal;
		Point3F velocity;
		bool hasCollider;
		SceneObject* obj;
		F32 friction;
		F32 restitution;
		F32 penetration;
	};

	struct Triangle
	{
		Point3F v1;
		Point3F v2;
		Point3F v3;
		Point3F normal;
		//S32 idx0;
		//S32 idx1;
		//S32 idx2;
		//S32 normalIdx;
		//S32 searchId;
	};

	static F32 MarblePointEpsilon = 1E-05f;
	static F32 MarbleDotEpsilon = 1E-05f;

	extern F32 findIntersectionTime(SceneObject* obj, Point3F pos0, Point3F pos1, F32 radius);
	extern void addPlatformContact(SceneObject* obj, Point3F pos, float radius, Vector<CollisionInfo>& contacts);
	//extern int addPoint(Vector<Point3F>& list, Point3F pnt);
	//extern int addNormal(Vector<Point3F>& list, Point3F normal);
	//void findPolys(Box3F box, Vector<MarbleTriangle>& triList, Vector<Point3F>& pointList, Vector<Point3F>& normList);
}

#endif // _MARBLECOLLISION_H_