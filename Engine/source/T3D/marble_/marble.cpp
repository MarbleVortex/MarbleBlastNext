#include "T3D/marble/marble.h"

#include "T3D/fx/cameraFXMgr.h"
#include "core/iTickable.h"
#include "core/stream/bitStream.h"
#include "console/consoleTypes.h"
#include "T3D/marble/newCollision.h"
#include "math/mathIO.h"
#include "sfx/sfxTrack.h"

using namespace MarbleCollision;

static U32 sClientCollisionMask =
TerrainObjectType | InteriorObjectType |
PlayerObjectType | StaticShapeObjectType |
VehicleObjectType | VehicleBlockerObjectType;// |
//StaticTSObjectType | AtlasObjectType;

IMPLEMENT_CO_DATABLOCK_V1(MarbleData);

MarbleData::MarbleData()
{
	for (S32 i = 0; i < MaxSounds; i++)
		sound[i] = 0;

	this->mMaxRollVelocity = 15.0f;
	this->mAngularAcceleration = 75.0f;
	this->mBrakingAcceleration = 30.0f;
	this->mStaticFriction = 1.1f;
	this->mKineticFriction = 0.7f;
	this->mBounceKineticFriction = 0.2f;
	this->mGravity = 20.0f;
	this->mMaxDotSlide = 0.5f;
	this->mBounceRestitution = 0.5f;
	this->mAirAccel = 5.0f;
	this->mEnergyRechargeRate = 0.0f;
	this->mJumpImpulse = 7.5f;
	this->mMaxForceRadius = 0.0f;
	this->mCameraDistance = 2.3f;
	this->mMinBounceVel = 0.1f;
	this->mMinTrailSpeed = 0.0f;
	this->mMinBounceSpeed = 0.0f;
	this->mSize = 1.0f;

	this->mBounceEmitter = NULL;
	this->mTrailEmitter = NULL;
	this->mPowerUpEmitter = NULL;

	this->mPowerUpTime = 0;
	
}

MarbleData::~MarbleData()
{

}

bool MarbleData::preload(bool server, String &errorStr)
{
	if (!Parent::preload(server, errorStr))
		return false;

	if (!(shapeName && shapeName[0]))
	{
		errorStr = String::ToString("MarbleData: No Shape Specified");
		return false;
	}

	return true;
}

void MarbleData::initPersistFields()
{
	// Physics
	ConsoleObject::addField("maxRollVelocity", TypeF32, Offset(mMaxRollVelocity, MarbleData));
	ConsoleObject::addField("angularAcceleration", TypeF32, Offset(mAngularAcceleration, MarbleData));
	ConsoleObject::addField("brakingAcceleration", TypeF32, Offset(mBrakingAcceleration, MarbleData));
	ConsoleObject::addField("staticFriction", TypeF32, Offset(mStaticFriction, MarbleData));
	ConsoleObject::addField("kineticFriction", TypeF32, Offset(mKineticFriction, MarbleData));
	ConsoleObject::addField("bounceKineticFriction", TypeF32, Offset(mBounceKineticFriction, MarbleData));
	ConsoleObject::addField("gravity", TypeF32, Offset(mGravity, MarbleData));
	ConsoleObject::addField("maxDotSlide", TypeF32, Offset(mMaxDotSlide, MarbleData));
	ConsoleObject::addField("bounceRestitution", TypeF32, Offset(mBounceRestitution, MarbleData));
	ConsoleObject::addField("airAcceleration", TypeF32, Offset(mAirAccel, MarbleData));
	ConsoleObject::addField("energyRechargeRate", TypeF32, Offset(mEnergyRechargeRate, MarbleData));
	ConsoleObject::addField("jumpImpulse", TypeF32, Offset(mJumpImpulse, MarbleData));
	ConsoleObject::addField("maxForceRadius", TypeF32, Offset(mMaxForceRadius, MarbleData));
	ConsoleObject::addField("cameraDistance", TypeF32, Offset(mCameraDistance, MarbleData));
	ConsoleObject::addField("minBounceVel", TypeF32, Offset(mMinBounceVel, MarbleData));
	ConsoleObject::addField("minTrailSpeed", TypeF32, Offset(mMinTrailSpeed, MarbleData));
	ConsoleObject::addField("minBounceSpeed", TypeF32, Offset(mMinBounceSpeed, MarbleData));
	ConsoleObject::addField("size", TypeF32, Offset(mSize, MarbleData));

	// Particles
	ConsoleObject::addField("bounceEmitter", TYPEID< ParticleEmitterData >(), Offset(mBounceEmitter, MarbleData));
	ConsoleObject::addField("trailEmitter", TYPEID< ParticleEmitterData >(), Offset(mTrailEmitter, MarbleData));
	ConsoleObject::addField("powerUpEmitter", TYPEID< ParticleEmitterData >(), Offset(mPowerUpEmitter, MarbleData));

	ConsoleObject::addField("powerUpTime", TypeS32, Offset(mPowerUpTime, MarbleData));

	// Sounds
	ConsoleObject::addField("RollHardSound", TYPEID< SFXTrack >(), Offset(sound[RollHardSound], MarbleData));
	ConsoleObject::addField("SlipSound", TYPEID< SFXTrack >(), Offset(sound[SlipSound], MarbleData));
	ConsoleObject::addField("Bounce1", TYPEID< SFXTrack >(), Offset(sound[Bounce1], MarbleData));
	ConsoleObject::addField("Bounce2", TYPEID< SFXTrack >(), Offset(sound[Bounce2], MarbleData));
	ConsoleObject::addField("Bounce3", TYPEID< SFXTrack >(), Offset(sound[Bounce3], MarbleData));
	ConsoleObject::addField("Bounce4", TYPEID< SFXTrack >(), Offset(sound[Bounce4], MarbleData));
	ConsoleObject::addField("JumpSound", TYPEID< SFXTrack >(), Offset(sound[JumpSound], MarbleData));

	Parent::initPersistFields();
}

void MarbleData::packData(BitStream* stream)
{
	Parent::packData(stream);
}

void MarbleData::unpackData(BitStream* stream)
{
	Parent::unpackData(stream);
}

static Marble* sMarble = NULL;

IMPLEMENT_CO_NETOBJECT_V1(Marble);

Marble::Marble()
{
	mNetFlags.set(Ghostable);

	this->mDataBlock = NULL;
	this->mCameraY = 0;
	this->mCameraX = 0;
	this->mVelocity = Point3F(0, 0, 0);
	this->mAppliedForce = Point3F(0, 0, 0);
	this->mRotVelocity = Point3F(0, 0, 0);
	this->mBouncePos = Point3F(0, 0, 0);
	this->mBounceNormal = Point3F(0, 0, 0);
	this->mBounceYet = false;
	this->mBounceSpeed = 0;
	this->mSlipAmount = 0;
	this->mContactTime = 0;
	this->mTotalTime = 0;
	this->mStartPos = Point3F(0, 0, 0);
	this->mMove = NULL;
	this->mElasticity = 0.0f;
	this->mFriction = 0.8f;
	this->mVisibilityLevel = 1.0f;
	this->mGravity = Point3F(0, 0, 0);
	this->mRollVolume = 0.0f;
	this->mRollHandle = NULL;
	this->mStatic = false;
	this->mDisablePhysics = false;
	this->mDisableRotation = false;
	this->mDisableCollision = false;
	this->mIgnoreMarbles = false;
	this->mContacts = Vector<CollisionInfo>();
}

Marble::~Marble()
{
	if (this->mRollHandle != NULL)
		this->mRollHandle->stop();
}

void Marble::initPersistFields()
{
	ConsoleObject::addField("Controllable", TypeBool, Offset(mControllable, Marble));
	ConsoleObject::addField("static", TypeBool, Offset(mStatic, Marble));
	ConsoleObject::addField("disablePhysics", TypeBool, Offset(mDisablePhysics, Marble));
	ConsoleObject::addField("disableRotation", TypeBool, Offset(mDisableRotation, Marble));
	ConsoleObject::addField("disableCollision", TypeBool, Offset(mDisableCollision, Marble));
	ConsoleObject::addField("ignoreMarbles", TypeBool, Offset(mIgnoreMarbles, Marble));
	Parent::initPersistFields();
}

void Marble::consoleInit()
{
	Parent::consoleInit();
}

bool Marble::onAdd()
{
	if (!Parent::onAdd() || !mDataBlock)
		return false;

	this->mRadius = 0.3f;

	addToScene();

	if (isServerObject())
		scriptOnAdd();

	return true;
}

void Marble::onRemove()
{
	scriptOnRemove();

	removeFromScene();

	Parent::onRemove();
}

bool Marble::onNewDataBlock(GameBaseData* dptr, bool reload)
{
	mDataBlock = dynamic_cast<MarbleData*>(dptr);
	if (!mDataBlock || !Parent::onNewDataBlock(dptr, reload))
		return false;

	F32 scale = mDataBlock->mSize;

	this->mObjScale = Point3F(scale, scale, scale);

	scriptOnNewDataBlock();

	return true;
}

void Marble::getCameraTransform(F32* pos, MatrixF* mat)
{
	// Jeff: get variables from script
	F32 pitch = this->mCameraY;
	F32 yaw = this->mCameraX;

	// Jeff: distance
	F32 distance = this->mDataBlock->mCameraDistance; //2.3f;//mDataBlock->cameraMaxDist;
	F32 horizontalDist = mCos(pitch) * distance;
	F32 verticalDist = mSin(pitch) * distance;

	// Jeff: make the camera "orbit" around the marble
	Point3F ortho(-mSin(yaw) * horizontalDist,
		-mCos(yaw) * horizontalDist,
		verticalDist);

	// Matt: Get top offset of marble
	Point3F sideDir = Point3F(0, 0, 0);
	Point3F motionDir = Point3F(0, 0, 0);
	Point3F upDir = Point3F(0, 0, 0);
	this->getMarbleAxis(sideDir, motionDir, upDir);

	Point3F offset = upDir * this->mRadius;

	// Jeff: add the ortho position to the object's current position
	Point3F position = getPosition() + offset + ortho;

	disableCollision();

	//HiGuy: Do a raycast so we don't have the camera clipping the everything
	RayInfo info;

	if (mContainer->castRay(getPosition(), position, ~(WaterObjectType | GameBaseObjectType | DefaultObjectType), &info)) {
		// S22: -measure difference of collision point and marble location
		// -normalize (make it into a unit vector)
		// -subtract .001 * unit vector from original position of rayhit
		Point3F dist = Point3F(info.point - getPosition());
		dist.normalize();
		dist *= 0.01f;

		// combine vectors
		position = info.point - dist;
	}

	// Jeff: calculate rotation of the camera by doing matrix multiplies
	AngAxisF rot1(Point3F(0.0f, 0.0f, 1.0f), yaw);
	AngAxisF rot2(Point3F(1.0f, 0.0f, 0.0f), pitch);
	MatrixF mat1, mat2;

	rot1.setMatrix(&mat1);
	rot2.setMatrix(&mat2);

	// Jeff: set position and apply rotation
	mat1.mul(mat2);
	mat1.setPosition(position);

	enableCollision();

	*mat = mat1;

	// Apply Camera FX.
	mat->mul( gCamFXMgr.getTrans() );
}

void Marble::advanceTime(F32 dt)
{
	Parent::advanceTime(dt);
}

void Marble::updateAnimation(F32 dt)
{
	if (this->mStatic)
	{
		return;
	}
	Point3F pos0 = this->getTransform().getPosition(); // Translation
	Point3F pos = pos0;
	QuatF rot0 = QuatF(this->getTransform()); // Quat from Rot Matrix
	QuatF rot = rot0;

	this->mBounceYet = false;
	this->mSlipAmount = 0.0f;
	this->mContactTime = 0.0f;
	this->mTotalTime = 0.0f;
	this->mStartPos = pos0;

	if (!mDisableCollision)
	{
		int retryCount = 0;
		Point3F vel0 = this->mVelocity;
		Point3F avel0 = this->mRotVelocity;
		while (dt > 0.0f)
		{
			if (retryCount++ >= 5)
			{
				break;
			}
			Point3F vel = vel0;
			Point3F avel = avel0;
			this->updateMove(dt, pos, rot, vel, avel);
			this->updateIntegration(dt, pos, rot, vel, avel);
			this->mContacts.clear();
			float remainingTime = this->updateCollision(pos, pos0, rot, rot0, dt);
			if (remainingTime > 0.0f)
			{
				float i = remainingTime / dt;
				vel0 = vel - (vel - vel0) * i;
				avel0 = avel - (avel - avel0) * i;
			}
			else
			{
				vel0 = vel;
				avel0 = avel;
			}
			this->mVelocity = vel0;
			this->mRotVelocity = avel0;
			if (this->mContacts.size() != 0)
			{
				this->resolveCollisions(this->mContacts);
			}
			pos0 = pos;
			rot0 = rot;
			dt = remainingTime;
		}
	}
	else
	{
		this->updateMove(dt, pos, rot, this->mVelocity, this->mRotVelocity);
		this->updateIntegration(dt, pos, rot, this->mVelocity, this->mRotVelocity);
	}

	MatrixF mat(1);
	rot.setMatrix(&mat);
	mat.setPosition(pos);
	
	// Apply the movement
	this->setTransform(mat);

	this->updateBounceSound(pos);
	this->updateRollSound(pos);
	//this->updateItems(this->startPos, pos);

	if (isServerObject())
		this->setMaskBits(MoveMask);
}

void Marble::updateBounceSound(Point3F pos)
{
	if (this->mBounceYet)
	{
		float minSize = 1.0f;
		float maxSize = 2.0f;
		float curSize = 1.0f;
		bool isMega = (curSize - minSize) / (maxSize - minSize) > 0.5f;
		float softBounceSpeed = isMega ? 2.5f : 2.5f;
		if (this->mBounceSpeed >= softBounceSpeed)
		{
			float hardBounceSpeed = isMega ? 12.0f : 8.0f;
			int index = mRandI(0, 3);//TorqueUtil.GetRandomInt(4);
			AssertFatal(index >= 0 && index < 4, "Bounce sound index out of range");
			index += this->mDataBlock->Bounce1;//(isMega ? 8 : 4);
			float minGain = isMega ? 0.2f : 0.5f;
			this->mBounceSpeed *= 2.0f;
			float gain;
			if (this->mBounceSpeed < hardBounceSpeed)
			{
				gain = (1.0f - minGain) * (this->mBounceSpeed - softBounceSpeed) / (hardBounceSpeed - softBounceSpeed) + minGain;
			}
			else
			{
				gain = 1.0f;
			}
			if (isClientObject() && this->mDataBlock->sound[index] != NULL)
			{
				
				/*Cue sound = MarbleControlComponent._soundBank.GetCue(MarbleControlComponent._sounds[index]);
				if (sound != null)
				{
					sound.SetVariable("MarbleGain", gain);
					sound.Play();
				}*/
				MatrixF transform = getTransform();
				Point3F velocity = getVelocity();
				SFXSource* src = SFX->createSource(this->mDataBlock->sound[index], &transform, &velocity);
				if (src == NULL)
					return;
				src->setVolume(gain);
				src->play();
			}
		}
	}
}

void Marble::updateRollSound(Point3F pos)
{
	float contactPct = 0.0f;
	if (this->mTotalTime > 0.0001f)
	{
		contactPct = this->mContactTime / this->mTotalTime;
	}
	if (this->mRollHandle == NULL)
	{
		//this->mRollHandle = MarbleControlComponent._soundBank.GetCue(MarbleControlComponent._sounds[0]);
		//this->mRollHandle.Play();
		//this->mRollHandle = SFX->playOnce()
		if (isClientObject())
		{
			this->mRollHandle = SFX->createSource(this->mDataBlock->sound[this->mDataBlock->RollHardSound]);//, &getTransform(), &getVelocity());
			if (this->mRollHandle != NULL)
				this->mRollHandle->play();
		}
	}
	if (this->mRollHandle == NULL)
	{
		return;
	}
	float megaAmt = 0.0f;
	float regAmt = 1.0f - megaAmt;
	float velLen = this->mVelocity.len();
	velLen *= 2.0f;
	float scale = velLen / 15.0f;
	float rollVolume = scale * 2.0f;
	if (rollVolume > 1.0f)
	{
		rollVolume = 1.0f;
	}
	if (contactPct < 0.001f)
	{
		rollVolume = 0.0f;
	}
	float rollSmooth = 0.25f;
	float useRoll = getMax(this->mRollVolume, rollVolume);
	this->mRollVolume = useRoll;
	this->mRollVolume *= rollSmooth;
	this->mRollVolume += rollVolume * (1.0f - rollSmooth);
	rollVolume = useRoll;
	//this->mRollHandle.SetVariable("MarbleGain", regAmt * rollVolume);
	if (isClientObject())
	{
		float sndVol = regAmt * rollVolume;
		//this->mRollHandle->setVolume(sndVol); // Original Does This but the sound won't stop for some reason
		this->mRollHandle->setVolume(sndVol < 0.25f ? 0 : sndVol);
		this->mRollHandle->setTransform(this->getTransform());
		this->mRollHandle->setVelocity(this->getVelocity());
	}
}

void Marble::updateIntegration(F32 dt, Point3F& pos, QuatF& rot, Point3F& vel, Point3F& omega)
{
	if (!mDisablePhysics)
		pos += vel * dt;
	if (mDisableRotation)
		return;
	Point3F axis = omega;
	float angle = axis.len();
	angle = mDegToRad(angle);
	if (angle > 1E-05f)
	{
		axis *= 1.0f / angle;
		AngAxisF ang = AngAxisF(axis, dt * -angle);
		QuatF deltaRot = QuatF(ang);
		float len = deltaRot.len();
		if (len > 0.001f)
		{
			deltaRot *= 1.0f / len;
		}
		QuatF newRot = QuatF(0, 0, 0, 0);
		newRot.mul(rot, deltaRot);
		rot = newRot;
		len = rot.len();
		if (len > 0.001f)
		{
			rot *= 1.0f / len;
		}
	}
}

void Marble::processTick(const Move *move)
{
	Parent::processTick(move);
	this->mMove = move;
	if (move != NULL)
	{
		this->mCameraY += move->pitch;
		this->mCameraX += move->yaw;
		if (mRadToDeg(this->mCameraY) > 90)
			this->mCameraY = mDegToRad((float)90);
		if (mRadToDeg(this->mCameraY) < -90)
			this->mCameraY = mDegToRad((float)-90);
	}

	if (!mStatic)
	{
		this->updateAnimation(TickSec);
	}

	//if (Con::isMethod(this->mDataBlock, "processTick"))
	//	Con::executef(this->mDataBlock, "processTick", this->getIdString(), Con::getIntArg(isClientObject()));
}

void Marble::updateMove(float dt, Point3F& pos, QuatF& rot, Point3F& vel, Point3F& omega)
{
	this->findContacts(pos, rot);
	Point3F aControl = Point3F(0, 0, 0);
	Point3F desiredOmega = Point3F(0, 0, 0);
	bool isCentered = this->computeMoveForces(omega, &aControl, &desiredOmega);
	this->velocityCancel(vel, omega, isCentered, false);

	Point3F A = this->getExternalForces(dt);
	Point3F a = Point3F(0, 0, 0);
	this->applyContactForces(dt, isCentered, aControl, desiredOmega, vel, omega, A, &a);
	vel += A * dt;
	omega += a * dt;

	this->velocityCancel(vel, omega, isCentered, true);

	this->mTotalTime += dt;
	if (mContacts.size() != 0)
	{
		this->mContactTime += dt;
	}
	mContacts.clear();
}

bool Marble::computeMoveForces(Point3F omega, Point3F* aControl, Point3F* desiredOmega)
{
	Point3F gWorkGravityDir = Point3F(0.0f, 0.0f, -1.0f);
	Point3F R = -gWorkGravityDir * this->mRadius;
	Point3F rollVelocity = mCross(omega, R);
	Point3F sideDir = Point3F(0, 0, 0);
	Point3F motionDir = Point3F(0, 0, 0);
	Point3F upDir = Point3F(0, 0, 0);
	this->getMarbleAxis(sideDir, motionDir, upDir);
	float currentYVelocity = mDot(rollVelocity, motionDir);
	float currentXVelocity = mDot(rollVelocity, sideDir);
	Point2F mv = (this->mMove != NULL) ? Point2F(mMove->x, mMove->y) : Point2F(0, 0);
	mv *= 1.53846157f;
	float mvlen = mv.len();
	if (mvlen > 1.0f)
	{
		mv *= 1.0f / mvlen;
	}
	float desiredYVelocity = this->mDataBlock->mMaxRollVelocity * mv.y;
	float desiredXVelocity = this->mDataBlock->mMaxRollVelocity * mv.x;
	if (desiredYVelocity != 0.0f || desiredXVelocity != 0.0f)
	{
		if (currentYVelocity > desiredYVelocity && desiredYVelocity > 0.0f)
		{
			desiredYVelocity = currentYVelocity;
		}
		else if (currentYVelocity < desiredYVelocity && desiredYVelocity < 0.0f)
		{
			desiredYVelocity = currentYVelocity;
		}
		if (currentXVelocity > desiredXVelocity && desiredXVelocity > 0.0f)
		{
			desiredXVelocity = currentXVelocity;
		}
		else if (currentXVelocity < desiredXVelocity && desiredXVelocity < 0.0f)
		{
			desiredXVelocity = currentXVelocity;
		}
		*desiredOmega = mCross(R, desiredYVelocity * motionDir + desiredXVelocity * sideDir) / R.lenSquared();
		*aControl = (*desiredOmega - omega);
		float aScalar = aControl->len();
		if (aScalar > this->mDataBlock->mAngularAcceleration)
		{
			*aControl *= this->mDataBlock->mAngularAcceleration / aScalar;
		}
		return false;
	}
	return true;
}

void Marble::applyContactForces(float dt, bool isCentered, Point3F aControl, Point3F desiredOmega, Point3F& vel, Point3F& omega, Point3F& A, Point3F* a)
{
	this->mSlipAmount = 0.0f;
	Point3F gWorkGravityDir = Point3F(0.0f, 0.0f, -1.0f);
	int bestSurface = -1;
	float bestNormalForce = 0.0f;
	for (int i = 0; i < mContacts.size(); i++)
	{
		if (!mContacts[i].hasCollider)
		{
			float normalForce = -mDot(mContacts[i].normal, A);
			if (normalForce > bestNormalForce)
			{
				bestNormalForce = normalForce;
				bestSurface = i;
			}
		}
	}
	CollisionInfo bestContact = (bestSurface != -1) ? mContacts[bestSurface] : CollisionInfo();
	bool canJump = bestSurface != -1;
	if (canJump && this->mMove != NULL && this->mMove->trigger[2])
	{
		Point3F velDifference = vel - bestContact.velocity;
		float sv = mDot(bestContact.normal, velDifference);
		if (sv < 0.0f)
		{
			sv = 0.0f;
		}
		if (sv < this->mDataBlock->mJumpImpulse)
		{
			vel += bestContact.normal * (this->mDataBlock->mJumpImpulse - sv);
			if (isClientObject() && this->mDataBlock->sound[this->mDataBlock->JumpSound] != NULL)
			{
				MatrixF transform = getTransform();
				Point3F velocity = getVelocity();
				SFX->playOnce(this->mDataBlock->sound[this->mDataBlock->JumpSound], &transform, &velocity);
			}
		}
	}
	for (int j = 0; j < mContacts.size(); j++)
	{
		float normalForce2 = -mDot(mContacts[j].normal, A);
		Point3F thing = vel - mContacts[j].velocity;
		Point3F norm = mContacts[j].normal;
		float temp = mDot(norm, thing);
		if (normalForce2 > 0.0f && temp <= 0.0001f)
		{
			A += mContacts[j].normal * normalForce2;
		}
	}
	if (bestSurface != -1)
	{
		// TODO: FIX
		//bestContact.velocity - bestContact.normal * Vector3.Dot(bestContact.normal, bestContact.velocity);
		Point3F vAtC = vel + mCross(omega, -bestContact.normal * this->mRadius) - bestContact.velocity;
		float vAtCMag = vAtC.len();
		bool slipping = false;
		Point3F aFriction = Point3F(0.0f, 0.0f, 0.0f);
		Point3F AFriction = Point3F(0.0f, 0.0f, 0.0f);
		if (vAtCMag != 0.0f)
		{
			slipping = true;
			float friction = this->mDataBlock->mKineticFriction * bestContact.friction;
			float angAMagnitude = 5.0f * friction * bestNormalForce / (2.0f * this->mRadius);
			float AMagnitude = bestNormalForce * friction;
			float totalDeltaV = (angAMagnitude * this->mRadius + AMagnitude) * dt;
			if (totalDeltaV > vAtCMag)
			{
				float fraction = vAtCMag / totalDeltaV;
				angAMagnitude *= fraction;
				AMagnitude *= fraction;
				slipping = false;
			}
			Point3F vAtCDir = vAtC / vAtCMag;
			aFriction = angAMagnitude * mCross(-bestContact.normal, -vAtCDir);
			AFriction = -AMagnitude * vAtCDir;
			this->mSlipAmount = vAtCMag - totalDeltaV;
		}
		if (!slipping)
		{
			Point3F R = -gWorkGravityDir * this->mRadius;
			Point3F aadd = mCross(R, A) / R.lenSquared();
			if (isCentered)
			{
				Point3F nextOmega = omega + (*a) * dt;
				aControl = desiredOmega - nextOmega;
				float aScalar = aControl.len();
				if (aScalar > this->mDataBlock->mBrakingAcceleration)
				{
					aControl *= this->mDataBlock->mBrakingAcceleration / aScalar;
				}
			}
			Point3F Aadd = -mCross(aControl, -bestContact.normal * this->mRadius);
			float aAtCMag = (mCross(aadd, -bestContact.normal * this->mRadius) + Aadd).len();
			float friction2 = this->mDataBlock->mStaticFriction * bestContact.friction;
			if (aAtCMag > friction2 * bestNormalForce)
			{
				friction2 = this->mDataBlock->mKineticFriction * bestContact.friction;
				Aadd *= friction2 * bestNormalForce / aAtCMag;
			}
			A += Aadd;
			*a += aadd;
		}
		A += AFriction;
		*a += aFriction;
	}
	*a += aControl;
}

Point3F Marble::getExternalForces(float dt)
{
	Point3F gWorkGravityDir = Point3F(0.0f, 0.0f, -1.0f);
	Point3F A = gWorkGravityDir * this->mDataBlock->mGravity;
	if (mContacts.size() == 0 && this->mMove != NULL)
	{
		Point3F sideDir = Point3F(0, 0, 0);
		Point3F motionDir = Point3F(0, 0, 0);
		Point3F upDir = Point3F(0, 0, 0);
		this->getMarbleAxis(sideDir, motionDir, upDir);
		A += (sideDir * this->mMove->x + motionDir * this->mMove->y) * this->mDataBlock->mAirAccel;
	}
	return A;
}

void Marble::velocityCancel(Point3F& vel, Point3F& omega, bool surfaceSlide, bool noBounce)
{
	// TODO: FIX
	//new Point3F(0.0f, 0.0f, -1.0f);
	float SurfaceDotThreshold = 0.001f;
	bool looped = false;
	int itersIn = 0;
	bool done;
	do
	{
		done = true;
		itersIn++;
		for (int i = 0; i < mContacts.size(); i++)
		{
			Point3F sVel = vel - mContacts[i].velocity;
			float surfaceDot = mDot(mContacts[i].normal, sVel);
			if ((!looped && surfaceDot < 0.0f) || surfaceDot < -SurfaceDotThreshold)
			{
				float velLen = vel.len();
				Point3F surfaceVel = surfaceDot * mContacts[i].normal;
				this->reportBounce(mContacts[i].point, mContacts[i].normal, -surfaceDot);
				if (noBounce)
				{
					vel -= surfaceVel;
				}
				else if (mContacts[i].hasCollider)
				{
					CollisionInfo info = mContacts[i];
					// TODO: Verify
					// Marble to Marble Collision
					SceneObject* obj = info.obj;
					if (obj != NULL)
					{
						float ourMass = 1.0f;
						float theirMass = 1.0f;
						float bounce = 0.5f;
						Point3F dp = vel * ourMass - obj->getVelocity() * theirMass;
						Point3F normP = mDot(dp, info.normal) * info.normal;
						normP *= 1.0f + bounce;
						vel -= normP / ourMass;
						obj->setVelocity(obj->getVelocity() + normP / theirMass);
						info.velocity = obj->getVelocity();
						this->mContacts[i] = info;
					}
					else
					{
						float bounce2 = 0.5f;
						Point3F normV = mDot(vel, info.normal) * info.normal;
						normV *= 1.0f + bounce2;
						vel -= normV;
					}
				}
				else
				{
					Point3F velocity2 = mContacts[i].velocity;
					if (velocity2.len() > 0.0001f && !surfaceSlide && surfaceDot > -this->mDataBlock->mMaxDotSlide * velLen)
					{
						vel -= surfaceVel;
						vel.normalize();
						vel *= velLen;
						surfaceSlide = true;
					}
					else if (surfaceDot > -this->mDataBlock->mMinBounceVel)
					{
						vel -= surfaceVel;
					}
					else
					{
						float restitution = this->mDataBlock->mBounceRestitution;
						restitution *= mContacts[i].restitution;
						Point3F velocityAdd = -(1.0f + restitution) * surfaceVel;
						Point3F vAtC = sVel + mCross(omega, -mContacts[i].normal * this->mRadius);
						float normalVel = -mDot(mContacts[i].normal, sVel);
						vAtC -= mContacts[i].normal * mDot(mContacts[i].normal, sVel);
						float vAtCMag = vAtC.len();
						if (vAtCMag != 0.0f)
						{
							float friction = this->mDataBlock->mBounceKineticFriction * mContacts[i].friction;
							float angVMagnitude = 5.0f * friction * normalVel / (2.0f * this->mRadius);
							if (angVMagnitude > vAtCMag / this->mRadius)
							{
								angVMagnitude = vAtCMag / this->mRadius;
							}
							Point3F vAtCDir = vAtC / vAtCMag;
							Point3F deltaOmega = angVMagnitude * mCross(-mContacts[i].normal, -vAtCDir);
							omega += deltaOmega;
							vel -= mCross(-deltaOmega, -mContacts[i].normal * this->mRadius);
						}
						vel += velocityAdd;
					}
				}
				done = false;
			}
		}
		looped = true;
		if (itersIn > 6 && noBounce)
		{
			done = true;
		}
	} while (!done);
	if (vel.lenSquared() < 625.0f)
	{
		bool gotOne = false;
		Point3F dir = Point3F(0.0f, 0.0f, 0.0f);
		for (int j = 0; j < mContacts.size(); j++)
		{
			Point3F dir2 = dir + mContacts[j].normal;
			if (dir2.lenSquared() < 0.01f)
			{
				dir2 += mContacts[j].normal;
			}
			dir = dir2;
			gotOne = true;
		}
		if (gotOne)
		{
			dir.normalize();
			float soFar = 0.0f;
			for (int k = 0; k < mContacts.size(); k++)
			{
				if (mContacts[k].penetration < this->mRadius)
				{
					float timeToSeparate = 0.1f;
					float dist = mContacts[k].penetration;
					float outVel = mDot(vel + soFar * dir, mContacts[k].normal);
					if (timeToSeparate * outVel < dist)
					{
						soFar += (dist - outVel * timeToSeparate) / timeToSeparate / mDot(mContacts[k].normal, dir);
					}
				}
			}
			soFar = mClampF(soFar, -25.0f, 25.0f);
			vel += soFar * dir;
		}
	}
}

void Marble::getMarbleAxis(Point3F& sideDir, Point3F& motionDir, Point3F& upDir)
{
	Point3F gWorkGravityDir = Point3F(0.0f, 0.0f, -1.0f);
	MatrixF camMat = MatrixF::Identity;

	AngAxisF xAng = AngAxisF(Point3F(-1.0f, 0.0f, 0.0f), this->mCameraY);
	AngAxisF zAng = AngAxisF(Point3F(0.0f, 0.0f, -1.0f), this->mCameraX);
	MatrixF xRot(true);
	MatrixF zRot(true);
	xAng.setMatrix(&xRot);
	zAng.setMatrix(&zRot);
	camMat = xRot * zRot;

	upDir = -gWorkGravityDir;
	camMat.getRow(1, &motionDir);
	sideDir = mCross(motionDir, upDir);
	sideDir.normalize();
	motionDir = mCross(upDir, sideDir);
}

void Marble::reportBounce(Point3F pos, Point3F normal, float speed)
{
	if (this->mBounceYet && speed < this->mBounceSpeed)
	{
		return;
	}
	this->mBounceYet = true;
	this->mBouncePos = pos;
	this->mBounceSpeed = speed;
	this->mBounceNormal = normal;
}

void Marble::collisionCallback(SceneObject* so, void* key)
{
	Marble* marble = dynamic_cast<Marble*>(so);
	
	if (!this->mIgnoreMarbles && marble != NULL)
	{
		if (marble->getId() != this->getId())
		{
			this->dtx2 = getMin(this->dtx2, this->dtx1 * marble->findMarbleCollisionTime(this->posx0, this->posx1, this->dtx1, this->mRadius));
		}
	}
	else {
		this->dtx2 = getMin(this->dtx2, this->dtx1 * findIntersectionTime(so, this->posx0, this->posx1, this->mRadius));
	}
}

void doCollisionCallback(SceneObject* so, void* key)
{
	AssertWarn(sMarble != NULL, "Marble::updateCollision: Unable to call Marble::collisionCallback()");
	if (sMarble != NULL)
		sMarble->collisionCallback(so, key);
}

F32 Marble::updateCollision(Point3F& pos1, Point3F pos0, QuatF& rot1, QuatF rot0, float dt)
{
	this->posx0 = pos0;
	this->posx1 = pos1;
	this->dtx1 = dt;
	this->dtx2 = dt;
	Box3F searchBox = this->mObjBox;
	Box3F startBox = searchBox;
	Point3F rad = Point3F(this->mRadius, this->mRadius, this->mRadius);
	startBox.minExtents += this->posx0 - rad;
	startBox.maxExtents += this->posx0 + rad;
	Box3F endBox = searchBox;
	endBox.minExtents += this->posx1 - rad;
	endBox.maxExtents += this->posx1 + rad;
	searchBox = startBox;
	searchBox.extend(endBox.minExtents);
	searchBox.extend(endBox.maxExtents);

	sMarble = this;
	if (isServerObject())
		gServerContainer.findObjects(sClientCollisionMask, doCollisionCallback, &searchBox);
	else
		gClientContainer.findObjects(sClientCollisionMask, doCollisionCallback, &searchBox);

	F32 i;
	if (this->dtx1 - this->dtx2 < 0.001f)
	{
		i = this->dtx2 / this->dtx1;
		this->posx1 = i * (this->posx1 - this->posx0) + this->posx0;
		return 0.0f;
	}
	i = this->dtx2 / this->dtx1;
	this->posx1 = i * (this->posx1 - this->posx0) + this->posx0;

	// This was the pesky collision issue T_T
	pos1 = this->posx1;

	return this->dtx1 - this->dtx2;
}

void Marble::findContactCallback(SceneObject* so, void* key)
{
	F32 ContactThreshold = 0.0001f;
	Marble* marble = dynamic_cast<Marble*>(so);
	if (!this->mIgnoreMarbles && marble != NULL)
	{
		if (marble->getId() != this->getId())
		{
			marble->addMarbleContact(this->posx2, this->mRadius + ContactThreshold, mContacts);
		}
	}
	else {
		addPlatformContact(so, this->posx2, this->mRadius + ContactThreshold, mContacts);
	}
}

void doFindContactCallback(SceneObject* so, void* key)
{
	AssertWarn(sMarble != NULL, "Marble::findContacts: Unable to call Marble::findContactCallback()");
	if (sMarble != NULL)
		sMarble->findContactCallback(so, key);
}

void Marble::findContacts(Point3F pos, QuatF rot)
{
	Box3F searchBox = this->mObjBox;
	Point3F rad = Point3F(this->mRadius, this->mRadius, this->mRadius);
	searchBox.minExtents += pos - rad;
	searchBox.maxExtents += pos + rad;
	this->posx2 = pos;
	
	sMarble = this;
	if (isServerObject())
		gServerContainer.findObjects(sClientCollisionMask, doFindContactCallback, &searchBox);
	else
		gClientContainer.findObjects(sClientCollisionMask, doFindContactCallback, &searchBox);
}

F32 Marble::findMarbleCollisionTime(Point3F pos0, Point3F pos1, F32 dt, F32 themRadius)
{
	F32 t = 1.0f;
	Point3F usCenter = this->mWorldToObj.getPosition();
	F32 tCap;
	if (!NewCollision::intersectSegmentCapsule(pos0, pos1, usCenter, usCenter, themRadius + this->mRadius, &t, &tCap))
	{
		return 1.0f;
	}
	Point3F delta = pos1 - pos0;
	Point3F endPos = pos0 + t * delta;
	Point3F norm = endPos - usCenter;
	norm.normalize();
	Point3F deltaVel = pos1 - pos0 - dt * this->mVelocity;
	if (mDot(deltaVel, norm) >= 0.0f)
	{
		return 1.0f;
	}
	return t;
}

void Marble::addMarbleContact(Point3F pos, F32 radius, Vector<CollisionInfo>& contacts)
{
	// TODO: Verify
	//Point3F usCenter = this->worldTransformInterface.Value.Translation;
	Point3F usCenter = this->mWorldToObj.getPosition();
	F32 distSq = (usCenter - pos).lenSquared();
	if (distSq <= (radius + this->mRadius) * (radius + this->mRadius))
	{
		Point3F normal = pos - usCenter;
		normal.normalize();
		Point3F collPos = usCenter + normal * this->mRadius;
		CollisionInfo info = CollisionInfo();
		info.hasCollider = true;
		info.velocity = this->mVelocity;
		info.normal = normal;
		info.point = collPos;
		info.penetration = radius - mDot(pos - info.point, normal);
		info.restitution = 0.5f;
		info.friction = 0.0f;
		info.obj = this;
		contacts.push_back(info);
	}
}

void Marble::resolveCollision(Point3F pos, Point3F norm)
{
	Point3F normVel = mDot(this->mVelocity, norm) * norm;
	Point3F tangVel = this->mVelocity - normVel;
	float normSpeed = normVel.len();
	float tangSpeed = tangVel.len();
	this->mVelocity -= (1.0f + this->mElasticity) * normVel;
	if (tangSpeed < normSpeed * this->mFriction)
	{
		this->mVelocity -= tangVel;
		return;
	}
	this->mVelocity -= normSpeed * this->mFriction / (tangSpeed + 0.001f) * tangVel;
}

void Marble::resolveCollisions(Vector<CollisionInfo>& list)
{
	for (int i = 0; i < list.size(); i++)
	{
		this->resolveCollision(list[i].point, list[i].normal);
	}
}

void Marble::writePacketData(GameConnection * conn, BitStream *stream)
{
	Parent::writePacketData(conn, stream);
}

void Marble::readPacketData(GameConnection * conn, BitStream *stream)
{
	Parent::readPacketData(conn, stream);
}

U32  Marble::packUpdate(NetConnection * conn, U32 mask, BitStream *stream)
{
	U32 retMask = Parent::packUpdate(conn, mask, stream);

	if (isServerObject())
	{
		stream->write(mStatic);
		stream->write(mDisableRotation);
		stream->write(mDisablePhysics);

		if (stream->writeFlag(mask & MoveMask))
		{
			mathWrite(*stream, mVelocity);
			mathWrite(*stream, this->getTransform());
		}
	}

	return retMask;
}

void Marble::unpackUpdate(NetConnection * conn, BitStream *stream)
{
	Parent::unpackUpdate(conn, stream);

	if (isClientObject())
	{
		stream->read(&mStatic);
		stream->read(&mDisableRotation);
		stream->read(&mDisablePhysics);

		if (stream->readFlag())
		{
			mathRead(*stream, &mVelocity);

			MatrixF mat;
			mathRead(*stream, &mat);

			this->setTransform(mat);
		}
	}
}

void Marble::setPosition(const Point3F& pos, const AngAxisF& rot, float rot1)
{
	MatrixF mat;
	rot.setMatrix(&mat);
	mat.setColumn(3, pos);
	Parent::setTransform(mat);
	Parent::setRenderTransform(mat);

	this->mCameraY = rot1;

	setMaskBits(MoveMask);
}

void Marble::setPosition(const Point3F& pos)
{
	MatrixF mat;
	mat.setColumn(3, pos);
	Parent::setTransform(mat);
	Parent::setRenderTransform(mat);

	setMaskBits(MoveMask);
}

void Marble::setPosition(const Point3F& pos, const QuatF& rot)
{
	MatrixF mat;
	rot.setMatrix(&mat);
	mat.setColumn(3, pos);

	setMaskBits(MoveMask);
	Parent::setTransform(mat);
}

const MatrixF Marble::getTransform()
{
	return Parent::getTransform();
}

void Marble::setTransform(const MatrixF& newMat)
{
	Parent::setTransform(newMat);
	mContacts.clear();
}

void Marble::setVelocity(Point3F velocity)
{
	Parent::setVelocity(velocity);
	this->mVelocity = velocity;
}

Point3F Marble::getVelocity()
{
	return this->mVelocity;
}

AngAxisF Marble::getRotation()
{
	return AngAxisF(this->getTransform());
}

ConsoleMethod(Marble, setPosition, void, 4, 4, "(Position P, Rotation r)")
{
	Point3F pos;
	const MatrixF& tmat = object->getTransform();
	tmat.getColumn(3, &pos);
	AngAxisF aa(tmat);

	dSscanf(argv[2], "%f %f %f %f %f %f %f",
		&pos.x, &pos.y, &pos.z, &aa.axis.x, &aa.axis.y, &aa.axis.z, &aa.angle);

	F32 rotVal = dAtof(argv[3]);

	MatrixF mat;
	aa.setMatrix(&mat);
	mat.setColumn(3, pos);
	object->setPosition(pos, aa, rotVal);
}

ConsoleMethod(Marble, getVelocity, const char *, 2, 2, "")
{
	const VectorF& vel = object->getVelocity();
	char* buff = Con::getReturnBuffer(100);
	dSprintf(buff, 100, "%g %g %g", vel.x, vel.y, vel.z);
	return buff;
}

ConsoleMethod(Marble, setVelocity, bool, 3, 3, "(Vector3F vel)")
{
	VectorF vel(0, 0, 0);
	dSscanf(argv[2], "%f %f %f", &vel.x, &vel.y, &vel.z);
	object->setVelocity(vel);
	return true;
}

ConsoleMethod(Marble, getRotation, const char *, 2, 2, "")
{
	const AngAxisF& rot = object->getRotation();
	char* buff = Con::getReturnBuffer(100);
	dSprintf(buff, 100, "%g %g %g %g", rot.angle, rot.axis.x, rot.axis.y, rot.axis.z);
	return buff;
}
