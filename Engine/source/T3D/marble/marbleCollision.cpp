#include "T3D/marble/marbleCollision.h"
#include "T3D/marble/collisionUtil.h"

#include "collision/concretePolyList.h"
#include "collision/convex.h"

#include "T3D/marble/marble.h"
//#include "T3D/marble/epsilonUtil.h"
#include "T3D/marble/precision.h"

namespace MarbleCollision
{
	F32 findIntersectionTime(Marble* marble, SceneObject* obj, Point3F pos0, Point3F pos1, F32 radius)
	{
		Box3F searchBox = marble->getObjBox(); // Maybe this?
		float bigRad = radius * 1.05f;
		Point3F radial = Point3F(bigRad, bigRad, bigRad);
		searchBox.minExtents = (searchBox.maxExtents = pos0 - radial);
		searchBox.extend(pos0 + radial);
		searchBox.extend(pos1 - radial);
		searchBox.extend(pos1 + radial);
		ConcretePolyList list = ConcretePolyList();

		SphereF searchSphere;
		searchSphere.center = (searchBox.minExtents + searchBox.maxExtents) * 0.5;
		VectorF bv = searchBox.maxExtents - searchSphere.center;
		searchSphere.radius = bv.len();

		obj->buildPolyList(PLC_Collision, &list, searchBox, searchSphere);
		list.triangulate();

		float tmin = 2.0f;
		for (int i = 0; i < list.mPolyList.size(); i++)
		{
			ConcretePolyList::Poly poly = list.mPolyList[i];
			AssertWarn(poly.vertexCount == 3, "We triangulated the vertices...right?");

			Point3F v0 = list.mVertexList[list.mIndexList[poly.vertexStart + 0]];
			Point3F v1 = list.mVertexList[list.mIndexList[poly.vertexStart + 1]];
			Point3F v2 = list.mVertexList[list.mIndexList[poly.vertexStart + 2]];
			Point3F norm = poly.plane;
			
			float t = 0.0f;
			Point3F closest = Point3F(0, 0, 0);
			if (CollisionUtil::intersectMovingSphereTriangle(pos0, pos1, radius, v0, v1, v2, norm, t, closest) && t < tmin)
			{
 				norm = pos0 + t * (pos1 - pos0) - closest;
				if (norm.lenSquared() > 1E-05f)
				{
					norm.normalize();
					F32 dir = mDot(norm, pos1 - pos0);
					if (dir < -0.0001f)
					{
						tmin = t;
					}
				}
			}
		}
		return tmin;
	}

	void addPlatformContact(Marble* marble, SceneObject* obj, Point3F pos, F32 radius, CollisionList& contacts)
	{
		Box3F searchBox = marble->getObjBox();
		float bigRad = radius * 1.1f;
		Point3F radial = Point3F(bigRad, bigRad, bigRad);
		searchBox.minExtents = (searchBox.maxExtents = pos - radial);
		searchBox.extend(pos + radial);
		ConcretePolyList list = ConcretePolyList();
		SphereF searchSphere;
		searchSphere.center = (searchBox.minExtents + searchBox.maxExtents) * 0.5;
		VectorF bv = searchBox.maxExtents - searchSphere.center;
		searchSphere.radius = bv.len();

		obj->buildPolyList(PLC_Collision, &list, searchBox, searchSphere);
		list.triangulate();

		for (int i = 0; i < list.mPolyList.size(); i++)
		{
			ConcretePolyList::Poly poly = list.mPolyList[i];
			AssertWarn(poly.vertexCount == 3, "We triangulated the vertices...right?");

			Point3F v0 = list.mVertexList[list.mIndexList[poly.vertexStart + 0]];
			Point3F v1 = list.mVertexList[list.mIndexList[poly.vertexStart + 1]];
			Point3F v2 = list.mVertexList[list.mIndexList[poly.vertexStart + 2]];
			Point3F norm = poly.plane;

			Point3F closest = Point3F(0, 0, 0);

			// TODO:
			// This line is causing the collision issue!
			// It is sometimes false when it shouldn't be and closest seems wrong sometimes.
			if (CollisionUtil::closestPtPointTriangle(pos, radius, v0, v1, v2, norm, closest) && (pos - closest).lenSquared() < radius * radius)
			{
				Point3F normal = (pos - closest);
				if (mDot(pos - closest, norm) > 0.0f)
				{
					normal.normalize();
					Collision* info = &contacts.increment();
					info->normal = normal;
					info->point = closest;
					info->material = poly.material;
					info->distance = radius - mDot(pos - closest, normal);
					info->hasCollider = false;
					info->object = obj;
					info->velocity = obj->getVelocity();
				}
			}
			else if (CollisionUtil::closestPtPointTriangle(pos, radius, v0, v1, v2, norm, closest))
			{
				AssertFatal((pos - closest).len() >= radius, "should have found contact");
			}
		}
	}
}