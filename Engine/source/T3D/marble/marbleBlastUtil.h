#ifndef __MARBLEBLAST_UTIL_H_
#define __MARBLEBLAST_UTIL_H_

#include "math/mMath.h"

extern void rotateMatrix(MatrixF& mat, Point3F pnt);

#endif // __MARBLEBLAST_UTIL_H_