#ifndef _POWERUP_H_
#define _POWERUP_H_

#ifndef _GAMEBASE_H_
#include "T3D/gameBase/gameBase.h"
#endif

#ifndef _H_PARTICLE_EMITTER
#include "T3D/fx/particleEmitter.h"
#endif

#define MAX_POWERUPS 10

struct PowerUp
{
	/// Particle emitter when active.
	ParticleEmitterData* emitter;

	/// Duration of powerup effects.
	S32 duration;

	/// Time, in ms, for powerup to be activated and have an effect.
	S32 activateTime;

	/// Image to mount while powerup is active.
	GameBaseData* image;

	/// Boost Direction.
	Point3F boostDir;

	/// Amount to boost (not affected by duration).
	F32 boostAmount;

	/// Amount to boost without taking mass into account (not affected by duration).
	F32 boostMassless;

	/// Freeze the clock for this amount of time.
	S32 timeFreeze;

	/// Should this powerup recharge blast on pickup
	bool blastRecharge;

	/// Modify air acceleration by this factor
	F32 airAccel;
	
	/// Modify gravity by this factor
	F32 gravityMod;

	/// Change bounce restitution to this value
	F32 bounce;

	/// Modify bounce restitution by this factor
	F32 boost;

	/// Apply up to this much force to other marbles
	F32 repulseMax;

	/// Max distance at which repulse force is applied
	F32 repulseDist;

	/// Scale mass by this amount
	F32 massScale;

	/// Scale size by this amount
	F32 sizeScale;

	/// Should this powerup enable parkour
	bool enableParkour;
};

class PowerUpData : public GameBaseData
{
	typedef GameBaseData Parent;

public:
	//PowerUp mPowerUps[MAX_POWERUPS];

protected:
	ParticleEmitterData* mEmitter[MAX_POWERUPS];
	S32 mDuration[MAX_POWERUPS];
	S32 mActivateTime[MAX_POWERUPS];
	GameBaseData* mImage[MAX_POWERUPS];
	Point3F mBoostDir[MAX_POWERUPS];
	F32 mBoostAmount[MAX_POWERUPS];
	F32 mBoostMassless[MAX_POWERUPS];
	S32 mTimeFreeze[MAX_POWERUPS];
	bool mBlastRecharge[MAX_POWERUPS];
	F32 mAirAccel[MAX_POWERUPS];
	F32 mGravityMod[MAX_POWERUPS];
	F32 mBounce[MAX_POWERUPS];
	F32 mBoost[MAX_POWERUPS];
	F32 mRepulseMax[MAX_POWERUPS];
	F32 mRepulseDist[MAX_POWERUPS];
	F32 mMassScale[MAX_POWERUPS];
	F32 mSizeScale[MAX_POWERUPS];
	bool mEnableParkour[MAX_POWERUPS];

public:
	PowerUpData();
	~PowerUpData();

	static void initPersistFields();
	//bool preload(bool, char errorBuffer[256]);

	virtual void packData(BitStream* stream);
	virtual void unpackData(BitStream* stream);

	virtual PowerUp* getPowerUp(S32 index);

	DECLARE_CONOBJECT(PowerUpData);
};

#endif // _POWERUP_H_