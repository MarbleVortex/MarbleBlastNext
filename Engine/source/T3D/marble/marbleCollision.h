#ifndef _MARBLECOLLISION_H_
#define _MARBLECOLLISION_H_

#ifndef _SCENEOBJECT_H_
#include "scene/sceneObject.h"
#endif

#ifndef _ABSTRACTPOLYLIST_H_
#include "collision/abstractPolyList.h"
#endif

#ifndef _COLLISION_H_
#include "collision/collision.h"
#endif

class Marble;

#define MARBLE_COLLISION_TOLERANCE 0.0001 //0.0337368 // <<< Normally 0.0001 but this seems to somewhat fix the collision issue.

namespace MarbleCollision
{
	static F32 MarblePointEpsilon = 1E-05f;
	static F32 MarbleDotEpsilon = 1E-05f;

	extern F32 findIntersectionTime(Marble* marble, SceneObject* obj, Point3F pos0, Point3F pos1, F32 radius);
	extern void addPlatformContact(Marble* marble, SceneObject* obj, Point3F pos, F32 radius, CollisionList& contacts);
}

#endif // _MARBLECOLLISION_H_