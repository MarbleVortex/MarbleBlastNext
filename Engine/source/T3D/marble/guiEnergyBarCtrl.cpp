#include "T3D/marble/guiEnergyBarCtrl.h"

#include "T3D/gameBase/gameConnection.h"
#include "T3D/marble/marble.h"
#include "gfx/gfxDrawUtil.h"

//#include "windowManager/platformWindowMgr.h"

IMPLEMENT_CONOBJECT(GuiEnergyBarCtrl);

ImplementEnumType(GuiEnergyBarDirection, "Direction for EnergyBar")
	{ GuiEnergyBarCtrl::LeftRight, "LeftRight", "Left to Right" },
	{ GuiEnergyBarCtrl::RightLeft, "RightLeft", "Right to Left" }
EndImplementEnumType;

GuiEnergyBarCtrl::GuiEnergyBarCtrl()
{
	this->mInactiveColor = ColorI(128, 128, 128);
	this->mActiveColor = ColorI(0, 64, 255);
	this->mUltraColor = ColorI(128, 128, 0);
	this->mMinActiveValue = 0.25;
	this->mTextureName = StringTable->insert("");
}

GuiEnergyBarCtrl::~GuiEnergyBarCtrl()
{

}

void GuiEnergyBarCtrl::initPersistFields()
{
	addField("inactiveColor", TypeColorI, Offset(mInactiveColor, GuiEnergyBarCtrl));
	addField("activeColor", TypeColorI, Offset(mActiveColor, GuiEnergyBarCtrl));
	addField("ultraColor", TypeColorI, Offset(mUltraColor, GuiEnergyBarCtrl));
	addField("minActiveValue", TypeF32, Offset(mMinActiveValue, GuiEnergyBarCtrl));

	addField("direction", TypeGuiEnergyBarDirection, Offset(mDirection, GuiEnergyBarCtrl));
	addField("texture", TypeFilename, Offset(mTextureName, GuiEnergyBarCtrl));

	Parent::initPersistFields();
}

bool GuiEnergyBarCtrl::onWake()
{
	if (!Parent::onWake())
		return false;

	if (!mTexHandle && (mTextureName && mTextureName[0]))
		mTexHandle.set(mTextureName, &GFXDefaultGUIProfile, avar("%s() - mTexHandle (line %d)", __FUNCTION__, __LINE__));

	return true;
}

void GuiEnergyBarCtrl::onSleep()
{
	mTexHandle = NULL;
	Parent::onSleep();
}

void GuiEnergyBarCtrl::onRender(Point2I offset, const RectI &updateRect)
{
	// Temporarily disable clipping since we don't have a second yellow blast bar on top yet
	//Point2I res = PlatformWindowManager::get()->getDesktopResolution();
	//RectI newClip = RectI(0, 0, res.x, res.y);
	//GFX->setClipRect(newClip);

	GameConnection* conn = GameConnection::getConnectionToServer();
	if (!conn)
		return;

	Marble* marble = dynamic_cast<Marble*>(conn->getControlObject());
	if (!marble)
		return;

	F32 blastEnergy = marble->getBlastEnergy();
	F32 progress = blastEnergy * 100.0f / MAX_NATURAL_BLAST;

	F32 value = mClampF(progress / 100.0f, 0.f, 1.f);

	RectI ctrlRect(offset, getExtent());

	//draw the progress
	S32 width = (S32)((F32)(getWidth()) * value);
	if (width > 0)
	{
		RectI progressRect = ctrlRect;
		progressRect.extent.x = width;
		ColorI color = this->mActiveColor;
		if (value < 0.25f)
			color = this->mInactiveColor;
		//else if (value > 1.0f)
			//color = this->mUltraColor;
		GFX->getDrawUtil()->drawRectFill(progressRect, color);
	}

	progress = (blastEnergy - MAX_NATURAL_BLAST) * 100.0f / (MAX_BLAST - MAX_NATURAL_BLAST);

	value = mClampF(progress / 100.0f, 0.f, 1.f);

	width = (S32)((F32)(getWidth()) * value);
	if (width > 0)
	{
		RectI progressRectUltra = ctrlRect;
		progressRectUltra.extent.x = width;
		GFX->getDrawUtil()->drawRectFill(progressRectUltra, this->mUltraColor);
	}

	//now draw the border
	//if (mProfile->mBorder)
	//	GFX->getDrawUtil()->drawRect(ctrlRect, mProfile->mBorderColor);

	Parent::onRender(offset, updateRect);

	//render the children
	renderChildControls(offset, updateRect);
}
