#ifndef _MARBLE_H_
#define _MARBLE_H_

#ifndef _SHAPEBASE_H_
#include "T3D/shapeBase.h"
#endif

#ifndef _H_PARTICLE_EMITTER
#include "T3D/fx/particleEmitter.h"
#endif

#ifndef _RIGID_H_
#include "T3D/rigid.h"
#endif

#ifndef _MARBLECOLLISION_H_
#include "T3D/marble/marbleCollision.h"
#endif

#ifndef _POWERUP_H_
#include "T3D/marble/powerup.h"
#endif

#ifndef _SFXSYSTEM_H_
#include "sfx/sfxSystem.h"
#endif

#ifndef _SFXTRACK_H_
#include "sfx/sfxTrack.h"
#endif

#ifndef _SFXSOURCE_H_
#include "sfx/sfxSource.h"
#endif

#ifndef _SFXTYPES_H_
#include "sfx/sfxTypes.h"
#endif

#ifndef _ITEM_H_
#include "T3D/item.h"
#endif

#define MAX_BLAST 1125.0f
#define MAX_NATURAL_BLAST 940.0f
#define MIN_USABLE_BLAST 235.0f

class Marble;

class MarbleData : public ShapeBaseData
{
	typedef ShapeBaseData Parent;

	friend class Marble;

protected:
	const char* mPowerUpsName;
	PowerUpData* mPowerUps;

	F32 mMaxRollVelocity;
	F32 mAngularAcceleration;
	F32 mBrakingAcceleration;
	F32 mStaticFriction;
	F32 mKineticFriction;
	F32 mBounceKineticFriction;
	F32 mGravity;
	F32 mMaxDotSlide;
	F32 mBounceRestitution;
	F32 mAirAccel;
	F32 mEnergyRechargeRate;
	F32 mMaxNaturalBlastRecharge;
	F32 mJumpImpulse;
	F32 mMaxForceRadius;
	F32 mCameraDistance;
	F32 mMinBounceVel;
	F32 mMinTrailSpeed;
	F32 mMinBounceSpeed;
	F32 mSize;
	//F32 mCollisionRadius;

	ParticleEmitterData* mBounceEmitter;
	ParticleEmitterData* mTrailEmitter;
	ParticleEmitterData* mPowerUpEmitter;

	S32 mPowerUpTime;

	enum Sounds {
		RollHardSound,
		RollMegaSound,
		RollIceSound,
		SlipSound,
		Bounce1,
		Bounce2,
		Bounce3,
		Bounce4,
		MegaBounce1,
		MegaBounce2,
		MegaBounce3,
		MegaBounce4,
		JumpSound,
		MaxSounds
	};

	SFXProfile* sound[MaxSounds];

public:

	MarbleData();
	~MarbleData();

	static void initPersistFields();
	bool preload(bool, String &errorStr);
	virtual void packData(BitStream* stream);
	virtual void unpackData(BitStream* stream);

	DECLARE_CONOBJECT(MarbleData);

	DECLARE_CALLBACK(void, onEnterLiquid, (Marble* obj, F32 coverage, const char* type));
	DECLARE_CALLBACK(void, onUpdateLiquid, (Marble* obj, F32 coverage, const char* type));
	DECLARE_CALLBACK(void, onLeaveLiquid, (Marble* obj, const char* type));
};

class Marble : public ShapeBase
{
	typedef ShapeBase Parent;

public:
	// Temp
	//static F32 smCollisionTolerance;
	bool mCameraAngleOrderFlip;
	bool mCameraAngleFlip;
	bool mCameraAnglePB;
	bool mCameraAngleYB;
	Point3F mCameraPAngle;
	Point3F mCameraYAngle;
	AngAxisF mCameraAngle;

protected:

	static bool smHitShake;

	MarbleData* mDataBlock;

	// Client interpolation data
	/*struct StateDelta {
		Point3F pos;
		VectorF posVec;
		EulerF rot;
		VectorF rotVec;
		S32 warpTicks;
		Point3F warpOffset;
		Point3F rotOffset;
		F32     dt;
	};
	StateDelta delta;*/

	// Temporarily Public for testing
public:
	enum MaskBits {
		MoveMask		= Parent::NextFreeMask,
		InitialMask		= Parent::NextFreeMask << 1,
		BoundsMask		= Parent::NextFreeMask << 2,
		UpdateMask		= Parent::NextFreeMask << 3,
		CameraSetMask	= Parent::NextFreeMask << 4,
		TimeMask		= Parent::NextFreeMask << 5,
		NextFreeMask	= Parent::NextFreeMask << 6
	};
protected:

	enum MarbleMode {
		Start,
		Normal,
		Victory
	};

	bool mHitShake;

	// TODO: TEMP TIMER
	//S32 mTempBlastTimer;

	bool mStatic;
	bool mDisablePhysics;
	bool mDisableRotation;
	bool mDisableCollision;
	bool mIgnoreMarbles;

	//bool mJumped;

	MarbleMode mMode;
	bool mUseFullTime;

	struct ActivePowerUp
	{
		S32 id;
		PowerUp* powerUp;
		F32 timer;
		ParticleEmitter* emitter;
		S32 mountIndex;
		F32 blastEnergy;
		bool initialized;
	};

	bool inLiquid;

	Vector<ActivePowerUp> mActivePowerUps;

	F32 mCurrentBounce;
	F32 mCurrentGravity;
	F32 mCurrentAirAccel;
	F32 mCurrentSize;
	F32 mDesiredSize;
	F32 mCurrentMass;
	bool mParkour;

	//bool mSpawnDelay;

	bool mOutOfBounds;
	S32 mTime;
	S32 mBonusTime;

	F32 mBlastEnergy;
	F32 mBlastTimer;

	S32 mPowerUpId;
	bool mPowerUpReset;

	MatrixF mGravityMatrix;
	Point3F mGravityDir;
	bool mGravitySnap;
	
	SceneObject* mPad;
	bool mOnPad;

	F32 mRollVolume;
	SFXSource* mRollHandle;
	SFXSource* mRollMegaHandle;
	SFXSource* mSlipHandle;

	Point3F mVelocity;
	Point3F mRotVelocity;

	F32 mRadius;
	bool mControllable;
	bool mBounceYet;
	bool mOnIce;
	bool mSlipping;
	F32 mSlipAmount;
	F64 mContactTime;
	F64 mTotalTime;
	Point3F mBouncePos;
	Point3F mBounceNormal;
	F32 mBounceSpeed;

	F32 mElasticity;
	F32 mFriction;
	F32 mVisibilityLevel;
	Point3F mGravity;

	F32 mCameraY;
	F32 mCameraX;

	Point3F mStartPos;

	const Move* mMove;

	//Box3F mBoundingBox;
	//SphereF mBoundingSphere;

	F64 dtx1;
	F64 dtx2;
	Point3F posx0;
	Point3F posx1;
	Point3F posx2;

	Box3F box0;

	//Vector<MarbleCollision::CollisionInfo> mContacts;
	CollisionList mContacts;

	Point3F mLastContactPosition;

	void updateClientPhysics(F32 dt);
	void updateServerPhysics(F32 dt);
	void updateIntegration(F64 dt, Point3F& pos, QuatF& rot, Point3F& vel, Point3F& omega);
	void updateData(F32 dt);

	void updateBounceSound(Point3F pos);
	void updateRollSound(Point3F pos);
	void updateItems(Point3F startPos, Point3F endPos);

	bool computeMoveForces(Point3F omega, Point3F* aControl, Point3F* desiredOmega);
	void applyContactForces(F64 dt, bool isCentered, Point3F aControl, Point3F desiredOmega, Point3F& vel, Point3F& omega, Point3F& A, Point3F* a);
	Point3F getExternalForces(F64 dt);
	void velocityCancel(Point3F& vel, Point3F& omega, bool surfaceSlide, bool noBounce);
	void getMarbleAxis(Point3F& sideDir, Point3F& motionDir, Point3F& upDir);
	void reportBounce(Point3F pos, Point3F normal, float speed);
	F64 updateCollision(Point3F& pos1, Point3F pos0, QuatF& rot1, QuatF rot0, F64 dt);
	void findContacts(Point3F pos, QuatF rot);
	F32 findMarbleCollisionTime(Point3F pos0, Point3F pos1, F32 dt, F32 themRadius);
	void addMarbleContact(Point3F pos, F32 radius, CollisionList& contacts); //Vector<MarbleCollision::CollisionInfo>& contacts);

	//void resolveCollision(Point3F pos, Point3F norm);
	//void resolveCollisions(CollisionList& contacts); //Vector<MarbleCollision::CollisionInfo>& list);

    void drawContactsDebug();

	void checkTriggers();
	static void findTriggerCallback(SceneObject* obj, void *key);

	void checkPad();
	static void findPadCallback(SceneObject* obj, void *key);

public:


	//U32 testLoopCount;
	//U32 peakTestLoopCount;

	static void initPersistFields();
	static void consoleInit();

	Marble();
	~Marble();

	bool onAdd();
	void onRemove();

	virtual F32 getMass() const;

	virtual void applyImpulse(const Point3F &pos, const VectorF &vec);

	virtual void setVelocity(const VectorF& vel);
	virtual Point3F getVelocity() const;
	virtual void setVelocityRot(const VectorF& velocity);
	virtual Point3F getVelocityRot() const;
	AngAxisF getRotation();

	void setPosition(const Point3F& pos, const AngAxisF& rot, float rot1);
	void setPosition(const Point3F& pos);
	void setPosition(const Point3F& pos, const QuatF& rot);
	void setTransform(const MatrixF& newMat);
	const MatrixF getTransform();

	bool onNewDataBlock(GameBaseData* dptr, bool reload);
	void getCameraTransform(F32* pos, MatrixF* mat);
	//virtual void getRenderMountTransform(U32 index, MatrixF* mat);
	virtual void getRenderImageTransform(U32 imageSlot, MatrixF* mat, bool noEyeOffset);
	void advanceTime(F32 dt);
	void interpolateTick(F32 dt);
	void processTick(const Move *move);
	void updateMove(F64 dt, Point3F& pos, QuatF& rot, Point3F& vel, Point3F& omega);

	void writePacketData(GameConnection * conn, BitStream *stream);
	void readPacketData(GameConnection * conn, BitStream *stream);
	U32  packUpdate(NetConnection * conn, U32 mask, BitStream *stream);
	void unpackUpdate(NetConnection * conn, BitStream *stream);

	virtual bool buildPolyList(PolyListContext context, AbstractPolyList* polyList, const Box3F &box, const SphereF &sphere);

	void setOOB(bool flag);
	bool isOOB();
	void setMode(const char* mode);

	void setTime(S32 time);
	S32 getTime();
	void setUseFullTime(bool flag);

	void incBonusTime(S32 time);
	void setBonusTime(S32 time);
	S32 getBonusTime();

	void setPowerUpId(S32 id, bool reset);
	void setBlastEnergy(F32 energy);
	F32 getBlastEnergy();

	void setGravityDir(Point3F dir, MatrixF gravMat, bool b1);
	EulerF getGravityDir();

	void setPad(SceneObject* pad);
	SceneObject* getPad();

	Point3F getLastContactPosition();
	void cameraLookAtPt(Point3F pt);

	void doPowerUp(S32 id, PowerUp* powerUp);
	void updatePowerUp(F32 dt, Point3F& pos, QuatF& rot, Point3F& vel, Point3F& omega);

	static void itemCallback(SceneObject* so, void* key);
	static void collisionCallback(SceneObject* so, void* key);
	static void findContactCallback(SceneObject* so, void* key);

	void applyCameraShake(F32 amplifier, F32 freq, F32 falloff, F32 duration);

	//virtual void prepBatchRender(SceneState *state, S32 mountedImageIndex);
	//virtual void renderObject(SceneState *state, RenderInst *ri);

	//void renderLineCircle(F32 offset);

	DECLARE_CONOBJECT(Marble);
};

#endif // _MARBLE_H_
