#ifndef __GUIENERGYBARCTRL_H_
#define __GUIENERGYBARCTRL_H_

#include "gui/core/guiControl.h"

class GuiEnergyBarCtrl : public GuiControl
{
	typedef GuiControl Parent;

public:
	enum Direction
	{
		LeftRight,
		RightLeft
	};

protected:
	ColorI mInactiveColor;
	ColorI mActiveColor;
	ColorI mUltraColor;
	F32 mMinActiveValue;
	Direction mDirection;
	StringTableEntry mTextureName;
	GFXTexHandle mTexHandle;

public:
	GuiEnergyBarCtrl();
	~GuiEnergyBarCtrl();

	static void initPersistFields();

	bool onWake();
	void onSleep();

	void onRender(Point2I offset, const RectI &updateRect);

	DECLARE_CONOBJECT(GuiEnergyBarCtrl);
};

typedef GuiEnergyBarCtrl::Direction GuiEnergyBarDirection;
DefineEnumType(GuiEnergyBarDirection);

#endif // __GUIENERGYBARCTRL_H_
