#include "T3D/marble/collisionUtil.h"
#include "T3D/marble/precision.h"

namespace CollisionUtil
{
	F32 closestPtSegmentSegment(Point3F p1, Point3F q1, Point3F p2, Point3F q2, F32* s, F32* t, Point3F& c1, Point3F& c2)
	{
		float Epsilon = MARBLE_EPSILON;
		Point3F d = q1 - p1;
		Point3F d2 = q2 - p2;
		Point3F r = p1 - p2;
		float a = mDot(d, d);
		float e = mDot(d2, d2);
		float f = mDot(d2, r);
		if (a <= Epsilon && e <= Epsilon)
		{
			*s = (*t = 0.0f);
			c1 = p1;
			c2 = p2;
			return mDot(c1 - c2, c1 - c2);
		}
		if (a <= Epsilon)
		{
			*s = 0.0f;
			*t = f / e;
			*t = mClampF(*t, 0.0f, 1.0f);
		}
		else
		{
			float c3 = mDot(d, r);
			if (e <= Epsilon)
			{
				*t = 0.0f;
				*s = mClampF(-c3 / a, 0.0f, 1.0f);
			}
			else
			{
				float b = mDot(d, d2);
				float denom = a * e - b * b;
				if (denom != 0.0f)
				{
					*s = mClampF((b * f - c3 * e) / denom, 0.0f, 1.0f);
				}
				else
				{
					*s = 0.0f;
				}
				*t = (b * (*s) + f) / e;
				if (*t < 0.0f)
				{
					*t = 0.0f;
					*s = mClampF(-c3 / a, 0.0f, 1.0f);
				}
				else if (*t > 1.0f)
				{
					*t = 1.0f;
					*s = mClampF((b - c3) / a, 0.0f, 1.0f);
				}
			}
		}
		c1 = p1 + d * (*s);
		c2 = p2 + d2 * (*t);
		return mDot(c1 - c2, c1 - c2);
	}

	bool intersectSegmentCapsule(Point3F segStart, Point3F segEnd, Point3F capStart, Point3F capEnd, F32 radius, F32* tSeg, F32* tCap)
	{
		Point3F p = Point3F(0, 0, 0);
		Point3F p2 = Point3F(0, 0, 0);
		return closestPtSegmentSegment(segStart, segEnd, capStart, capEnd, tSeg, tCap, p, p2) < radius * radius;
	}

	bool intersectMovingSphereTriangle(Point3F startCenter, Point3F endCenter, F32 radius, Point3F p0, Point3F p1, Point3F p2, Point3F normal, F32& t, Point3F& closest)
	{
		t = 10.0f;
		float startDot = mDot(startCenter, normal);
		float endDot = mDot(endCenter, normal);
		float triDot = mDot(p0, normal);
		if ((startDot - triDot) * (endDot - triDot) > 0.0f && mFabs(startDot - triDot) > radius && mFabs(endDot - triDot) > radius)
		{
			return false;
		}
		if (endDot - startDot > -0.001f)
		{
			return false;
		}
		if (startDot < triDot + radius)
		{
			closest = startCenter;
			t = 0.0f;
		}
		else
		{
			t = (triDot + radius - startDot) / (endDot - startDot);
			closest = startCenter + t * (endCenter - startCenter);
		}
		closest += (triDot - mDot(closest, normal)) * normal;
		if (pointInTriangle(closest, p0, p1, p2))
		{
			return true;
		}
		t = 10.0f;
		float tTest;
		float tCap;
		if (intersectSegmentCapsule(startCenter, endCenter, p0, p1, radius, &tTest, &tCap) && tTest < t)
		{
			closest = p0 + tCap * (p1 - p0);
			t = tTest;
		}
		if (intersectSegmentCapsule(startCenter, endCenter, p1, p2, radius, &tTest, &tCap) && tTest < t)
		{
			closest = p1 + tCap * (p2 - p1);
			t = tTest;
		}
		if (intersectSegmentCapsule(startCenter, endCenter, p2, p0, radius, &tTest, &tCap) && tTest < t)
		{
			closest = p2 + tCap * (p0 - p2);
			t = tTest;
		}
		return t <= 1.0f;
	}

	bool pointInTriangle(Point3F pnt, Point3F a, Point3F b, Point3F c)
	{
		a -= pnt;
		b -= pnt;
		c -= pnt;
		Point3F u = mCross(b, c);
		Point3F v = mCross(c, a);
		// Using F32p here fixes the collision bug on MBG's King of the Mountain
		if (mDot(u, v) < 0.0f)
		{
			return false;
		}
		Point3F w = mCross(a, b);
		return mDot(u, w) >= 0.0f;
	}

	bool closestPtPointTriangle(Point3F pt, float radius, Point3F p0, Point3F p1, Point3F p2, Point3F normal, Point3F& closest)
	{
		float ptDot = mDot(pt, normal);
		float triDot = mDot(p0, normal);
		if (mFabs(ptDot - triDot) > radius * 1.1f)
		{
			return false;
		}
		closest = pt + (triDot - ptDot) * normal;
		if (pointInTriangle(closest, p0, p1, p2))
		{
			return true;
		}
		float t = 10.0f;
		float tTest;
		float tCap;
		if (intersectSegmentCapsule(pt, pt, p0, p1, radius, &tTest, &tCap) && tTest < t)
		{
			closest = p0 + tCap * (p1 - p0);
			t = tTest;
		}
		if (intersectSegmentCapsule(pt, pt, p1, p2, radius, &tTest, &tCap) && tTest < t)
		{
			closest = p1 + tCap * (p2 - p1);
			t = tTest;
		}
		if (intersectSegmentCapsule(pt, pt, p2, p0, radius, &tTest, &tCap) && tTest < t)
		{
			closest = p2 + tCap * (p0 - p2);
			t = tTest;
		}
		return t < 1.0f;
	}
}
