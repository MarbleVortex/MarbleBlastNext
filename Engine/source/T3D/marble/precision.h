#ifndef __PRECISION_H_
#define __PRECISION_H_

#define F32P_EPSILON 0.0001
#define F32P2_EPSILON 0.0001 //0.02145

struct F32p
{
public:
	float value;

	F32p(float value) : value(value)
	{

	}

	// Assignment
	F32p operator=(float val)
	{
		return F32p(val);
	}

	// Math
	F32p operator+(F32p other)
	{
		return this->value + other.value;
	}

	F32p operator-(F32p other)
	{
		return this->value - other.value;
	}

	F32p operator*(F32p other)
	{
		return this->value * other.value;
	}

	F32p operator/(F32p other)
	{
		return this->value / other.value;
	}

	// Comparison
	bool operator==(F32p other)
	{
		return (((other.value - F32P_EPSILON) < this->value) && (this->value < (other.value + F32P_EPSILON)));
	}

	bool operator!=(F32p other)
	{
		return !(*this == other);
	}

	bool operator<(F32p other)
	{
		//return this->value < other.value;
		return this->value < (other.value + F32P_EPSILON);
	}

	bool operator<=(F32p other)
	{
		return *this < other || *this == other;
	}

	bool operator>(F32p other)
	{
		//return this->value > other.value;
		return (other.value - F32P_EPSILON) < this->value;
	}

	bool operator>=(F32p other)
	{
		return *this > other || *this == other;
	}
};

struct F32p2
{
public:
	float value;

	F32p2(float value) : value(value)
	{

	}

	// Assignment
	F32p2 operator=(float val)
	{
		return F32p2(val);
	}

	// Math
	F32p2 operator+(F32p2 other)
	{
		return this->value + other.value;
	}

	F32p2 operator-(F32p2 other)
	{
		return this->value - other.value;
	}

	F32p2 operator*(F32p2 other)
	{
		return this->value * other.value;
	}

	F32p2 operator/(F32p2 other)
	{
		return this->value / other.value;
	}

	// Comparison
	bool operator==(F32p2 other)
	{
		return (((other.value - F32P2_EPSILON) < this->value) && (this->value < (other.value + F32P2_EPSILON)));
	}

	bool operator!=(F32p2 other)
	{
		return !(*this == other);
	}

	bool operator<(F32p2 other)
	{
		//return this->value < other.value;
		return this->value < (other.value + F32P2_EPSILON);
	}

	bool operator<=(F32p2 other)
	{
		return *this < other || *this == other;
	}

	bool operator>(F32p2 other)
	{
		//return this->value > other.value;
		return (other.value - F32P2_EPSILON) < this->value;
	}

	bool operator>=(F32p2 other)
	{
		return *this > other || *this == other;
	}
};

#endif // __PRECISION_H_