#include "T3D/marble/powerup.h"

#include "core/stream/bitStream.h"
#include "math/mathIO.h"

IMPLEMENT_CO_DATABLOCK_V1(PowerUpData);

PowerUpData::PowerUpData()
{
	/*for (int i = 0; i < MAX_POWERUPS; i++)
	{
		mPowerUps[i].emitter = NULL;
		mPowerUps[i].duration = 0;
		mPowerUps[i].activateTime = 0;
		mPowerUps[i].image = NULL;
		mPowerUps[i].boostDir = Point3F(0, 0, 0);
		mPowerUps[i].boostAmount = 0.0f;
		mPowerUps[i].boostMassless = 0.0f;
		mPowerUps[i].timeFreeze = 0;
		mPowerUps[i].blastRecharge = false;
		mPowerUps[i].airAccel = 0.0f;
		mPowerUps[i].gravityMod = 0.0f;
		mPowerUps[i].bounce = 0.0f;
		mPowerUps[i].repulseMax = 0.0f;
		mPowerUps[i].repulseDist = 0.0f;
		mPowerUps[i].massScale = 0.0f;
		mPowerUps[i].sizeScale = 0.0f;
	}*/

	for (int i = 0; i < MAX_POWERUPS; i++)
	{
		mEmitter[i] = NULL;
		mDuration[i] = 0;
		mActivateTime[i] = 0;
		mImage[i] = NULL;
		mBoostDir[i] = Point3F(0, 0, 0);
		mBoostAmount[i] = 0.0f;
		mBoostMassless[i] = 0.0f;
		mTimeFreeze[i] = 0;
		mBlastRecharge[i] = false;
		mAirAccel[i] = 0.0f;
		mGravityMod[i] = 0.0f;
		mBounce[i] = -1.0f;
		mBoost[i] = 0.0f;
		mRepulseMax[i] = 0.0f;
		mRepulseDist[i] = 0.0f;
		mMassScale[i] = 0.0f;
		mSizeScale[i] = 0.0f;
		mEnableParkour[i] = false;
	}
}

PowerUpData::~PowerUpData()
{

}

void PowerUpData::initPersistFields()
{
	/*ConsoleObject::addField("emitter", TypeParticleEmitterDataPtr, Offset(mPowerUps, PowerUpData) + Offset(emitter, PowerUp) * MAX_POWERUPS, MAX_POWERUPS);
	ConsoleObject::addField("duration", TypeS32, Offset(mPowerUps, PowerUpData) + Offset(duration, PowerUp) * MAX_POWERUPS, MAX_POWERUPS);
	ConsoleObject::addField("activateTime", TypeS32, Offset(mPowerUps, PowerUpData) + Offset(activateTime, PowerUp) * MAX_POWERUPS, MAX_POWERUPS);
	ConsoleObject::addField("image", TypeGameBaseDataPtr, Offset(mPowerUps, PowerUpData) + Offset(image, PowerUp) * MAX_POWERUPS, MAX_POWERUPS);
	ConsoleObject::addField("boostDir", TypePoint3F, Offset(mPowerUps, PowerUpData) + Offset(boostDir, PowerUp) * MAX_POWERUPS, MAX_POWERUPS);
	ConsoleObject::addField("boostAmount", TypeF32, Offset(mPowerUps, PowerUpData) + Offset(boostAmount, PowerUp) * MAX_POWERUPS, MAX_POWERUPS);
	ConsoleObject::addField("boostMassless", TypeF32, Offset(mPowerUps, PowerUpData) + Offset(boostMassless, PowerUp) * MAX_POWERUPS, MAX_POWERUPS);
	ConsoleObject::addField("timeFreeze", TypeS32, Offset(mPowerUps, PowerUpData) + Offset(timeFreeze, PowerUp) * MAX_POWERUPS, MAX_POWERUPS);
	ConsoleObject::addField("blastRecharge", TypeBool, Offset(mPowerUps, PowerUpData) + Offset(blastRecharge, PowerUp) * MAX_POWERUPS, MAX_POWERUPS);
	ConsoleObject::addField("airAccel", TypeF32, Offset(mPowerUps, PowerUpData) + Offset(airAccel, PowerUp) * MAX_POWERUPS, MAX_POWERUPS);
	ConsoleObject::addField("gravityMod", TypeF32, Offset(mPowerUps, PowerUpData) + Offset(gravityMod, PowerUp) * MAX_POWERUPS, MAX_POWERUPS);
	ConsoleObject::addField("bounce", TypeF32, Offset(mPowerUps, PowerUpData) + Offset(bounce, PowerUp) * MAX_POWERUPS, MAX_POWERUPS);
	ConsoleObject::addField("repulseMax", TypeF32, Offset(mPowerUps, PowerUpData) + Offset(repulseMax, PowerUp) * MAX_POWERUPS, MAX_POWERUPS);
	ConsoleObject::addField("repulseDist", TypeF32, Offset(mPowerUps, PowerUpData) + Offset(repulseDist, PowerUp) * MAX_POWERUPS, MAX_POWERUPS);
	ConsoleObject::addField("massScale", TypeF32, Offset(mPowerUps, PowerUpData) + Offset(massScale, PowerUp) * MAX_POWERUPS, MAX_POWERUPS);
	ConsoleObject::addField("sizeScale", TypeF32, Offset(mPowerUps, PowerUpData) + Offset(sizeScale, PowerUp) * MAX_POWERUPS, MAX_POWERUPS);*/

	ConsoleObject::addField("emitter", TYPEID< ParticleEmitterData >(), Offset(mEmitter, PowerUpData), MAX_POWERUPS);
	ConsoleObject::addField("duration", TypeS32, Offset(mDuration, PowerUpData), MAX_POWERUPS);
	ConsoleObject::addField("activateTime", TypeS32, Offset(mActivateTime, PowerUpData), MAX_POWERUPS);
	ConsoleObject::addField("image", TYPEID< GameBaseData >(), Offset(mImage, PowerUpData), MAX_POWERUPS);
	ConsoleObject::addField("boostDir", TypePoint3F, Offset(mBoostDir, PowerUpData), MAX_POWERUPS);
	ConsoleObject::addField("boostAmount", TypeF32, Offset(mBoostAmount, PowerUpData), MAX_POWERUPS);
	ConsoleObject::addField("boostMassless", TypeF32, Offset(mBoostMassless, PowerUpData), MAX_POWERUPS);
	ConsoleObject::addField("timeFreeze", TypeS32, Offset(mTimeFreeze, PowerUpData), MAX_POWERUPS);
	ConsoleObject::addField("blastRecharge", TypeBool, Offset(mBlastRecharge, PowerUpData), MAX_POWERUPS);
	ConsoleObject::addField("airAccel", TypeF32, Offset(mAirAccel, PowerUpData), MAX_POWERUPS);
	ConsoleObject::addField("gravityMod", TypeF32, Offset(mGravityMod, PowerUpData), MAX_POWERUPS);
	ConsoleObject::addField("bounce", TypeF32, Offset(mBounce, PowerUpData), MAX_POWERUPS);
	ConsoleObject::addField("boost", TypeF32, Offset(mBoost, PowerUpData), MAX_POWERUPS);
	ConsoleObject::addField("repulseMax", TypeF32, Offset(mRepulseMax, PowerUpData), MAX_POWERUPS);
	ConsoleObject::addField("repulseDist", TypeF32, Offset(mRepulseDist, PowerUpData), MAX_POWERUPS);
	ConsoleObject::addField("massScale", TypeF32, Offset(mMassScale, PowerUpData), MAX_POWERUPS);
	ConsoleObject::addField("sizeScale", TypeF32, Offset(mSizeScale, PowerUpData), MAX_POWERUPS);
	ConsoleObject::addField("enableParkour", TypeBool, Offset(mEnableParkour, PowerUpData), MAX_POWERUPS);

	Parent::initPersistFields();
}

/*bool PowerUpData::preload(bool server, char errorBuffer[256])
{
	if (!Parent::preload(server, errorBuffer))
		return false;

	for (int i = 0; i < MAX_POWERUPS; i++)
	{
		mPowerUps[i].emitter = mEmitter[i];
		mPowerUps[i].duration = mDuration[i];
		mPowerUps[i].activateTime = mActivateTime[i];
		mPowerUps[i].image = mImage[i];
		mPowerUps[i].boostDir = mBoostDir[i];
		mPowerUps[i].boostAmount = mBoostAmount[i];
		mPowerUps[i].boostMassless = mBoostMassless[i];
		mPowerUps[i].timeFreeze = mTimeFreeze[i];
		mPowerUps[i].blastRecharge = mBlastRecharge[i];
		mPowerUps[i].airAccel = mAirAccel[i];
		mPowerUps[i].gravityMod = mGravityMod[i];
		mPowerUps[i].bounce = mBounce[i];
		mPowerUps[i].repulseMax = mRepulseMax[i];
		mPowerUps[i].repulseDist = mRepulseDist[i];
		mPowerUps[i].massScale = mMassScale[i];
		mPowerUps[i].sizeScale = mSizeScale[i];
	}

	return true;
}*/

void PowerUpData::packData(BitStream* stream)
{
	Parent::packData(stream);

	/*for (int i = 0; i < MAX_POWERUPS; i++)
	{
		//stream->write?(mPowerUps[i].mEmitter);
		stream->write(mPowerUps[i].duration);
		stream->write(mPowerUps[i].activateTime);
		//stream->write?(mPowerUps[i].mImage);
		mathWrite(*stream, mPowerUps[i].boostDir);
		stream->write(mPowerUps[i].boostAmount);
		stream->write(mPowerUps[i].boostMassless);
		stream->write(mPowerUps[i].timeFreeze);
		stream->write(mPowerUps[i].blastRecharge);
		stream->write(mPowerUps[i].airAccel);
		stream->write(mPowerUps[i].gravityMod);
		stream->write(mPowerUps[i].bounce);
		stream->write(mPowerUps[i].repulseMax);
		stream->write(mPowerUps[i].repulseDist);
		stream->write(mPowerUps[i].massScale);
		stream->write(mPowerUps[i].sizeScale);
	}*/

	for (int i = 0; i < MAX_POWERUPS; i++)
	{
		//stream->write?(mEmitter[i]);
		stream->write(mDuration[i]);
		stream->write(mActivateTime[i]);
		//stream->write?(mImage[i]);
		mathWrite(*stream, mBoostDir[i]);
		stream->write(mBoostAmount[i]);
		stream->write(mBoostMassless[i]);
		stream->write(mTimeFreeze[i]);
		stream->write(mBlastRecharge[i]);
		stream->write(mAirAccel[i]);
		stream->write(mGravityMod[i]);
		stream->write(mBounce[i]);
		stream->write(mBoost[i]);
		stream->write(mRepulseMax[i]);
		stream->write(mRepulseDist[i]);
		stream->write(mMassScale[i]);
		stream->write(mSizeScale[i]);
		stream->write(mEnableParkour[i]);
	}
}

void PowerUpData::unpackData(BitStream* stream)
{
	Parent::unpackData(stream);

	/*for (int i = 0; i < MAX_POWERUPS; i++)
	{
		//stream->read?(&mPowerUps[i].mEmitter);
		stream->read(&mPowerUps[i].duration);
		stream->read(&mPowerUps[i].activateTime);
		//stream->read?(&mPowerUps[i].mImage);
		mathRead(*stream, &mPowerUps[i].boostDir);
		stream->read(&mPowerUps[i].boostAmount);
		stream->read(&mPowerUps[i].boostMassless);
		stream->read(&mPowerUps[i].timeFreeze);
		stream->read(&mPowerUps[i].blastRecharge);
		stream->read(&mPowerUps[i].airAccel);
		stream->read(&mPowerUps[i].gravityMod);
		stream->read(&mPowerUps[i].bounce);
		stream->read(&mPowerUps[i].repulseMax);
		stream->read(&mPowerUps[i].repulseDist);
		stream->read(&mPowerUps[i].massScale);
		stream->read(&mPowerUps[i].sizeScale);
	}*/

	for (int i = 0; i < MAX_POWERUPS; i++)
	{
		//stream->read?(&mEmitter[i]);
		stream->read(&mDuration[i]);
		stream->read(&mActivateTime[i]);
		//stream->read?(&mImage[i]);
		mathRead(*stream, &mBoostDir[i]);
		stream->read(&mBoostAmount[i]);
		stream->read(&mBoostMassless[i]);
		stream->read(&mTimeFreeze[i]);
		stream->read(&mBlastRecharge[i]);
		stream->read(&mAirAccel[i]);
		stream->read(&mGravityMod[i]);
		stream->read(&mBounce[i]);
		stream->read(&mBoost[i]);
		stream->read(&mRepulseMax[i]);
		stream->read(&mRepulseDist[i]);
		stream->read(&mMassScale[i]);
		stream->read(&mSizeScale[i]);
		stream->read(&mEnableParkour[i]);
	}
}

PowerUp* PowerUpData::getPowerUp(S32 index)
{
	PowerUp* powerUp = new PowerUp();

	powerUp->emitter = mEmitter[index];
	powerUp->duration = mDuration[index];
	powerUp->activateTime = mActivateTime[index];
	powerUp->image = mImage[index];
	powerUp->boostDir = mBoostDir[index];
	powerUp->boostAmount = mBoostAmount[index];
	powerUp->boostMassless = mBoostMassless[index];
	powerUp->timeFreeze = mTimeFreeze[index];
	powerUp->blastRecharge = mBlastRecharge[index];
	powerUp->airAccel = mAirAccel[index];
	powerUp->gravityMod = mGravityMod[index];
	powerUp->bounce = mBounce[index];
	powerUp->boost = mBoost[index];
	powerUp->repulseMax = mRepulseMax[index];
	powerUp->repulseDist = mRepulseDist[index];
	powerUp->massScale = mMassScale[index];
	powerUp->sizeScale = mSizeScale[index];
	powerUp->enableParkour = mEnableParkour[index];

	/*powerUp.emitter = mEmitter[index];
	powerUp.duration = mDuration[index];
	powerUp.activateTime = mActivateTime[index];
	powerUp.image = mImage[index];
	mImage[i] = NULL;
	mBoostDir[i] = Point3F(0, 0, 0);
	mBoostAmount[i] = 0.0f;
	mBoostMassless[i] = 0.0f;
	mTimeFreeze[i] = 0;
	mBlastRecharge[i] = false;
	mAirAccel[i] = 0.0f;
	mGravityMod[i] = 0.0f;
	mBounce[i] = 0.0f;
	mRepulseMax[i] = 0.0f;
	mRepulseDist[i] = 0.0f;
	mMassScale[i] = 0.0f;
	mSizeScale[i] = 0.0f;*/

	return powerUp;
}
