#include "T3D/marble/marble.h"

#include "T3D/fx/cameraFXMgr.h"
#include "core/iTickable.h"
#include "core/stream/bitStream.h"
#include "console/consoleTypes.h"
#include "T3D/marble/collisionUtil.h"
#include "math/mathIO.h"
#include "math/mTransform.h"
#include "sfx/sfxSystem.h"
#include "sfx/sfxTrack.h"
#include "sfx/sfxSource.h"
#include "sfx/sfxTypes.h"
#include "sfx/sfxProfile.h"
#include "gfx/primBuilder.h"
#include "T3D/gameBase/gameConnection.h"
#include "T3D/trigger.h"
#include "T3D/staticShape.h"
#include "console/engineAPI.h"
#include "ts/tsMaterialList.h"
#include "materials/matInstance.h"
#include "gfx/sim/debugDraw.h"

#include "interior/pathedInterior.h"

#include "T3D/marble/marbleBlastUtil.h"

#include "T3D/marble/precision.h"
#include <string>

using namespace MarbleCollision;

// Client prediction
static F32 sMinWarpTicks = 0.5;        // Fraction of tick at which instant warp occures
static S32 sMaxWarpTicks = 3;           // Max warp duration in ticks

bool Marble::smHitShake = true;
//F32 Marble::smCollisionTolerance = 0.0337368f; //0.0001f;

static U32 sClientCollisionMask =
TerrainObjectType | InteriorObjectType |
PlayerObjectType | StaticShapeObjectType |
VehicleObjectType | VehicleBlockerObjectType;// |
//StaticTSObjectType | AtlasObjectType;

IMPLEMENT_CO_DATABLOCK_V1(MarbleData);

IMPLEMENT_CALLBACK(MarbleData, onEnterLiquid, void, (Marble* obj, F32 coverage, const char* type), (obj, coverage, type),
	"Called when the marble enters liquid.\n"
	"@param obj the Marble object\n"
	"@param coverage percentage of the marble's bounding box covered by the liquid\n"
	"@param type type of liquid the marble has entered\n");

IMPLEMENT_CALLBACK(MarbleData, onUpdateLiquid, void, (Marble* obj, F32 coverage, const char* type), (obj, coverage, type),
	"Called when the marble is in liquid.\n"
	"@param obj the Marble object\n"
	"@param coverage percentage of the marble's bounding box covered by the liquid\n"
	"@param type type of liquid the marble has entered\n");

IMPLEMENT_CALLBACK(MarbleData, onLeaveLiquid, void, (Marble* obj, const char* type), (obj, type),
	"Called when the marble leaves liquid.\n"
	"@param obj the Marble object\n"
	"@param type type of liquid the marble has left\n");

MarbleData::MarbleData()
{
	for (S32 i = 0; i < MaxSounds; i++)
		sound[i] = NULL;

	this->mMaxRollVelocity = 15.0f;
	this->mAngularAcceleration = 75.0f;
	this->mJumpImpulse = 7.5f;
	this->mKineticFriction = 0.7f;
	this->mStaticFriction = 1.1f;
	this->mBrakingAcceleration = 30.0f;
	this->mGravity = 20.0f;
	this->mAirAccel = 5.0f;
	this->mEnergyRechargeRate = 36000.0f;
	this->mMaxNaturalBlastRecharge = 30000.0f;
	this->mMaxDotSlide = 0.5f;
	this->mMinBounceVel = 0.1f;
	this->mBounceKineticFriction = 0.2f;
	this->mBounceRestitution = 0.5f;
	this->mSize = 1.0f;
	this->mBounceEmitter = NULL;
	this->mTrailEmitter = NULL;
	this->mPowerUpEmitter = NULL;

	this->mPowerUpsName = NULL;
	this->mPowerUps = NULL;

	// TEMP
	//this->mCollisionRadius = 0.3f;
}

MarbleData::~MarbleData()
{

}

bool MarbleData::preload(bool server, String &errorStr)
{
	if (!Parent::preload(server, errorStr))
		return false;

	if (!(shapeName && shapeName[0]))
	{
		errorStr = String::ToString("MarbleData: No Shape Specified");
		return false;
	}

	if (!server)
	{
		for (S32 i = 0; i < MaxSounds; i++)
		{
			if (sound[i])
				Sim::findObject(SimObjectId((uintptr_t)sound[i]), sound[i]);
		}

		if (this->mBounceEmitter)
			Sim::findObject(SimObjectId((uintptr_t)this->mBounceEmitter), this->mBounceEmitter);
		if (this->mTrailEmitter)
			Sim::findObject(SimObjectId((uintptr_t)this->mTrailEmitter), this->mTrailEmitter);
		if (this->mPowerUpEmitter)
			Sim::findObject(SimObjectId((uintptr_t)this->mPowerUpEmitter), this->mPowerUpEmitter);
	}

	if (mPowerUpsName != NULL)
		mPowerUps = dynamic_cast<PowerUpData*>(Sim::findObject(mPowerUpsName));

	return true;
}

void MarbleData::initPersistFields()
{
	// Physics
	ConsoleObject::addField("maxRollVelocity", TypeF32, Offset(mMaxRollVelocity, MarbleData));
	ConsoleObject::addField("angularAcceleration", TypeF32, Offset(mAngularAcceleration, MarbleData));
	ConsoleObject::addField("brakingAcceleration", TypeF32, Offset(mBrakingAcceleration, MarbleData));
	ConsoleObject::addField("staticFriction", TypeF32, Offset(mStaticFriction, MarbleData));
	ConsoleObject::addField("kineticFriction", TypeF32, Offset(mKineticFriction, MarbleData));
	ConsoleObject::addField("bounceKineticFriction", TypeF32, Offset(mBounceKineticFriction, MarbleData));
	ConsoleObject::addField("gravity", TypeF32, Offset(mGravity, MarbleData));
	ConsoleObject::addField("maxDotSlide", TypeF32, Offset(mMaxDotSlide, MarbleData));
	ConsoleObject::addField("bounceRestitution", TypeF32, Offset(mBounceRestitution, MarbleData));
	ConsoleObject::addField("airAcceleration", TypeF32, Offset(mAirAccel, MarbleData));
	ConsoleObject::addField("energyRechargeRate", TypeF32, Offset(mEnergyRechargeRate, MarbleData));
	ConsoleObject::addField("maxNaturalBlastRecharge", TypeF32, Offset(mMaxNaturalBlastRecharge, MarbleData));
	ConsoleObject::addField("jumpImpulse", TypeF32, Offset(mJumpImpulse, MarbleData));
	ConsoleObject::addField("maxForceRadius", TypeF32, Offset(mMaxForceRadius, MarbleData));
	ConsoleObject::addField("cameraDistance", TypeF32, Offset(mCameraDistance, MarbleData));
	ConsoleObject::addField("minBounceVel", TypeF32, Offset(mMinBounceVel, MarbleData));
	ConsoleObject::addField("minTrailSpeed", TypeF32, Offset(mMinTrailSpeed, MarbleData));
	ConsoleObject::addField("minBounceSpeed", TypeF32, Offset(mMinBounceSpeed, MarbleData));
	ConsoleObject::addField("size", TypeF32, Offset(mSize, MarbleData));

	ConsoleObject::addField("powerUps", TypeString, Offset(mPowerUpsName, MarbleData));

	// TEMP
	//ConsoleObject::addField("collisionRadius", TypeF32, Offset(mCollisionRadius, MarbleData));

	// Particles
	ConsoleObject::addField("bounceEmitter", TYPEID< ParticleEmitterData >(), Offset(mBounceEmitter, MarbleData));
	ConsoleObject::addField("trailEmitter", TYPEID< ParticleEmitterData >(), Offset(mTrailEmitter, MarbleData));
	ConsoleObject::addField("powerUpEmitter", TYPEID< ParticleEmitterData >(), Offset(mPowerUpEmitter, MarbleData));

	ConsoleObject::addField("powerUpTime", TypeS32, Offset(mPowerUpTime, MarbleData));

	// Sounds
	ConsoleObject::addField("RollHardSound", TYPEID< SFXProfile >(), Offset(sound[RollHardSound], MarbleData));
	ConsoleObject::addField("RollMegaSound", TYPEID< SFXProfile >(), Offset(sound[RollMegaSound], MarbleData));
	ConsoleObject::addField("RollIceSound", TYPEID< SFXProfile >(), Offset(sound[RollIceSound], MarbleData));
	ConsoleObject::addField("SlipSound", TYPEID< SFXProfile >(), Offset(sound[SlipSound], MarbleData));
	ConsoleObject::addField("Bounce1", TYPEID< SFXProfile >(), Offset(sound[Bounce1], MarbleData));
	ConsoleObject::addField("Bounce2", TYPEID< SFXProfile >(), Offset(sound[Bounce2], MarbleData));
	ConsoleObject::addField("Bounce3", TYPEID< SFXProfile >(), Offset(sound[Bounce3], MarbleData));
	ConsoleObject::addField("Bounce4", TYPEID< SFXProfile >(), Offset(sound[Bounce4], MarbleData));
	ConsoleObject::addField("MegaBounce1", TYPEID< SFXProfile >(), Offset(sound[MegaBounce1], MarbleData));
	ConsoleObject::addField("MegaBounce2", TYPEID< SFXProfile >(), Offset(sound[MegaBounce2], MarbleData));
	ConsoleObject::addField("MegaBounce3", TYPEID< SFXProfile >(), Offset(sound[MegaBounce3], MarbleData));
	ConsoleObject::addField("MegaBounce4", TYPEID< SFXProfile >(), Offset(sound[MegaBounce4], MarbleData));
	ConsoleObject::addField("JumpSound", TYPEID< SFXProfile >(), Offset(sound[JumpSound], MarbleData));

	Parent::initPersistFields();
}

void MarbleData::packData(BitStream* stream)
{
	Parent::packData(stream);

	for (int i = 0; i < MaxSounds; i++)
	{
		if (stream->writeFlag(sound[i]))
		{
			SimObjectId writtenId = packed ? SimObjectId((uintptr_t)sound[i]) : sound[i]->getId();
			stream->writeRangedU32(writtenId, DataBlockObjectIdFirst, DataBlockObjectIdLast);
		}
	}

	stream->write(this->mMaxRollVelocity);
	stream->write(this->mAngularAcceleration);
	stream->write(this->mJumpImpulse);
	stream->write(this->mKineticFriction);
	stream->write(this->mStaticFriction);
	stream->write(this->mBrakingAcceleration);
	stream->write(this->mGravity);
	stream->write(this->mAirAccel);
	stream->write(this->mEnergyRechargeRate);
	stream->write(this->mMaxNaturalBlastRecharge);
	stream->write(this->mMaxDotSlide);
	stream->write(this->mMinBounceVel);
	stream->write(this->mBounceKineticFriction);
	stream->write(this->mBounceRestitution);
	stream->write(this->mSize);

	if (stream->writeFlag(this->mBounceEmitter))
		stream->writeRangedU32(packed ? SimObjectId((uintptr_t)(this->mBounceEmitter)) : this->mBounceEmitter->getId(), DataBlockObjectIdFirst, DataBlockObjectIdLast);
	if (stream->writeFlag(this->mTrailEmitter))
		stream->writeRangedU32(packed ? SimObjectId((uintptr_t)(this->mTrailEmitter)) : this->mTrailEmitter->getId(), DataBlockObjectIdFirst, DataBlockObjectIdLast);
	if (stream->writeFlag(this->mPowerUpEmitter))
		stream->writeRangedU32(packed ? SimObjectId((uintptr_t)(this->mPowerUpEmitter)) : this->mPowerUpEmitter->getId(), DataBlockObjectIdFirst, DataBlockObjectIdLast);
}

void MarbleData::unpackData(BitStream* stream)
{
	Parent::unpackData(stream);

	for (int i = 0; i < MaxSounds; i++)
	{
		sound[i] = NULL;
		if (stream->readFlag())
		{
			sound[i] = (SFXProfile*)stream->readRangedU32(DataBlockObjectIdFirst, DataBlockObjectIdLast);
		}
	}

	stream->read(&this->mMaxRollVelocity);
	stream->read(&this->mAngularAcceleration);
	stream->read(&this->mJumpImpulse);
	stream->read(&this->mKineticFriction);
	stream->read(&this->mStaticFriction);
	stream->read(&this->mBrakingAcceleration);
	stream->read(&this->mGravity);
	stream->read(&this->mAirAccel);
	stream->read(&this->mEnergyRechargeRate);
	stream->read(&this->mMaxNaturalBlastRecharge);
	stream->read(&this->mMaxDotSlide);
	stream->read(&this->mMinBounceVel);
	stream->read(&this->mBounceKineticFriction);
	stream->read(&this->mBounceRestitution);
	stream->read(&this->mSize);

	if (stream->readFlag())
		this->mBounceEmitter = (ParticleEmitterData*)stream->readRangedU32(DataBlockObjectIdFirst, DataBlockObjectIdLast);
	if (stream->readFlag())
		this->mTrailEmitter = (ParticleEmitterData*)stream->readRangedU32(DataBlockObjectIdFirst, DataBlockObjectIdLast);
	if (stream->readFlag())
		this->mPowerUpEmitter = (ParticleEmitterData*)stream->readRangedU32(DataBlockObjectIdFirst, DataBlockObjectIdLast);
}

//static Marble* sMarble = NULL;

IMPLEMENT_CO_NETOBJECT_V1(Marble);

Marble::Marble()
{
	mTypeMask |= PlayerObjectType;

	mNetFlags.set(Ghostable);

	this->mDataBlock = NULL;
	this->mCameraY = 0;
	this->mCameraX = 0;
	this->mVelocity = Point3F(0, 0, 0);
	this->mAppliedForce = Point3F(0, 0, 0);
	this->mRotVelocity = Point3F(0, 0, 0);
	this->mBouncePos = Point3F(0, 0, 0);
	this->mBounceNormal = Point3F(0, 0, 0);
	this->mBounceYet = false;
	this->mOnIce = false;
	this->mSlipping = false;
	this->mBounceSpeed = 0;
	this->mSlipAmount = 0;
	this->mContactTime = 0;
	this->mTotalTime = 0;
	this->mStartPos = Point3F(0, 0, 0);
	this->mMove = NULL;
	this->mElasticity = 0.0f;
	this->mFriction = 0.8f;
	this->mVisibilityLevel = 1.0f;
	this->mGravity = Point3F(0, 0, 0);
	this->mRollVolume = 0.0f;
	this->mRollHandle = NULL;
	this->mRollMegaHandle = NULL;
	this->mSlipHandle = NULL;
	this->mStatic = false;
	this->mDisablePhysics = false;
	this->mDisableRotation = false;
	this->mDisableCollision = false;
	this->mIgnoreMarbles = false;
	this->mContacts = CollisionList(); //Vector<CollisionInfo>();
	//this->mSpawnDelay = false;
	this->mOutOfBounds = false;
	this->mMode = Marble::Normal;
	this->mUseFullTime = false;
	this->mTime = 0;
	this->mBonusTime = 0;
	this->mBlastEnergy = 0.0f;
	this->mPowerUpId = 0;
	this->mPowerUpReset = false;
	// TODO: Verify if this should be an identity matrix or not
	this->mGravityDir = Point3F(0, 0, -1);
	this->mGravityMatrix = MatrixF(1);
	//this->mGravityMatrix.
	this->mGravitySnap = false;

	this->mPad = NULL;
	this->mOnPad = false;
	
	this->mCurrentBounce = 0.0f;
	this->mCurrentGravity = 0.0f;
	this->mCurrentAirAccel = 0.0f;
	this->mCurrentMass = 0.0f;
	this->mDesiredSize = 1.0f;
	this->mCurrentSize = this->mDesiredSize;

	this->inLiquid = false;

	//this->mActivePowerUp = NULL;
	//this->mActivePowerUpTimer = 0;
	//this->mActivePowerUpEmitter = NULL;
	//this->mActiveBlast = false;

	// TODO: TEMP TIMER
	//this->mTempBlastTimer = 0;

	//this->peakTestLoopCount = 0;

	this->mLastContactPosition = Point3F(0, 0, 0);

	this->mCameraAngle = AngAxisF(Point3F(0.0f, 1.0f, 0.0f), 0.0f);
	this->mCameraYAngle = Point3F(0.0f, 0.0f, 1.0f);
	this->mCameraPAngle = Point3F(1.0f, 0.0f, 0.0f);

	this->mCameraAnglePB = false;
	this->mCameraAngleYB = false;
	this->mCameraAngleFlip = false;
	this->mCameraAngleOrderFlip = false;

	//this->mJumped = false;

	this->mBlastTimer = 0.0f;

	this->mHitShake = false;

	/*delta.pos = Point3F(0, 0, 0);
	delta.rot = Point3F(0, 0, 0);
	delta.rotOffset.set(0.0f, 0.0f, 0.0f);
	delta.warpOffset.set(0.0f, 0.0f, 0.0f);
	delta.posVec.set(0.0f, 0.0f, 0.0f);
	delta.rotVec.set(0.0f, 0.0f, 0.0f);
	delta.warpTicks = 0;
	delta.dt = 1.0f;*/

	this->mParkour = false;
}

Marble::~Marble()
{
	//if (this->mRollHandle != NULL)
	//alxStop(this->mRollHandle);
}

void Marble::initPersistFields()
{
	ConsoleObject::addField("Controllable", TypeBool, Offset(mControllable, Marble));
	ConsoleObject::addField("static", TypeBool, Offset(mStatic, Marble));
	ConsoleObject::addField("disablePhysics", TypeBool, Offset(mDisablePhysics, Marble));
	ConsoleObject::addField("disableRotation", TypeBool, Offset(mDisableRotation, Marble));
	ConsoleObject::addField("disableCollision", TypeBool, Offset(mDisableCollision, Marble));
	ConsoleObject::addField("ignoreMarbles", TypeBool, Offset(mIgnoreMarbles, Marble));
	Parent::initPersistFields();
}

void Marble::consoleInit()
{
	Con::addVariable("pref::Marble::hitShake", TypeBool, &smHitShake, "Shake the camera when the marble hits a platform.");

	Con::addVariable("Marble::minWarpTicks", TypeF32, &sMinWarpTicks,
		"@brief Fraction of tick at which instant warp occures on the client.\n\n"
		"@ingroup GameObjects");
	Con::addVariable("Marble::maxWarpTicks", TypeS32, &sMaxWarpTicks,
		"@brief When a warp needs to occur due to the client being too far off from the server, this is the "
		"maximum number of ticks we'll allow the client to warp to catch up.\n\n"
		"@ingroup GameObjects");

	// Temp
	//Con::addVariable("pref::Marble::collisionTolerance", TypeF32, &smCollisionTolerance, "Collision Tolerance (Temporary for Debugging Purposes)");
}

bool Marble::onAdd()
{
	if (!Parent::onAdd() || !mDataBlock)
		return false;

	this->mCurrentBounce = this->mDataBlock->mBounceRestitution;
	this->mCurrentGravity = this->mDataBlock->mGravity;
	this->mCurrentAirAccel = this->mDataBlock->mAirAccel;
	this->mCurrentMass = this->mDataBlock->mass;
	this->mDesiredSize = this->mDataBlock->mSize;
	this->mCurrentSize = this->mDesiredSize;

	this->mRadius = this->mCurrentSize / 5.0f; //this->mDataBlock->mCollisionRadius;

	F32 scale = this->mCurrentSize;

	this->mObjScale = Point3F(scale, scale, scale);

	//this->mObjBox.min = Point3F(-this->mRadius, -this->mRadius, -this->mRadius);
	//this->mObjBox.max = Point3F(this->mRadius, this->mRadius, this->mRadius);
	//this->resetWorldBox();

	setMaskBits(UpdateMask);

	/*Box3F searchBox = this->mObjBox;
	Point3F rad = Point3F(this->mRadius, this->mRadius, this->mRadius);
	searchBox.min -= rad;
	searchBox.max += rad;
	mBoundingBox = searchBox;

	mBoundingSphere = SphereF(Point3F(0, 0, 0), this->mRadius);*/

	addToScene();

	if (isServerObject())
		scriptOnAdd();

	setMaskBits(InitialMask);

	return true;
}

void Marble::onRemove()
{
	SFX_DELETE(mSlipHandle);
	SFX_DELETE(mRollMegaHandle);
	SFX_DELETE(mRollHandle);

	scriptOnRemove();
	removeFromScene();
	Parent::onRemove();
}

bool Marble::onNewDataBlock(GameBaseData* dptr, bool reload)
{
	mDataBlock = dynamic_cast<MarbleData*>(dptr);
	if (!mDataBlock || !Parent::onNewDataBlock(dptr, reload))
		return false;

	if (isGhost())
	{
		SFX_DELETE(this->mSlipHandle);
		SFX_DELETE(this->mRollMegaHandle);
		SFX_DELETE(this->mRollHandle);

		this->mSlipHandle = SFX->createSource(this->mDataBlock->sound[this->mDataBlock->SlipSound]);
		this->mRollMegaHandle = SFX->createSource(this->mDataBlock->sound[this->mDataBlock->RollMegaSound]);
		this->mRollHandle = SFX->createSource(this->mDataBlock->sound[this->mDataBlock->RollHardSound]);
	}

	/*Box3F bounds = mDataBlock->shape->bounds;
	this->mObjBox.min.x = bounds.min.x;
	this->mObjBox.min.y = bounds.min.y;
	this->mObjBox.min.z = bounds.min.z;
	this->mObjBox.max.x = bounds.max.x;
	this->mObjBox.max.y = bounds.max.y;
	this->mObjBox.max.z = bounds.max.z;
	SphereF sphere;
	sphere.center.x = this->mObjBox.max.x - this->mObjBox.min.x;
	sphere.center.y = this->mObjBox.max.y - this->mObjBox.min.y;
	sphere.center.z = this->mObjBox.max.z - this->mObjBox.min.z;
	sphere.radius = sphere.center.x * 0.5;
	this->mObjBox.min.x = sphere.center.x * -0.5;
	this->mObjBox.min.y = sphere.center.y * -0.5;
	this->mObjBox.min.z = sphere.center.z * -0.5;
	this->mObjBox.max.z = sphere.center.z * 0.5;
	this->mObjBox.max.y = sphere.center.y * 0.5;
	this->mObjBox.max.x = sphere.center.x * 0.5;

	// Decompiling MBG in IDA reveals the following
	// line, no clue what it does yet.
	//&this->something = sphere.radius;

	// Possibly this?:
	this->mWorldSphere = sphere;

	this->resetWorldBox();*/

	//this->mObjBox.min = Point3F(-0.3f, -0.3f, -0.3f);
	//this->mObjBox.max = Point3F(0.3f, 0.3f, 0.3f);
	//this->resetWorldBox();

	//F32 scale = mDataBlock->mSize;
	//
	//this->mObjScale = Point3F(scale, scale, scale);

	scriptOnNewDataBlock();

	return true;
}

void Marble::getCameraTransform(F32* pos, MatrixF* mat)
{
	bool zPos = F32p(mGravityDir.x) == F32p(0.0f) && F32p(mGravityDir.y) == F32p(0.0f) && F32p(mGravityDir.z) == F32p(1.0f);
	bool zNeg = F32p(mGravityDir.x) == F32p(0.0f) && F32p(mGravityDir.y) == F32p(0.0f) && F32p(mGravityDir.z) == F32p(-1.0f);
	bool yPos = F32p(mGravityDir.x) == F32p(0.0f) && F32p(mGravityDir.y) == F32p(1.0f) && F32p(mGravityDir.z) == F32p(0.0f);
	bool yNeg = F32p(mGravityDir.x) == F32p(0.0f) && F32p(mGravityDir.y) == F32p(-1.0f) && F32p(mGravityDir.z) == F32p(0.0f);
	bool xPos = F32p(mGravityDir.x) == F32p(1.0f) && F32p(mGravityDir.y) == F32p(0.0f) && F32p(mGravityDir.z) == F32p(0.0f);
	bool xNeg = F32p(mGravityDir.x) == F32p(-1.0f) && F32p(mGravityDir.y) == F32p(0.0f) && F32p(mGravityDir.z) == F32p(0.0f);

	// Jeff: get variables from script
	F32 pitch = this->mCameraY;
	F32 yaw = this->mCameraX;

	if (mCameraAnglePB || zPos)
		pitch = mDegToRad(mRadToDeg(pitch)*-1);
	if (yNeg)
		pitch = mDegToRad(mRadToDeg(pitch) + 90);
	if (yPos)
		pitch = mDegToRad((mRadToDeg(pitch) - 90) * -1);
	//if (xPos)
		//pitch = mDegToRad((mRadToDeg(pitch) - 90) * -1);
	if (mCameraAngleYB || zPos)
		yaw = mDegToRad((mRadToDeg(yaw) + 180)*-1);
	if (yNeg)
		yaw = mDegToRad((mRadToDeg(yaw) - 90));//*-1);
	if (yPos)
		yaw = mDegToRad((mRadToDeg(yaw) - 90)*-1);
	//if (xPos)
		//yaw = mDegToRad((mRadToDeg(yaw) + 90)*-1);

	if (mCameraAngleFlip)// || yNeg)
		swap(pitch, yaw);

	// Jeff: distance
	F32 distance = 2.3f;//mDataBlock->cameraMaxDist;
	F32 horizontalDist = mCos(pitch) * distance;
	F32 verticalDist = mSin(pitch) * distance;

	if (yNeg || yPos)// || xPos)
		swap(horizontalDist, verticalDist);

	// Jeff: make the camera "orbit" around the marble
	Point3F ortho;
	
	ortho = Point3F(-mSin(yaw) * horizontalDist,
		-mCos(yaw) * horizontalDist,
		verticalDist);

	if (yNeg || yPos)
	{
		ortho = Point3F(-mCos(yaw) * horizontalDist,
						-verticalDist,
						-mSin(yaw) * horizontalDist);
	}
	/*else if (xPos)
	{
		ortho = Point3F(-verticalDist,
						-mCos(yaw) * horizontalDist,
						-mSin(yaw) * horizontalDist);
	}*/

	// Matt: Get top offset of marble
	Point3F sideDir = Point3F(0, 0, 0);
	Point3F motionDir = Point3F(0, 0, 0);
	Point3F upDir = Point3F(0, 0, 0);
	this->getMarbleAxis(sideDir, motionDir, upDir);

	Point3F offset = upDir * this->mRadius;

	// Jeff: add the ortho position to the object's current position
	Point3F position = getPosition() + offset;
	//if (yNeg)
	//	position -= ortho;
	//else
	//if (!yPos)

	// Apply Ortho
	position += ortho;

	disableCollision();

	//HiGuy: Do a raycast so we don't have the camera clipping the everything
	RayInfo info;
	if (mContainer->castRay(getPosition(), position, ~(WaterObjectType | StaticObjectType | GameBaseObjectType | TriggerObjectType | DefaultObjectType), &info)) {
	//if (mContainer->castRay(getPosition(), position, ~(WaterObjectType | InteriorObjectType | TerrainObjectType), &info)) {
		// S22: -measure difference of collision point and marble location
		// -normalize (make it into a unit vector)
		// -subtract .001 * unit vector from original position of rayhit
		Point3F dist = Point3F(info.point - getPosition());
		dist.normalize();
		dist *= 0.01f;

		// combine vectors
		position = info.point - dist;
	}

	// Jeff: calculate rotation of the camera by doing matrix multiplies
	//AngAxisF rot1(upDir, yaw);
	//AngAxisF rot1(mCross(Point3F(0.0f, 0.0f, 1.0f), mGravityDir), yaw);
	//AngAxisF rot1(Point3F(0.0f, 0.0f, 1.0f), yaw);

	Point3F axisY = mCameraYAngle;
	Point3F axisP = mCameraPAngle;

	if (mCameraAngleFlip)
		swap(axisY, axisP);

	AngAxisF rot1;
	if (yNeg || yPos)
		rot1 = AngAxisF(Point3F(0.0f, 1.0f, 0.0f), yaw);
	//else if (xPos)
	//	rot1 = AngAxisF(Point3F(1.0f, 0.0f, 0.0f), yaw);
	else
		rot1 = AngAxisF(axisY, yaw);
	//AngAxisF rot2(mCross(Point3F(0.0f, 1.0f, 0.0f), upDir), pitch);
	//AngAxisF rot2(Point3F(1.0f, 0.0f, 0.0f), pitch);
	AngAxisF rot2;
	if (yNeg || yPos)
		rot2 = AngAxisF(Point3F(0.0f, 0.0f, 1.0f), pitch);
	//else if (xPos)
	//	rot2 = AngAxisF(Point3F(0.0f, 1.0f, 0.0f), pitch);
	else
		rot2 = AngAxisF(axisP, pitch);
	//AngAxisF rot3(this->mGravityDir, mDegToRad(90.0f));//1.0f);
	AngAxisF rot3(mCameraAngle.axis, mCameraAngle.angle);//mDegToRad(mCameraAngle.angle));//1.0f);

	MatrixF mat1, mat2, mat3, mat4;

	if (mCameraAngleOrderFlip)
	{
		rot1.setMatrix(&mat2);
		rot2.setMatrix(&mat1);
	}
	else {
		rot1.setMatrix(&mat1);
		rot2.setMatrix(&mat2);
	}
	rot3.setMatrix(&mat3);

	mat1.mulL(mGravityMatrix);

	// Jeff: set position and apply rotation
	mat1.mul(mat2);

	// Matt: Gravity Dir
	mat1.mul(mat3);

	if (zPos)
	{
		AngAxisF rot4(Point3F(0.0f, 1.0f, 0.0f), mDegToRad(180.0f));
		rot4.setMatrix(&mat4);
		mat1.mul(mat4);
	}
	else if (yNeg)
	{
		AngAxisF rot4(Point3F(0.0f, 1.0f, 0.0f), mDegToRad(90.0f));
		rot4.setMatrix(&mat4);
		mat1.mul(mat4);
	}
	else if (yPos)
	{
		AngAxisF rot4(Point3F(0.0f, -1.0f, 0.0f), mDegToRad(90.0f));
		rot4.setMatrix(&mat4);
		mat1.mul(mat4);
	}
	/*else if (xPos)
	{
		AngAxisF rot4(Point3F(0.0f, 0.0f, 1.0f), mDegToRad(90.0f));
		rot4.setMatrix(&mat4);
		mat1.mul(mat4);
	}*/

	//MatrixF gravMat = mGravityMatrix;

	//Con::errorf("here: %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g", gravMat.m[0], gravMat.m[1], gravMat.m[2], gravMat.m[3], gravMat.m[4], gravMat.m[5], gravMat.m[6], gravMat.m[7], gravMat.m[8], gravMat.m[9], gravMat.m[10], gravMat.m[11], gravMat.m[12], gravMat.m[13], gravMat.m[14], gravMat.m[15]);


	//// ---------

	//Point3F dir = mGravityDir;
	//dir.normalize();

	////Point3F dir = Point3F(-1, 0, 0);

	//rotateMatrix(mat1, dir);
	//mat1.m[2] = -mGravityDir.x;
	//mat1.m[6] = -mGravityDir.y;
	//mat1.m[10] = -mGravityDir.z;
	//
	//// ---------

	// LOL
	// ------------------------
	if (Con::getBoolVariable("LOL"))
	{
		AngAxisF lolA(Point3F(0.34f, 0.52f, 0.14f), 3.47f);
		//AngAxisF lolA(Point3F(0.0f, 1.0f, 0.0f), mDegToRad(90.0f));
		MatrixF lolM;
		lolA.setMatrix(&lolM);
		mat1.mulL(lolM);//MatrixF(EulerF(0.0f, 0.0f, 1.0f)));
		//mat1.mulP(Point3F(0.0f, 0.0f, 1.0f));
	}
	// ------------------------
	
	mat1.setPosition(position);

	enableCollision();

	*mat = mat1;

	AngAxisF ang(*mat);

	char buf[2048];
	dSprintf(buf, sizeof(buf), "%g %g %g %g", ang.axis.x, ang.axis.y, ang.axis.z, mRadToDeg(ang.angle));

	//Con::setVariable("$camInfo", buf);

	// Apply Camera FX.
	mat->mul( gCamFXMgr.getTrans() );
}

void Marble::getRenderImageTransform(U32 imageSlot, MatrixF* mat, bool noEyeOffset)
{
	AngAxisF ang(Point3F(1, 0, 0), 0);
	ang.setMatrix(mat);

	mat->setPosition(getPosition());

	F32 size = this->mCurrentSize;
	const Point3F& scale = Point3F(size, size, size);
	mat->scale(scale);
}

void Marble::advanceTime(F32 dt)
{
	Parent::advanceTime(dt);
}

void Marble::updateServerPhysics(F32 dt)
{
	if (this->mStatic)
	{
		return;
	}
	Point3F startPos = this->getTransform().getPosition();
	//delta.posVec = startPos;

	Point3F pos0 = this->getTransform().getPosition(); // Translation
	Point3F pos = pos0;
	QuatF rot0 = QuatF(this->getTransform()); // Quat from Rot Matrix
	QuatF rot = rot0;

	this->mBounceYet = false;
	this->mSlipAmount = 0.0f;
	this->mContactTime = 0.0f;
	this->mTotalTime = 0.0f;
	this->mStartPos = pos0;

	if (!this->mDisableCollision && this->mMode == Marble::Normal)
	{
		//int retryCount = 0;
		Point3F vel0 = this->mVelocity;
		Point3F avel0 = this->mRotVelocity;
		//while (dt > 0.0f)
		int iters = 0;
		F64 timeRemaining = dt / 1000.0;
		do
		{
			//if (retryCount++ >= 5)
			//{
			//	break;
			//}
			if (timeRemaining == 0.0)
				break;
			F64 timeStep = 0.008;
			if (timeRemaining < 0.008)
				timeStep = timeRemaining;
			Point3F vel = vel0;
			Point3F avel = avel0;
			this->updateMove(timeStep, pos, rot, vel, avel);
			this->updateIntegration(timeStep, pos, rot, vel, avel);
			this->mContacts.clear();
			F64 remainingTime = this->updateCollision(pos, pos0, rot, rot0, timeStep);
			if (remainingTime > 0.0f)
			{
				F64 i = remainingTime / timeStep;
				vel0 = vel - (vel - vel0) * i;
				avel0 = avel - (avel - avel0) * i;
			}
			else
			{
				vel0 = vel;
				avel0 = avel;
			}
			this->mVelocity = vel0;
			this->mRotVelocity = avel0;
			/*if (this->mContacts.getCount() != 0)
			{
				this->resolveCollisions(this->mContacts);
			}*/
			pos0 = pos;
			rot0 = rot;
			//dt = remainingTime;
			timeRemaining -= timeStep;
			iters++;
		} while(iters <= 10);
	}
	else if (this->mMode == Marble::Start)
	{
		this->updateMove(0.008, pos, rot, this->mVelocity, this->mRotVelocity);
		this->mVelocity = Point3F(0, 0, 0);
		this->updateIntegration(0.008, pos, rot, this->mVelocity, this->mRotVelocity);
	}
	else if (this->mMode == Marble::Victory)
	{
		// TODO: Finish pad physics
		this->updateMove(0.008, pos, rot, this->mVelocity, this->mRotVelocity);
		this->mVelocity = Point3F(0, 0, 0);
		this->updateIntegration(0.008, pos, rot, this->mVelocity, this->mRotVelocity);
	}
	else
	{
		this->updateMove(0.008, pos, rot, this->mVelocity, this->mRotVelocity);
		this->updateIntegration(0.008, pos, rot, this->mVelocity, this->mRotVelocity);
	}

	MatrixF mat(1);
	rot.setMatrix(&mat);
	mat.setPosition(pos);
	
	// Apply the movement
	//this->setTransform(mat);
	Parent::setTransform(mat);
	mContacts.clear();

	this->updateBounceSound(pos);
	this->updateRollSound(pos);
	this->updateItems(this->mStartPos, pos);
	if (isServerObject())
		this->checkTriggers();

	if (isServerObject())
		this->checkPad();

	//this->mSpawnDelay = false;

	//Point3F dif = pos - startPos;

	//Con::errorf("Time: %g", time);

	//if (isServerObject())// && time >= 1.0f)// && dif.len() > 3)
	//{
		this->setMaskBits(MoveMask);
		//time = 0.0f;
	//}
}

void Marble::updateClientPhysics(F32 dt)
{
	//Point3F pos = getPosition();

	/*{
		Point3F startPos = this->getTransform().getPosition();
		delta.posVec = startPos;
		delta.rotVec = this->getTransform().toEuler();

		Point3F pos = this->getTransform().getPosition(); // Translation
		QuatF rot = QuatF(this->getTransform()); // Quat from Rot Matrix

		Point3F vel = this->mVelocity;
		Point3F avel = this->mRotVelocity;
		this->updateMove(dt, pos, rot, vel, avel);

		if (isClientObject())
		{
			delta.pos = pos;
			MatrixF mat;
			rot.setMatrix(&mat);
			delta.rot = mat.toEuler();
			delta.posVec = delta.posVec - delta.pos;
			delta.rotVec = delta.rotVec - delta.rot;
		}

		//this->updateIntegration(dt, pos, rot, vel, avel);
		this->mContacts.clear();
		/*float remainingTime = this->updateCollision(pos, pos0, rot, rot0, dt);
		if (remainingTime > 0.0f)
		{
			float i = remainingTime / dt;
			vel0 = vel - (vel - vel0) * i;
			avel0 = avel - (avel - avel0) * i;
		}
		else
		{
			vel0 = vel;
			avel0 = avel;
		}
		this->mVelocity = vel0;
		this->mRotVelocity = avel0;
		if (this->mContacts.getCount() != 0)
		{
			this->resolveCollisions(this->mContacts);
		}
		pos0 = pos;
		rot0 = rot;
		dt = remainingTime;*/

		/*updateRollSound(pos);
	}*/

	//delta.pos = pos;
	//delta.posVec = delta.posVec - delta.pos;
	//delta.dt = 1.0f;
	//delta.posVec = pos;

	//updateRollSound(pos);

	//delta.rotVec = this->getTransform().toEuler();
	//
	//QuatF rot = this->getRotation();

	//Point3F vel(0, 0, 0);
	//this->updateMove(dt, pos, rot, vel, this->mRotVelocity);
	//vel = Point3F(0, 0, 0);
	//this->updateIntegration(dt, pos, rot, vel, this->mRotVelocity);

	//if (isClientObject())
	//{
	//	delta.pos = pos;
	//	MatrixF mat;
	//	rot.setMatrix(&mat);
	//	delta.rot = mat.toEuler();
	//	//delta.posVec = delta.posVec - delta.pos;
	//	delta.rotVec = delta.rotVec - delta.rot;
	//}

	//if (delta.warpTicks > 0) {
	//	delta.warpTicks--;

	//	// Set new pos
	//	getTransform().getColumn(3, &delta.pos);
	//	delta.pos += delta.warpOffset;
	//	delta.rot += delta.rotOffset;

	//	setPosition(delta.pos, delta.rot);

	//	// Backstepping
	//	delta.posVec = -delta.warpOffset;
	//	delta.rotVec = -delta.rotOffset;
	//}
}

void Marble::updateBounceSound(Point3F pos)
{
	if (this->mBounceYet)
	{
		float minSize = 1.0f;
		float maxSize = 2.0f;
		float curSize = this->mCurrentSize;
		bool isMega = (curSize - minSize) / (maxSize - minSize) > 0.5f;
		float softBounceSpeed = isMega ? 2.5f : 2.5f;
		if (this->mBounceSpeed >= softBounceSpeed)
		{
			float hardBounceSpeed = isMega ? 12.0f : 8.0f;
			int index = mRandI(0, 3);
			AssertFatal(index >= 0 && index < 4, "Bounce sound index out of range");
			index += isMega ? this->mDataBlock->MegaBounce1 : this->mDataBlock->Bounce1;
			float minGain = isMega ? 0.2f : 0.5f;
			this->mBounceSpeed *= 2.0f;
			float gain;
			if (this->mBounceSpeed < hardBounceSpeed)
			{
				gain = (1.0f - minGain) * (this->mBounceSpeed - softBounceSpeed) / (hardBounceSpeed - softBounceSpeed) + minGain;
			}
			else
			{
				gain = 1.0f;
			}
			if (isServerObject() && this->mDataBlock->sound[index] != NULL)
			{
				//MatrixF transform = getTransform();
				//Point3F velocity = getVelocity();

				this->playAudio(2, this->mDataBlock->sound[index], false, gain);//->setVolume(gain);
			}

			if (isServerObject() && this->mBounceSpeed / 2.0f >= hardBounceSpeed)
			{
				this->mHitShake = true;
				setMaskBits(UpdateMask);
				/*if (smHitShake)
				{
					this->applyCameraShake(this->mBounceSpeed / 100.0f, 10.0f, 10.0f, 0.5f);
				}*/
			}
		}
	}
}

void Marble::applyCameraShake(F32 amplifier, F32 freq, F32 falloff, F32 duration)
{
	if (isServerObject())
		return;

	Point3F camShakeFreq(freq, freq, freq);
	Point3F camShakeAmp(amplifier, amplifier, amplifier);

	CameraShake *camShake = new CameraShake;
	camShake->setDuration(duration);
	camShake->setFrequency(camShakeFreq);

	VectorF shakeAmp = camShakeAmp;
	camShake->setAmplitude(shakeAmp);
	camShake->setFalloff(falloff);
	camShake->init();
	gCamFXMgr.addFX(camShake);
}

void Marble::updateRollSound(Point3F pos)
{
	MatrixF transform = getTransform();
	Point3F velocity = getVelocity();

	float contactPct = 0.0f;
	if (this->mTotalTime > MARBLE_EPSILON)
	{
		contactPct = this->mContactTime / this->mTotalTime;
	}
	float megaAmt = 0.0f;
	float regAmt = 1.0f - megaAmt;
	float velLen = this->mVelocity.len();
	velLen *= 2.0f;
	float scale = velLen / 15.0f;
	float rollVolume = scale * 2.0f;
	if (rollVolume > 1.0f)
	{
		rollVolume = 1.0f;
	}
	if (contactPct < 0.001f)
	{
		rollVolume = 0.0f;
	}
	float rollSmooth = 0.25f;
	float useRoll = getMax(this->mRollVolume, rollVolume);
	this->mRollVolume = useRoll;
	this->mRollVolume *= rollSmooth;
	this->mRollVolume += rollVolume * (1.0f - rollSmooth);
	rollVolume = useRoll;
	float sndVol = regAmt * rollVolume;

	float minSize = 1.0f;
	float maxSize = 2.0f;
	float curSize = this->mCurrentSize;
	bool isMega = (curSize - minSize) / (maxSize - minSize) > 0.5f;

	SFXSource* source;

	if (/*mSlipping && (mSlipAmount >= 6.0f || */mOnIce)//))
		source = this->mSlipHandle;
	else if (isMega)
		source = this->mRollMegaHandle;
	else
		source = this->mRollHandle;

	if (source != mRollMegaHandle && mRollMegaHandle != NULL && !mRollMegaHandle->isStopped())
		this->mRollMegaHandle->stop();

	if (source != mRollHandle && mRollHandle != NULL && !mRollHandle->isStopped())
		this->mRollHandle->stop();

	if (source != mSlipHandle && mSlipHandle != NULL && !mSlipHandle->isStopped())
		this->mSlipHandle->stop();

	if (source != NULL)
	{
		source->setVolume(sndVol);
		source->setTransform(getTransform());
		source->setVelocity(getVelocity());

		if (!source->isPlaying())
			source->play();
	}
}

void Marble::itemCallback(SceneObject* so, void* key)
{
	Marble* marble = reinterpret_cast<Marble*>(key);
	Item* item = dynamic_cast<Item*>(so);
	//StaticShape* shape = dynamic_cast<StaticShape*>(so);

	if (item != NULL)
	{
		Box3F gemBox = item->getWorldBox();
		Point3F gemCenter = 0.5f * (gemBox.maxExtents + gemBox.minExtents);
		Point3F gemExtent = 0.5f * (gemBox.maxExtents - gemBox.minExtents);
		gemBox.minExtents = gemCenter - 0.5f * gemExtent;
		gemBox.maxExtents = gemCenter + 0.5f * gemExtent;
		if (marble->box0.isOverlapped(gemBox))
			marble->onCollision(item, VectorF(0, 0, 0));

		if (item->getDataBlock()->isField("powerUpId"))
		{
			const char* powerUpField = item->getDataBlock()->getFieldDictionary()->getFieldValue(StringTable->insert("powerUpId"));
			S32 powerUpId = dAtoi(powerUpField);
			PowerUpData* data = marble->mDataBlock->mPowerUps;
			if (data == NULL)
				return;

			PowerUp *powerUp = data->getPowerUp(powerUpId);
			if (powerUp == NULL)
				return;

			if (powerUp->blastRecharge)
				marble->setBlastEnergy(MAX_BLAST);
		}
	}
	//else if (shape != NULL) {
	//	Box3F shapeBox = shape->getWorldBox();
	//	//Point3F shapeCenter = 0.5f * (shapeBox.maxExtents + shapeBox.minExtents);
	//	//Point3F shapeExtent = 0.5f * (shapeBox.maxExtents - shapeBox.minExtents);
	//	//shapeBox.minExtents = shapeCenter - 0.5f * shapeExtent;
	//	//shapeBox.maxExtents = shapeCenter + 0.5f * shapeExtent;
	//	if (marble->box0.isOverlapped(shapeBox))
	//		shape->onCollision(marble, VectorF(0, 0, 0));
	//}
}

void Marble::updateItems(Point3F startPos, Point3F endPos)
{
	Box3F box = Box3F();
	box.maxExtents = startPos;
	box.minExtents = startPos;
	box.extend(endPos);
	Point3F expansion = Point3F(this->mRadius + 0.2f, this->mRadius + 0.2f, this->mRadius + 0.2f);
	box.minExtents -= expansion;
	box.maxExtents += expansion;
	this->box0 = box;

	if (isServerObject())
		gServerContainer.findObjects(box, ItemObjectType/* | StaticShapeObjectType*/, itemCallback, this); //&box);
	//else
	//	gClientContainer.findObjects(box, ItemObjectType, itemCallback, this); //&box);
}

void Marble::updateIntegration(F64 dt, Point3F& pos, QuatF& rot, Point3F& vel, Point3F& omega)
{
	if (!mDisablePhysics)
		pos += vel * dt;
	if (mDisableRotation)
		return;
	Point3F axis = omega;
	float angle = axis.len();
	angle = mDegToRad(angle);
	if (angle > 1E-05f)
	{
		axis *= 1.0f / angle;
		AngAxisF ang = AngAxisF(axis, dt * -angle);
		QuatF deltaRot = QuatF(ang);
		float len = deltaRot.len();
		if (len > 0.001f)
		{
			deltaRot *= 1.0f / len;
		}
		QuatF newRot = QuatF(0, 0, 0, 0);
		newRot.mul(rot, deltaRot);
		rot = newRot;
		len = rot.len();
		if (len > 0.001f)
		{
			rot *= 1.0f / len;
		}
	}
}

void Marble::interpolateTick(F32 dt)
{
	Parent::interpolateTick(dt);

	// Client side interpolation
	//Point3F pos = delta.pos + delta.posVec * dt;
	//EulerF rot = delta.rot + delta.rotVec * dt;

	//MatrixF mat = getTransform();//mRenderObjToWorld;
	//mat.set(rot);
	//mat.setColumn(3, pos);
	//setTransform(mat);//setRenderTransform(mat);
	//delta.dt = dt;
}

void Marble::updateData(F32 dt)
{
	// Blast Timer
	{
		if (this->mBlastEnergy < MAX_NATURAL_BLAST)
		{
			this->mBlastTimer += dt;// *10.0f;
			F32 rechargeRate = this->mDataBlock->mEnergyRechargeRate;
			//F32 milli = 10.0f;
			//F32 rate = (rechargeRate * 100.0f / MAX_NATURAL_BLAST) / 100.0f / milli;

			F32 second = 0.0001f;

			F32 rate = MAX_NATURAL_BLAST / (rechargeRate / 1000.0f) * second;

			while (this->mBlastTimer >= second)
			{
				this->mBlastEnergy += rate;
				if (this->mBlastEnergy > MAX_NATURAL_BLAST)
					this->mBlastEnergy = MAX_NATURAL_BLAST;
				this->mBlastTimer -= second;
			}
		}
	}

	// Timer
	if (this->mMode == Marble::Normal)
	{
		if (this->mBonusTime <= 0)
		{
			this->mTime += dt * 1000;
		}
		else {
			this->mBonusTime -= dt * 1000;

			if (this->mBonusTime < 0)
				this->mBonusTime = 0;
		}

		if (isClientObject())
			Con::evaluatef("PlayGui.updateTimer(%i,%i);", this->mTime, this->mBonusTime != 0);
	}

	// Marble Sizing
	{
		if (this->mCurrentSize != this->mDesiredSize)
		{
			F32 diff = mFabs(this->mCurrentSize - this->mDesiredSize);
			F32 oldSize = this->mCurrentSize;
			if (this->mCurrentSize < this->mDesiredSize)
			{
				this->mCurrentSize += diff * dt * 10;
			}
			else if (this->mCurrentSize > this->mDesiredSize) {
				this->mCurrentSize -= diff * dt * 10;
			}
			if (this->mCurrentSize == oldSize)
				this->mCurrentSize = this->mDesiredSize;

			F32 scale = this->mCurrentSize;

			this->mObjScale = Point3F(scale, scale, scale);

			this->mRadius = this->mCurrentSize / 5.0f;

			//this->mObjBox.min = Point3F(-this->mRadius, -this->mRadius, -this->mRadius);
			//this->mObjBox.max = Point3F(this->mRadius, this->mRadius, this->mRadius);
			//this->resetWorldBox();

			setMaskBits(UpdateMask);
		}
	}
}

void Marble::processTick(const Move *move)
{
	Parent::processTick(move);
	this->mMove = move;
	if (move != NULL)
	{
		this->mCameraY += move->pitch;
		this->mCameraX += move->yaw;
		if (mRadToDeg(this->mCameraY) > 90)
			this->mCameraY = mDegToRad((float)90);
		if (mRadToDeg(this->mCameraY) < -90)
			this->mCameraY = mDegToRad((float)-90);
	}

	this->updateData(TickSec);

	/*PathedInterior* i;
	if (isClientObject())
		i = PathedInterior::getClientPathedInteriors();
	else
		i = PathedInterior::getServerPathedInteriors();

	for (; i != NULL; i = i->getNext())
	{
		if (i)
		{
			//for (S32 j = 0; j < 1000; j++)
			{
				i->computeNextPathStep(TickMs);
				//for (S32 j = TickSec * 1000; ; )
				i->advance(TickSec);
			}
		}
	}*/

	if (!mStatic)
	{
		//if (isServerObject())// || (isClientObject() && getControllingClient() != NULL))
			this->updateServerPhysics(TickMs);//TickSec);
		//else
		if (isClientObject())
			this->updateClientPhysics(TickSec);
	}
	else {
		//delta.posVec.set(0, 0, 0);
	}

	if (this->mOutOfBounds && isServerObject())
	{
		if (this->mMove != NULL && this->mMove->trigger[2])
		{
			Con::executef(this, "onOOBClick");
		}
	}

	this->updateContainer();

	//if (this->mDataBlock->isMethod("processTick"))
		//Con::executef(this->mDataBlock, "processTick", this->getIdString(), Con::getIntArg(isClientObject()));
}

//----------------------------------------------------------------------------
/** Check collisions with trigger and items
Perform a container search using the current bounding box
of the main body, wheels are not included.  This method should
only be called on the server.
*/
void Marble::checkTriggers()
{
	//if (mSpawnDelay)
		//return;
	Box3F searchBox = this->mObjBox;
	Box3F startBox = searchBox;
	Point3F rad = Point3F(this->mRadius, this->mRadius, this->mRadius);
	startBox.minExtents += this->posx0 - rad;
	startBox.maxExtents += this->posx0 + rad;
	Box3F endBox = searchBox;
	endBox.minExtents += this->posx1 - rad;
	endBox.maxExtents += this->posx1 + rad;
	searchBox = startBox;
	searchBox.extend(endBox.minExtents);
	searchBox.extend(endBox.maxExtents);
	gServerContainer.findObjects(searchBox, TriggerObjectType, findTriggerCallback, this);
}

/** The callback used in by the checkTriggers() method.
The checkTriggers method uses a container search which will
invoke this callback on each obj that matches.
*/
void Marble::findTriggerCallback(SceneObject* obj, void *key)
{
	Marble* marble = reinterpret_cast<Marble*>(key);
	U32 objectMask = obj->getTypeMask();

	// Check: triggers, corpses and items, basically the same things
	// that the player class checks for
	if (objectMask & TriggerObjectType) {
		Trigger* pTrigger = static_cast<Trigger*>(obj);
		pTrigger->potentialEnterObject(marble);
	}
}

void Marble::checkPad()
{
	//if (mSpawnDelay)
	//return;
	/*Box3F searchBox = this->mObjBox;
	Box3F startBox = searchBox;
	Point3F rad = Point3F(this->mRadius, this->mRadius, this->mRadius);
	startBox.min += this->posx0 - rad;
	startBox.max += this->posx0 + rad;
	Box3F endBox = searchBox;
	endBox.min += this->posx1 - rad;
	endBox.max += this->posx1 + rad;
	searchBox = startBox;
	searchBox.extend(endBox.min);
	searchBox.extend(endBox.max);*/
	gServerContainer.findObjects(StaticShapeObjectType, findPadCallback, this);
}

void Marble::findPadCallback(SceneObject* obj, void *key)
{
	Marble* marble = reinterpret_cast<Marble*>(key);
	U32 objectMask = obj->getTypeMask();

	if (objectMask & StaticShapeObjectType) {
		StaticShape* pShape = static_cast<StaticShape*>(obj);
		if (marble->mPad != NULL && pShape == marble->mPad)
		{
			/*EarlyOutPolyList clippedList;
			clippedList.clear();

			SphereF sphere;
			sphere.center = (pShape->getWorldBox().min + pShape->getWorldBox().max) * 0.5;
			VectorF bv = pShape->getWorldBox().max - sphere.center;
			sphere.radius = bv.len();

			marble->buildPolyList(&clippedList, pShape->getWorldBox(), sphere);*/

			//F32 padHeightCheck = 7;

			bool onThePad = false;

			Box3F padBox = pShape->getWorldBox();

			/*Point3F sideDir = Point3F(0, 0, 0);
			Point3F motionDir = Point3F(0, 0, 0);
			Point3F upDir = Point3F(0, 0, 0);
			marble->getMarbleAxis(sideDir, motionDir, upDir);

			Point3F extendAmount = upDir * padHeightCheck;

			padBox.min -= extendAmount;
			padBox.max += extendAmount;*/

			//if (!padBox.isOverlapped(marble->getWorldBox()))
			//	marble->mOnPad = false;

			if (padBox.isOverlapped(marble->getWorldBox()))
				onThePad = true;

			if (onThePad)
			{
				if (marble->mOnPad || marble->mMode != Marble::Normal)
					return;
				marble->mOnPad = true;
				if (marble->getControllingClient() != NULL)
				{
					Con::executef(marble->getControllingClient(), "onEnterPad");
				}
			}
			else {
				marble->mOnPad = false;
			}
		}
	}
}

bool Marble::buildPolyList(PolyListContext context, AbstractPolyList* list, const Box3F& box, const SphereF& sphere)
{
	MatrixF transform = getTransform();
	list->setTransform(&transform, getScale());
	list->setObject(this);
	list->addBox(mObjBox);

	/*Box3F searchBox = this->mObjBox;
	Box3F startBox = searchBox;
	Point3F rad = Point3F(this->mRadius, this->mRadius, this->mRadius);
	startBox.min += this->posx0 - rad;
	startBox.max += this->posx0 + rad;
	Box3F endBox = searchBox;
	endBox.min += this->posx1 - rad;
	endBox.max += this->posx1 + rad;
	searchBox = startBox;
	searchBox.extend(endBox.min);
	searchBox.extend(endBox.max);
	list->addBox(searchBox);*/

	/*Box3F searchBox = this->mObjBox;
	Point3F rad = Point3F(this->mRadius, this->mRadius, this->mRadius);
	searchBox.min -= rad;
	searchBox.max += rad;

	SphereF searchSphere = SphereF(Point3F(0, 0, 0), this->mRadius);*/

	return !list->isEmpty();
}

void Marble::updateMove(F64 dt, Point3F& pos, QuatF& rot, Point3F& vel, Point3F& omega)
{
	this->findContacts(pos, rot);
    this->drawContactsDebug();
	Point3F aControl = Point3F(0, 0, 0);
	Point3F desiredOmega = Point3F(0, 0, 0);
	bool isCentered = this->computeMoveForces(omega, &aControl, &desiredOmega);
	this->velocityCancel(vel, omega, isCentered, false);

	Point3F A = this->getExternalForces(dt);
	Point3F a = Point3F(0, 0, 0);
	//if (!isClientObject())
	this->applyContactForces(dt, isCentered, aControl, desiredOmega, vel, omega, A, &a);
	vel += A * dt;
	omega += a * dt;
	this->updatePowerUp(dt, pos, rot, vel, omega);

	// Shape Collision
	for (S32 i = 0; i < mContacts.getCount(); i++)
	{
		Collision col = mContacts[i];

		if (col.object != NULL)
		{
			StaticShape* shape = dynamic_cast<StaticShape*>(col.object);

			if (shape != NULL)
			{
				shape->onCollision(this, VectorF(0, 0, 0));

				/*if (this->mPad != NULL && shape == this->mPad)
				{
					if (this->mOnPad || this->mMode != MarbleMode::Normal)
						return;
					this->mOnPad = true;
					if (this->getControllingClient() != NULL)
					{
						Con::executef(this->getControllingClient(), "onEnterPad");
					}
				}*/
			}
		}
	}

	this->velocityCancel(vel, omega, isCentered, true);

	this->mTotalTime += dt;
	if (mContacts.getCount() != 0)
	{
		this->mContactTime += dt;
		if (!mContacts[0].hasCollider)
			this->mLastContactPosition = mContacts[0].point;
	}
	mContacts.clear(); // Should we really clear here?

	// Water script callbacks
	if (!inLiquid && mWaterCoverage != 0.0f) {
		mDataBlock->onEnterLiquid_callback(this, mWaterCoverage, mLiquidType.c_str());
		inLiquid = true;
	}
	else if (inLiquid && mWaterCoverage == 0.0f) {
		mDataBlock->onLeaveLiquid_callback(this, mLiquidType.c_str());
		inLiquid = false;
	}

	if (inLiquid)
	{
		mDataBlock->onUpdateLiquid_callback(this, mWaterCoverage, mLiquidType.c_str());
	}
}

Point3F stupidThing(Point3F& a, Point3F& b, Point3F& c, Point3F& d)
{
	float x = (a.x * b.y) - (a.y * b.x);
	float y = (c.x * b.y) - (c.y * b.x);
	float z = (d.z * b.z);// -(d.z * b.x);
	return Point3F(x, y, z);
}

void Marble::updatePowerUp(F32 dt, Point3F& pos, QuatF& rot, Point3F& vel, Point3F& omega)
{
	for (S32 i = 0; i < mActivePowerUps.size(); i++)
	{
		ActivePowerUp *active = &mActivePowerUps[i];

		Point3F sideDir(0, 0, 0); // 1 0 0
		Point3F motionDir(0, 0, 0); // 0 1 0
		Point3F upDir(0, 0, 0); // 0 0 1
		getMarbleAxis(sideDir, motionDir, upDir);

		Point3F boostDir = stupidThing(motionDir, active->powerUp->boostDir, sideDir, upDir);

		if (active->powerUp->emitter)
		{
			if (!active->emitter)
			{
				active->emitter = new ParticleEmitter;
				active->emitter->onNewDataBlock(active->powerUp->emitter, false);
				if (!active->emitter->registerObject())
				{
					Con::warnf(ConsoleLogEntry::General, "Could not register particle emitter for class: %s", active->powerUp->emitter->getName());
					delete active->emitter;
					active->emitter = NULL;
				}
			}

			if (active->emitter)
			{
				active->emitter->emitParticles(pos, false, boostDir * -1, boostDir * -1, 2000 * dt);
			}
		}

		if (!active->initialized)
		{
			if (active->powerUp->image)
			{
				ShapeBaseImageData* image = dynamic_cast<ShapeBaseImageData*>(active->powerUp->image);
				if (image)
				{
					active->mountIndex = -1;

					for (S32 j = 0; j < MaxMountedImages; j++)
					{
						if (!this->mMountedImageList[j].dataBlock)
						{
							active->mountIndex = j;
							break;
						}
					}

					if (active->mountIndex >= 0)
					{
						NetStringHandle skin = NetStringHandle("");
						this->mountImage(image, active->mountIndex, false, skin);
					}
				}
			}

			// Powerup Effects
			F32 boostPercent = 100.0f;
			if (active->id == 0)
				boostPercent = active->blastEnergy * 100.0f / MAX_NATURAL_BLAST;
			vel += (boostDir * (active->powerUp->boostAmount + active->powerUp->boostMassless) * (boostPercent / 100.0f)) * dt * 30;

			if (active->powerUp->gravityMod != 0.0f)
				mCurrentGravity *= active->powerUp->gravityMod;
			if (active->powerUp->airAccel != 0.0f)
				mCurrentAirAccel *= active->powerUp->airAccel;
			if (active->powerUp->sizeScale != 0.0f)
				this->mDesiredSize *= active->powerUp->sizeScale;
			if (active->powerUp->massScale != 0.0f)
				this->mCurrentMass *= active->powerUp->massScale;

			// These two might conflict as they modify the same variable
			if (active->powerUp->bounce >= 0.0f)
				this->mCurrentBounce = active->powerUp->bounce;
			if (active->powerUp->boost != 0.0f)
				this->mCurrentBounce = active->powerUp->boost;

			// Custom Powerup Effects
			if (active->powerUp->enableParkour)
				this->mParkour = true;

			active->initialized = true;
		}

		active->timer += dt * 1000;
		if (active->timer >= active->powerUp->duration || mPowerUpReset)
		{
			if (active->powerUp->gravityMod != 0.0f)
				mCurrentGravity /= active->powerUp->gravityMod;
			if (active->powerUp->airAccel != 0.0f)
				mCurrentAirAccel /= active->powerUp->airAccel;
			if (active->powerUp->sizeScale != 0.0f)
				this->mDesiredSize /= active->powerUp->sizeScale;
			if (active->powerUp->massScale != 0.0f)
				this->mCurrentMass /= active->powerUp->massScale;

			// These two might conflict as they modify the same variable
			if (active->powerUp->bounce >= 0.0f)
				this->mCurrentBounce = this->mDataBlock->mBounceRestitution;
			if (active->powerUp->boost != 0.0f)
				this->mCurrentBounce = this->mDataBlock->mBounceRestitution;

			// Custom Powerup Effects
			if (active->powerUp->enableParkour)
				this->mParkour = false;

			// Unmount Powerup Image
			if (active->powerUp->image && active->mountIndex >= 0)
				this->unmountImage(active->mountIndex);

			SAFE_DELETE(active->powerUp);
			if (active->emitter)
			{
				active->emitter->deleteWhenEmpty();
				active->emitter = NULL;
			}
			//active->timer = 0;

			Con::executef(this, "onPowerUpExpired", Con::getIntArg(active->id));

			mActivePowerUps.erase(i);
		}
	}
	mPowerUpReset = false;

	/*for (S32 i = 0; i < mActivePowerUps.size(); i++)
	{
		if (mActivePowerUps[i].powerUp == NULL)
		{
			mActivePowerUps.erase(i);
		}
	}*/
}

bool Marble::computeMoveForces(Point3F omega, Point3F* aControl, Point3F* desiredOmega)
{
	//Point3F mGravityDir = Point3F(0.0f, 0.0f, -1.0f);
	Point3F R = -mGravityDir * this->mRadius;
	Point3F rollVelocity = mCross(omega, R);
	Point3F sideDir = Point3F(0, 0, 0);
	Point3F motionDir = Point3F(0, 0, 0);
	Point3F upDir = Point3F(0, 0, 0);
	this->getMarbleAxis(sideDir, motionDir, upDir);
	float currentYVelocity = mDot(rollVelocity, motionDir);
	float currentXVelocity = mDot(rollVelocity, sideDir);
	Point2F mv = (this->mMove != NULL) ? Point2F(mMove->x, mMove->y) : Point2F(0, 0);
	mv *= 1.53846157f;
	float mvlen = mv.len();
	if (mvlen > 1.0f)
	{
		mv *= 1.0f / mvlen;
	}
	float desiredYVelocity = this->mDataBlock->mMaxRollVelocity * mv.y;
	float desiredXVelocity = this->mDataBlock->mMaxRollVelocity * mv.x;
	if (desiredYVelocity != 0.0f || desiredXVelocity != 0.0f)
	{
		if (currentYVelocity > desiredYVelocity && desiredYVelocity > 0.0f)
		{
			desiredYVelocity = currentYVelocity;
		}
		else if (currentYVelocity < desiredYVelocity && desiredYVelocity < 0.0f)
		{
			desiredYVelocity = currentYVelocity;
		}
		if (currentXVelocity > desiredXVelocity && desiredXVelocity > 0.0f)
		{
			desiredXVelocity = currentXVelocity;
		}
		else if (currentXVelocity < desiredXVelocity && desiredXVelocity < 0.0f)
		{
			desiredXVelocity = currentXVelocity;
		}
		*desiredOmega = mCross(R, desiredYVelocity * motionDir + desiredXVelocity * sideDir) / R.lenSquared();
		*aControl = (*desiredOmega - omega);
		float aScalar = aControl->len();
		if (aScalar > this->mDataBlock->mAngularAcceleration)
		{
			*aControl *= this->mDataBlock->mAngularAcceleration / aScalar;
		}
		return false;
	}
	return true;
}

void Marble::doPowerUp(S32 id, PowerUp* powerUp)
{
	for (int i = 0; i < this->mActivePowerUps.size(); i++)
	{
		if (this->mActivePowerUps[i].id == id)
		{
			this->mActivePowerUps[i].timer = 0;
			return;
		}
	}

	ActivePowerUp active;
	active.id = id;
	active.emitter = NULL;
	active.powerUp = powerUp;
	active.timer = 0;
	if (id == 0)
		active.blastEnergy = this->mBlastEnergy;
	active.initialized = false;

	mActivePowerUps.push_back(active);
}

void Marble::applyContactForces(F64 dt, bool isCentered, Point3F aControl, Point3F desiredOmega, Point3F& vel, Point3F& omega, Point3F& A, Point3F* a)
{
	this->mSlipAmount = 0.0f;
	//Point3F mGravityDir = Point3F(0.0f, 0.0f, -1.0f);
	int bestSurface = -1;
	float bestNormalForce = 0.0f;
	for (int i = 0; i < mContacts.getCount(); i++)
	{
		if (!mContacts[i].hasCollider)
		{
			float normalForce = -mDot(mContacts[i].normal, A);
			if (normalForce > bestNormalForce)
			{
				bestNormalForce = normalForce;
				bestSurface = i;
			}
		}
	}
	Collision bestContact = (bestSurface != -1) ? mContacts[bestSurface] : Collision();

	int bestSurfaceJump = -1;
	float bestNormalForceJump = 0.0f;
	for (int i = 0; i < mContacts.getCount(); i++)
	{
		if (!mContacts[i].hasCollider)
		{
			float normalForce = -mDot(mContacts[i].normal, A);
			if (normalForce > bestNormalForceJump || mParkour)
			{
				bestNormalForceJump = normalForce;
				bestSurfaceJump = i;
			}
		}
	}
	Collision bestContactJump = (bestSurfaceJump != -1) ? mContacts[bestSurfaceJump] : Collision();

	bool canJump = bestSurfaceJump != -1;

	if (canJump && this->mMove != NULL && this->mMove->trigger[2])
	{
		Point3F velDifference = vel - bestContactJump.velocity;
		float sv = mDot(bestContactJump.normal, velDifference);
		if (sv < 0.0f)
		{
			sv = 0.0f;
		}

		// Jumping
		if (sv < this->mDataBlock->mJumpImpulse)
		{
			bool altChannel = false;
			Point3F normal = bestContactJump.normal;
			if (mParkour)
			{
				F32 dif = mDot(normal, -mGravityDir);
				if (dif < 0.9)
				{
					normal /= 2;
					normal += -mGravityDir * 2;
					altChannel = true;
				}
			}

			vel += normal * (this->mDataBlock->mJumpImpulse - sv);

			if (isServerObject() && this->mDataBlock->sound[this->mDataBlock->JumpSound] != NULL)
			{
				S32 channel = 1;
				if (altChannel)
					channel = 3;

				if (this->mDataBlock->sound[this->mDataBlock->JumpSound] != NULL)
					this->playAudio(channel, this->mDataBlock->sound[this->mDataBlock->JumpSound], true);
			}
		}
	}

	if (this->mDataBlock->mPowerUps != NULL && this->mPowerUpId && this->mMove != NULL && this->mMove->trigger[0])
	{
		// Use Powerup
		if (this->mDataBlock->mPowerUps != NULL)
		{
			PowerUpData* data = this->mDataBlock->mPowerUps;
			S32 index = this->mPowerUpId;

			this->doPowerUp(index, data->getPowerUp(index));
		}

		this->mPowerUpId = 0;
		Con::executef(this, "onPowerUpUsed");
	}

	if (this->mMove != NULL && this->mMove->trigger[1])
	{
		// Use Blast
		if (mBlastEnergy >= MIN_USABLE_BLAST && this->mDataBlock->mPowerUps != NULL)
		{
			PowerUpData* data = this->mDataBlock->mPowerUps;

			this->doPowerUp(0, data->getPowerUp(0));
			this->setBlastEnergy(0);

			if (isServerObject())
				Con::executef(this, "onBlastUsed");
		}

	}

	for (int j = 0; j < mContacts.getCount(); j++)
	{
		float normalForce2 = -mDot(mContacts[j].normal, A);
		Point3F thing = vel - mContacts[j].velocity;
		Point3F norm = mContacts[j].normal;
		float temp = mDot(norm, thing);
		if (normalForce2 > 0.0f && temp <= MARBLE_EPSILON)
		{
			A += mContacts[j].normal * normalForce2;
		}
	}
	if (bestSurface != -1)
	{
		// TODO: FIX
		//Point3F vector7 = bestContact.velocity - bestContact.normal * mDot(bestContact.normal, bestContact.velocity);

		F32 matFriction = 1.0f;
		F32 matForce = 0.0f;
		MatInstance* matInst = dynamic_cast<MatInstance*>(bestContact.material);
		if (matInst != NULL)
		{
			Material* mat = matInst->getMaterial();
			matFriction = mat->mFriction;
			matForce = mat->mForce;
		}

		if (matFriction < 1.0f)
		{
			mOnIce = true;
		}
		else {
			mOnIce = false;
		}

		Point3F vAtC = vel + mCross(omega, -bestContact.normal * this->mRadius) - bestContact.velocity;
		float vAtCMag = vAtC.len();
		this->mSlipping = false;
		Point3F aFriction = Point3F(0.0f, 0.0f, 0.0f);
		Point3F AFriction = Point3F(0.0f, 0.0f, 0.0f);
		if (vAtCMag != 0.0f)
		{
			this->mSlipping = true;
			float friction = this->mDataBlock->mKineticFriction * matFriction;
			float angAMagnitude = 5.0f * friction * bestNormalForce / (2.0f * this->mRadius);
			float AMagnitude = bestNormalForce * friction;
			float totalDeltaV = (angAMagnitude * this->mRadius + AMagnitude) * dt;
			if (totalDeltaV > vAtCMag)
			{
				float fraction = vAtCMag / totalDeltaV;
				angAMagnitude *= fraction;
				AMagnitude *= fraction;
				this->mSlipping = false;
			}
			Point3F vAtCDir = vAtC / vAtCMag;
			aFriction = angAMagnitude * mCross(-bestContact.normal, -vAtCDir);
			AFriction = -AMagnitude * vAtCDir;
			this->mSlipAmount = vAtCMag - totalDeltaV;
		}
		if (!this->mSlipping)
		{
			Point3F R = -mGravityDir * this->mRadius;
			Point3F aadd = mCross(R, A) / R.lenSquared();
			if (isCentered)
			{
				Point3F nextOmega = omega + (*a) * dt;
				aControl = desiredOmega - nextOmega;
				float aScalar = aControl.len();
				if (aScalar > this->mDataBlock->mBrakingAcceleration)
				{
					aControl *= this->mDataBlock->mBrakingAcceleration / aScalar;
				}
			}
			Point3F Aadd = -mCross(aControl, -bestContact.normal * this->mRadius);
			float aAtCMag = (mCross(aadd, -bestContact.normal * this->mRadius) + Aadd).len();
			float friction2 = this->mDataBlock->mStaticFriction * matFriction;
			if (aAtCMag > friction2 * bestNormalForce)
			{
				friction2 = this->mDataBlock->mKineticFriction * matFriction;
				Aadd *= friction2 * bestNormalForce / aAtCMag;
			}
			A += Aadd;
			*a += aadd;
		}
		A += AFriction;
		*a += aFriction;
	}
	*a += aControl;
}

Point3F Marble::getExternalForces(F64 dt)
{
	//Point3F mGravityDir = Point3F(0.0f, 0.0f, -1.0f);
	Point3F A = mGravityDir * this->mCurrentGravity;
	if (mContacts.getCount() == 0 && this->mMove != NULL)
	{
		Point3F sideDir = Point3F(0, 0, 0);
		Point3F motionDir = Point3F(0, 0, 0);
		Point3F upDir = Point3F(0, 0, 0);
		this->getMarbleAxis(sideDir, motionDir, upDir);
		A += (sideDir * this->mMove->x + motionDir * this->mMove->y) * this->mCurrentAirAccel;
	}
	return A;
}

void Marble::velocityCancel(Point3F& vel, Point3F& omega, bool surfaceSlide, bool noBounce)
{
	// TODO: FIX
	//new Point3F(0.0f, 0.0f, -1.0f);
	float SurfaceDotThreshold = 0.001f;
	bool looped = false;
	int itersIn = 0;
	bool done;
	do
	{
		done = true;
		itersIn++;
		for (int i = 0; i < mContacts.getCount(); i++)
		{
			F32 matFriction = 1.0f;
			F32 matRestitution = 1.0f;
			F32 matForce = 0.0f;
			MatInstance* matInst = dynamic_cast<MatInstance*>(mContacts[i].material);
			if (matInst != NULL)
			{
				Material* mat = matInst->getMaterial();
				matFriction = mat->mFriction;
				matRestitution = mat->mRestitution;
				matForce = mat->mForce;
			}

			Point3F sVel = vel - mContacts[i].velocity;
			float surfaceDot = mDot(mContacts[i].normal, sVel);
			if ((!looped && surfaceDot < 0.0f) || surfaceDot < -SurfaceDotThreshold)
			{
				float velLen = vel.len();
				Point3F surfaceVel = surfaceDot * mContacts[i].normal;
				this->reportBounce(mContacts[i].point, mContacts[i].normal, -surfaceDot);
				if (noBounce)
				{
					vel -= surfaceVel;
				}
				else if (mContacts[i].hasCollider)
				{
					Collision info = mContacts[i];
					SceneObject* obj = info.object;
					if (obj != NULL)
					{
						float ourMass = this->getMass();
						float theirMass = obj->getMass();
						float bounce = 0.5f;
						Point3F dp = vel * ourMass - obj->getVelocity() * theirMass;
						Point3F normP = mDot(dp, info.normal) * info.normal;
						normP *= 1.0f + bounce;
						vel -= normP / ourMass;
						obj->setVelocity(obj->getVelocity() + normP / theirMass);
						info.velocity = obj->getVelocity();
						this->mContacts[i] = info;
					}
					else
					{
						float bounce2 = 0.5f;
						Point3F normV = mDot(vel, info.normal) * info.normal;
						normV *= 1.0f + bounce2;
						vel -= normV;
					}
				}
				else
				{
					Point3F velocity2 = mContacts[i].velocity;
					if (velocity2.len() > MARBLE_EPSILON && !surfaceSlide && surfaceDot > -this->mDataBlock->mMaxDotSlide * velLen)
					{
						vel -= surfaceVel;
						vel.normalize();
						vel *= velLen;
						surfaceSlide = true;
					}
					else if (surfaceDot > -this->mDataBlock->mMinBounceVel)
					{
						vel -= surfaceVel;
					}
					else
					{
						float restitution = this->mCurrentBounce;
						restitution *= matRestitution;
						Point3F velocityAdd = -(1.0f + restitution) * surfaceVel;
						Point3F vAtC = sVel + mCross(omega, -mContacts[i].normal * this->mRadius);
						float normalVel = -mDot(mContacts[i].normal, sVel);
						vAtC -= mContacts[i].normal * mDot(mContacts[i].normal, sVel);
						float vAtCMag = vAtC.len();
						if (vAtCMag != 0.0f)
						{
							float friction = this->mDataBlock->mBounceKineticFriction * matFriction;
							float angVMagnitude = 5.0f * friction * normalVel / (2.0f * this->mRadius);
							if (angVMagnitude > vAtCMag / this->mRadius)
							{
								angVMagnitude = vAtCMag / this->mRadius;
							}
							Point3F vAtCDir = vAtC / vAtCMag;
							Point3F deltaOmega = angVMagnitude * mCross(-mContacts[i].normal, -vAtCDir);
							omega += deltaOmega;
							vel -= mCross(-deltaOmega, -mContacts[i].normal * this->mRadius);
						}
						vel += velocityAdd;
					}
				}
				done = false;
			}
		}
		looped = true;
		if ((itersIn > 6 && noBounce))// || itersIn > 500)
		{
			done = true;
		}
	} while (!done);
	if (vel.lenSquared() < 625.0f)
	{
		bool gotOne = false;
		Point3F dir = Point3F(0.0f, 0.0f, 0.0f);
		for (int j = 0; j < mContacts.getCount(); j++)
		{
			Point3F dir2 = dir + mContacts[j].normal;
			if (dir2.lenSquared() < 0.01f)
			{
				dir2 += mContacts[j].normal;
			}
			dir = dir2;
			gotOne = true;
		}
		if (gotOne)
		{
			dir.normalize();
			float soFar = 0.0f;
			for (int k = 0; k < mContacts.getCount(); k++)
			{
				if (mContacts[k].distance < this->mRadius)
				{
					float timeToSeparate = 0.1f;
					float dist = mContacts[k].distance;
					float outVel = mDot(vel + soFar * dir, mContacts[k].normal);
					if (timeToSeparate * outVel < dist)
					{
						soFar += (dist - outVel * timeToSeparate) / timeToSeparate / mDot(mContacts[k].normal, dir);
					}
				}
			}
			soFar = mClampF(soFar, -25.0f, 25.0f);
			vel += soFar * dir;
		}
	}
}

void Marble::getMarbleAxis(Point3F& sideDir, Point3F& motionDir, Point3F& upDir)
{
	bool zPos = F32p(mGravityDir.x) == F32p(0.0f) && F32p(mGravityDir.y) == F32p(0.0f) && F32p(mGravityDir.z) == F32p(1.0f);
	bool zNeg = F32p(mGravityDir.x) == F32p(0.0f) && F32p(mGravityDir.y) == F32p(0.0f) && F32p(mGravityDir.z) == F32p(-1.0f);
	bool yPos = F32p(mGravityDir.x) == F32p(0.0f) && F32p(mGravityDir.y) == F32p(1.0f) && F32p(mGravityDir.z) == F32p(0.0f);
	bool yNeg = F32p(mGravityDir.x) == F32p(0.0f) && F32p(mGravityDir.y) == F32p(-1.0f) && F32p(mGravityDir.z) == F32p(0.0f);
	bool xPos = F32p(mGravityDir.x) == F32p(1.0f) && F32p(mGravityDir.y) == F32p(0.0f) && F32p(mGravityDir.z) == F32p(0.0f);
	bool xNeg = F32p(mGravityDir.x) == F32p(-1.0f) && F32p(mGravityDir.y) == F32p(0.0f) && F32p(mGravityDir.z) == F32p(0.0f);

	F32 pitch = this->mCameraY;
	F32 yaw = this->mCameraX;

	if (mCameraAnglePB || zPos)
		pitch = mDegToRad(mRadToDeg(pitch)*-1);
	if (yNeg)
		pitch = mDegToRad(mRadToDeg(pitch) + 90);
	if (yPos)
		pitch = mDegToRad((mRadToDeg(pitch) - 90) * -1);
	//if (xPos)
	//	pitch = mDegToRad((mRadToDeg(pitch) - 90) * -1);
	if (mCameraAngleYB || zPos)
		yaw = mDegToRad((mRadToDeg(yaw) + 180)*-1);
	if (yNeg)
		yaw = mDegToRad((mRadToDeg(yaw) - 90)*-1);
	if (yPos)
		yaw = mDegToRad((mRadToDeg(yaw) - 90));
	//if (xPos)
		//yaw = mDegToRad((mRadToDeg(yaw) + 90));

	if (mCameraAngleFlip || yNeg || yPos)// || xPos)
		swap(pitch, yaw);

	//if (yNeg)
		//yaw *= -1;

	//Point3F mGravityDir = Point3F(0.0f, 0.0f, -1.0f);
	MatrixF camMat = MatrixF(true);

	AngAxisF xAng = AngAxisF(Point3F(-1.0f, 0.0f, 0.0f), pitch);
	AngAxisF zAng = AngAxisF(Point3F(0.0f, 0.0f, -1.0f), yaw);
	MatrixF xRot(true);
	MatrixF zRot(true);
	xAng.setMatrix(&xRot);
	zAng.setMatrix(&zRot);
	camMat = xRot * zRot;

	upDir = -mGravityDir;
	camMat.getRow(1, &motionDir);
	sideDir = mCross(motionDir, upDir);
	sideDir.normalize();
	motionDir = mCross(upDir, sideDir);
}

void Marble::reportBounce(Point3F pos, Point3F normal, float speed)
{
	if (this->mBounceYet && speed < this->mBounceSpeed)
	{
		return;
	}
	this->mBounceYet = true;
	this->mBouncePos = pos;
	this->mBounceSpeed = speed;
	this->mBounceNormal = normal;
}

void Marble::collisionCallback(SceneObject* so, void* key)
{
	Marble* self = reinterpret_cast<Marble*>(key);
	Marble* marble = dynamic_cast<Marble*>(so);
	
	if (!self->mIgnoreMarbles && marble != NULL)
	{
		if (marble->getId() != self->getId())
		{
			self->dtx2 = getMin(self->dtx2, self->dtx1 * marble->findMarbleCollisionTime(self->posx0, self->posx1, self->dtx1, self->mRadius));
		}
	}
	else {
		self->dtx2 = getMin(self->dtx2, self->dtx1 * findIntersectionTime(self, so, self->posx0, self->posx1, self->mRadius));
	}
}

F64 Marble::updateCollision(Point3F& pos1, Point3F pos0, QuatF& rot1, QuatF rot0, F64 dt)
{
	this->posx0 = pos0;
	this->posx1 = pos1;
	this->dtx1 = dt;
	this->dtx2 = dt;
	Box3F searchBox = this->mObjBox;
	Box3F startBox = searchBox;
	Point3F rad = Point3F(this->mRadius, this->mRadius, this->mRadius);
	startBox.minExtents += this->posx0 - rad;
	startBox.maxExtents += this->posx0 + rad;
	Box3F endBox = searchBox;
	endBox.minExtents += this->posx1 - rad;
	endBox.maxExtents += this->posx1 + rad;
	searchBox = startBox;
	searchBox.extend(endBox.minExtents);
	searchBox.extend(endBox.maxExtents);

	if (isServerObject())
		gServerContainer.findObjects(searchBox, sClientCollisionMask, collisionCallback, this);
	else
		gClientContainer.findObjects(searchBox, sClientCollisionMask, collisionCallback, this);

	F32 i;
	if (this->dtx1 - this->dtx2 < 0.001f)
	{
		i = this->dtx2 / this->dtx1;
		this->posx1 = i * (this->posx1 - this->posx0) + this->posx0;
		return 0.0f;
	}
	i = this->dtx2 / this->dtx1;
	this->posx1 = i * (this->posx1 - this->posx0) + this->posx0;

	// This was the pesky collision issue T_T
	pos1 = this->posx1;

	return this->dtx1 - this->dtx2;
}

void Marble::findContactCallback(SceneObject* so, void* key)
{
	Marble* self = reinterpret_cast<Marble*>(key);
	F32 ContactThreshold = (F32)MARBLE_COLLISION_TOLERANCE;
	Marble* marble = dynamic_cast<Marble*>(so);
	if (!self->mIgnoreMarbles && marble != NULL)
	{
		if (marble->getId() != self->getId())
		{
			marble->addMarbleContact(self->posx2, self->mRadius + ContactThreshold, self->mContacts);
		}
	}
	else {
		addPlatformContact(self, so, self->posx2, self->mRadius + ContactThreshold, self->mContacts);
	}
}

void Marble::findContacts(Point3F pos, QuatF rot)
{
	Box3F searchBox = this->mObjBox;
	Point3F rad = Point3F(this->mRadius, this->mRadius, this->mRadius);
	searchBox.minExtents += pos - rad;
	searchBox.maxExtents += pos + rad;
	this->posx2 = pos;
	
	if (isServerObject())
		gServerContainer.findObjects(searchBox, sClientCollisionMask, findContactCallback, this);
	else
		gClientContainer.findObjects(searchBox, sClientCollisionMask, findContactCallback, this);
}

F32 Marble::findMarbleCollisionTime(Point3F pos0, Point3F pos1, F32 dt, F32 themRadius)
{
	F32 t = 1.0f;
	Point3F usCenter = this->mWorldToObj.getPosition();
	F32 tCap;
	if (!CollisionUtil::intersectSegmentCapsule(pos0, pos1, usCenter, usCenter, themRadius + this->mRadius, &t, &tCap))
	{
		return 1.0f;
	}
	Point3F delta = pos1 - pos0;
	Point3F endPos = pos0 + t * delta;
	Point3F norm = endPos - usCenter;
	norm.normalize();
	Point3F deltaVel = pos1 - pos0 - dt * this->mVelocity;
	if (mDot(deltaVel, norm) >= 0.0f)
	{
		return 1.0f;
	}
	return t;
}

void Marble::addMarbleContact(Point3F pos, F32 radius, CollisionList& contacts)
{
	Point3F usCenter = this->mObjToWorld.getPosition();
	F32 distSq = (usCenter - pos).lenSquared();
	if (distSq <= (radius + this->mRadius) * (radius + this->mRadius))
	{
		Point3F normal = pos - usCenter;
		normal.normalize();
		Point3F collPos = usCenter + normal * this->mRadius;
		Collision* info = &contacts.increment();
		info->hasCollider = true;
		info->velocity = this->mVelocity;
		info->normal = normal;
		info->point = collPos;
		info->distance = radius - mDot(pos - info->point, normal);
		//info->restitution = 0.5f;
		//info->friction = 0.0f;
		if (this->getShape()->materialList->size() != 0)
			info->material = this->getShape()->materialList->getMaterialInst(0);
		info->object = this;
	}
}

/*void Marble::resolveCollision(Point3F pos, Point3F norm)
{
	Point3F normVel = mDot(this->mVelocity, norm) * norm;
	Point3F tangVel = this->mVelocity - normVel;
	float normSpeed = normVel.len();
	float tangSpeed = tangVel.len();
	this->mVelocity -= (1.0f + this->mElasticity) * normVel;
	if (tangSpeed < normSpeed * this->mFriction)
	{
		this->mVelocity -= tangVel;
		return;
	}
	this->mVelocity -= normSpeed * this->mFriction / (tangSpeed + 0.001f) * tangVel;
}

void Marble::resolveCollisions(CollisionList& list)
{
	for (int i = 0; i < list.getCount(); i++)
	{
		this->resolveCollision(list[i].point, list[i].normal);
	}
}*/

void Marble::cameraLookAtPt(Point3F pt)
{
	Point3F position = this->getPosition();

	F32 dx = pt.x - position.x;
	F32 dy = pt.y - position.y;

	F32 angle = mAtan2(dx, dy);

	this->mCameraX = angle;

	setMaskBits(CameraSetMask);
}

void Marble::writePacketData(GameConnection * conn, BitStream *stream)
{
	Parent::writePacketData(conn, stream);

	/*Point3F pos;
	getTransform().getColumn(3, &pos);
	Point3F rot = getTransform().toEuler();
	stream->setCompressionPoint(pos);
	stream->write(pos.x);
	stream->write(pos.y);
	stream->write(pos.z);
	stream->write(rot.x);
	stream->write(rot.y);
	stream->write(rot.z);
	stream->write(mVelocity.x);
	stream->write(mVelocity.y);
	stream->write(mVelocity.z);
	stream->write(mRotVelocity.x);
	stream->write(mRotVelocity.y);
	stream->write(mRotVelocity.z);*/
}

void Marble::readPacketData(GameConnection * conn, BitStream *stream)
{
	Parent::readPacketData(conn, stream);

	/*Point3F pos, rot;
	stream->read(&pos.x);
	stream->read(&pos.y);
	stream->read(&pos.z);
	stream->read(&rot.x);
	stream->read(&rot.y);
	stream->read(&rot.z);
	stream->read(&mVelocity.x);
	stream->read(&mVelocity.y);
	stream->read(&mVelocity.z);
	stream->read(&mRotVelocity.x);
	stream->read(&mRotVelocity.y);
	stream->read(&mRotVelocity.z);
	stream->setCompressionPoint(pos);*/

	//delta.pos = pos;
	//delta.rot = rot;

	//setPosition(pos, rot);
}

U32  Marble::packUpdate(NetConnection * conn, U32 mask, BitStream *stream)
{
	U32 retMask = Parent::packUpdate(conn, mask, stream);

	//if (isServerObject())
	//{
		if (stream->writeFlag(mask & InitialMask))
		{
			stream->write(mStatic);
			stream->write(mDisableRotation);
			stream->write(mDisablePhysics);
		}

		if (stream->writeFlag(mask & (BoundsMask)))
			stream->write(mOutOfBounds);

		if (stream->writeFlag(mask & (UpdateMask)))
		{
			stream->write(this->mCurrentMass);
			stream->write(this->mCurrentSize);
			stream->write(this->mDesiredSize);
			mathWrite(*stream, this->mCameraAngle);
			mathWrite(*stream, this->mCameraYAngle);
			mathWrite(*stream, this->mCameraPAngle);
			stream->write(mCameraAnglePB);
			stream->write(mCameraAngleYB);
			stream->write(mCameraAngleFlip);
			stream->write(mCameraAngleOrderFlip);

			mathWrite(*stream, this->mGravityDir);
			mathWrite(*stream, this->mGravityMatrix);

			stream->write(mMode);
			//mathWrite(*stream, this->getTransform());

			//stream->write(this->mJumped);
			//this->mJumped = false;

			stream->write(this->mBlastEnergy);

			stream->write(this->mHitShake);
			this->mHitShake = false;
			stream->write(this->mBounceSpeed);
		}

		if (stream->writeFlag(mask & MoveMask))
		{
			mathWrite(*stream, mVelocity);
			mathWrite(*stream, mRotVelocity);
			mathWrite(*stream, this->getTransform());

			//Point3F pos;
			//getTransform().getColumn(3, &pos);
			//stream->writeCompressedPoint(pos);

			//Point3F rot = getTransform().toEuler();
			//stream->writeCompressedPoint(rot);

			//F32 len = mVelocity.len();
			//if (stream->writeFlag(len > 0.02f))
			//{
			//	Point3F outVel = mVelocity;
			//	outVel *= 1.0f / len;
			//	stream->writeNormalVector(outVel, 10);
			//	len *= 32.0f;  // 5 bits of fraction
			//	if (len > 8191)
			//		len = 8191;
			//	stream->writeInt((S32)len, 13);
			//}

			stream->write(mTotalTime);
			stream->write(mContactTime);
			stream->write(mRollVolume);

			//stream->writeFlag(!(mask & NoWarpMask));
		}

		if (stream->writeFlag(mask & CameraSetMask))
		{
			stream->write(this->mCameraX);
			stream->write(this->mCameraY);
		}

		if (stream->writeFlag(mask & TimeMask))
		{
			stream->write(this->mTime);
			stream->write(this->mBonusTime);
		}
	//}

	return retMask;
}

void Marble::unpackUpdate(NetConnection * conn, BitStream *stream)
{
	Parent::unpackUpdate(conn, stream);

	//if (isClientObject())
	//{
		if (stream->readFlag())
		{
			stream->read(&mStatic);
			stream->read(&mDisableRotation);
			stream->read(&mDisablePhysics);
		}

		if (stream->readFlag())
			stream->read(&mOutOfBounds);

		if (stream->readFlag())
		{
			stream->read(&this->mCurrentMass);
			stream->read(&this->mCurrentSize);
			stream->read(&this->mDesiredSize);
			mathRead(*stream, &this->mCameraAngle);
			mathRead(*stream, &this->mCameraYAngle);
			mathRead(*stream, &this->mCameraPAngle);
			stream->read(&mCameraAnglePB);
			stream->read(&mCameraAngleYB);
			stream->read(&mCameraAngleFlip);
			stream->read(&mCameraAngleOrderFlip);

			mathRead(*stream, &this->mGravityDir);
			mathRead(*stream, &this->mGravityMatrix);

			S32 mode = 0;
			stream->read(&mode);
			this->mMode = (MarbleMode)mode;

			//MatrixF mat;
			//mathRead(*stream, &mat);

			//this->setTransform(mat);

			//stream->read(&mJumped);

			stream->read(&this->mBlastEnergy);

			stream->read(&this->mHitShake);
			stream->read(&this->mBounceSpeed);

			if (this->mHitShake && smHitShake)
			{
				this->mHitShake = false;
				this->applyCameraShake(this->mBounceSpeed / 100.0f, 10.0f, 10.0f, 0.5f);
			}
		}
		//MatrixF mat = getTransform();

		if (stream->readFlag())
		{
			MatrixF mat = getTransform();
			mathRead(*stream, &mVelocity);
			mathRead(*stream, &mRotVelocity);

			mathRead(*stream, &mat);

			this->setTransform(mat);

			//Point3F pos, rot;
			//stream->readCompressedPoint(&pos);
			//stream->readCompressedPoint(&rot);
			/*F32 speed = mVelocity.len();
			if (stream->readFlag())
			{
				stream->readNormalVector(&mVelocity, 10);
				mVelocity *= stream->readInt(13) / 32.0f;
			}
			else
			{
				mVelocity.set(0.0f, 0.0f, 0.0f);
			}*/

			stream->read(&mTotalTime);
			stream->read(&mContactTime);
			stream->read(&mRollVolume);

			//EulerF oldRot = getTransform().toEuler();
			//EulerF rot = mat.toEuler();

			//if (stream->readFlag() && isProperlyAdded())
			//{
			//	// Determine number of ticks to warp based on the average
			//	// of the client and server velocities.
			//	delta.warpOffset = pos - delta.pos;
			//	F32 as = (speed + mVelocity.len()) * 0.5f * TickSec;
			//	F32 dt = (as > 0.00001f) ? delta.warpOffset.len() / as : sMaxWarpTicks;
			//	delta.warpTicks = (S32)((dt > sMinWarpTicks) ? getMax(mFloor(dt + 0.5f), 1.0f) : 0.0f);

			//	if (delta.warpTicks)
			//	{
			//		// Setup the warp to start on the next tick.
			//		if (delta.warpTicks > sMaxWarpTicks)
			//			delta.warpTicks = sMaxWarpTicks;
			//		delta.warpOffset /= (F32)delta.warpTicks;

			//		delta.rotOffset = rot - delta.rot;

			//		// Ignore small rotation differences
			//		//if (mFabs(delta.rotOffset.z) < 0.001f)
			//		//	delta.rotOffset.z = 0;

			//		delta.rotOffset /= (F32)delta.warpTicks;
			//	}
			//	else
			//	{
			//		// Going to skip the warp, server and client are real close.
			//		// Adjust the frame interpolation to move smoothly to the
			//		// new position within the current tick.
			//		Point3F cp = delta.pos + delta.posVec * delta.dt;
			//		Point3F cr = delta.rot + delta.rotVec * delta.dt;
			//		if (delta.dt == 0)
			//		{
			//			delta.posVec.set(0.0f, 0.0f, 0.0f);
			//			delta.rotVec.set(0.0f, 0.0f, 0.0f);
			//		}
			//		else
			//		{
			//			F32 dti = 1.0f / delta.dt;
			//			delta.posVec = (cp - pos) * dti;
			//			//delta.rotVec.z = mRot.z - rot.z;
			//			delta.rotVec = (cr - rot) * dti; //oldRot - rot;

			//			//delta.rotVec *= dti;
			//		}
			//		delta.pos = pos;
			//		delta.rot = rot;
			//		setPosition(pos, rot);
			//	}
			//}
			//else {
			//	// Set the player to the server position
			//	delta.pos = pos;
			//	delta.rot = rot;
			//	delta.posVec.set(0.0f, 0.0f, 0.0f);
			//	delta.rotVec.set(0.0f, 0.0f, 0.0f);
			//	delta.warpTicks = 0;
			//	delta.dt = 0.0f;
			//	setPosition(pos, rot);
			//}
		}

		if (stream->readFlag())
		{
			stream->read(&this->mCameraX);
			stream->read(&this->mCameraY);
		}

		if (stream->readFlag())
		{
			stream->read(&this->mTime);
			stream->read(&this->mBonusTime);
		}

		/*if (mJumped)
		{
			if (isClientObject() && getControllingClient() == NULL && this->mDataBlock->sound[this->mDataBlock->JumpSound] != NULL)
			{
				MatrixF transform = getTransform();
				Point3F velocity = getVelocity();
				if (this->mDataBlock->sound[this->mDataBlock->JumpSound] != NULL)
					SFX->playOnce(this->mDataBlock->sound[this->mDataBlock->JumpSound], &transform, &velocity);
			}
			mJumped = false;
		}*/

		//this->setTransform(mat);
	//}
}

void Marble::drawContactsDebug()
{
    int ms = 250;
    for (int i = 0; i < this->mContacts.getCount(); i++)
    {
        Collision col = this->mContacts[i];

        Point3F point = col.point;
        VectorF normal = col.normal;
        F32 distance = col.distance;

        DebugDrawer::get()->drawText(point, std::to_string(distance).c_str(), ColorF(1, 1, 1));
        DebugDrawer::get()->setLastTTL(ms);
        DebugDrawer::get()->drawLine(point, point + (normal / 2.0f), ColorF(1.0f, 0.0f, 0.0f));
        DebugDrawer::get()->setLastTTL(ms);
    }
}


/*void Marble::prepBatchRender(SceneState *state, S32 mountedImageIndex)
{
	if (Con::getBoolVariable("RenderMarbleDebug", false))
	{
		RenderInst *ri = gRenderInstManager.allocInst();
		ri->obj = this;
		ri->state = state;
		ri->type = RenderInstManager::RIT_Object;
		gRenderInstManager.addInst(ri);
		return;
	}

	if (Con::getBoolVariable("RenderMarbleBounds", false))
	{
		wireCube(Point3F(this->mRadius, this->mRadius, this->mRadius), this->getPosition());
	}

	Parent::prepBatchRender(state, mountedImageIndex);
}

void Marble::renderLineCircle(F32 offset)
{
	PrimBuild::begin(GFXPrimitiveType::GFXLineList, 360 * 3 * 1);

	PrimBuild::color3f(0.0f, 0.3f, 0.8f);

	for (U32 i = 0; i < 360; i++)
	{
		F32 degInRad = mDegToRad((F32)i);
		PrimBuild::vertex3f(mCos(degInRad) * this->mRadius, mSin(degInRad) * this->mRadius, 0.0f + offset);
		//PrimBuild::vertex3f(mCos(degInRad) * this->mRadius, mSin(degInRad) * this->mRadius, 0.1f);
		//PrimBuild::vertex3f(mCos(degInRad) * this->mRadius, mSin(degInRad) * this->mRadius, -0.1f);
	}

	for (U32 i = 0; i < 360; i++)
	{
		F32 degInRad = mDegToRad((F32)i);

		PrimBuild::vertex3f(mCos(degInRad) * this->mRadius, 0.0f + offset, mSin(degInRad) * this->mRadius);
		//PrimBuild::vertex3f(mCos(degInRad) * this->mRadius, 0.1f, mSin(degInRad) * this->mRadius);
		//PrimBuild::vertex3f(mCos(degInRad) * this->mRadius, -0.1f, mSin(degInRad) * this->mRadius);
	}

	for (U32 i = 0; i < 360; i++)
	{
		F32 degInRad = mDegToRad((F32)i);
		PrimBuild::vertex3f(0.0f + offset, mCos(degInRad) * this->mRadius, mSin(degInRad) * this->mRadius);
		//PrimBuild::vertex3f(0.1f, mCos(degInRad) * this->mRadius, mSin(degInRad) * this->mRadius);
		//PrimBuild::vertex3f(-0.1f, mCos(degInRad) * this->mRadius, mSin(degInRad) * this->mRadius);
	}

	PrimBuild::end();
}

void Marble::renderObject(SceneState *state, RenderInst *ri)
{
	GFX->pushWorldMatrix();

	// setup world matrix - for fixed function
	MatrixF world = GFX->getWorldMatrix();
	MatrixF mat(1);
	AngAxisF rotation(Point3F(1, 0, 0), 0);
	rotation.setMatrix(&mat);
	mat.setPosition(this->getPosition());
	world.mul(mat);//getRenderTransform());
	world.scale(Point3F(1, 1, 1));//getScale());
	GFX->setWorldMatrix(world);

	// setup world matrix - for shaders
	MatrixF proj = GFX->getProjectionMatrix();
	proj.mul(world);
	proj.transpose();
	GFX->setVertexShaderConstF(0, (float*)&proj, 4);

	GFX->setAlphaBlendEnable(true);
	GFX->setSrcBlend(GFXBlendSrcAlpha);
	GFX->setDestBlend(GFXBlendInvSrcAlpha);

	GFX->setTextureStageColorOp(0, GFXTOPDisable);
	GFX->setTexture(0, NULL);

	// Circle
	this->renderLineCircle(-0.1f);
	this->renderLineCircle(0.0f);
	this->renderLineCircle(0.1f);

	PrimBuild::begin(GFXPrimitiveType::GFXLineList, 6 * 1);

	PrimBuild::color3f(1.0f, 0.0f, 0.0f);

	PrimBuild::vertex3f(-this->mRadius, 0.0f, 0.0f);
	PrimBuild::vertex3f(this->mRadius, 0.0f, 0.0f);

	PrimBuild::vertex3f(0.0f, -this->mRadius, 0.0f);
	PrimBuild::vertex3f(0.0f, this->mRadius, 0.0f);

	PrimBuild::vertex3f(0.0f, 0.0f, -this->mRadius);
	PrimBuild::vertex3f(0.0f, 0.0f, this->mRadius);

	PrimBuild::end();
	*/
	/*PrimBuild::begin(GFXPrimitiveType::GFXPointList, 1);

	PrimBuild::color3f(0.0f, 0.0f, 1.0f);

	PrimBuild::vertex3f(0.0f, 0.0f, 0.0f);

	PrimBuild::end();*/
/*
	GFX->popWorldMatrix();
}*/

F32 Marble::getMass() const
{
	return mCurrentMass;
}

void Marble::setPosition(const Point3F& pos, const AngAxisF& rot, float rot1)
{
	//this->mSpawnDelay = true;

	// TODO: mat is not initialized...is this a problem?
	MatrixF mat;
	rot.setMatrix(&mat);
	mat.setColumn(3, pos);
	Parent::setTransform(mat);
	Parent::setRenderTransform(mat);

	this->mCameraY = rot1;

	EulerF euler = mat.toEuler();
	this->mCameraX = euler.z;

	setMaskBits(MoveMask | CameraSetMask | NoWarpMask);
}

void Marble::setPosition(const Point3F& pos)
{
	MatrixF mat;
	mat.setColumn(3, pos);
	Parent::setTransform(mat);
	Parent::setRenderTransform(mat);

	setMaskBits(MoveMask | NoWarpMask);
}

void Marble::setPosition(const Point3F& pos, const QuatF& rot)
{
	MatrixF mat;
	rot.setMatrix(&mat);
	mat.setColumn(3, pos);

	setMaskBits(MoveMask | NoWarpMask);
	Parent::setTransform(mat);
}

const MatrixF Marble::getTransform()
{
	return Parent::getTransform();
}

void Marble::setTransform(const MatrixF& newMat)
{
	Parent::setTransform(newMat);
	mContacts.clear();

	setMaskBits(NoWarpMask);
}

void Marble::applyImpulse(const Point3F &pos, const VectorF &vec)
{
	// Matt: This needs more work, I think

	//Point3F adjustedVec = vec * 10;

	//Con::errorf("Marble::applyImpulse(\"%g %g %g\", \"%g %g %g\")", pos.x, pos.y, pos.z, adjustedVec.x, adjustedVec.y, adjustedVec.z);
	this->mVelocity += vec;
}

void Marble::setVelocity(const VectorF& velocity)
{
	Parent::setVelocity(velocity);
	this->mVelocity = velocity;
}

Point3F Marble::getVelocity() const
{
	return this->mVelocity;
}

void Marble::setVelocityRot(const VectorF& velocity)
{
	this->mRotVelocity = velocity;
}

Point3F Marble::getVelocityRot() const
{
	return this->mRotVelocity;
}

AngAxisF Marble::getRotation()
{
	return AngAxisF(this->getTransform());
}

void Marble::setOOB(bool flag)
{
	this->mOutOfBounds = flag;
	this->setMaskBits(BoundsMask);
}

bool Marble::isOOB()
{
	return this->mOutOfBounds;
}

void Marble::setMode(const char* m)
{
	if (strlen(m) >= 1024)
	{
		Con::warnf("Marble::setMode: mode string too long!");
		return;
	}
	char m2[1024];
	dStrcpy(m2, m);
	const char* mode = dStrlwr(m2);
	if (!dStrcmp(mode, "normal"))
	{
		this->mMode = Marble::Normal;
	}
	else if (!dStrcmp(mode, "start"))
	{
		this->mMode = Marble::Start;
	}
	else if (!dStrcmp(mode, "victory"))
	{
		this->mMode = Marble::Victory;
	}
	else {
		Con::warnf("Marble::setMode: Unknown Marble Mode!");
	}
	this->setMaskBits(UpdateMask);
}

void Marble::setTime(S32 time)
{
	this->mTime = time;
	setMaskBits(TimeMask);
}

S32 Marble::getTime()
{
	return this->mTime;
}

void Marble::setUseFullTime(bool flag)
{
	this->mUseFullTime = flag;
	setMaskBits(TimeMask);
}

void Marble::incBonusTime(S32 time)
{
	this->mBonusTime += time;
	setMaskBits(TimeMask);
}

void Marble::setBonusTime(S32 time)
{
	this->mBonusTime = time;
	setMaskBits(TimeMask);
}

S32 Marble::getBonusTime()
{
	return this->mBonusTime;
}

void Marble::setPowerUpId(S32 id, bool reset)
{
	this->mPowerUpId = id;
	this->mPowerUpReset = reset;
}

void Marble::setBlastEnergy(F32 energy)
{
	this->mBlastEnergy = energy;
	setMaskBits(UpdateMask);
}

F32 Marble::getBlastEnergy()
{
	return this->mBlastEnergy;
}

void Marble::setGravityDir(Point3F dir, MatrixF gravMat, bool snap)
{
	//MatrixF mat;
	//dir.setMatrix(&mat);
	//EulerF euler = mat.toEuler();
	//dir.getColumn(2, &euler);

	//Point3F dir;
	//dir.x = dirO.x;
	//dir.y = dirO.z;
	//dir.z = dirO.y;

	//dir *= -1;

	//Con::printf("Angle: EulerF(%g, %g, %g)", dir.x, dir.y, dir.z);

	//AngAxisF ang = AngAxisF(dir);
	
	//Con::printf("Angle: AxisAngleF(%g, %g, %g, %g)", ang.axis.x, ang.axis.y, ang.axis.z, ang.angle);

	//MatrixF mat;
	//ang.setMatrix(&mat);
	//this->mGravityDir = mat;

	//MatrixF mat = MatrixF(EulerF(dir));

	this->mGravityMatrix = gravMat;

	this->mGravityDir = dir;

	this->mGravitySnap = snap;

	setMaskBits(UpdateMask);
}

EulerF Marble::getGravityDir()
{
	return this->mGravityDir;
}

void Marble::setPad(SceneObject* pad)
{
	this->mPad = pad;
}

SceneObject* Marble::getPad()
{
	return this->mPad;
}

Point3F Marble::getLastContactPosition()
{
	return this->mLastContactPosition;
}

DefineEngineMethod(Marble, setPosition, void, (TransformF transform, F32 rotation),, "setPosition(Position P, Rotation r)")
{
	//Point3F pos;
	//const MatrixF& tmat = object->getTransform();
	//tmat.getColumn(3, &pos);
	//AngAxisF aa(tmat);

	//dSscanf(argv[2], "%f %f %f %f %f %f %f",
	//	&pos.x, &pos.y, &pos.z, &aa.axis.x, &aa.axis.y, &aa.axis.z, &aa.angle);
	//pos = transform.mPosition;
	//aa = transform.mOrientation;

	//F32 rotVal = rotation;//dAtof(argv[3]);

	//MatrixF mat;
	//transform.getOrientation().setMatrix(&mat);
	//mat.setColumn(3, transform.getPosition());
	object->setPosition(transform.getPosition(), transform.getOrientation(), rotation);
}

ConsoleMethod(Marble, getVelocity, const char *, 2, 2, "getVelocity()")
{
	const VectorF& vel = object->getVelocity();
	char* buff = Con::getReturnBuffer(100);
	dSprintf(buff, 100, "%g %g %g", vel.x, vel.y, vel.z);
	return buff;
}

ConsoleMethod(Marble, setVelocity, bool, 3, 3, "setVelocity(Vector3F vel)")
{
	VectorF vel(0, 0, 0);
	dSscanf(argv[2], "%f %f %f", &vel.x, &vel.y, &vel.z);
	object->setVelocity(vel);
	return true;
}

ConsoleMethod(Marble, getVelocityRot, const char *, 2, 2, "getVelocityRot")
{
	const VectorF& vel = object->getVelocityRot();
	char* buff = Con::getReturnBuffer(100);
	dSprintf(buff, 100, "%g %g %g", vel.x, vel.y, vel.z);
	return buff;
}

ConsoleMethod(Marble, setVelocityRot, bool, 3, 3, "setVelocityRot(Vector3F vel)")
{
	VectorF vel(0, 0, 0);
	dSscanf(argv[2], "%f %f %f", &vel.x, &vel.y, &vel.z);
	object->setVelocityRot(vel);
	return true;
}

ConsoleMethod(Marble, getRotation, const char *, 2, 2, "getRotation()")
{
	const AngAxisF& rot = object->getRotation();
	char* buff = Con::getReturnBuffer(100);
	dSprintf(buff, 100, "%g %g %g %g", rot.angle, rot.axis.x, rot.axis.y, rot.axis.z);
	return buff;
}

/*ConsoleMethod(Marble, getTestLoopCount, const char *, 2, 2, "")
{
	const U32& count = object->testLoopCount;
	char* buff = Con::getReturnBuffer(100);
	dSprintf(buff, 100, "%d", count);
	return buff;
}

ConsoleMethod(Marble, getPeakTestLoopCount, const char *, 2, 2, "")
{
	const U32& count = object->peakTestLoopCount;
	char* buff = Con::getReturnBuffer(100);
	dSprintf(buff, 100, "%d", count);
	return buff;
}*/

ConsoleMethod(Marble, setOOB, void, 3, 3, "setOOB(bool)")
{
	bool oob = dAtob(argv[2]);
	object->setOOB(oob);
}

DefineEngineMethod(Marble, isOOB, bool, (),, "isOOB()")
{
	return object->isOOB();
}

ConsoleMethod(Marble, setMode, void, 3, 3, "setMode(mode)")
{
	const char* mode = argv[2];
	object->setMode(mode);
}

ConsoleMethod(Marble, setMarbleTime, void, 3, 3, "setMarbleTime(int)")
{
	S32 time = dAtoi(argv[2]);
	object->setTime(time);
}

ConsoleMethod(Marble, getMarbleTime, const char *, 2, 2, "getMarbleTime()")
{
	S32 time = object->getTime();
	char* buff = Con::getReturnBuffer(100);
	dSprintf(buff, 100, "%d", time);

	return buff;
}

ConsoleMethod(Marble, setUseFullMarbleTime, void, 3, 3, "setUseFullMarbleTime(bool)")
{
	bool flag = dAtob(argv[2]);
	object->setUseFullTime(flag);
}

ConsoleMethod(Marble, setMarbleBonusTime, void, 3, 3, "setMarbleBonusTime(int)")
{
	S32 bonusTime = dAtoi(argv[2]);
	object->setBonusTime(bonusTime);
}

ConsoleMethod(Marble, getMarbleBonusTime, const char *, 2, 2, "getMarbleBonusTime()")
{
	S32 bonusTime = object->getBonusTime();
	char* buff = Con::getReturnBuffer(100);
	dSprintf(buff, 100, "%d", bonusTime);

	return buff;
}

ConsoleMethod(Marble, setPowerUpId, void, 3, 4, "setPowerUpId(int)")
{
	S32 id = dAtoi(argv[2]);
	bool reset = false;
	if (argc > 2)
		reset = dAtob(argv[3]);
	object->setPowerUpId(id, reset);
}

ConsoleMethod(Marble, setBlastEnergy, void, 3, 3, "setBlastEnergy(float)")
{
	F32 energy = dAtof(argv[2]);
	object->setBlastEnergy(energy);
}

DefineConsoleMethod(Marble, getBlastEnergy, F32, (), , "getBlastEnergy()")
{
	return object->getBlastEnergy();
}

ConsoleMethod(Marble, setGravityDir, void, 3, 4, "setGravityDir(ortho, instant)")
//DefineEngineMethod(Marble, setGravityDir, void, (MatrixF dir, bool instant), (false), "setGravityDir(dir, bool)")
{
	bool instant = false;
	if (argc > 3)
		instant = dAtob(argv[3]);

	Point3F col0, col1, col2;

	dSscanf(argv[2], "%g %g %g %g %g %g %g %g %g", &col0.x, &col0.y, &col0.z, &col1.x, &col1.y, &col1.z, &col2.x, &col2.y, &col2.z);

	// need to figure out how this works properly...
	// maybe inverting col1 under certain conditions?
	MatrixF gravMat(1);

	object->setGravityDir(col2, gravMat, instant);
}

DefineEngineMethod(Marble, getGravityDir, EulerF, (),, "getGravityDir()")
{
	return object->getGravityDir();
}

DefineEngineMethod(Marble, setCameraAngleTest, void, (AngAxisF ang, Point3F yAngle, Point3F pAngle, bool pInv, bool yInv, bool flip, bool orderFlip), , "setCameraAngle(rotation, yAngle, pAngle, pInv, yInv, flip, orderFlip)")
{
	object->mCameraAngle = ang;
	object->mCameraYAngle = yAngle;
	object->mCameraPAngle = pAngle;
	object->mCameraAnglePB = pInv;
	object->mCameraAngleYB = yInv;
	object->mCameraAngleFlip = flip;
	object->mCameraAngleOrderFlip = orderFlip;

	object->setMaskBits(Marble::UpdateMask);
}

DefineEngineMethod(Marble, setCameraAngle, void, (AngAxisF ang), , "setCameraAngle(rotation)")
{
	object->mCameraAngle = ang;
	object->mCameraYAngle = Point3F(0.0f, 0.0f, 1.0f);
	object->mCameraPAngle = Point3F(1.0f, 0.0f, 0.0f);
	object->mCameraAnglePB = false;
	object->mCameraAngleYB = false;
	object->mCameraAngleFlip = false;
	object->mCameraAngleOrderFlip = false;

	object->setMaskBits(Marble::UpdateMask);
}

ConsoleMethod(Marble, setPad, void, 3, 3, "setPad()")
{
	SceneObject* pad = dynamic_cast<SceneObject*>(Sim::findObject(argv[2]));
	object->setPad(pad);
}

ConsoleMethod(Marble, getPad, const char *, 2, 2, "getPad()")
{
	SceneObject* pad = object->getPad();
	char* buff = Con::getReturnBuffer(100);
	dSprintf(buff, 100, "%s", pad->getIdString());

	return buff;
}

DefineEngineMethod(Marble, cameraLookAtPt, void, (Point3F pt), , "cameraLookAtPt(pt)")
{
	// Matt: Not yet implemented so why not xD
	//Con::warnf("Marble::cameraLookAtPt: Sorry I won't look at that gem group because it's not poutine.");

	object->cameraLookAtPt(pt);
}

DefineEngineMethod(Marble, getLastContactPosition, Point3F, (), , "cameraLookAtPt(position)")
{
	return object->getLastContactPosition();
}

DefineEngineMethod(Marble, applyCameraShake, void, (F32 amplifier, F32 freq, F32 falloff, F32 duration), , "applyCameraShake(amplifier, freq, falloff, duration)")
{
	object->applyCameraShake(amplifier, freq, falloff, duration);
}

// Matt: why did GG put this in GameConnection instead of marble?
ConsoleMethod(GameConnection, incBonusTime, void, 3, 3, "incBonusTime()")
{
	Marble* marble = dynamic_cast<Marble*>(object->getControlObject());
	if (marble != NULL)
	{
		marble->incBonusTime(dAtoi(argv[2]));
	}
}
