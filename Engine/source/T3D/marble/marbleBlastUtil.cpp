#include "T3D/marble/marbleBlastUtil.h"

#include "console/console.h"
#include "console/simBase.h"
#include "scene/sceneObject.h"
#include "T3D/missionMarker.h"

Vector<SpawnSphere*> usedSpawnSpheres;

//void findSpawnSphereCallback(SceneObject* obj, void* key)
//{
//	SpawnSphere* spawnSphere = dynamic_cast<SpawnSphere*>(obj);
//
//	if (spawnSphere)
//	{
//		usedSpawnSpheres.push_back(spawnSphere);
//		SimGroup* group = (SimGroup*)key;
//		group->addObject(obj);
//	}
//}

SpawnSphere* findClosestSpawnSphere(SimGroup* gemSpawns, Point3F position, S32 radius)
{
	SpawnSphere* bestSpawnSphere = NULL;
	S32 bestDist = 0;

	for (SimGroup::iterator i = gemSpawns->begin(); i != gemSpawns->end(); i++)
	{
		SpawnSphere* spawnSphere = dynamic_cast<SpawnSphere*>(*i);

		if (spawnSphere)
		{
			if (usedSpawnSpheres.contains(spawnSphere))
				continue;

			F32 dist = mFabs(mFabs(spawnSphere->getPosition().len()) - mFabs(position.len()));

			if (bestSpawnSphere == NULL || dist < bestDist)
			{
				bestSpawnSphere = spawnSphere;
				bestDist = dist;
			}
		}
	}
	return bestSpawnSphere;
}

void findSpawnSphere(Point3F position, SimGroup* gemSpawns, SimGroup* group, Box3F searchBox, S32 &index, S32 &index2, S32 radius, S32 maxGems)
{
	//for (SimGroup::iterator i = gemSpawns->begin(); i != gemSpawns->end(); i++)
	while (index < maxGems)
	{
		SpawnSphere* spawnSphere = findClosestSpawnSphere(gemSpawns, position, radius);//dynamic_cast<SpawnSphere*>(*i);

		if (spawnSphere)
		{
			//if (usedSpawnSpheres.contains(spawnSphere))
			//	continue;

			if (!searchBox.isOverlapped(spawnSphere->getObjBox()))
				continue;

			usedSpawnSpheres.push_back(spawnSphere);
			group->addObject(spawnSphere);
			index++;
			index2++;
		}
	}
}

ConsoleFunction(SetupGemSpawnGroups, void, 3, 3, "SetupGemSpawnGroups(%gemGroupRadius, %maxGemsPerGroup)")
{
	S32 radius = dAtoi(argv[1]);
	S32 maxGems = dAtoi(argv[2]);

	usedSpawnSpheres.clear();

	SimObject* oldSpawnGroups = Sim::findObject("GemSpawnGroups");
	if (oldSpawnGroups)
		oldSpawnGroups->deleteObject();

	SimGroup* simGroup = new SimGroup;
	simGroup->assignName("GemSpawnGroups");
	if (!simGroup->registerObject())
	{
		Con::errorf("Failed to create GemSpawnGroups object!");
		return;
	}
	SimGroup *cleanup = dynamic_cast<SimGroup *>(Sim::findObject("MissionCleanup"));
	if (cleanup)
	{
		cleanup->addObject(simGroup);
	}

	SimGroup* gemSpawns = dynamic_cast<SimGroup*>(Sim::findObject("GemSpawns"));
	if (!gemSpawns)
	{
		Con::errorf("Failed to find GemSpawns group!");
		return;
	}

	/*for (SimGroup::iterator i = gemSpawns->begin(); i != gemSpawns->end(); i++)
	{
		SceneObject* sceneObj = dynamic_cast<SceneObject*>(*i);

		if (sceneObj && !sceneObj->getContainer())
			sceneObj->addToScene();
	}*/

	SimGroup* group = NULL;

	S32 index = 0;
	S32 index2 = 0;
	for (SimGroup::iterator i = gemSpawns->begin(); i != gemSpawns->end(); i++)
	{
		if (index2 >= gemSpawns->size())
			break;

		if (group == NULL || index >= maxGems)
		{
			index = 0;
			group = new SimGroup;
			group->registerObject();
			simGroup->addObject(group);
		}

		//SceneObject* sceneObj = dynamic_cast<SceneObject*>(*i);

		/*if (!sceneObj)
		{
			SceneObject* sceneObj2 = new SceneObject;
			if (!sceneObj2->registerObject())
			{
				Con::warnf("Failed to create SceneObject!");
				continue;
			}


		}*/

		SpawnSphere* spawnSphere = dynamic_cast<SpawnSphere*>(*i);

		if (spawnSphere)
		{
			//if (spawnSphere->getGroup() != NULL)
			//	continue;
			if (usedSpawnSpheres.contains(spawnSphere))
				continue;
			usedSpawnSpheres.push_back(spawnSphere);
			group->addObject(spawnSphere);
			index++;
			index2++;
			/*if (spawnSphere->isServerObject())
			{
				Box3F searchBox = spawnSphere->getObjBox();
				Point3F rad = Point3F(radius, radius, radius);
				searchBox.minExtents -= rad;
				searchBox.maxExtents += rad;

				gServerContainer.findObjects(searchBox, StaticShapeObjectType | StaticObjectType, findSpawnSphereCallback, group);
			}
			else {
				Box3F searchBox = spawnSphere->getObjBox();
				Point3F rad = Point3F(radius, radius, radius);
				searchBox.minExtents -= rad;
				searchBox.maxExtents += rad;

				gClientContainer.findObjects(searchBox, StaticShapeObjectType | StaticObjectType, findSpawnSphereCallback, group);
			}*/

			Box3F searchBox = spawnSphere->getObjBox();
			Point3F rad = Point3F(radius, radius, radius);
			searchBox.minExtents -= rad;
			searchBox.maxExtents += rad;
			findSpawnSphere(spawnSphere->getPosition(), gemSpawns, group, searchBox, index, index2, radius, maxGems);
		}
	}

	Con::printf("Created %d gem spawn groups using radius %d and max gems %d", simGroup->size(), radius, maxGems);
}

void rotateMatrix(MatrixF& mat, Point3F pnt)
{
	const F32 len = pnt.len();
	if (len != 0.0f)
	{
		pnt.x = (1.0f / len) * pnt.x;
		pnt.y = (1.0f / len) * pnt.y;
		pnt.z = (1.0f / len) * pnt.z;
		F32 sin;
		F32 cos;
		mSinCos(len, sin, cos);

		int i = 0;
		do
		{
			F32 mX = mat.m[i];//*(mat + i);
			F32 mY = mat.m[i + 4];//*(mat + i + 4);
			F32 mZ = mat.m[i + 8];//*(mat + i + 8);

			F32 pZ = pnt.z;
			F32 pY = pnt.y;
			F32 pX = pnt.x;

			F32 value1 = ((mX * pnt.x) + (mY * pnt.y)) + (mZ * pnt.z);
			F32 value2 = (((mX * pX) + (mY * pnt.y)) + (mZ * pnt.z)) * pX;

			F32 yVal = pY * value1;
			F32 zVal = value1 * pZ;
			F32 xVal2 = mX - value2;
			F32 yVal2 = mY - yVal;
			F32 zVal2 = mZ - zVal;

			mat.m[0] = (((pY * zVal2) - (yVal2 * pZ)) * sin) + ((xVal2 * cos) + value2);
			mat.m[4] = (((pZ * xVal2) - (zVal2 * pX)) * sin) + ((yVal2 * cos) + yVal);
			mat.m[8] = (((pX * yVal2) - (pY * xVal2)) * sin) + (zVal + (zVal2 * cos));

			++i;

			*mat = *(mat + 4);
		} while (i != 3);
		mat.normalize();
	}
}
