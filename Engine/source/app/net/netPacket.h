#ifndef _NETPACKET_H_
#define _NETPACKET_H_

#include "platform/platform.h"
#include "core/dnet.h"
#include "core/idGenerator.h"
#include "core/stream/bitStream.h"
#include "console/simBase.h"
#include "console/console.h"
#include "console/consoleTypes.h"
#include "sim/netConnection.h"
#include "sim/netObject.h"
#include "app/net/serverQuery.h"
#include "console/engineAPI.h"

class NetPacketEvent : public NetEvent
{
public:
	typedef NetEvent Parent;

private:
	BitStream* mBStream;
	typedef void(*OnPacketReceivedType)(BitStream* bstream, NetConnection *conn);
	static Vector<OnPacketReceivedType> sgListeners;

public:
	NetPacketEvent(BitStream* bstream = NULL, NetConnection *conn = NULL);

	~NetPacketEvent();

	virtual void pack(NetConnection* conn, BitStream *bstream);

	virtual void write(NetConnection* conn, BitStream *bstream);

	virtual void unpack(NetConnection* conn, BitStream *bstream);

	virtual void process(NetConnection *conn);

	static void sendPacket(NetConnection *conn, BitStream* bstream);

	static void addListener(OnPacketReceivedType receiveFunction);

	DECLARE_CONOBJECT(NetPacketEvent);
};

#endif // _NETPACKET_H_