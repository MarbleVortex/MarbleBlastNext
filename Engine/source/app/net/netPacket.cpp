//-----------------------------------------------------------------------------
// Copyright (c) 2012 GarageGames, LLC
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
//-----------------------------------------------------------------------------

#include "platform/platform.h"
#include "core/dnet.h"
#include "core/idGenerator.h"
#include "core/stream/bitStream.h"
#include "console/simBase.h"
#include "console/console.h"
#include "console/consoleTypes.h"
#include "sim/netConnection.h"
#include "sim/netObject.h"
#include "app/net/serverQuery.h"
#include "console/engineAPI.h"
#include <vector>
#include "app/net/netPacket.h"

IMPLEMENT_CO_NETEVENT_V1(NetPacketEvent);

Vector<NetPacketEvent::OnPacketReceivedType> NetPacketEvent::sgListeners = Vector<NetPacketEvent::OnPacketReceivedType>();

NetPacketEvent::NetPacketEvent(BitStream* bstream, NetConnection *conn)
{
	this->mBStream = bstream;
}

NetPacketEvent::~NetPacketEvent()
{
	SAFE_DELETE(this->mBStream);
}

void NetPacketEvent::pack(NetConnection* conn, BitStream *bstream)
{
	U32 streamSize = this->mBStream->getStreamSize();
	bstream->write(streamSize);

	this->mBStream->setPosition(0);
	bstream->copyFrom(this->mBStream);
}

void NetPacketEvent::write(NetConnection* conn, BitStream *bstream)
{
	pack(conn, bstream);
}

void NetPacketEvent::unpack(NetConnection* conn, BitStream *bstream)
{
	U32 bufSize;
	bstream->read(&bufSize);
	void* buffer = dMalloc(bufSize);
	bstream->read(bufSize, buffer);
	this->mBStream = new BitStream(buffer, bufSize, 0);
}

void NetPacketEvent::process(NetConnection *conn)
{
	for (S32 i = 0; i < sgListeners.size(); i++)
	{
		OnPacketReceivedType listener = sgListeners[i];
		if (listener != NULL)
		{
			this->mBStream->setPosition(0);
			listener(this->mBStream, conn);
		}
	}
}

void NetPacketEvent::sendPacket(NetConnection *conn, BitStream* bstream)
{
	NetPacketEvent *cevt = new NetPacketEvent(bstream, conn);
	conn->postNetEvent(cevt);
}

void NetPacketEvent::addListener(OnPacketReceivedType receiveFunction)
{
	if (!sgListeners.contains(receiveFunction))
		sgListeners.push_back(receiveFunction);
}
